#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
prep_self_assembly_ligands_non_fragment_based.py

The purpose of this script is to prepare self-assembly ligands using a non-fragment 
based approach. The idea would be to input a mol2 file or SMILES string, then 
we will check to see if the ligand is within a folder. If it is not, then we 
will ask for a structure smiles. If you are missing a smiles string, then 
it may be helpful to draw chemical structures, then convert it to a SMILES string. 

Written by: Alex K. Chew (08/13/2020)

"""
## IMPORTING PANDAS
import os
import pandas as pd
import numpy as np
import sys

## LOADING RDKIT
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem import AllChem

import sys
is_py2 = sys.version[0] == '2'
if is_py2:
    import Queue as queue
else:
    import queue as queue

## LOADING FORCEFIELD INFORMATION
from MDBuilder.core.forcefield import FORCEFIELD # FORCE FIELD CLASS
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names

from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

## GETTING LIGAND ARGS
from MDBuilder.builder.ligand_builder import get_ligand_args

## IMPORTING TOPOLOGY INFORMATION
from MDBuilder.ligand.prep_self_assembly_ligands import print_top_file, print_force_field_itp

## DEFINING MASS OF HYDROGEN
MASS_OF_HYDROGEN = 1.008

## DEFINING CHARGE
DEFAULT_CHARGE = 0.000

## DEFINING DEFAULT FOLDER
DEFAULT_FF_FOLDER = "pool.ff"

## GETTING FORCEFIELD
forcefield = FORCEFIELD( forcefield_folder = DEFAULT_FF_FOLDER)

## GETTING ATOMIC INFORMATION
def get_atomic_information_from_molecule(m):
    '''
    This function creates a dataframe with atomic information. 
    INPUTS:
        m: [obj]
            rdkit mol from smiles string
    OUTPUTS:
        atomic_info: [df]
            pandas dataframe contining atomic information
    '''
    ## CREATING DATAFRAME
    atomic_info = []
    
    ## GETTING ATOMS
    for atom in m.GetAtoms():
        ## DEFINING INPUTS
        symbol = atom.GetSymbol()
        atomic_num = atom.GetAtomicNum()
        index = atom.GetIdx()
        atomic_mass = atom.GetMass()
        ## FINDING TOTAL NUMBER OF HYDROGENS
        total_hydrogens = atom.GetTotalNumHs()
        
        ## DEFINING ATOMIC DIC
        atomic_dict = {
                'symbol': symbol,
                'atomic_num': atomic_num,
                'atomic_mass': atomic_mass,
                'index': index,
                'total_hydrogens': total_hydrogens,
                'atomic_mass_with_H': atomic_mass + total_hydrogens * MASS_OF_HYDROGEN,
                }
        ## STORING
        atomic_info.append(atomic_dict)
    
    ## CREATING DATAFRAME
    atomic_info = pd.DataFrame(atomic_info)
    return atomic_info
# test2.GetTotalNumHs()

### FUNCTION TO GET POSITIONS
def get_positions_from_molecule(m,
                                want_nm = True):
    '''
    This function simply gets positions of the molecule
    INPUTS:
        m: [obj]
            rdkit mol from smiles string
        want_nm: [str]
            True if you want nanometer scale
    OUTPUTS:
        positions: [list]
            list of positions for each conformer
    '''
    ## COMPUTING 2D COORD
    AllChem.Compute2DCoords(m)
    positions = [ c.GetPositions() for c in m.GetConformers()]
    
    ## CONVERTING POSITIONS FROM ANGSTROMS TO NM
    if want_nm is True:
        positions = [each_position / 10.0 for each_position in positions ]
    
    return positions

### FUNCTION TO GET BONDING INFORMATION
def get_bond_list(m):
    '''
    This function uses rdkit molecule to generate bond list
    INPUTS:
        m: [obj]
            rdkit mol from smiles string
    OUTPUTS:
        bond_list: [list]
            list of all bonding based on indexes
    '''
    ## DEFINING BOND LIST
    bond_list = [ [bonds.GetBeginAtomIdx(), bonds.GetEndAtomIdx()]
                    for bonds in m.GetBonds() ]
    ## CONVERTING TO NUMPY
    bond_list = np.array(bond_list)
    return bond_list

def find_paths_bfs_search(ligand_num_atoms, matrix, root, max_distance):
    '''
    This function finds all paths.
    INPUTS:
        ligand_num_atoms: [int]
            total number of ligands
        matrix: [np.array, shape = (N, N)]
            adjacency matrix
        root: [int]
            current atom index that you care about
        max_distance: [int]
            maximum distance to search. This is typically 3 atoms away
    OUTPUTS:
        all_paths: [list]
            list of all possible paths, e.g.:
            [[0], [0, 1], [0, 1, 2], [0, 1, 2, 3], [1], [1, 0], [1, 2], [1, 2, 3], [1, 2, 3, 4], 
            [2], [2, 1], [2, 3], [2, 1, 0], [2, 3, 4], 
            [3], [3, 2], [3, 4], [3, 2, 1], [3, 2, 1, 0],
            [4], [4, 3], [4, 3, 2], [4, 3, 2, 1]]
            
            This could inform the angles, dihedrals, etc.
    '''
    bfs_queue = queue.Queue()
    # put in root element used to initiate search
    bfs_queue.put(root)
    node_distances = np.zeros(ligand_num_atoms, dtype='int')
    # list that keeps track of paths
    all_paths=[[root]]
    while (not bfs_queue.empty()):
        # get the top of the queue
        current = bfs_queue.get()
        # iterate across row for this element
        for i in range(0, ligand_num_atoms):
            if (matrix[current][i] == 1):
                # if distance = 0, then distances have not yet been set
                # so this node hasn't been visited (must check to avoid infinite loops)
                if (node_distances[i] == 0):
                    # set distance to this node by incrementing distance of parent
                    node_distances[i] = node_distances[current] + 1
                    # If below maximum desired distance, continue search from this child
                    if (node_distances[i] <= max_distance):
                        # adds child; update paths as well
                        bfs_queue.put(i)
                        # for each path currently being tracked, add this node
                        # to all paths that end in parent node
                        for cur_path in all_paths:
                            # append if last entry is "current" and entry not already in this path
                            if cur_path[-1] == current and not i in cur_path:
                                # need to create actual new entry before appending
                                new_path = cur_path[:]
                                new_path.append(i)
                                all_paths.append(new_path)
    return all_paths

## CREATING ADJACENCY MATRIX
def generate_adjacency_matrix_from_bond_list(atomic_info,
                                             bond_list,):
    '''
    This function creates an adjacency matrix based on bonding information 
    and atomic information.
    INPUTS:
        atomic_info: [dict]
            dictionary contianing atomic information
        bond_list: [list]
            list of bonding information
    OUTPUTS
        adjacency_matrix: [np.array, shape = (N, N)]
            bonding information between each atom index
    '''
    ## GETTING TOTAL NUMBER OF ATOMS
    num_atoms = len(atomic_info)
    
    ## GETTING ADJACENCY MATRIX OF ZEROS
    adjacency_matrix = np.zeros((num_atoms, num_atoms)).astype(int)
    
    ## ADDING TO ADJACENCY MATRIX
    for each_bond in bond_list:            
        ## ADDING EACH BOND
        adjacency_matrix[tuple(each_bond)]+=1
        
    ## ADDING TRANSPOSE SINCE IT IS SYMMETRIC
    adjacency_matrix += adjacency_matrix.T
    
    
    return adjacency_matrix

### FUNCTION TO CONVERT ATOM TYPE NAMES
def add_column_with_atom_types( atomic_df, 
                                ):
    '''
    This function adds a column to signify atom types. Note that this 
    only takes in S, C symbols. Oxygens and nitrogens are not considered!
    '''
    ## CREATING ATOM TYPE
    atomtype_list = []
    atomname_list = []
    
    ## LOOPING THROUGH ROWS
    for row, item in atomic_df.iterrows():
        ## GETTING SYMBOL AND HYDROGENS
        symbol = item['symbol']
        total_H = item['total_hydrogens']
        
        ## IDENTIFYING
        if symbol == "S":
            atomtype = "SH"
        elif symbol == "C":
            if total_H == 3:
                atomtype = "CH3"
            else:
                atomtype = "CH2"
            ## FOR NOW, ASSUME ALL OTHER CARBON TYPES AS CH2. THESE WILL KEEP CH2 INTERACTIONS, BUT 
            ## ASSUMES THAT WE KNOW NOTHING OF CH, AND C ALONE.
        else:
            print("Error! Symbol not defined in this force field: %(symbol)")
            print("Applicable symbols: C, S")
            ## EXITING
            sys.exit(1)
                
        ## STORING
        atomname = "%s%d"%(symbol, row + 1)
        atomname_list.append(atomname)
        atomtype_list.append(atomtype)
    
    ## ADDING EMPTY CHARGES
    charges_list = [ DEFAULT_CHARGE for each in range(len(atomic_df)) ]
    
    ## CREATING NEW COLUMN
    atomic_df['atomname'] = atomname_list
    atomic_df['atomtype'] = atomtype_list
    atomic_df['charge'] = charges_list
        
    return atomic_df

### FUNCTION TO GET ALL BONDS, ANGLES, AND DIHEDRALS FROM PATHS
def get_bonds_angles_dihedrals_from_paths(all_paths):
    '''
    The purpose of this function is to get all the bonds, angles, 
    and dihedrals from the paths. 
    INPUTS:
        all_paths: [list]
            list of all the paths
    OUTPUTS:
        ligand_bonds: [list]
            list of ligand bonds
        ligand_angles: [list]
            list of ligand angles
        ligand_dihedrals: [list]
            list of ligand dihedrals
    '''

    ## DETERMINING LIGAND BOND, ANGLES, AND DIHEDRALS
    ligand_bonds = []
    ligand_angles = []
    ligand_dihedrals = []
    for path in all_paths:
        # check for duplicates and for reversed duplicates via [::-1] syntax
        if len(path) == 2 and not path in ligand_bonds  and not path[::-1] in ligand_bonds:
            # increment by [1 1] to match correct syntax when outputting
            ligand_bonds.append(path)
        elif len(path) == 3 and not path in ligand_angles and not path[::-1] in ligand_angles:
            ligand_angles.append(path)
        elif len(path) == 4 and not path in ligand_dihedrals and not path[::-1] in ligand_dihedrals:
            ligand_dihedrals.append(path)
    return ligand_bonds, ligand_angles, ligand_dihedrals

### DEFINING CLASS OBJECT
class get_ligand_info_from_database:
    '''
    This function gets ligand information from database.
    INPUTS:
        database_df: [df]
            database containing ligand information
        ligand_name: [str]
            ligand name that you care about
    OUTPUTS:
        
    '''
    ## INITIALIZING
    def __init__(self,
                 database_df,
                 ligand_name):
        ## STORING INPUTS
        self.ligand_name = ligand_name
        
        ## IDENTIFYING SMILES STRING
        self.smiles_string, self.resname = self.check_availability(database_df,
                                                                   ligand_name = self.ligand_name)
        
        ## LOADING MOLECULES FROM SMILES
        self.m = Chem.MolFromSmiles(self.smiles_string)
        
        ## GETTING POSITIONS OF FIRST CONFORMER
        self.positions = get_positions_from_molecule(self.m)[0]
        
        ## GETTING ATOMIC INFORMATION
        self.atomic_info = get_atomic_information_from_molecule(self.m)
                
        ## GETTING BOND LIST
        self.bond_list = get_bond_list(self.m)
        
        ## CREATING ADJACENCY MATRIX
        self.adjacency_matrix = generate_adjacency_matrix_from_bond_list(self.atomic_info,
                                                                         self.bond_list,)
        
        ## CONVERTING BOND LIST TO ADJACENCY MATRIX
        self.ligand_num_atoms = len(self.atomic_info)
        all_paths = []
        for i in range(len(self.atomic_info)):
            paths = find_paths_bfs_search(self.ligand_num_atoms, 
                                          matrix = self.adjacency_matrix, 
                                          root = i, 
                                          max_distance = 3)
            all_paths.extend(paths)
        
        ## GETTING ANGLES, ETC. FROM PATHS
        self.ligand_bonds, self.ligand_angles, self.ligand_dihedrals = get_bonds_angles_dihedrals_from_paths(all_paths = all_paths)
        
        return
    
    ## CHECKING IF LIGAND IS AVAILABLE
    @staticmethod
    def check_availability(database_df,
                           ligand_name,):
        '''
        This function checks if your ligand is in the database. 
        INPUTS:
            database_df: [df]
                database containing ligand information
            ligand_name: [str]
                ligand name that you care about
        OUTPUTS:
            smiles_string: [str]
                SMILES string of interest
            resname: [str]
                residue name of interest
        '''
        ## LOCATING
        location = database_df.loc[database_df['ligand_name'] == ligand_name]
        
        ## CHECKING IF LOCATION IS NOT 1
        if len(location) == 0:
            print("Error! Ligand name (%s) is not found in the database"%(ligand_name))
            print("Check database in %s"%(database_path))
            sys.exit(1)
        elif len(location) > 1:
            print("Error, multiple ligand entries found for: %s"%(ligand_name))
            print("Please check and make sure that you correctly added unique ligand names")
            print("Check database in %s"%(database_path))
            sys.exit(1)
        else:
            ## DEFINING INPUT STRING
            smiles_string = location['SMILES'].iloc[0]
            resname = location['residue_name'].iloc[0]
            print("Ligand (%s) [%s] SMILES string: %s"%(ligand_name, resname, smiles_string))
        
        return smiles_string, resname
    
# OUTPUTTING ITP FILE FOR GROMACS -READIBLE FILES
def print_itp_file(ligand_name, 
                   ligand_resname, 
                   output_folder,
                   atomic_df,
                   ligand_bonds,
                   ligand_angles,
                   ligand_dihedrals,
                   angle_functional,
                   dihedral_functional,
                   atom_type_name = 'atomtype',
                   atom_atomname = 'atomname',
                   atom_charge_name = 'charge', 
                   atom_mass_name = "atomic_mass_with_H",):
    '''
    Function that prints itp files FOR LIGANDS
    INPUTS:
        ligand_name: [str]
            ligand name
        ligand_resname: [str]
            ligand residue name
        output_folder: [str]
            output folder to save to
        atomic_df: [df]
            dataframe containing all information, e.g.:
                
                ligand_atomtypes: [list]
                    list of atom types
                ligand_atomnames: [list]
                    list of atom names
                ligand_atomcharges: [list]
                    list of atom charges    
        ligand_bonds: [list]
            list of ligand bonds
        angle_functional: [int]
            functional to use for GROMACS angles
        dihedral_functional: [int]
            dihedral functions to use in GROMACS
        atom_type_name: [str]
            key within atomic_df for typenames
        atom_atomname: [str]
            key within atomic_df for atomnames
        atom_charge_name: [str]
            key within atomic df for charges
        atom_mass_name: [str]
            key within atom mass
    OUTPUTS:
        output_itp_name: [str]
            output itp path
    '''
    
    ## DEFINING INPUTS FROM DATAFRAME
    ligand_atomtypes = atomic_df[atom_type_name]
    ligand_atomnames = atomic_df[atom_atomname]
    ligand_atomcharges = atomic_df[atom_charge_name]
    ligand_atommasses = atomic_df[atom_mass_name]
    
    ## OUTPUTTING ITP FILE
    output_itp_name = os.path.join(output_folder, ligand_name + '.itp' ) # Output .itp file name
    print("Outputting %s"%(output_itp_name))
    with open(output_itp_name, "w") as outputfile:
        # MOLECULE INFO
        # write molecule type info; for GROMOS force field
        outputfile.write("[ moleculetype ]\n")
        outputfile.write("; Name   nrexcl\n")
        #now molecule name + 3 for exclusions
        outputfile.write("%s    3\n\n"%(ligand_resname))
        # ATOM PROPERTIES
        outputfile.write("[ atoms ]\n")
        outputfile.write(";   nr    type   resnr  residu    atom    cgnr   charge    mass\n") # assume charge, mass already defined by atomtypes
        for i in range(len(ligand_atomtypes)):
            # Charge groups don't matter for recent
            outputfile.write("%4d %15s  %d %s %s %4d  %.3f   %.4f\n"%(i+1, ligand_atomtypes[i], 
                                                                        1, 
                                                                        ligand_resname.rjust(10, ' '),
                                                                        ligand_atomnames[i].rjust(5, ' '), 
                                                                        i+1, 
                                                                        ligand_atomcharges[i], 
                                                                        ligand_atommasses[i]))
        # BONDS
        outputfile.write("\n[ bonds ]\n")
        outputfile.write(";  ai    aj   funct  ; bond properties inferred from atom types \n")
        for i in range(0, len(ligand_bonds)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   1\n"%(ligand_bonds[i][0]+1, 
                                              ligand_bonds[i][1]+1))
        # ANGLES
        outputfile.write("\n[ angles ]\n")
        outputfile.write(";  ai    aj    ak  funct  ; angle properties inferred from atom types \n")
        for i in range(0, len(ligand_angles)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   %d   %d\n"%(ligand_angles[i][0]+1, 
                                                    ligand_angles[i][1]+1, 
                                                    ligand_angles[i][2]+1, angle_functional)) # 1 for function type
        # DIHEDRALS
        outputfile.write("\n[ dihedrals ]\n")
        outputfile.write(";  ai    aj    ak   al  funct  ; dihedral properties inferred from atom types \n")
        for i in range(0, len(ligand_dihedrals)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   %d   %d    %d\n"%(ligand_dihedrals[i][0]+1, 
                                                          ligand_dihedrals[i][1]+1, 
                                                          ligand_dihedrals[i][2]+1, 
                                                          ligand_dihedrals[i][3]+1, 
                                                          dihedral_functional)) # 3 for function type
            
            
        # pairs
        outputfile.write("\n[ pairs ]\n")
        outputfile.write(";  ai    aj  \n")
        # infer pairs from all dihedrals
        for i in range(0, len(ligand_dihedrals)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   1\n"%(ligand_dihedrals[i][0]+1, ligand_dihedrals[i][3]+1))
            
    return output_itp_name

### FUNCTION TO GET OUTPUTNAMES
def get_outputnames_from_ligand(ligand_res_name,
                                atom_name_list):
    '''
    This function gets ligand output names.
    INPUTS:
        ligand_res_name: [str]
            name of the ligand residue
        atom_name_list: [list]
            list of atom names
    OUTPUTS:
        ligand_outputnames: [list]
            list of ligand output names
    '''
    ## GETTING LIGAND OUTPUT NAMES
    ligand_outputnames = []
    for i in range(len(atom_name_list)):
        # convert ligname to 5 characters
        ligname = ligand_res_name.rjust(5, ' ')
        # make atom name + id as well
        atomname = atom_name_list[i]
        comboname = ligname + atomname.rjust(5, ' ')
        ligand_outputnames.append(comboname)
    return ligand_outputnames


### MAIN FUNCTION TO GENERATE SELF ASSEMBLY LIGANDS
def main_generate_self_assembly_ligands_nonfragment(ligand_names,
                                                    database_path,
                                                    output_folder):
    '''
    This function is the main function that generates self-assembly 
    ligands in a non-fragment basis.
    INPUTS:
        ligand_name: [list]
            List of ligand names
        database_path: [str]
            database path
        output_folder: [str]
            output folder path
    OUTPUTS:
        void, this function will output a gro, itp and top file
    '''
    
    ## PRINTING FORCEFIELD OUTPUT FILE
    print_force_field_itp(output_folder)
    
    ## READING DATABASE
    database_df = pd.read_excel(database_path)
    
    ## LOOPING
    for ligand_name in ligand_names:
    
        ## GETTING LIGAND INFORMATION        
        ligand = get_ligand_info_from_database(database_df = database_df,
                                               ligand_name = ligand_name)
        
        ## UPDATING ATOMIC INFORMATION WITH ATOM TYPES
        ligand.atomic_info = add_column_with_atom_types(ligand.atomic_info)
        
        ## WRITING ITP FILE
        print_itp_file(ligand_name = ligand.ligand_name, 
                         ligand_resname =  ligand.resname, 
                         output_folder = output_folder,
                         atomic_df = ligand.atomic_info,
                         ligand_bonds = ligand.ligand_bonds,
                         ligand_angles = ligand.ligand_angles,
                         ligand_dihedrals = ligand.ligand_dihedrals,
                         angle_functional = forcefield.angleFunc,
                         dihedral_functional = forcefield.dihedralFunc,
                         atom_type_name = 'atomtype',
                         atom_atomname = 'atomname',
                         atom_charge_name = 'charge', 
                         atom_mass_name = "atomic_mass_with_H",)
        
        #########################
        ### PRINTING GRO FILE ###
        #########################
        
        ## GETTING RESIDUE IDs
        resids = [1] * ligand.ligand_num_atoms
        
        ## GETTING LIGAND OUTPUT NAMES
        outputnames = get_outputnames_from_ligand(ligand_res_name = ligand.resname,
                                                  atom_name_list = ligand.atomic_info['atomname'].to_list())
        
        ## DEFINING THE GEOMETRY
        geom = ligand.positions
        
        # Getting dimension of box by fitting the box to a single molecule
        box_dimension = [0]*3 # [ np.max(np.max(current_ligand.ligand_geom,axis=0) - np.min(current_ligand.ligand_geom,axis=0)) ]*3  #[0]*3 # 
        
        ### PRINTING GRO FILE
        print_gro_file_combined_res_atom_names(output_gro= ligand.ligand_name+ '.gro',
                         output_folder=output_folder,
                         resids= resids,
                         geom=geom,
                         outputnames= outputnames ,
                         box_dimension=box_dimension ,
                         )
        
        #########################
        ### PRINTING TOP FILE ###
        #########################
        
        ### PRINTING TOPOLOGY FILE
        print_top_file(output_top = ligand.ligand_name + '.top',
               output_folder = output_folder,
               itp_file_name = ligand.ligand_name,
               res_name = ligand.resname,
               forcefield_folder = DEFAULT_FF_FOLDER)

    return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TURNING TESTING ON / OFF
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    ### IF YOU ARE TESTING
    if testing is True:
        ## DEFINING EXCEL PATH
        database_path = "/Volumes/akchew/scratch/nanoparticle_project/database/self_assembly_ligands.xlsx"
    
        ## DEFINING OUTPUT FOLDER
        output_folder = r"/Volumes/akchew/scratch/nanoparticle_project/prep_system/prep_gold/self_assembly_surfactants/test"
        
        ## DEFINING LIGAND NAME
        ligand_names = ["bidente"]
        # "butanethiol"
    else:
        # Adding options for command line input (e.g. --ligx, etc.)
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ### DEFINING PARSER OPTIONS
        ## LIGAND TYPES
        parser.add_option("--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
              help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        ## OUTPUT FOLDER
        parser.add_option("--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        ## PREPARATION FOLDER
        parser.add_option("--database", dest="database_path", action="store", type="string", help="Database for excel", default=".")
        
        #### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
        
        ## OUTPUT/INPUT FILES
        database_path = options.database_path # Preparation folder
        output_folder = options.output_folder   # Output folder
    
    ## DEFINING INPUTS
    non_frag_inputs= {
            'ligand_names' : ligand_names,
            'database_path': database_path,
            'output_folder': output_folder,
            }
    
    ## GENERATING SELF ASSEMBLY LIGAND INFORMATION
    main_generate_self_assembly_ligands_nonfragment(**non_frag_inputs)
    
    
