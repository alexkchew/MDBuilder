# -*- coding: utf-8 -*-
"""
prep_self_assembly_ligands.py
The purpose of this script is to self-assemble the ligands according to the paper below:
    Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).
    
This uses Pool's force field parameters to characterize a united-atom alkyl chain. This script will simply output the force field parameters and topology file for simuations

CREATED ON: 12/17/2017

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)
    
INPUTS:
    - ligands: [list] list of your ligand names
    - output directory: [str] output directory name
    - output topology file name: [ str ] output topology name
    
OUTPUTS:
    pool.ff folder -- This has important information on the forcefield parameters
    topology file -- This has topology file required to run the simulation
    itp files -- ligand itp files

*** UPDATES ***
20180304 - Fixed issue with missing forcefield links
20180502 - Made major changes to the layout of Ligand Builder
"""
#%% IMPORTING FUNCTIONS
import numpy as np # Math functions
# import ligand_builder # for ligand functions
from MDBuilder.core.check_tools import check_dir, check_server_path # CHECKS DIRECTORY, SERVER PATH
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
from MDBuilder.builder.ligand_builder import get_ligand_args
from MDBuilder.builder.ligand_builder_fragments import Ligand # Ligand class that uses fragment based approaches
from MDBuilder.core.forcefield import FORCEFIELD, FORCEFIELD_FRAGMENT # FORCE FIELD CLASS
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names
import time
import os

## IMPORTING GLOBAL VARIABLES
from MDBuilder.core.global_vars import k_B # Boltzmann constant in kJ/(mol K)

### DEFINING FORCE FIELD FOLDER
FORCEFIELD_FOLDER = "pool.ff" # default
FORCEFIELD_ITP_NAME='forcefield.itp' # Default

## DEFAULTS
# nonbonded functions, combination rules, gen-pairs, fudge LJ, fudge qq
defaults = { 'nbfunc': 1,
             'comb-rule': 3, # Lorentz-Berthelot mixing rules
             'gen-pairs': 'yes',  # Generate new pairs
             'fudgeLJ': 1.0, # Factor to multiply LJ interactions (default = 1)
             'fudgeQQ': 1.0, # Factor to multiply electrostatics 1-4 interactions (default = 1)
             }
# DEFINING FUNCTIONS FOR BONDS, ANGLES, ETC.
gromacs_func={'bond_func': 1, # Harmonic potential
              'angle_func': 2, # GROMOS FF angle potential (cosines)
              'dihedral_func':3 # Ryckaert Bellemans function
              }

## BONDING PARAMETERS -- HARMONIC ( GROMACS BOND FUNC 1 )
equil_CC_bond_length = 1.54 # Angstroms
equil_CS_bond_length = 1.85 # Angstroms
bond_k_by_k_B = 96500 # Kelvins / Angstroms^2

##BENDING PARAMETERS -- GROMOS96 ( GROMACS BEND FUNC 2 )
equil_angle = 114 # Degrees for all angles
bend_k_by_k_B = 62500 # Kelvins / Radians^2

## TORSION PARAMETERS -- RYCKAERT-BELLEMAN ( GROMACS DIHEDRAL FUNC. 3 )
# nabla_0 ,.... , nabla_5
# NOTE: Not too sure about nabla 2, 3, and 5. Questionable if they should be negative since they contradict paper:
    # Dubbeldam, D. et al. United atom force field for alkanes in nanoporous materials. J. Phys. Chem. B 108, 12301–12313 (2004). -- Table 3
#nabla_vec_K =np.array([ 1204.654, 1947.740, 357.845, 1944.666, 715.690, 1565.572 ]) # Kelvins < -- from paper
    
## TESTING NABLA VECTOR, WHICH MAY GIVE MORE CURVED LIGANDS
# nabla_vec_K =np.array([ 1204.654, 1947.740, 357.845, 1944.666, 715.690, 1565.572 ]) # Kelvins < -- from paper
# CURVES THE LIGANDS
    
## ORIGINAL NABLA ( turning off for testing )
nabla_vec_K =np.array([ 1204.654, 1947.740, -357.845, -1944.666, 715.690, -1565.572 ]) # Kelvins

## LJ PARMS
# rows/cols: CH3, CH2, SH , Au (T=300K)
sigma_angstroms = np.array([[3.76, 3.86, 4.11, 3.54],
                            [3.86, 3.96, 4.21, 3.54],
                            [4.11, 4.21, 4.45, 2.65],
                            [3.54, 3.54, 2.65, 0.00]
                            ]) # in angstroms
epsilon_by_k_B = np.array([[108, 78, 117, 108],
                           [78, 56, 84, 88],
                           [117, 84, 126, 2795],
                           [108, 88, 2795, 0.00]]) # Kelvins

### CONVERTING TO GROMACS READABLE FORMATS
## BONDS
CC_bond_nm = equil_CC_bond_length / 10.0 # nms
CS_bond_nm = equil_CS_bond_length / 10.0 # nms
bond_k = ( bond_k_by_k_B ) * (100) * k_B # kJ / (mol nm^2)

## DEFINING SULFUR-SULFUR BOND LENGTH
SS_bond_nm = 0.189 # nm
# Taken from ref: https://pubs.acs.org/doi/10.1021/ja00230a005

## ANGLES
bend_k = (bend_k_by_k_B) * k_B # kJ / (mol rad^2) -- NOTE -- RADIANS^2 should do nothing. In fact, those units should not even be there!

## TORSION PARAMETERS
nabla = nabla_vec_K * k_B

## LJ PARMS
sigma_nm = sigma_angstroms / 10.0
epsilon = epsilon_by_k_B * k_B # kJ/mol

### DEFINING THE DIFFERENT ATOMS
CH3_ATOMTYPES={'mass': 12.011000 + 1.0080*3,
               'charge': 0.000,
               'resname': 'CH3',
               'atom': 'C',
               'atom_num': 12,
               'sigma': sigma_nm[0,0],
               'epsilon': epsilon[0,0]
               }
CH2_ATOMTYPES={'mass': 12.011000 + 1.0080*2,
               'charge': 0.000,
               'resname': 'CH2',
               'atom': 'C',
               'atom_num': 12,
               'sigma': sigma_nm[1,1],
               'epsilon': epsilon[1,1]
               }
SH_ATOMTYPES={'mass': 32.060000 + 1.0080,
           'charge': 0.000,
           'resname': 'SH',
           'atom': 'S',
           'atom_num': 16,
           'sigma': sigma_nm[2,2],
           'epsilon': epsilon[2,2]
           }
AU_ATOMTYPES={'mass': 196.9965,
           'charge': 0.000,
           'resname': 'AU',
           'atom': 'Au',
           'atom_num': 79,
           'sigma': sigma_nm[3,3],
           'epsilon': epsilon[3,3]
           }

### DEFINING FUNCTIONS
### FUNCTION TO PRINT FORCE FIELD ITP PARAMETERS
def print_force_field_itp( output_folder ):
    '''
    The purpose of this function is to print things required for a force field including: (HARDCODED)
        defaults, atomtype, bondtype, dihedraltypes, etc.
    INPUTS:
        output_folder: Where you want to output all itp stuff
    OUTPUTS:
        forcefield.itp in the directory pool.ff
    '''
    ### DEFINING FORCE FIELD ITP
    path_force_field_folder= check_server_path( os.path.join(output_folder , FORCEFIELD_FOLDER) ); 
    ## CHECKING DIRECTORY
    check_dir(path_force_field_folder) # Force field folder + creating if necessary
    path_forcefield_itp = check_server_path( os.path.join(path_force_field_folder, FORCEFIELD_ITP_NAME) )

    ### PRINTING ITP FILE
    print("OUTPUTTING FORCE FIELD PARAMETERS(%s): %s"%(FORCEFIELD_FOLDER, path_forcefield_itp))
    with open(path_forcefield_itp, "w") as itp_file:
        ## WRITING HEADERS
        itp_file.write("; GROMACS TOPOLOGY FOR SELF-ASSEMBLY PROCESSES\n")
        itp_file.write("; FORCE FIELD PARAMETERS BASED ON THE FOLLOWING PAPERS:\n")
        itp_file.write(r"; Automating paper: Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013)." +  "\n")
        itp_file.write(r"; United atom force field params: Dubbeldam, D. et al. United atom force field for alkanes in nanoporous materials. J. Phys. Chem. B 108, 12301–12313 (2004)." + "\n")
        itp_file.write(r"; LJ params: Pool, R., Schapotschnikow, P. & Vlugt, T. J. H. Solvent effects in the adsorption of alkyl thiols on gold structures: A molecular simulation study. J. Phys. Chem. C 111, 10201–10212 (2007)." + "\n\n")
        ### DEFAULTS
        itp_file.write("[ defaults ]\n")
        itp_file.write("; nbfunc	comb-rule	gen-pairs	fudgeLJ	fudgeQQ\n")
        itp_file.write("%d   %d   %s   %.1f   %.1f\n"%( defaults['nbfunc'],
                                                    defaults['comb-rule'],
                                                    defaults['gen-pairs'],
                                                    defaults['fudgeLJ'],
                                                    defaults['fudgeQQ'],))
        
        ### ATOMTYPES -- HARDCODED
        itp_file.write("\n[ atomtypes ]\n")
        itp_file.write(";type atnum mass       charge   ptype   sigma (nm)  epsilon (kJ/mol) \n")
        # CH3 ATOMS
        itp_file.write("%s    %d   %.6f   %.3f   A   %.12f   %.5f\n"%(CH3_ATOMTYPES['resname'], 
                                                                      CH3_ATOMTYPES['atom_num'],
                                                                      CH3_ATOMTYPES['mass'],
                                                                      CH3_ATOMTYPES['charge'],
                                                                      CH3_ATOMTYPES['sigma'],
                                                                      CH3_ATOMTYPES['epsilon'],
                                                                      ),)
        # CH2 ATOMS
        itp_file.write("%s    %d   %.6f   %.3f   A   %.12f   %.5f\n"%(CH2_ATOMTYPES['resname'], 
                                                                      CH2_ATOMTYPES['atom_num'],
                                                                      CH2_ATOMTYPES['mass'],
                                                                      CH2_ATOMTYPES['charge'],
                                                                      CH2_ATOMTYPES['sigma'],
                                                                      CH2_ATOMTYPES['epsilon'],
                                                                      ),)
    
        # SH ATOMS
        itp_file.write("%s    %d   %.6f   %.3f   A   %.12f   %.5f\n"%(SH_ATOMTYPES['resname'], 
                                                                      SH_ATOMTYPES['atom_num'],
                                                                      SH_ATOMTYPES['mass'],
                                                                      SH_ATOMTYPES['charge'],
                                                                      SH_ATOMTYPES['sigma'],
                                                                      SH_ATOMTYPES['epsilon'],
                                                                      ),)
        
        # AU ATOMS
        itp_file.write("%s    %d   %.6f   %.3f   A   %.12f   %.5f\n"%(AU_ATOMTYPES['resname'], 
                                                                      AU_ATOMTYPES['atom_num'],
                                                                      AU_ATOMTYPES['mass'],
                                                                      AU_ATOMTYPES['charge'],
                                                                      AU_ATOMTYPES['sigma'],
                                                                      AU_ATOMTYPES['epsilon'],
                                                                      ),)
        
        ### BONDTYPES -- HARDCODED
        itp_file.write("\n[ bondtypes ]\n")
        itp_file.write(";      i        j  func           b0(nm)           kb(kJ/mol/nm^2)\n")
        # CH3-CH3
        itp_file.write("     %s      %s     %d   %.8f    %.2f\n"%(CH3_ATOMTYPES['resname'], 
                                                                  CH3_ATOMTYPES['resname'],
                                                                  gromacs_func['bond_func'],
                                                                  CC_bond_nm,
                                                                  bond_k,
                                                                  ))
        # CH3-CH2
        itp_file.write("     %s      %s     %d   %.8f    %.2f\n"%(CH3_ATOMTYPES['resname'], 
                                                                  CH2_ATOMTYPES['resname'],
                                                                  gromacs_func['bond_func'],
                                                                  CC_bond_nm,
                                                                  bond_k,
                                                                  ))
        # CH2-CH2
        itp_file.write("     %s      %s     %d   %.8f    %.2f\n"%(CH2_ATOMTYPES['resname'], 
                                                                  CH2_ATOMTYPES['resname'],
                                                                  gromacs_func['bond_func'],
                                                                  CC_bond_nm,
                                                                  bond_k,
                                                                  ))
        # CH2-SH
        itp_file.write("     %s      %s     %d   %.8f    %.2f\n"%(CH2_ATOMTYPES['resname'], 
                                                                  SH_ATOMTYPES['resname'],
                                                                  gromacs_func['bond_func'],
                                                                  CS_bond_nm,
                                                                  bond_k,
                                                                  ))
        
        # SH-SH
        itp_file.write("     %s      %s     %d   %.8f    %.2f\n"%(SH_ATOMTYPES['resname'], 
                                                                  SH_ATOMTYPES['resname'],
                                                                  gromacs_func['bond_func'],
                                                                  SS_bond_nm,
                                                                  bond_k,
                                                                  ))
    
        ### ANGLETYPES -- HARDCODED
        itp_file.write("\n[ angletypes ]\n")
        itp_file.write(";      i        j        k  func       theta0(degrees)       ktheta(kJ/mol) \n")
        # SH-CH2-CH2
        itp_file.write("%8s%8s%8s%5d   %.5f   %.5f\n"%(SH_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       gromacs_func['angle_func'],
                                                       equil_angle,
                                                       bend_k
                                                      ))
        # SH-CH2-CH3
        itp_file.write("%8s%8s%8s%5d   %.5f   %.5f\n"%(SH_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH3_ATOMTYPES['resname'],
                                                       gromacs_func['angle_func'],
                                                       equil_angle,
                                                       bend_k
                                                      ))
        # CH2-CH2-CH3
        itp_file.write("%8s%8s%8s%5d   %.5f   %.5f\n"%(CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH3_ATOMTYPES['resname'],
                                                       gromacs_func['angle_func'],
                                                       equil_angle,
                                                       bend_k
                                                      ))
        
        # CH2-CH2-CH2
        itp_file.write("%8s%8s%8s%5d   %.5f   %.5f\n"%(CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       gromacs_func['angle_func'],
                                                       equil_angle,
                                                       bend_k
                                                      ))
        
#        # CH2-CH2-SH
#        itp_file.write("%8s%8s%8s%5d   %.5f   %.5f\n"%(CH2_ATOMTYPES['resname'],
#                                                       CH2_ATOMTYPES['resname'],
#                                                       SH_ATOMTYPES['resname'],
#                                                       gromacs_func['angle_func'],
#                                                       equil_angle,
#                                                       bend_k
#                                                      ))
        
        ### DIHEDRAL -- HARDCODED
        itp_file.write("\n[ dihedraltypes ]\n")
        itp_file.write(";     i       j       k       l   func     coefficients\n")
        ## SH-CH2-CH2-CH2
        itp_file.write("%8s%8s%8s%8s%5d   %.5f   %.5f   %.5f   %.5f   %.5f   %.5f\n"%(SH_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       gromacs_func['dihedral_func'],
                                                       nabla[0], nabla[1], nabla[2], nabla[3], nabla[4], nabla[5]
                                                      ))
        ## CH2-CH2-CH2-CH2
        itp_file.write("%8s%8s%8s%8s%5d   %.5f   %.5f   %.5f   %.5f   %.5f   %.5f\n"%(CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       gromacs_func['dihedral_func'],
                                                       nabla[0], nabla[1], nabla[2], nabla[3], nabla[4], nabla[5]
                                                      ))
        ## CH2-CH2-CH2-CH3
        itp_file.write("%8s%8s%8s%8s%5d   %.5f   %.5f   %.5f   %.5f   %.5f   %.5f\n"%(CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH2_ATOMTYPES['resname'],
                                                       CH3_ATOMTYPES['resname'],
                                                       gromacs_func['dihedral_func'],
                                                       nabla[0], nabla[1], nabla[2], nabla[3], nabla[4], nabla[5]
                                                      ))
        
        ### LJ PARAMETERS -- HARDCODED
        itp_file.write("\n[ nonbond_params ]\n")
        itp_file.write(";    i       j      func     sigma(nm)    epsilon(kJ/mol)  \n")
        # CH3-CH2
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( CH3_ATOMTYPES['resname'], 
                                                                          CH2_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[0,1],
                                                                          epsilon[0,1]
                                                                          ))
        # CH3-SH
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( CH3_ATOMTYPES['resname'], 
                                                                          SH_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[0,2],
                                                                          epsilon[0,2]
                                                                          ))
        # CH3-AU
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( CH3_ATOMTYPES['resname'], 
                                                                          AU_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[0,3],
                                                                          epsilon[0,3]
                                                                          ))
        # CH2-SH
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( CH2_ATOMTYPES['resname'], 
                                                                          SH_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[1,2],
                                                                          epsilon[1,2]
                                                                          ))
        # CH2-AU
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( CH2_ATOMTYPES['resname'], 
                                                                          AU_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[1,3],
                                                                          epsilon[1,3]
                                                                          ))
        
        # SH-AU
        itp_file.write("     %s      %s     %d      %.3f        %.5f\n"%( SH_ATOMTYPES['resname'], 
                                                                          AU_ATOMTYPES['resname'],
                                                                          1,
                                                                          sigma_nm[2,3],
                                                                          epsilon[2,3]
                                                                          ))
        return
    
### FUNCTION TO GET SINGLE LIGAND PARAMETERS -- GRO, TOP, ITP FILES
def get_single_ligand_parameters(ligand_names, prep_folder, forcefield_folder, output_folder):
    '''
    The purpose of this script is to use LigandBuilder to create a single ligand with a gro, itp, and top file for you to play with
    This script specifically uses pool force field, but it does not necessarily have to.
    
    By default, position restraints are turned off for the ligands
    INPUTS:
        ligand_names: names of your ligands
        prep_folder: location of your preparation folder
        forcefield_folder: type of force field you have
        output_folder: location of where you want your gro, itp, etc. files to be
    OUTPUTS:
        ligand.gro: gro file with your ligand name as the name of the gro file
        ligand.itp: itp file
        ligand.top: topology file that you can use
        Note: By default,the gro file is left with 0 0 0 PBC's -- use gmx to fix this
    '''
    ### USING FORCE FIELD CLASS
    force_field = FORCEFIELD_FRAGMENT(forcefield_folder = forcefield_folder, prep_folder = prep_folder )
    # FORCEFIELD(forcefield_folder = forcefield_folder, prep_folder = prep_folder )
    '''
    ### FINDING DEFAULT PREPARATION FOLDER
    fragment_folder, fragment_extension, ligand_folder, ligand_extension = force_field.fragment_folder, force_field.fragment_extension, force_field.ligand_folder, force_field.ligand_extension    
    ### FINDING DEFAULT FORCE FILED PARAMETERS
    fragment_ending, angleFunc, dihedralFunc = force_field.fragment_ending, force_field.angleFunc, force_field.dihedralFunc
    '''
    ### CREATING LIGAND CLASSES
    # list of ligands to be loaded
    ligands = []
    # load in all ligands used; output corresponding ITP files too.
    # Loading ligands also loads fragments, etc.
    ### PRINTING ITP FILES
    for cur_lig in ligand_names: # Uses Ligand class
        print('Working on ligand:', cur_lig)
        ligands.append(Ligand(lig_file_name = cur_lig, 
                              output_folder = output_folder, 
                              forcefield = force_field,
                              want_position_restraint = False))
    
    if len(ligands) > 1:
        print("NOTE: Self assembly preparation ligands is not prepared for mixed ligands!!!")
        print("Stopping here so you can see it!")
        time.sleep(5)
    
    ## LOOPING THROUGH THE LIGANDS
    for index, current_ligand in enumerate(ligands):
        
        ## GETTING RESIDUE IDs
        resids = [1] * current_ligand.ligand_num_atoms
        
        ## DEFINING THE GEOMETRY
        geom = current_ligand.ligand_geom
        
        # Output name
        outputnames = current_ligand.ligand_outputnames
        
        # Getting dimension of box by fitting the box to a single molecule
        box_dimension = [0]*3 # [ np.max(np.max(current_ligand.ligand_geom,axis=0) - np.min(current_ligand.ligand_geom,axis=0)) ]*3  #[0]*3 # 
        
        ### PRINTING GRO FILE
        print_gro_file_combined_res_atom_names(output_gro=ligand_names[index] + '.gro',
                         output_folder=output_folder,
                         resids= resids,
                         geom=geom,
                         outputnames= outputnames ,
                         box_dimension=box_dimension ,
                         )
        
        ### PRINTING TOPOLOGY FILE
        print_top_file(output_top = ligand_names[index] + '.top',
               output_folder = output_folder,
               itp_file_name = ligand_names[index],
               res_name = current_ligand.ligand_name,
               forcefield_folder = forcefield_folder)
    
    return
    
### PRINTING TOPOLGOY FILE FUNCTION
def print_top_file(output_top, output_folder, itp_file_name, res_name, forcefield_folder):
    '''
    The purpose of this script is to create a topology file
    INPUTS:
        output_top: Output topology folder
        output_folder: Output folder
        itp_file_name: name of your gold itp file
        res_name: residue name for your gold
        forcefield_folder: Force field folder (i.e. opls-aa, etc.)
    OUTPUTS:
        output_top.top file
    '''
    ### USING FORCEFIELD FUNCTION CLASS
    force_field = FORCEFIELD(forcefield_folder=forcefield_folder)
    forcefield_itp = force_field.forcefield_itp
    
    output_top_name = output_folder + '/' + output_top
    outputfile = open(output_top_name, "w")
    print("Outputting %s"%(output_top_name))
    ## OUTPUTTING
    outputfile.write(";  Topology file for self-assembly ligands developed by print_top_file in prep_self_assembly_ligand.py \n" )
    ## INCLUDE STATEMETNS
    outputfile.write("#include \"%s\"\n"%(forcefield_itp))
    outputfile.write("#include \"%s\"\n"%(itp_file_name + ".itp"))
    ## DEFINING SYSTEM
    outputfile.write("\n[ system ]\n")
    outputfile.write("frame t=1.000 in water\n")
    ## MOLECULE NAMES
    outputfile.write("\n[ molecules ] \n")
    outputfile.write("; compound   nr_mol\n")
    outputfile.write(" %s     %d\n"%(res_name, 1))
    outputfile.close()
    return
#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TURNING TESTING ON / OFF
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    ### IF YOU ARE TESTING
    if testing is True:
    
        ### DEFINING INPUTS
        ligand_names=['butanethiol'] # Name of the ligand
        
        ### DIRECTORY INFORMATION    
        output_folder=r'R:\scratch\nanoparticle_project\prep_system\prep_gold\self_assembly_surfactants\test_folder'
        
        ## DEFINING PREPARATION FOLDER
        prep_folder=r'R:\scratch\nanoparticle_project\prep_files\fragment_based_ligands'
        
    else:
        # Adding options for command line input (e.g. --ligx, etc.)
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ### DEFINING PARSER OPTIONS
        ## LIGAND TYPES
        parser.add_option("--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
              help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        ## OUTPUT FOLDER
        parser.add_option("--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        ## PREPARATION FOLDER
        parser.add_option("--prep", dest="prep_Folder", action="store", type="string", help="Preparation folder", default=".")
        
        #### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
        
        ## OUTPUT/INPUT FILES
        prep_folder = options.prep_Folder # Preparation folder
        output_folder = options.output_folder   # Output folder

    ## PRINTING FORCE FIELD ITP FILE
    print_force_field_itp( output_folder )

    ### GETTING THE GRO, ITP, TOP FILES
    get_single_ligand_parameters(ligand_names, prep_folder, forcefield_folder = FORCEFIELD_FOLDER, output_folder = output_folder)
