# -*- coding: utf-8 -*-
"""
combine_head_group.py
The purpose of this function is to input a gro and itp file, then combine sulfur-head groups. It will output the gro file and itp file (with corrected atom numbers)

CREATED ON: 05/01/2018

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    - input directory
    - input gro file
    - input itp file
    - output directory
    - output gro file
    - output itp file
OUTPUTS:
    - updated gro file (correct atom numbers)
    - updated itp file (correct atom numbers)

ALGORITHM:
    - Read in gro file
    - Read in itp file
    - Combine the itp file's charges for sulfur and hydrogen
    - Re-correct for all atom numbers
    - Output to a itp file
    - Output to a gro file
    
FUNCTIONS:
    gro_file_remove_atoms: removes atom from gro file (based on extract_gro class)
    remove_atom_list_of_list: removes atom (all atoms of this type) from a list of list
    correct_atom_numbers: corrects atoms numbers for a list given a conversion legend
    itp_file_remove_atoms: remoes atoms from an itp file (based on extract_itp class)
    plot_geometry: plots a 3D structure of your ligand (debugging purposes)
    
**** UPDATES ****

"""
#%%
### MODULE IMPORTS
import os
import numpy as np # For math
from MDBuilder.core.import_tools import extract_gro, extract_itp ## EXTRACTION GRO AND ITP FILE SCHEME
from MDBuilder.core.export_tools import print_gro_file ## PRINTING GENERAL GRO FILE
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
from MDBuilder.math.rotation import rotate_coord ## ROTATION OF LIGANDS CODE
import time
import sys
import re # used to extract string information

### DEFINING GLOBAL VARIABLES
## DIRECTION OF WHERE TO OUTPUT THE LIGAND DIRECTION
LIGAND_DIR_VECTOR= np.array([0, 0, 1]) # z-axis for now

#################
### FUNCTIONS ###
#################
### FUNCTION TO REMOVE ATOMS FROM A GRO FILE
def gro_file_remove_atoms( gro_file, atom_index = [] ):
    '''
    The purpose of this function is to update the gro file class based on the inputs of atoms to remove
    INPUTS:
        gro_file: gro file class
        atom_index: [list] list of atom indexes to remove
    OUTPUTS:
        updated_gro_file: updated gro file class with the atoms removed -- no returns necessary. This will edit the class instance
    '''
    print("\n~~~ gro_file_remove_atoms ~~~")
    print("TOTAL ATOMS REMOVED: %d"%(len(atom_index)))
    ## FINDING INDEX OF ALL ATOMS TO REMOVE
    gro_atom_index_remove = [ gro_file.AtomNum[gro_file.AtomNum.index(each_atom_index)] for each_atom_index in atom_index]
    if len(gro_atom_index_remove) > 0:
        ## FINDING ALL INDEXES TO KEEP
        gro_atom_index_keep = [ index for index, each_atom_index in enumerate(gro_file.AtomNum) if each_atom_index not in gro_atom_index_remove]
        ## REMOVING ALL CORRESPONDING GRO FILE INFORMATION
        gro_file.AtomName = [ gro_file.AtomName[each_index] for each_index in gro_atom_index_keep]
        gro_file.AtomNum = [ gro_file.AtomNum[each_index] for each_index in gro_atom_index_keep]
        gro_file.xCoord = [ gro_file.xCoord[each_index] for each_index in gro_atom_index_keep]
        gro_file.yCoord = [ gro_file.yCoord[each_index] for each_index in gro_atom_index_keep]
        gro_file.zCoord = [ gro_file.zCoord[each_index] for each_index in gro_atom_index_keep]
        
        ## CORRECTING ATOM NAMES AND NUMBERS
        # FINDING ALL NAMES
        gro_file.AtomName = [  ''.join(re.findall(r'\D', each_atom)+ [str(index + 1)]) for index, each_atom in enumerate(gro_file.AtomName) ]
        gro_file.AtomNum = [each_index+1 for each_index in range(len(gro_file.AtomNum))]
        
        ## CORRECTING ATOM NAMES AND ATOM NUMBERS
        gro_file.total_atoms = len(gro_file.AtomNum)

    return

### FUNCTION TO REMOVE A ROW IF AN INDEX IS FOUND
def remove_atom_list_of_list(array, atom_index):
    '''
    The purpose of this function is to remove all rows that have an index within a list of list. This is useful for cases where you have an atom to remove.
    INPUTS:
        array: [np.array] list of list, such as bonding information, etc.
        atom_index: [list] list of atom indexes to remove
    OUTPUTS:
        new_array: [np.array] updated array
        rows_to_keep: [list] list of updated rows to keep
        total_removed: [int] total of the list removed
    '''
    ## DEFINING ROWS TO KEEP
    rows_to_keep = []; initial_length=len(array)
    ## LOOPING THROUGH EACH LIST
    for index, each_list in enumerate(array):
        ## SEEING IF THE ATOM INDEX IS WITHIN
        if np.any(np.isin(atom_index, each_list)) == False:
            rows_to_keep.append(index)
    ## FINDING NEW ARRAY
    new_array = array[rows_to_keep, :]
    ## FINDING TOTAL REMOVED
    total_removed = initial_length - len(new_array)
    return new_array, rows_to_keep, total_removed

### FUNCTION TO CORRECT ATOM NUMBERS
def correct_atom_numbers( list_to_convert, conversion_legend ):
    '''
    The purpose of this function is to take a list of list and convert all the numbers according to a conversion list. The conversion list is also a list of list (i.e. [[1,2], [2,3]])
    The way this script works is that it flattens out the list of list, then converts the numbers, then re-capitulates the list of list.
    INPUTS:
        list_to_convert: list of list that you want to fix in terms of numbers (i.e. atom numbers)
        conversion_legend: list of list that has indexes where the first index is the original and the next index is the new value
    OUTPUTS:
        converted_list: list of list with the corrected values
    '''
    # Start by converting the list to a numpy array
    converted_list = np.array(list_to_convert).astype('int')
    # Copying list so we do not lose track of it
    orig_list = converted_list[:]
    # Looping through each conversion list value and replacing
    for legend_values in conversion_legend:
        converted_list[orig_list==legend_values[0]] = legend_values[1]
    return converted_list

### FUNCTION TO REMOVE ATOMS FROM ITP FILE
def itp_file_remove_atoms( itp_file, atom_index = [] ):
    '''
    The purpose of this function is to remove itp file atoms, and update the following:
        bonds, angles, pairs, dihedrals
    INPUTS:
        itp_file: itp file class
        atom_index: [list] atom index to remove
    OUTPUTS:
        updated itp file class
    '''
    print("\n~~~ itp_file_remove_atoms ~~~")
    print("TOTAL ATOM REMOVED: %d"%(len(atom_index)))
    ## FINDING ALL INDEX TO REMOVE
    itp_atom_serial_remove = [ input_itp_file.atom_num.index(each_index) for each_index in atom_index ] # Starts from 0
    itp_atom_index_remove = [ input_itp_file.atom_num[each_index] for each_index in itp_atom_serial_remove ] # Starts from atom name
    ## FINDING ALL INDEX TO KEEP
    itp_atom_serial_keep = [ each_index for each_index in range(len(input_itp_file.atom_num)) if each_index not in itp_atom_serial_remove ]
    ## CREATING ATOM DICTIONARY
    itp_atom_index_dict = [ [ itp_atom_serial + 1 , i + 1 ]  for i, itp_atom_serial in enumerate(itp_atom_serial_keep)] # Conversion table for correct atom numbers when withholding atoms old -> new
    ### FINDING ALL INDEXES TO KEEP
    ## ATOM INFORMATION
    itp_file.atom_num = [ itp_file.atom_num[each_index] for each_index in itp_atom_serial_keep]
    itp_file.atom_type = [ itp_file.atom_type[each_index] for each_index in itp_atom_serial_keep]
    itp_file.atom_resnr = [ itp_file.atom_resnr[each_index] for each_index in itp_atom_serial_keep]
    itp_file.atom_atomname = [ itp_file.atom_atomname[each_index] for each_index in itp_atom_serial_keep]
    itp_file.atom_charge = [ itp_file.atom_charge[each_index] for each_index in itp_atom_serial_keep]
    itp_file.atom_mass = [ itp_file.atom_mass[each_index] for each_index in itp_atom_serial_keep]
    
    ## FIXING ATOM INFORMATION
    itp_file.atom_atomname = [  ''.join(re.findall(r'\D', each_atom)+ [str(index + 1)]) for index, each_atom in enumerate(itp_file.atom_atomname) ]
    itp_file.atom_num = [each_index+1 for each_index in range(len(itp_file.atom_num))]
    
    ## CORRECTING BONDING INFORMATION
    if len(itp_file.bonds) > 0:
        ## REMOVING ALL UNNECESSARY BONDS
        itp_file.bonds, bond_index, num_bonds_removed = remove_atom_list_of_list(itp_file.bonds, itp_atom_index_remove)
        itp_file.bonds_func = [ itp_file.bonds_func[each_index] for each_index in bond_index]
        ## CORRECTING ATOMIC NUMBERS
        itp_file.bonds = correct_atom_numbers(itp_file.bonds, itp_atom_index_dict )
        ## PRINTING
        print("TOTAL BONDS REMOVED: %d"%(num_bonds_removed))
    
    ## CORRECTING ANGLES
    if len(itp_file.angles) > 0:
        itp_file.angles, angle_index, num_angles_removed = remove_atom_list_of_list(itp_file.angles, itp_atom_index_remove)
        itp_file.angles_func = [ itp_file.angles_func[each_index] for each_index in angle_index]
        ## CORRECTING ATOMIC NUMBERS
        itp_file.angles = correct_atom_numbers(itp_file.angles, itp_atom_index_dict )
        ## PRINTING
        print("TOTAL ANGLES REMOVED: %d"%(num_angles_removed))

    ## CORRECTING DIHEDERALS
    if len(itp_file.dihedrals) > 0:
        itp_file.dihedrals, dihedral_index, num_dihedrals_removed = remove_atom_list_of_list(itp_file.dihedrals, itp_atom_index_remove)
        itp_file.dihedrals_func = [ itp_file.dihedrals_func[each_index] for each_index in dihedral_index]
        ## CORRECTING ATOMIC NUMBERS
        itp_file.dihedrals = correct_atom_numbers(itp_file.dihedrals, itp_atom_index_dict )
        ## PRINTING
        print("TOTAL DIHEDRALS REMOVED: %d"%(num_dihedrals_removed))
    
    ## CORRECTING PAIRS
    if len(itp_file.pairs) > 0:
        itp_file.pairs, pairs_index, num_pairs_removed = remove_atom_list_of_list(itp_file.pairs, itp_atom_index_remove)
        itp_file.pairs_func = [ itp_file.pairs_func[each_index] for each_index in pairs_index]
        ## CORRECTING ATOMIC NUMBERS
        itp_file.pairs = correct_atom_numbers(itp_file.pairs, itp_atom_index_dict )
        ## PRINTING
        print("TOTAL PAIRS REMOVED: %d"%(num_pairs_removed))
    
    return

### FUNCTION TO PLOT GEOMETRY
def plot_geometry( xyz_coordinates, want_plot):
    '''
    The purpose of this script is to plot a given geometry
    INPUTS:
        xyz_coordinates: XYZ coordinates as a form of Nx3 numpy array
        want_plot: True or False -- True if you want a plot
    OUTPUTS
        3D figure of your data
    '''
    if want_plot:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D # For 3D axes
        ### PLOTTING
        fig, ax = plt.subplots(1, 1, subplot_kw={'projection':'3d', 'aspect':'equal'})
        # Setting x, y, z labels
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        
        ax.plot(xyz_coordinates[:,0], xyz_coordinates[:,1], xyz_coordinates[:,2],color='k')
        
        plt.axis('equal')
        plt.show(fig)
        
    return


#%% MAIN SCRIPT
if __name__ == "__main__": 
    
    ### SEEING IF TEST IS TRUE
    testing = check_testing() # False if you are on command line
    
    ############################
    ### DEFINING INPUT FILES ###
    ############################
    if testing is True:
        ## DEFINING DIRECTORY PATH
        input_dir=r'R:\scratch\nanoparticle_project\prep_system\prep_ligand\minimized_structures\dodecanethiol'
        ## DEFINING INPUT FILES
        input_gro=r'dodecanethiol.gro'
        input_itp=r'dodecanethiol.itp'
        
        input_dir=r'R:\scratch\nanoparticle_project\prep_system\prep_ligand\minimized_structures\C11COOm'
        ## DEFINING INPUT FILES
        input_gro=r'C11COOm.gro'
        input_itp=r'C11COOm.itp'
        
        ## DEFINING OUTPUT FILES
        output_folder=input_dir
        output_gro=r'C11COOm_fixed.gro'
        output_itp=r'C11COOm_fixed.itp'
        
        output_gro_path = os.path.join( output_folder, output_gro  )
        output_itp_path = os.path.join( output_folder, output_itp  )
        
        ## INPUT FILES
        input_gro_path  =   input_dir + '/' + input_gro
        input_itp_path  =   input_dir + '/' + input_itp
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ### ADDING OPTIONS
        ## INPUT FILES
        parser.add_option("--igro", dest="input_gro_path", action="store", type="string", help="Input gro file", default="test_dir")
        parser.add_option("--iitp", dest="input_itp_path", action="store", type="string", help="Input itp file", default="test_dir")
        ## OUTPUT FILES
        parser.add_option("--ogro", dest="output_gro_path", action="store", type="string", help="Output gro file", default="test_dir")
        parser.add_option("--oitp", dest="output_itp_path", action="store", type="string", help="Output itp file", default="test_dir")
        
        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"

        ## DEFINING INPUT FILES
        input_gro_path= options.input_gro_path        # Input gro file
        input_itp_path= options.input_itp_path        # Input itp file
        
        ## DEFINING OUTPUT FILES
        output_gro_path      = options.output_gro_path   # Output gro file
        output_itp_path      = options.output_itp_path   # Output itp file

    ###################
    ### MAIN SCRIPT ###
    ################### 
    
    ## DEFINING OUTPUT GRO
    output_folder   = os.path.dirname(output_gro_path)
    output_gro      = os.path.basename(output_gro_path)   # Output gro file
    output_itp      = os.path.basename(output_itp_path)   # Output itp file
    
    ###########################
    ### EXTRACTION OF FILES ###
    ###########################    
    ### EXTRACTION OF THE GRO FILE
    input_gro_file = extract_gro(input_gro_path)
    
    ### EXTRACTION OF THE ITP FILE
    input_itp_file = extract_itp(input_itp_path)

    ##############################################################################
    ### LOCATING ALL INDEXES THAT HAVE HYDROGEN BONDED WITH SULFUR OF INTEREST ###
    ##############################################################################
    
    ### CREATING ATOM INDEX TO WITHHOLD
    atom_index_withhold = [] # atom index that will be withheld in both gro and itp file
    
    ### FINDING ATOM VALUE OF FIRST SULFUR
    itp_sulfur_atom_index = [ index for index, each_atom_name in enumerate(input_itp_file.atom_atomname) if 'S' in each_atom_name][0]
    itp_sulfur_atom_num = input_itp_file.atom_num[itp_sulfur_atom_index]

    ### FINDING ALL ATOMS BONDED TO THAT INDEX
    atom_bound = input_itp_file.find_atoms_bonded(itp_sulfur_atom_num)
    
    ### SEEING IF ANY OF THOSE BONDS ARE HYDROGENS
    itp_sulfur_hydrogen_bond_atom_num = [ each_atom_bound for each_atom_bound in atom_bound if 'H' in input_itp_file.atom_atomname[ input_itp_file.atom_num.index(each_atom_bound) ]  ]
    
    ## STORING IN WITHHOLD VARIABLE
    atom_index_withhold.extend(itp_sulfur_hydrogen_bond_atom_num)
    
    #############################################
    ### COMBINING SULFUR AND HYDROGEN CHARGES ###
    #############################################
    
    ## FINDING CHARGES (SULFUR + HYDROGEN)
    sulfur_charge = float(input_itp_file.atom_charge[input_itp_file.atom_num.index(itp_sulfur_atom_num)])
    ## SEEING IF HYDROGEN IS BOUND TO SULFUR
    if len(itp_sulfur_hydrogen_bond_atom_num) > 0:
        if len(itp_sulfur_hydrogen_bond_atom_num) > 1:
            print("There are two hydrogens bound to your sulfur --- are you sure your ligand is correct?")
            print("Pausing here and stopping -- please double check the structure or combine head group script")
            time.sleep(5); sys.exit()
        else:
            hydrogen_charge = [ float(input_itp_file.atom_charge[input_itp_file.atom_num.index(each_atom_index)]) for each_atom_index in itp_sulfur_hydrogen_bond_atom_num ][0]
    else:
        hydrogen_charge = 0.000
    
    ## COMBINING CHARGES
    combined_charge = sulfur_charge + hydrogen_charge
    
    ## EDITING SULFUR CHARGE OF SULFUR GROUP
    input_itp_file.atom_charge[itp_sulfur_atom_index] = combined_charge
    
    ###################################
    ### CORRECTING GRO AND ITP FILE ###
    ###################################
    ## REMOVING ATOMS FROM GRO FILE
    gro_file_remove_atoms(input_gro_file, atom_index_withhold)
    ## REMOVING ATOMS FROM ITP FILE
    itp_file_remove_atoms(input_itp_file, atom_index_withhold)
    
    ####################################################
    ### MOVING LIGAND TO POINT AT A SINGLE DIRECTION ###
    ####################################################
    ## DEFINING XYZ COORDINATES
    xyz_coord = np.array( [input_gro_file.xCoord, input_gro_file.yCoord, input_gro_file.zCoord ] ).T  ## OUTPUTS N X 3 MATRIX
    ## FINDING THE ORIGINAL LOCATION
    gro_sulfur_atom_index = [ index for index, each_atom_name in enumerate(input_gro_file.AtomName) if 'S' in each_atom_name][0]
    ## DEFINING ORIGIN
    origin = xyz_coord[gro_sulfur_atom_index,:]
    ## TRANSLATING
    trans_xyz_coord = xyz_coord - origin
    ## FINDING AVERAGE DIRECTION
    avg_direction = np.average( trans_xyz_coord, axis = 0 )
    ## ROTATING THE DATA
    rotated_xyz_coord = rotate_coord( avg_vector_dir = avg_direction, desired_dir = LIGAND_DIR_VECTOR, rotated_data=trans_xyz_coord )
    ''' Debugging, plotting the coordinates
    plot_geometry(xyz_coord,True)
    plot_geometry(rotated_xyz_coord,True)
    '''
    ###################################
    ### EXTRACTING GRO AND ITP FILE ###
    ###################################
    ## PRINTING GRO FILE
    print_gro_file(output_gro = output_gro,
                   output_folder = output_folder,
                   resids = input_gro_file.ResidueNum,
                   residue_name = input_gro_file.ResidueName,
                   atom_name = input_gro_file.AtomName,
                   geom = rotated_xyz_coord,
                   box_dimension = input_gro_file.Box_Dimensions
                   )
    
    ## PRINTING ITP FILE
    input_itp_file.print_itp_file(output_itp = output_itp,
                                  output_folder = output_folder,
                                  )
    