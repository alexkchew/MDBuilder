#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot_grossfield_wham_fe.py

This script plots the free energy from Alan Grossfield's WHAM code. The idea 
is that we will extract the data from the outputs of the WHAM code, then convert 
energies, and generate a plot of free energy versus your reaction coordinate

Created on Tue May 26 09:40:22 2020
nplm_prod_COVAR.dat

@author: alex
"""
import os
import numpy as np
import pandas as pd
import glob

## SETTING DEFAULTS
import MDDescriptors.core.plot_tools as plot_funcs
plot_funcs.set_mpl_defaults()

## IMPORTING READ COVAR CODE
from MDDescriptors.core.import_tools import read_plumed_covar, read_xvg

## LOADING SIGNALS
from scipy.signal import find_peaks

## DEFINING FIGURE SIZE
FIGURE_SIZE = plot_funcs.FIGURE_SIZES_DICT_CM['1_col_landscape']

## DEFINING IMAGE LOCATION
IMAGE_LOC="/Users/alex/Box Sync/VanLehnGroup/2.Research Documents/Alex_RVL_Meetings/20200629/images/nplm_project"

###################################
### CLASS FUNCTION TO READ WHAM ###
###################################
class read_grossfield_wham:
    '''
    This function reads the Alan Grossfield wham output.
    INPUTS:
        path_to_wham: [str]
            path to wham energy file (*.dat)
    OUTPUTS:
        self.path_to_wham: [str]
            path to wham file
        self.data: [list]
            list of the data
        self.df: [pd.DataFrame]
            dataframe output
            
    FUNCTIONS:
        read_data:
            reads the data
        extract_data:
            extracts the data after reading
        convert_free_energies:
            converts the free energies into a different unit system
    '''
    def __init__(self,
                 path_to_wham):
        ## STORING
        self.path_to_wham = path_to_wham
        
        ## READING THE DATA
        self.data = self.read_data()
        
        ## EXTRACTING THE DATA
        self.df = self.extract_data()
        
        return
    
    ### FUNCTION TO READ THE DATA
    def read_data(self):
        '''
        This function reads the data.
        INPUTS:
            self:
                class property
        OUTPUTS:
            data: [list]
                list of the data line by line
        '''
        ### LOADING THE FILE
        with open( self.path_to_wham, 'r' ) as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        data = [ line for line in fileData if '#' not in line ]
        data = [ n.strip( '\n' ) for n in data ]
        data = [ n.split( ) for n in data if len(n) > 0 ]
        data = [ [ float( n ) for n in line if n != '' ] for line in data ]
        return data
    
    ### FUNCTION TO EXTRACT THE DATA
    def extract_data(self):
        '''
        The purpose of this function is to extract the data outputted from 
        INPUTS:
            
        OUTPUTS:
            df: [pd.DataFrame]
                pandas dataframe containing the information that you need
            df outputs:
                       coord  free_energy  probability
                0      0.275     0.000000     0.982999
                1      0.825     2.485703     0.015197
        '''
        ## DEFINING DATAFRAME
        df = []
        
        for each_line in self.data:
            ## REMOVING ALL LINES WITH INFINITY FREE ENERGIES
            if str(each_line[1]) != "inf":
                ## APPENDING
                df.append({
                        'coord': each_line[0],
                        'free_energy': each_line[1],                    
                        'probability': each_line[3],     
                        })
        
        ## CREATING DATAFRAME
        df = pd.DataFrame(df)
        
        return df
    
    ### FUNCTION TO CONVERT FREE ENERGIES
    def convert_free_energies(self,
                              free_energies,
                              units = 'kJ',
                              T = 300):
        '''
        The purpose of this function is to convert the free energies to a desired type.
        By default, the free energies are outputted in kcal/mol.
        INPUTS:
            free_energies: [list]
                free energies 
            units: [str]
                Units type:
                    'kcal': Free energies in kcal/mol. This is the default, if so, nothing will change
                    'kJ': Free energies in kJ/mol
                    'kT': Free energies in kT
            T: [float]
                temperature in Kelvins, by default this is 300 K. This is used 
                when converting free energies in terms of kT
                
        OUTPUTS:
            free_energies_new_units: [np.array]
                free energies as new units
        '''
        ## DEFINING CONVERSION FACTORS
        kcal2kJ = 4.184 
        
        ## DEFINING BETA
        if units == "kT":
            kB = 1.9871708e-3              # Boltzmann constant in kcal
            kT = kB * T 
        
        available_units = ['kcal', 'kJ', 'kT']
        if units not in available_units:
            print("Error! Units are not available")
            print("Available types: %s"%( ', '.join(available_units) ) )
        
        ## CONVERTING FREE ENERGIES TO NUMPY
        free_energies_np = np.array(free_energies)
        
        ## CHANGING THE FREE ENERGIES
        if units == "kcal":
            free_energies_new_units = free_energies_np[:]
        if units == "kJ":
            free_energies_new_units = free_energies_np * kcal2kJ
        elif units =="kT":
            free_energies_new_units = free_energies_np / kT
        
        return free_energies_new_units

### FUNCTION TO CREATE CONTACTS VERSUS FREE ENERGY
def plot_free_energy_vs_coord(coord,
                              fe,
                              fe_error = None,
                              ylabel="Free energy (kJ/mol)",
                              xlabel = "Contacts",
                              style={
                                      'linestyle': '-',
                                      'color': 'k',
                                      },
                              fig = None,
                              ax = None,
                              fig_size = FIGURE_SIZE,
                            errorbar_format={
                                    "fmt": ".",
                                    "capthick": 1.5,
                                    "markersize": 8,
                                    "capsize": 3,
                                    "elinewidth": 1.5,
                                    },
                            x_line = None,):
    '''
    This function plots free energy versus reaction coordination number
    INPUTS:
        coord: [np.array]
            reaction coordinate values
        fe: [np.array]
            free energy values
        fe_error: [np.array]
            error for fe error
        ylabel: [str]
            y label
        xlabel: [str]
            x label
        style: [dict]
            dictionary containing line details
        fig, ax: 
            figure and axis. If either is None, we will create a new figure
        errorbar_format: [dict]
            dictionary for erro bar
    OUTPUTS:
        fig, ax: [obj]
            figure and axis for object
    '''
    ## PLOTTING FIGURE
    if fig is None or ax is None:
        fig, ax = plot_funcs.create_fig_based_on_cm(fig_size)
    
        ## ADDING AXIS LABELS
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        
        ## ADDING X LINE
        if x_line is not None:
            ax.axvline(x = x_line, color = 'k', linestyle = '--')
    
    ## PLOTTING
    if fe_error is not None:
        ## DEFINING X Y
        x = coord
        y = fe
        y_err = fe_error
        
        ## MERGING DICT
        merged_dict = {**style, **errorbar_format}
#        ax.errorbar(x = x,
#                    y = y,
##                    yerr = fe_error,
#                    **merged_dict
#                    )
        ## ADDING ERROR BAR AS SHADED
        ax.fill_between(x, y-y_err, y+y_err, color = merged_dict['color'], alpha = 0.4)

    ## PLOTTING lINE
    ax.plot(coord, fe, **style)
    
    ## TIGHT LAYOUT
    fig.tight_layout()
    
    return fig, ax

### FUNCTION TO PLOT SAMPLING TIME
def plot_PMF_sampling_time(wham_storage,
                       fig_size = (9.55, 6.5),
                       ylabel = "Free energy (kJ/mol)",
                       xlabel = "Contacts",
                       fig = None,
                       ax = None,
                       want_legend = True,
                       want_total_time = False,
                       zero_location = None,
                       colors = None,):
    '''
    The purpose of this function is to plot sampling time for different 
    wham storage
    INPUTS:
        wham_storage: [df]
            dataframe containing wham storage information
        fig_size: [tuple]
            size of the figure
        fig, ax:
            Object to generate the figure
        want_legend: [logical]
            True if you want legend
        want_total_time: [logical]
            True if you want total time printed on. It will also reverse labels 
            so that the colors are consistent with total time. 
        zero_location: [idx]
            zero location 
        colors: [obj, default None]
            colors to use. If None, then we will generate colors
    OUTPUTS:
        fig, ax: 
            figure and axis for plot
    '''    
    ## GENERATING COLOR 
    if colors is None:
        print("Since colors is none, generating new colors for sampling time plot")
        colors = plot_funcs.get_cmap(len(wham_storage))
        colors = [ colors(each_idx) for each_idx in range(len(wham_storage))]
        colors[-1] = 'k'
    
    if want_total_time is True:
        ## GETTING TOTAL TIME
        wham_storage['total_time'] = np.abs(wham_storage['end'] - wham_storage['begin'])
        ## STORING
        wham_storage = wham_storage.sort_values(by = 'total_time', ignore_index = True)
    
    ## LOOPING AND PLOTTING
    for idx, row in wham_storage.iterrows():
        ## GETTING WHAM
        wham = row['wham']
        ## GETTING BEGINNING AND END LABEL
        if want_total_time is False:
            label = "%d-%d"%( row['begin'], row['end'] )
        else:
            label = "%d"%(row['total_time'])
        
        ## GETTING COORD
        coord = wham.df['coord']
        
        ## GETTING FREE ENERGIES
        free_energy_kJ = wham.convert_free_energies(free_energies = wham.df['free_energy'],
                                                    units = 'kJ')
        
        ## DEFINING ZERO LOCATION
        if zero_location is not None:
            ## GETTING ZEROS                
            zero_energy = free_energy_kJ[zero_location]
            ## SAVING
            free_energy_kJ-=zero_energy
        
#        ## SEEING IF IDX IS THE LAST ONE
#        if idx == len(wham_storage) - 1:
#            color = 'k'
#        else:
#            color =  colors(idx)
            
        ## PLOTTING 
        fig, ax = plot_free_energy_vs_coord(coord = coord,
                                            fe = free_energy_kJ,
                                            ylabel=ylabel,
                                            xlabel = xlabel,
                                            style={
                                                    'linestyle': '-',
                                                    'color': colors[idx],
                                                    'label': label
                                                    },
                                            fig = fig,
                                            ax = ax,
                                            fig_size = fig_size)
        
    ## ADDING LEGEND
    if want_legend is True:
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ## TIGHT
    fig.tight_layout()
    
    return fig, ax


### FUNCTION TO PLOT HISTOGRAM
def plot_PMF_histogram(covar_df,
                       histogram_range = (0, 110),
                       bin_width = 2,
                       fig_size = (9.55, 6.5),
                       want_legend = True,
                       time_starting_from = 0,
                       times_from_end = None,
                       xlabel = "Contacts",
                       ylabel = "Probability denstiy function",
                       fig = None,
                       ax = None,
                       want_black_color=False):
    '''
    This plots the probability density function versus number of contacts
    
    INPUTS:
        covar_df: [pd.dataframe]
            dataframe containing covar information
        histogram_range: [tuple]
            range of the histogram
        bin_width: [float]
            bin width
        fig_size: [tuple]
            figure size
        want_legend: [logical]
            True if you want legend
        time_starting_from: [float]
            time to start from
        times_from_end: [float]
            times from end to extract the data
        want_black_color: [logical]
            True if you want the histograms plotted in black
    OUTPUTS:
        fig, ax:
            figure and axis
    '''    

    ## DEFINING BINS
    bins = np.arange(*histogram_range,bin_width)
    
    ## DEFINING CENTER
    center = (bins[:-1] + bins[1:]) / 2
    
    ## GENERATING COLOR 
    colors = plot_funcs.get_cmap(len(covar_df))
    
    ## CREATING FIGURE
    if fig is None or ax is None:
        ## CREATING FIGURE
        fig, ax = plot_funcs.create_fig_based_on_cm(fig_size)
    
        ## ADDING LABELS
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
    
    ## LOOPING THROUGH DF
    for idx, row in covar_df.iterrows():
        ## GETTING TIME
        if time_starting_from > 0:
            print("Locating for times greater than %d"%(time_starting_from))
        
        if times_from_end is not None:
            ## GETTING END TIME
            end_time = row['covar']['time'].iloc[-1]
            time_starting_from = end_time - times_from_end
            print("Getting times from end to %d ps: %d to %d"%(times_from_end, time_starting_from, end_time) )
        
        ## DEFINING INDEX OF TIMES
        idx_times = row['covar']['time'] > time_starting_from
        
        ## DEFINING COVAR
        coord = row['covar']['coord'][idx_times]
        ## GENERATING HISTOGRAM
        hist, bin_edges = np.histogram(coord,
                                       bins = bins,
                                       density = True,
                                       )
        ## DEFINING LABEL
        label = row['contacts']
        
        ## DEFINING COLOR
        if want_black_color is False:
            current_color = colors(idx)
        else:
            current_color = 'k'
        
        
        ## PLOTTING
        ax.plot(center, hist, 
                label = label,
                color = current_color)
        
    ## ADDING LEGEND
    if want_legend is True:
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    
    ## TIGHT LAYOUT
    fig.tight_layout()
    
    return fig, ax

### CLASS FUNCTION TO LOAD SAMPLING TIME
class sampling_time_grossfield_wham:
    '''
    This function extracts the free energies from sampling time computations. 
    INPUTS:
        void
    FUNCTIONS:
        find_sampling_dat_files:
            finds the dat files from a path
        extract_sampling_time_names_from_filename:
            extracts the sampling time for a given filename
        extract_wham_from_paths:
            extracts wham files given the paths from 'find_sampling_dat_files'
        plot_sampling_time:
            function that plots the sampling time after extraction is complete
    USAGE EXAMPLE:
        ## CREATING SAMPLING TIME
        sampling_time = sampling_time_grossfield_wham()
        
        ## FINDING FILES
        fe_files_path = sampling_time.find_sampling_dat_files(wham_folder,
                                                              fe_file_prefix = "umbrella_free_energy-",
                                                              )
        
        ## LOADING THE FILES
        wham_storage = sampling_time.extract_wham_from_paths(fe_files_path = fe_files_path)
        
        ## REORDER BY END
        wham_storage = wham_storage.sort_values(by="end").reset_index(drop = True)
        
        ## PLOTTING WHAM STORAGE        
        fig, ax = sampling_time.plot_sampling_time(wham_storage = wham_storage,
                                                  fig_size = (9.55, 6.5))
        
    '''
    def __init__(self):
        
        return
    
    ### FUNCTION TO GET ALL PATHS
    @staticmethod
    def find_sampling_dat_files(wham_folder,
                                fe_file_prefix = "umbrella_free_energy-",
                                ):
        '''
        This function looks for all the *.dat files that follow a specific 
        prefix. The idea is that we use the file names to denote what the 
        free energy is sampling from. 
        INPUTS:
            wham_folder: [str]
                path to the wham folder
            fe_file_prefix: [str]
                prefix for the free enrgy files, e.g. "umbrella_free_energy-0_1000.dat"
        OUTPUTS:
            fe_files_path: [list]
                list of all the free energy file paths
        '''
        ## FINDING ALL SAMPLING TIME
        fe_files_path = glob.glob( os.path.join(wham_folder, fe_file_prefix) + "*.dat" )
        ## SORTING
        fe_files_path.sort()
        
        return fe_files_path
    
    ### FUNCTION TO EXTRACT SAMPLING TIME FROM NAME
    @staticmethod
    def extract_sampling_time_names_from_filename( filename = "umbrella_free_energy-0_6000.dat" ):
        '''
        The purpose of this function is to extract the sampling time from the 
        filename.
        INPUTS:
            filename: [str]
                name of the file
        OUTPUTS:
            extracted_name: [str]
                extracted sampling time, e.g. '0-6000' meaning that the free energy 
                was computed using 0 - 6000 ps.
            extracted_times: [dict]
                dictionary containg times of beginning and end
        '''
        ## REMOVING EXTENSSION
        filename_without_extension=os.path.splitext(filename)[0]
        ## SPLITTING
        filename_split=filename_without_extension.split('-')
        ## GETTING THE LATTER
        extracted_name=filename_split[-1]
        
        ## GETTING BEGININNG AND LAST TIME
        extracted_name_split = extracted_name.split('_')
        
        ## CREATING DICT
        extracted_times = {
                'begin': float(extracted_name_split[0]),
                'end': float(extracted_name_split[1]),
                }
    
        return extracted_name, extracted_times
    
    ### FUNCTION TO EXTRACT ALL THE WHAMS 
    def extract_wham_from_paths(self,
                                fe_files_path,
                                ):
        '''
        This function extracts the WHAM information for each free energy file. 
        INPUTS:
            fe_files_path: [list]
                list of all the free energy file paths. This can be 
                computed using the 'find_sampling_dat_files' function
        OUTPUTS:
            wham_storage: [dataframe]
                dataframe containing beginning, end, and wham object, e.g.
                  begin    end                                               wham
                0     0   1000  <__main__.read_grossfield_wham object at 0x131...
                1     0  10000  <__main__.read_grossfield_wham object at 0x131...
        '''
        ## STORING EACH WHAM
        wham_storage = []
        
        ## LOOPING THROUGH EACH PATH AND READING THE FILE
        for each_fe_file in fe_files_path:
            ## GETTING FE NAME
            current_fe_basename = os.path.basename(each_fe_file)
            ## GETTING EXTRACTED NAME
            extracted_name, extracted_times = self.extract_sampling_time_names_from_filename(current_fe_basename)
            ## READING WHAM
            wham = read_grossfield_wham(path_to_wham = each_fe_file)
            wham_dict = {
                    **extracted_times,
                    'wham': wham,
                    }
            ## STORING
            wham_storage.append(wham_dict)
            
        ## CREATING DATAFRAME
        wham_storage = pd.DataFrame(wham_storage)
            
        return wham_storage
    
    ### FUNCTION TO PLOT SAMPLING TIME
    @staticmethod
    def plot_sampling_time(**kwargs):
        '''
        The purpose of this function is to plot sampling time for different 
        wham storage
        INPUTS:
            wham_storage: [df]
                dataframe containing wham storage information
            fig_size: [tuple]
                size of the figure
        OUTPUTS:
            fig, ax: 
                figure and axis for plot
        '''
        return plot_PMF_sampling_time(**kwargs)
    


### FUNCTION TO LOAD COVAR FOR HISTOGRAM
class plot_covar_histogram:
    '''
    This function plots the histogram using covar information.
    INPUTS:
        void
    FUNCTIONS:
        load_covar:
            loads covar information
        plot:
            plots the probability density function versus contacts
    '''
    def __init__(self):
        
        return
    
    ### FUNCTION TO LOAD COVAR DATAFRAME
    @staticmethod
    def load_covar(path_to_sim,
                   contacts,
                   covar_file = "nplm_prod_COVAR.dat",
                   ):
        '''
        This function loads the covar information for multiple contacts.
        INPUTS:
            path_to_sim: [str]
                path to simulation
            contacts: [list]
                list of contacts that you want loaded
            covar_file: [str]
                name of covar file
        OUTPUTS:
            covar_df: [pd.dataframe]
                dataframe containing covar information
        '''

        ## DEFINING STORAGE FOR COVAR
        covar_storage = []
    
        ## LOOPING THROUGH CONTACTS AND LOADING
        for each_contacts in contacts:

            ## DEFINING PATH
            path_to_covar = os.path.join(path_to_sim,
                                         "%s"%(each_contacts),
                                         covar_file)
            print("-----")
            print("Path: %s"%(path_to_covar))
            
            ## GETTING FILE NAME
            filename, file_extension = os.path.splitext(covar_file)
                
            try:
                if file_extension == ".xvg":
                    ## READING COVAR
                    data_full, data_extract = read_xvg(path_to_covar)
                    
                    ## CREATING COVAR
                    covar = pd.DataFrame(data = data_extract, columns=['time', 'coord'])
                    ## MAKING TO NUMERIC
                    covar = covar.astype(float)
                else:
                    ## READING COVAR FILE
                    covar = read_plumed_covar(path_to_covar)
                
                ## APPENDING
                covar_storage.append(
                        {'contacts': each_contacts,
                         'covar': covar,
                         }
                         )
            except FileNotFoundError:
                print("--> Covar file not found!, skipping!")
                pass
            
        ## CREATING DATAFRAME
        covar_df = pd.DataFrame(covar_storage)
        
        return covar_df
    
    ### FUNCTION TO PLOT HISTOGRAM
    @staticmethod
    def plot(**kwargs):
        '''
        This plots the probability density function versus number of contacts
        
        INPUTS:
            covar_df: [pd.dataframe]
                dataframe containing covar information
            histogram_range: [tuple]
                range of the histogram
            bin_width: [float]
                bin width
            fig_size: [tuple]
                figure size
            want_legend: [logical]
                True if you want legend
            time_starting_from: [float]
                time to start from
            times_from_end: [float]
                times from end to extract the data
            want_black_color: [logical]
                True if you want the histograms plotted in black
        OUTPUTS:
            fig, ax:
                figure and axis
        '''    
        return plot_PMF_histogram(**kwargs)
    
    
### FUNCTION TO PLOT SAMPLING TIME
def main_sampling_time(wham_folder,
                       fe_file_prefix = "umbrella_free_energy-forward",
                       sort_values = "end",):
    '''
    this function simply runs the sampling time by using the sampling time 
    grossfield wham function. The idea is to generate a plot for the sampling 
    time giving the folder prefix,
    INPUTS:
        wham_folder: [str]
            directory to run sampling time for
        fe_file_prefix; [str]
            prefix to look for the free energy files
        sort_values: [str]
            sort values from specific columns. By default, we sort by 
            the end trajectory.
    OUTPUTS:
        wham_storage: [df]
            dataframe containing the free energy values
        sampling_time: [obj]
            sampling time object
        fig, ax:
            figure and axis for the sampling time plot
    '''
    ## CREATING SAMPLING TIME
    sampling_time = sampling_time_grossfield_wham()
    
    ## FINDING FILES
    fe_files_path = sampling_time.find_sampling_dat_files(wham_folder,
                                                          fe_file_prefix = fe_file_prefix,
                                                          )
    
    ## LOADING THE FILES
    wham_storage = sampling_time.extract_wham_from_paths(fe_files_path = fe_files_path)
#    print(wham_storage)
    
    ## REORDER BY END
    wham_storage = wham_storage.sort_values(by=sort_values).reset_index(drop = True)
    
    return wham_storage, sampling_time


### FUNCTION TO LOAD WHAM
def load_free_energies_grossfield(path_to_wham):
    '''
    This function loads the free energies from grossfield wham.
    INPUTS:
        path_to_wham: [str]
            path to wham folder
    OUTPUTS:
        wham: [obj]
            wham object output
        coord: [np.array]
            coordination numbers
        free_energies_kJ: [np.array]
            free energies in kJ
    '''
    
    ## READING WHAM
    wham = read_grossfield_wham(path_to_wham = path_to_wham)
    
    ## DEFINING COORD
    coord = wham.df['coord'].to_numpy()
    
    ## GETTING FREE ENERGIES IN KJ
    free_energies_kJ = wham.convert_free_energies(free_energies = wham.df['free_energy'],
                                                  units = 'kJ')

    return wham, coord, free_energies_kJ

### FUNCTION TO GET AVG AND STD OF ENERGIES
def get_wham_avg_std(storage_list):
    '''
    The purpose of this function is to get the average and standard 
    deviation from the wham storage list. The list should contain 
    'wham' information. 
    INPUTS:
        storage_list: [list]
            list of storage information for wham
    OUTPUTS:
        initial_wham: [df]
            dataframe containing the wham information with 'avg' and 'std' columns
    '''
    ## LOOPING THROUGH EACH LIST
    for idx, current_dict in enumerate(storage_list):

        ## IF FIRST INDEX
        if idx == 0:
            ## GETTING INITIAL WHAM
            initial_wham = storage_list[idx]['wham'].df.copy()
            initial_wham['free_energies_kJ_1'] = storage_list[idx]['free_energies_kJ']
            ## SPECIFYING SPECIFIC
            initial_wham = initial_wham[['coord','free_energies_kJ_1']]
        else:
            ## GETTING CURRENT WHAM            
            current_wham = storage_list[idx]['wham'].df.copy()
            ## INCLUDING FREE ENERGIES
            current_wham['free_energies_kJ_%d'%(idx+1)] = storage_list[idx]['free_energies_kJ']
            ## SPECIFYING SPECIFIC
            current_wham = current_wham[['coord','free_energies_kJ_%d'%(idx+1)]]
            
            ## MERGED DICT
            initial_wham = initial_wham.merge(current_wham, 
                                              left_on = 'coord', 
                                              right_on = 'coord', 
                                              how = 'outer')
            
            
    ## AFTER FOR LOOP, SORT
    initial_wham = initial_wham.sort_values(by = 'coord', ignore_index = True)
    
    ## GETTING LIST
    list_without_coord = initial_wham.keys().to_list()
    list_without_coord.remove('coord')
    
    ## AVERAGING
    initial_wham['avg'] = initial_wham[list_without_coord].mean(axis=1)
    initial_wham['std'] = initial_wham[list_without_coord].std(axis=1)

    return initial_wham

### FUNCTION TO PLOT FREE ENERGY
def plot_free_energies_for_wham_folder(wham_folder_dict,
                                       fe_file = "umbrella_free_energy.dat",
                                       fe_file_prefix_suffix = {
                                               'prefix': '',
                                               'suffix': '',
                                               },
                                       zero_location = None,
                                       fig = None,
                                       ax = None,
                                       want_peaks = False,
                                       peaks_inputs={
                                                'distance': 5
                                                },
                                        wham_styles_dict = {
                                                'C1':{
                                                    'color': 'r',
                                                    },
                                                'C10':{
                                                    'color': 'k'
                                                        },
                                                },
                                        fig_size = FIGURE_SIZE,
                                        print_min = False,
                                        xlabel = "Contacts",
                                        errorbar_format={
                                                "fmt": ".",
                                                "capthick": 1.5,
                                                "markersize": 8,
                                                "capsize": 3,
                                                "elinewidth": 1.5,
                                                },
                                        x_line = None,):
    '''
    This function plots the free energies for the wham folder.
    INPUTS:
        wham_folder_dict: [dict]
            dictionary for whatm details
        fe_file: [str]
            file for free energy output
        fe_file_prefix: [dict]
            file prefix and suffix to look for - this is useful for multiple 
            free energy results. 
        zero_location: [index]
            index to set zero
        want_peaks: [logical]
            True if you want peaks
        peaks_inputs: [dict]
            dictionary for peak inputs
        fig_size: [tuple]
            figure size in cm
        print_min: [logical]
            True if you want to print minima
        errorbar_format: [dict]
            error bar format
        x_line: [float]
            if None, no x line will be drawn. If it is a float, we will draw a line.
    OUTPUTS:
        stored_average_values: [dict]
            dictionary containing average free energy values across multiple trials
    '''
    
    ## STORING EACH
    stored_average_values = {}
    
    ## LOOPING
    for each_key in wham_folder_dict:
        ## DEFINING FOLDER
        wham_folder = wham_folder_dict[each_key]
        
        ## CHECKING PREFIX AND SUFFIX
        if fe_file_prefix_suffix['prefix'] != '' or fe_file_prefix_suffix['suffix'] != '':
            fe_file_list = glob.glob(wham_folder + "/%s*%s"%(fe_file_prefix_suffix['prefix'],
                                                             fe_file_prefix_suffix['suffix'],
                                                             ) )
        else:
            fe_file_list = [ os.path.join(wham_folder, fe_file) ]
        
        ## SORTING
        fe_file_list.sort()
        
        ## CREATING LIST
        storage_list = []
        
        ## LOOPING THROUGH EACH FE FILE
        for path_to_wham in fe_file_list:        
            ## LOADING WHAM
            wham, coord, free_energies_kJ = load_free_energies_grossfield(path_to_wham)
            ## STORING
            storage_list.append({
                    'wham': wham,
                    'coord': coord,
                    'free_energies_kJ': free_energies_kJ,
                    })

        ## DEFINING ZERO LOCATION
        if zero_location is not None:
            for each_dict in storage_list:
                ## GETTING FREE ENERGIES
                free_energies_kJ = each_dict['free_energies_kJ']
                ## GETTING ZEROS                
                zero_energy = free_energies_kJ[zero_location]
                ## SAVING
                each_dict['free_energies_kJ']-=zero_energy
        
        ## GETTING DATAFRAME
        avg_std_df = get_wham_avg_std(storage_list)
        
        ## DEFINING X, Y, YERR
        x = avg_std_df['coord']
        y = avg_std_df['avg']
        y_err = avg_std_df['std']
        
        ## STORING INPUTS
        stored_average_values[each_key] = avg_std_df.copy()
        
        ## PLOTTING WHAM
        fig, ax = plot_free_energy_vs_coord(coord = x,
                                            fe = y,
                                            fe_error = y_err,
                                            ylabel="Free energy (kJ/mol)",
                                            xlabel = xlabel,
                                            style={
                                                    'linestyle': '-',
                                                    'color': wham_styles_dict[each_key]['color'],
                                                    'label': each_key
                                                    },
                                            fig = fig,
                                            ax = ax,
                                            fig_size = fig_size,
                                            errorbar_format = errorbar_format,
                                            x_line = x_line)
        ## PLOTTING PEAKS
        if want_peaks is True:
            
            ## FINDING PEAKS
            peaks = find_peaks(y, **peaks_inputs)[0]
            coord_peaks = x[peaks]
            
            ## PRINTING
            print("Peaks for %s"%(each_key))
            print(coord_peaks)
            
            ## PLOTTING PEAKS
            ax.plot(coord_peaks,free_energies_kJ[peaks],"x", 
                    linestyle = None, 
                    linewidth = 0,
                    color = wham_styles_dict[each_key]['color'])
        
        if print_min is True:
            idx = np.argmin(y)
            loc_min = np.array(x)[idx]
            print("Minimum for %s: %.3f"%(each_key, loc_min))
            
    ## ADDING LEGEND
    ax.legend()
    
    return fig,ax, stored_average_values

#%%
### MAIN FUNCTION
if __name__ == "__main__":    
    
    ## DEFINING PATH
    wham_folder = r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200522-plumed_US_initial/US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/wham"
    wham_folder = r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200522-plumed_US_initial/US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/wham"

    
    #%%
    
    ## DEFINING PATH
    path_to_sim =r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims"
    
    ## DEFINING PARENT DICT
    parent_folder_dict = {
            'all_np_contacts': '20200522-plumed_US_initial',
            'hydrophobic_contacts': "20200615-US_PLUMED_rerun_with_10_spring",
            'hydrophobic_contacts_spr1': "20200609-US_with_hydrophobic_contacts"
                # '20200609-US_with_hydrophobic_contacts',
            }
    
    ## DEFINING PATH DICT
    path_dict = {'all_np_contacts': { 
                    'C1': os.path.join(path_to_sim,
                                       parent_folder_dict['all_np_contacts'],
                                       'US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/wham'),
                    'C10': os.path.join(path_to_sim,
                                       parent_folder_dict['all_np_contacts'],
                                       'US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/wham'), },
                'hydrophobic_contacts': { 
                    'C1': os.path.join(path_to_sim,
                                       parent_folder_dict['hydrophobic_contacts'],
                                       'UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/wham'),
                    'C10': os.path.join(path_to_sim,
                                       parent_folder_dict['hydrophobic_contacts'],
                                       'UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/wham'), 
                                        },
                'hydrophobic_contacts_spr1': { 
                    'C1': os.path.join(path_to_sim,
                                       parent_folder_dict['hydrophobic_contacts_spr1'],
                                       'UShydrophobic_1-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/wham'),
                    'C10': os.path.join(path_to_sim,
                                       parent_folder_dict['hydrophobic_contacts_spr1'],
                                       'UShydrophobic_1-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/wham'), 
                                        }
                
                }
                
    ## DEFINING DESIRED DICT
    desired_key = 'hydrophobic_contacts'
    # "hydrophobic_contacts"
    # 'hydrophobic_contacts'
    # desired_key = 'all_np_contacts'
    
    
    ## DEFINING WHAM FOLDERS
    wham_folder_dict = {
            **path_dict[desired_key]
            }
    
    ## DEFINING FILE
    fe_file = "umbrella_free_energy.dat"
    
    #%%
    

    
    ## DEFINING COLOR DICT
    wham_styles_dict = {
            'C1':{
                'color': 'r',
                },
            'C10':{
                'color': 'k'
                    },
            }
            
    
    #%%
    
    ## PLOTTING FREE ENERGIES
    fig, ax = plot_free_energies_for_wham_folder(wham_folder_dict,
                                                 want_peaks = False)
    
    
    #%%
    
    ## FIGURE NAME
    figure_name = "%s-WHAM_PMF"%(desired_key)
    
    ## SAVING FIGURE
    plot_funcs.store_figure(fig = fig, 
                 path = os.path.join(IMAGE_LOC,
                                     figure_name), 
                 fig_extension = 'png', 
                 save_fig=True,)
                 
    fig, ax = plot_free_energies_for_wham_folder(wham_folder_dict,zero_location=0 )
                 
    #%%
    ## FIGURE NAME
    figure_name = "%s-WHAM_PMF_CENTERZERO"%(desired_key)
    
    ## SAVING FIGURE
    plot_funcs.store_figure(fig = fig, 
                 path = os.path.join(IMAGE_LOC,
                                     figure_name), 
                 fig_extension = 'png', 
                 save_fig=True,)
    


    #%%

    

    #%% PLOTTING SAMPLING TIMES (FORWARD)

    ## LOOPING THROUGH EACH KEY
    for each_key in wham_folder_dict:
        ## DEFINING FOLDER
        wham_folder = wham_folder_dict[each_key]

        ## EXTRACTING WHAM INFO
        wham_storage, sampling_time = main_sampling_time(wham_folder = wham_folder,
                                                         fe_file_prefix = "umbrella_free_energy-forward",
                                                         sort_values = "end",)
        
        ## PLOTTING WHAM STORAGE        
        fig, ax = sampling_time.plot_sampling_time(wham_storage = wham_storage,
                                                  fig_size = (9.55, 8)) # 6.5
        
        ## FIGURE NAME
        figure_name = "%s-WHAM_SAMPLING_FORWARD-"%(desired_key) + os.path.basename(os.path.dirname(wham_folder))
    
        ## SAVING FIGURE
        plot_funcs.store_figure(fig = fig, 
                     path = os.path.join(IMAGE_LOC,
                                         figure_name), 
                     fig_extension = 'png', 
                     save_fig=True,)
        
        ## GETTING REVERSE
        
    #%% REVERSE SAMPLING TIMES
        
    ## LOOPING THROUGH EACH KEY
    for each_key in wham_folder_dict:
        
        ## DEFINING FOLDER
        wham_folder = wham_folder_dict[each_key]
        
        ## EXTRACTING WHAM INFO
        wham_storage, sampling_time = main_sampling_time(wham_folder = wham_folder,
                                                         fe_file_prefix = "umbrella_free_energy-rev",
                                                         sort_values = "begin",)
        
        ## PLOTTING WHAM STORAGE        
        fig, ax = sampling_time.plot_sampling_time(wham_storage = wham_storage,
                                                  fig_size = (9.55, 8))
        
        ## FIGURE NAME
        figure_name = "%s-WHAM_SAMPLING_REVERSE-"%(desired_key) + os.path.basename(os.path.dirname(wham_folder))
    
        ## SAVING FIGURE
        plot_funcs.store_figure(fig = fig, 
                     path = os.path.join(IMAGE_LOC,
                                         figure_name), 
                     fig_extension = 'png', 
                     save_fig=True,)
        
        
    
    #%% READING HISTOGRAM INFORMATION
    
    ## IMPORTING READ COVAR CODE
    from MDDescriptors.core.import_tools import read_plumed_covar
    
    ## DEFINING FOLDERS
    contacts = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65,
                70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125,
                130, 135, 140, 145, 150,]
    
    contacts = [0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 22.5, 25, 27.5, 30, 32.5, 35, 37.5, 
                40, 42.5, 45, 50, 55, 60, 65,
                70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125,
                130, 135, 140, 145, 150,]
    
    ## DEFINING CONTACTS ARRAY
    contacts = np.arange(0, 102.5, 2.5)
    contacts = np.append( contacts, 4.0 )
    # 160, 170, 180, 190, 200, 210, 220, 230
    
    ## DEFINING TIME
    time_starting_from = 5000

    ## LOOPING
    for each_key in wham_folder_dict:
        ## IF NONE, IT'LL LOOK FOR ALL OF THEM
        contacts = None
        
        ## DEFINING FOLDER
        wham_folder = wham_folder_dict[each_key]
    
        ## DEFINING SIMULATION PATH
        sim_folder = os.path.dirname(wham_folder)
        
        ## DEFINING RELATIVE PATH TO SIM
        relative_sim_path = "4_simulations"
        
        ## DEFINING PATH TO SIM
        path_to_sim = os.path.join(sim_folder,
                                   relative_sim_path)
        
        if contacts is None:
            contacts = [ float(os.path.basename(each)) for each in glob.glob(path_to_sim + "/*") ]
            delta = 5
            histogram_range = (np.min(contacts)-delta, np.max(contacts)+delta)
        else:
            histogram_range = (-5, 105)
            
        ## CHANGING EACH BASED ON INTEGER OR FLOAT
        contacts = [ "%d"%(each_contant) if each_contant.is_integer() is True else "%.1f"%(each_contant) for each_contant in contacts ]

        ## DEFINIG COVAR FILE
        covar_file = "nplm_prod_COVAR.dat"
        
        ## GENERATING HISTOGRAM
        hist = plot_covar_histogram()
        
        ## LOADING DATA
        covar_df = hist.load_covar(path_to_sim = path_to_sim,
                                   contacts = contacts,
                                   covar_file = "nplm_prod_COVAR.dat",
                                   )
        ## GENERATING PLOT
        fig, ax = hist.plot(covar_df = covar_df,
                            histogram_range = histogram_range,
                            bin_width = 1, # 2
                            fig_size = (9.55, 6.5),
                            want_legend = False, 
                            time_starting_from = time_starting_from)
        
        ## MODIFYING LEGEND
        # Removing legend
        # ax.get_legend().remove()
        # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=3)
        # #%%
        ## FIGURE NAME
        figure_name = "%s-WHAM_HIST-"%(desired_key) + os.path.basename(os.path.dirname(wham_folder))
    
        ## SAVING FIGURE
        plot_funcs.store_figure(fig = fig, 
                     path = os.path.join(IMAGE_LOC,
                                         figure_name), 
                     fig_extension = 'png', 
                     save_fig=True,)

    
    #%%
    
    ## PLOTTING FREE ENERGIES

    
    ## GETTING COORD
    coord = []
    fe = []
    prob = []


    
    #%%
    
    neg_beta = -1.0/kT 
    
    ### METHOD 1: REID/BRAD'S METHOD
    
    ## GETTING PROBABILITIES
    prob = np.exp(df['free_energy'] * neg_beta)
    
    ## NORMALIZING
    norm_prob = prob/ prob.sum()
    
    ## GETTING FREE ENERGY
    fe = -1.0 * kT * np.log(norm_prob) * kcal2kJ
    
    ### METHOD 2: USING SIMPLE CONVERSION
    
    ## FINDING FREE ENERGY IN KJ
    fe_kj = df['free_energy']*kcal2kJ
    
    print(fe - fe.min())
    print(fe_kj)
    
    ''' OUTPUT RESULTS
    METHOD 1: Using Reid/Brads's method:
        0       0.000000
        1      10.400181
        2      19.315549
        
    METHOD 2: Using simple conversion:
        0       0.000000
        1      10.400181
        2      19.315549
        
    Conclusion: the two methods give you the same results
    '''
    
    #%%
    
    ## GETTING KT RESULTS
    # METHOD 1
    fe_ke_method_1 = -1.0 * np.log(norm_prob)
    # METHOD 2
    fe_ke_method_2 = df['free_energy']/kT
                        
    ## PRINTING
    print(fe_ke_method_1 - fe_ke_method_1.min())
    print(fe_ke_method_2)
    
    ''' OUTPUT RESULTS
    METHOD 1: Using Reid/Brads's method:
        0       0.000000
        1       4.169585
        2       7.743886
        
    METHOD 2: Using simple conversion:
        0       0.000000
        1       4.169585
        2       7.743886
    '''
    
    
    #%%
    
    ## COMPUTING SUM
    prob_sum = df['probability'].sum()
    
    ## GETTING PROBABILITY
    prob_norm = df['probability'] / prob_sum
    
    
    
    #%%
    

    
    ## FIGURE NAME
    figure_name = "WHAM-" + os.path.basename(os.path.dirname(wham_folder))

    ## SETTING AXIS
    plot_funcs.store_figure(fig = fig, 
                 path = os.path.join(IMAGE_LOC,
                                     figure_name), 
                 fig_extension = 'png', 
                 save_fig=True,)