# -*- coding: utf-8 -*-
##############################################################################
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# Modified by Alex Chew (05/26/2020)
##############################################################################​

from __future__ import print_function, division
from optparse import OptionParser # Used to allow commands within command line
import numpy as np
import os

##############################################################################
# Classes and Functions
##############################################################################
class indus_data:
    '''
    Class containing discrete and smeared INDUS data.
    '''
    def __init__( self, wd, infile, outfile, start = 1000, end = -1 ):
        # collect inputs
        self.wd = wd
        self.infile = infile
        self.outfile = outfile
        self.start = start
        self.end = end
        
    def load_csv( self, filename ):
        '''
        Loads a .csv file and returns the data as an np.array
        '''
        ## DEFINING PATH TO THE FILE
        fullPath2File = os.path.join(self.wd , filename)
        ## READING THE FILE
        with open( fullPath2File, 'r' ) as outputfile:
            fileData = outputfile.readlines()
    
        ## REMOVE ALL COMMENTS
        data = [ line for line in fileData if '#' not in line ]
        ## CLEANING THE DATA
        data = [ n.strip( '\n' ) for n in data ]
        ## SPLITTING THE DATA
        data = [ n.split( ) for n in data ]
        data = np.array( [ [ float( n ) for n in line if n != '' ] for line in data ] )
        ## CHECKING TIMES
        if self.end > 0:
            ndx = int( self.end / ( data[1,0] - data[0,0] ) )
            data = data[:ndx,:]
        
        if self.start > 0:
            ndx = int( self.start / ( data[1,0] - data[0,0] ) )
            data = data[ndx:,:]
        
        return data
    
    def write_file( self ):
        '''
        Write out new truncated .csv file
        '''
        data = self.load_csv( self.infile )
        
        # write out mu data profile
        fullPath2File = os.path.join( self.wd , self.outfile)
        
        print( '  Outputting file to {:s}'.format( fullPath2File ) )
        with open( fullPath2File, 'w+' ) as outfile:
            for line in data:
                # The following outputs the entire line
                outfile.write( '%s\n'%(' '.join(['{:0.6f}'.format(each_float) for each_float in line] )  ) )
                # outfile.write( '{:0.6f} {:0.6f} {:0.6f}\n'.format( line[0], line[1], line[2] ) )

        return
#%%                
##############################################################################
# Execute script
##############################################################################

if __name__ == "__main__":    
    
    testing = 'False'
    if ( testing == 'True' ):
        wd = r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200615-US_PLUMED_rerun_with_10_spring/UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/4_simulations/0.0"
        
        # r'R:/simulations/test_indus/n11_plumed_indus_1ps/indus_z38.95/umbrella/indus_40.0/'
        inname = "nplm_prod_COVAR.dat"
        # 'plumed.out'
        outname = "nplm_prod_COVAR-last_10000.000_ps.dat"
        start = 10000.000950
        end = 20000.000950 
        
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-w', '--wd', dest = 'wd', action = 'store', type = 'string', help = 'working directory', default = '.' )
        parser.add_option( '-f', '--in', dest = 'inname', action = 'store', type = 'string', help = 'input file', default = '.' )
        parser.add_option( '-o', '--out', dest = 'outname', action = 'store', type = 'string', help = 'output file', default = '.' )
        parser.add_option( '-b', '--begin', dest = 'start', action = 'store', type = 'float', help = 'Equilibration time', default = '1000' )
        parser.add_option( '-e', '--end', dest = 'end', action = 'store', type = 'float', help = 'Last frame considered', default = '-1' )
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        # assign input parameters
        wd = options.wd
        inname = options.inname
        outname = options.outname
        start = options.start
        end = options.end
    
    ## GETTING DATA
    indus_data = indus_data( wd = wd, 
                             infile = inname, 
                             outfile = outname, 
                             start = start, 
                             end = end )
    data = indus_data.load_csv( inname )
    indus_data.write_file()