# -*- coding: utf-8 -*-
"""
prep_itp_file_.py
The purpose of this function is to find the atomtypes and copy them to an itp file. 
The idea is that we would like to prepare an itp file for running alchemical-setup 
tool -- which enables us to run alchemical calculations

    
AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

CREATED ON: 02/21/2019
"""

### MATH FUNCTIONS
import numpy as np

### IMPORTING FUNCTIONS FROM MDBUILDERS
import MDBuilder.core.import_tools as import_tools
from MDBuilder.core.check_tools import check_testing


### DEFINING DICTIONARY FOR DIFFERENT TOPLOGY DEFINITIONS
TOPOLOGY_DEFINITIONS={
        'atomtypes':
            { # e.g. ALG1    13    26.981540    0.000  A  0.356359487256  2.71960
                # label, index
                'type'      : 0,
                'atomnum'   : 1,
                'mass'      : 2,
                'charge'    : 3,
                'ptype'     : 4,
                'sigma'     : 5,
                'epsilon'   : 6,
                    },
        'pairtypes': 
            { # e.g. CP1     C  1  0.347450500075  0.138767581228
                 'atom_type_1': 0,
                 'atom_type_2': 1,
                 'pairtype'   : 2,
                 'sigma'      : 3,
                 'epsilon'    : 4, 
                    },
        'bondtypes':
            { # e.g. NH2      CT1     1   0.14550000    200832.00
                  'atom_type_1': 0,
                  'atom_type_2': 1,
                  'func': 2,
                  'b0': 3,
                  'kb': 4,
                    },
        'angletypes':
            { # e.g. H      NH2      CT1     5   111.000000   418.400000   0.00000000         0.00
                  'atom_type_1': 0,
                  'atom_type_2': 1,
                  'atom_type_3': 2,
                  'func': 3,
                  'theta0': 4,
                  'ktheta': 5,
                  'ub0': 6,
                  'kub': 7,
                    },
        'dihedraltypes':
            { # e.g. NH2      CT1        C        O     9     0.000000     0.000000     1
                  'atom_type_1': 0,
                  'atom_type_2': 1,
                  'atom_type_3': 2,
                  'atom_type_4': 3,
                  'func': 4,
                  'phi0': 5,
                  'kphi': 6,
                  'mult': 7,
                    },

        }

### FUNCTION TO EXTRACT DATASET
def extract_data_based_on_type( clean_itp, desired_type ):
    '''
    The purpose of this function is to take your itp file and the desired type (i.e. bonds) and get the information from it. It assumes your itp file has been cleaned of comments
    INPUTS:
        clean_itp: itp data as a list without comments (semicolons)
        desired_type: Types that you want (i.e. [bonds])
    OUTPUTS:
        DataOfInterest: Data for that type as a list of list
    '''
    # Finding bond index
    IndexOfExtract = clean_itp.index(desired_type)
    
    # Defining the data we are interested in
    DataOfInterest = []
    currentIndexCheck = IndexOfExtract+1 
    # Using while loop to go through the list to see when this thing ends
    while (('[' in clean_itp[currentIndexCheck]) == False and '' != clean_itp[currentIndexCheck]): # -1 Checks if the file ended  and currentIndexCheck <= len(clean_itp)
        # Appending to the data set
        DataOfInterest.append(clean_itp[currentIndexCheck])
        # Incrementing the index
        currentIndexCheck+=1
        ## IF REACHING THE END OF THE FILE, THEN LET'S MAKE SURE THE LAST DATA POINT IS INCLUDED
        if currentIndexCheck >= len(clean_itp)-1:
            ## TRYING TO ADD LAST LINE
            try:
                ## ADD DATA IF WE HAVE REACHED END
                if ( ('[' in clean_itp[currentIndexCheck]) == False and '' != clean_itp[currentIndexCheck] ):
                    DataOfInterest.append(clean_itp[currentIndexCheck])
                ## CASE WHERE WE HAVE NO MORE LINES LEFT!
            except Exception:
                pass
            ## FILE HAS ENDED, TURNING WHILE LOOP OFF
            break                        
        
    # Removing any blanks and then splitting to columns
    DataOfInterest = [ currentLine.split() for currentLine in DataOfInterest if len(currentLine) != 0 ]
    
    return DataOfInterest 

### FUNCTION THAT CLEANS ATOMTYPE EXTRACT
def clean_data_type(data_list):
    '''
    The purpose of this function is to clean the data from 'extract_data_based_on_type'.
    We will clean all commented information. We will also clean all '#' statements.
    INPUTS:
        data_list: [list]
            list of strings of data
    OUTPUTS:
        clean_data_list: [list]
            clean data with comments and hashtags removed
    '''
    ## DEFINING A CLEAN DATA
    clean_data_list=[]
    ## LOOPING FOR EACH LINE
    for each_line in data_list:
        if each_line[0].startswith(';') is not True and each_line[0].startswith('#') is not True:
            clean_data_list.append(each_line)
    return clean_data_list

### FUNCTION THAT TAKES THE DATA LIST AND CONVERTS IT TO SPECIFIC LABELS
def convert_data_type_to_labels( data_lines, data_type ):
    '''
    The purpose of this function is to convert the data type to specific labels
    INPUTS:
        data_lines: [list]
            data from the topology file
        data_type: [dict]
            dictionary of the datatype that you have
            see "TOPOLOGY_DEFINITIONS" variables for example
    OUTPUTS:
        data_labels: [dict]
            dictionary containing list of the information
    '''
    ## CREATING EMPTY DICTIONARY BASED ON THE KEYS OF THE DATA TYPES
    data_labels = { each_data_label: []  for each_data_label in data_type.keys()}
    ## LOOPING THROUGH DATA LINES
    for each_line in data_lines:
        ## STORING EACH OF THE DATA LINES BASED ON THE KEYS
        for each_data_label in data_type.keys():
            data_labels[each_data_label].append(each_line[data_type[each_data_label]])
    return data_labels

### FUNCTION TO FIND INTERSECTION BETWEEN TWO LISTS
def intersection(lst1, lst2): 
    return list(set(lst1) & set(lst2))

### FUNCTION TO REMOVE PREFIX
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  # or whatever
### FUNCTION TO REMOVE PREFIX
def remove_suffix(text, prefix):
    if text.endswith(prefix):
        return text[:len(text)-len(prefix)]
    return text  # or whatever

### FUNCTION TO REMOVE DUPLICATES FROM LIST
def remove_list_duplicates( my_list ):
    ''' This function removes duplicates from list'''
    return list(set(my_list))


###################################################
### CLASS FUNCTION TO READ FFNONBONDED ITP FILE ###
###################################################
class extract_ffnonbonded:
    '''
    The purpose of this function is to extract the ffnonbonded interactions from 
    a force field folder (e.g. ffnonbonded.itp). Here, we will extract the atomtypes, pairtypes, 
    etc. into a comprehensive list. We will ignore all comments.
    INPUTS:
        file_path: [str]
            path to nonbonded force field. Note that we assume this file has 'atomtypes' and 'pairtypes'.
        desired_fields: [str]
            desired fields you want from the file
    OUTPUTS:
        self.file_path: [str]
            path to ff file
        self.topology_definitions: [dict]
            dictionary storing all topology deifnitions
        self.lines: [list]
            extracted files in a form of lines
        self.available_types: [list]
            list of available types
        
    '''
    def __init__(self, file_path, desired_fields=['atomtypes', 'pairtypes'], topology_definitions = TOPOLOGY_DEFINITIONS  ):
        ## STORING VARIABLES
        self.file_path = file_path
        self.topology_definitions = topology_definitions
        ## READING FILE
        self.lines= import_tools.load_and_clean_text_file(self.file_path)
        ## FINDING ALL POSSIBLE EXTRACTION KEYS
        self.available_types = [ eachLine for eachLine in self.lines if '[' in eachLine ]
        ## CONVERTING DESIRE FIELDS
        desired_fields_with_brackets =['[ ' + each_field + ' ]'   for each_field in desired_fields]
        ## COMPARING AND SEE ALL COMMON FIELDS
        self.desired_fields_with_brackets = intersection(self.available_types, desired_fields_with_brackets )
        self.desired_fields_no_brackets =[ remove_suffix( remove_prefix(each_field,'[ '), ' ]' ) for each_field in self.desired_fields_with_brackets]
        ## LOOPING AND CREATING DESIRED TYPES
        # self.desired_fields_with_brackets =['[ ' + each_field + ' ]'   for each_field in self.desired_fields]
        ## DEFINING EMPTY STORAGE
        self.extract_data = {}
        ## LOOPING THROUGH DESIRED FIELDS
        for idx, each_field in enumerate(self.desired_fields_with_brackets):
            ## DEFINING DESIRED TYPE
            desired_type_no_brackets = self.desired_fields_no_brackets[idx]
            ## EXTRACTING THE DATA BASED ON TYPE
            extract = extract_data_based_on_type( clean_itp = self.lines,
                                                 desired_type = each_field )
            ## CLEANING UP THE DATA
            extract_clean = clean_data_type( extract )
            
            ## IDENTIFYING LABELS
            labels = convert_data_type_to_labels( data_lines = extract_clean,
                                                 data_type = TOPOLOGY_DEFINITIONS[desired_type_no_brackets])
            ## STORING
            self.extract_data[each_field]=labels

    
### FUNCTION TO FIND ALL ATOM TYPES GIVEN FORCE FIELD INFORMATION
def find_atom_types_from_ff( atomtypes, ffnonbonded ):
    '''
    The purpose of this function is to find all atom types from a force field
    INPUTS:
        atomtypes: [list]
            list of atomtypes that you want
            e.g. ['CG331',
                  'CG311',...]
        ffnonbonded: [class]
            ffnonbonded class extracted from 'extract_ffnonbonded'
    OUTPUTS:
        atomtypes_ff: [list]
            list of atomtype parameters
        
    '''
    ## DEFINING FORCE FIELD ATOMTYPES
    ff_atomtypes = ffnonbonded.extract_data['[ atomtypes ]']['type']
    
    ## STORING INDEX INFORMATION
    atom_type_index = []
    
    ## LOOPING THROUGH EACH ATOM TYPE
    for each_atom_type in atomtypes:
        ## SEEING IF WITHIN LIST
        current_type_index = ff_atomtypes.index(each_atom_type)
        ## APPENDING
        atom_type_index.append(current_type_index)
        
    return atom_type_index
    ## SEARCH WITHIN THE LIST

### FUNCTION TO CONVERT ATOM TYPE INDEX TO FORCE FIELD INFORMATION
def convert_atom_type_to_ff_info( atom_type_index, ffnonbonded ):
    '''
    The purpose of this function is to convert the atom type to forcefield 
    information. We use the forcefield information class and the atom types 
    found for specific types.
    INPUTS:
        atom_type_index: [list]
            list of atomtype parameters
        ffnonbonded: [class]
            ffnonbonded class extracted from 'extract_ffnonbonded'
    OUTPUTS:
        atom_type_list: [list]
            list of strings that you need for atomtypes
    '''
    ## DEFINING FORCE FIELD DEFINIITIONS
    force_field_definitions = ffnonbonded.topology_definitions['atomtypes']
    '''
    returns information about the topology, e.g.
        {'atomtypes': {'atomnum': 1,
          'charge': 3,...
    '''
    ## FINDING LENGTH OF DEFINITION
    n_definitions = len(force_field_definitions)
    
    ## DEFINING EMPTY LIST
    atom_type_list=[]; copy_atom_list = [ [] for each_def in range(n_definitions)]
    
    ## LOOPING THROUGH EACH ATOM TYPE INDEX
    for each_atom_index in atom_type_index:
        ## CREATING EMPTY LIST FOR EACH DEFINITION
        current_atom_list = copy_atom_list[:]
        ## LOOPING THROUGH EACH DEFINITION
        for each_definition in force_field_definitions:
            ## DEFINING INDEX
            def_index = force_field_definitions[each_definition]
            ## STORING INFORMATION
            current_atom_list[def_index] = ffnonbonded.extract_data['[ atomtypes ]'][each_definition][each_atom_index]
        ## STORING INTO ATOM LIST
        atom_type_list.append(current_atom_list)
    
    return atom_type_list
    
### FUNCTION THAT READS LINES AND PRINTS THEM
def write_atom_type_list_to_file( itp_file_path, output_itp_file_path, atom_type_list  ):
    '''
    The purpose of this function is to write the atom type list to file. Here, 
    we copy over the itp file, then add the atomtypes
    INPUTS:
        itp_file_path: [str]
            input itp file path
        output_itp_file_path: [str]
            output itp file path
        atom_type_list: [list]
            list of strings that you need for atomtypes
    OUTPUTS:
        This will output a file to output itp file        
    '''
    ## READING ITP FILE
    with open(itp_file_path,'r') as f:
        input_itp_lines = f.readlines()
        if input_itp_lines[0][0]==';':
            ## SKIPPING FIRST LINE
            input_itp_lines = input_itp_lines[1:]
        
    ## WRITING TO FILE
    with open(output_itp_file_path, 'w+') as f:
        ## WRITING THE ITP FILE
        for each_line in input_itp_lines:
            f.write( each_line )
        
    ## APPENDING TO FILE
    with open(output_itp_file_path, 'r+') as f:
        ## STORING CONTENT
        content = f.read()
        ## SEEKING FIRST LINE
        f.seek(0,0)
        ## WRITING PARAMETER FILE
        f.write("%s\n"%( "[ atomtypes ]" )  )
        f.write("%s\n"%( "; type atnum mass charge ptype sigma epsilon" )  )
        ## WRITING EACH WITH A SPACE
        for each_atom_type in atom_type_list:
            f.write("%s\n"%( '\t'.join(each_atom_type)   ) )
        f.write(content)
    return

### MAIN FUNCTION
def main( itp_file_path, force_field_nonbonded, output_itp_file_path ):
    '''
    This main function takes the force field itp file and force field 
    information to extract the atom parameters.
    INPUTS:
        itp_file_path: [str]
            input itp file path
        force_field_nonbonded: [str]
            force field nonbonded itp file with atomtypes and pairtypes
        output_itp_file_path: [str]
            output itp file path
    OUTPUTS:
        writes an output ile 
    '''
    
    ## IMPORTING ITP FILE
    molecule_itp = import_tools.extract_itp( itp_file_path )
    
    ## FINDING ATOM TYPES
    atom_types = molecule_itp.atom_type
    
    ## REMOVING DUPLICATES
    atom_types = remove_list_duplicates(atom_types)
    
    ## READING FORCE FIELD FILE
    force_field_info = extract_ffnonbonded( file_path =  force_field_nonbonded )
    
    ## FINDING ATOM INDEX OF FORCE FIELD INFORMATION
    atom_type_index = find_atom_types_from_ff( atomtypes = atom_types, 
                                               ffnonbonded = force_field_info  )
    
    ## FINDING THE LIST OF ATOM INFORMATION
    atom_type_list = convert_atom_type_to_ff_info( atom_type_index = atom_type_index, 
                                                   ffnonbonded = force_field_info )
    
    ## WRITING TO FILE
    write_atom_type_list_to_file( itp_file_path = itp_file_path, 
                                  output_itp_file_path = output_itp_file_path, 
                                  atom_type_list = atom_type_list  )
    
    ## CHECKING IF WE NEED TO CORRECT THE ITP FILE
    import_tools.correct_duplicate_type_itp_file( itp_file_path = output_itp_file_path)
    
    return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
            
        ## DEFINING FULL PATH TO ITP FILE
        itp_file_path=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal.itp"
        top_file_path=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal.top"
        gro_file_path=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal.itp"
        ## DEFINING FORCE FIELD FILE
        force_field_nonbonded=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\charmm36-nov2016.ff\ffnonbonded.itp"
        ## DEIING FULL PATH TO OUTPUT ITP
        output_itp_file_path=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal_output.itp"
        
        ## DEFINING FF BONDED INFORMATION
        forcefield_bonded=r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\charmm36-nov2016.ff\ffbonded.itp"
    
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## FORCE FIELD FILE PATH
        parser.add_option('-f', '--ffpath', dest = 'force_field_nonbonded', help = 'Full path to nonbonded force field information', default = '.')
        ## ITP FILE PATH
        parser.add_option('-i', '--inputitp', dest = 'itp_file_path', help = 'Full path to input itp file', default = '.')
        ## DEFINING OUTPUT ITP
        parser.add_option('-o', '--outputitp', dest = 'output_itp_file_path', help = 'Full path to input itp file', default = '.')
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ### DEFINING ARGUMENTS
        force_field_nonbonded = options.force_field_nonbonded
        itp_file_path = options.itp_file_path
        output_itp_file_path = options.output_itp_file_path
        
    ## RUNNING MAIN FUNCTION
    # main( itp_file_path, force_field_nonbonded, output_itp_file_path )
    
    
    import parmed as pmd
    from parmed import gromacs, charmm, amber, unit as u
    ## DEFINING TOP DIRECTORY
    # gromacs.GROMACS_TOPDIR = r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files"
    top = gromacs.GromacsTopologyFile(top_file_path)
    top.write("Testing.top")
    
    #save molecular topology as a class
    # gmx_top=gromacs.GromacsTopologyFile(top_file_path,xyz=gro_file_path)
    #outputs single-file topology from class after doing any edits
    # gmx_top.write(out_path+out_file+'.top')
    
    
    
    
    
    
    
    
    
    #%%
    
    
    
    ## IMPORTING ITP FILE
    molecule_itp = import_tools.extract_itp( itp_file_path )
    #%%
    ##################
    ### ATOM TYPES ###
    ##################
    
    ## FINDING ATOM TYPES
    atom_types = molecule_itp.atom_type
    
    ## REMOVING DUPLICATES
    atom_types = remove_list_duplicates(atom_types)
    
    ## READING FORCE FIELD FILE
    force_field_info = extract_ffnonbonded( file_path =  force_field_nonbonded )
    
    ## FINDING ATOM INDEX OF FORCE FIELD INFORMATION
    atom_type_index = find_atom_types_from_ff( atomtypes = atom_types, 
                                               ffnonbonded = force_field_info  )
    
    ## FINDING THE LIST OF ATOM INFORMATION
    atom_type_list = convert_atom_type_to_ff_info( atom_type_index = atom_type_index, 
                                                   ffnonbonded = force_field_info )
    
    
    ##################
    ### BOND TYPES ###
    ##################
    ## READING FORCE FIELD FILE
    force_field_info_bonded = extract_ffnonbonded( file_path =  forcefield_bonded ,
                                            desired_fields = ['bondtypes','angletypes','dihedraltypes'])
    ## DEFINING BONDS
    bonds = molecule_itp.bonds
    
    ## FINDING ATOM TYPES
    atom_types_from_index = np.array(molecule_itp.atom_type)[molecule_itp.bonds-1] # -1 to go back to python indexing
    
    ## FINDING FORCE FIELD BOND TYPE
    ff_bond_types = np.array([ force_field_info_bonded.extract_data['[ bondtypes ]'][each_atomtype] for each_atomtype in  ['atom_type_1', 'atom_type_2'] ] ).T
    ## LOOKS LIKE: array([['NH2', 'CT1'],
    
    ## SEEING IF EQUIVALENCE ACROSS THE ROWS
    equivalence =  np.all(np.isin(ff_bond_types, atom_types_from_index),axis=1)
    # RETURNS ROW VECTOR SAME LENGTH AS NUMBER OF BOND TYPES
    equivalence_index = np.where( equivalence )
    
    ## FINDING BOND TYPE EQUIVALENCE
    equivalence_types = ff_bond_types[equivalence_index]
    
    
    ## USING ARGSORT
    atom_types_sort_index = np.argsort(atom_types_from_index, kind='stable')
    atom_types_sort_index_reverse = np.argsort(atom_types_sort_index)
    # To use argsort: np.take_along_axis(atom_types_from_index, atom_types_sort_index, axis=0)
    ## USING ARG SORT 
    equiv_sort_index = np.argsort(equivalence_types, kind='stable')
    
    
    ## NEEDTODO: Fix up this sorting mess
    ## NEED TO DO: Make this into a quick function -- then eliminate all this junk
    
    
    ## FINDING MAPPING BETWEEN FORCE FIELD AND TYPES
    mapping_ff_index = [(equivalence_types[i] in atom_types_from_index[i]) for i in range(len(equivalence_types))] # [ for each_atom in atom_types_from_index ]
    
    ## DEFINING BONDTYPES TO IGNORE
    ignore_bondtypes = [ each_key for each_key in TOPOLOGY_DEFINITIONS['bondtypes'].keys() if 'atom_type' in each_key ]
    
    ## FINDING ALL OTHER KEYS
    other_keys = np.array([ each_key for each_key in TOPOLOGY_DEFINITIONS['bondtypes'].keys() if each_key not in ignore_bondtypes ])
    
    ## FINDING ORDER FROM THE KEYS
    key_order = np.argsort([ TOPOLOGY_DEFINITIONS['bondtypes'][each_key] for each_key in other_keys])
    ## REDEFINING OTHER KEYS
    other_keys = other_keys[key_order]
    
    ## GETTING ALL VALUES
    equivalent_values = np.array([ [  force_field_info_bonded.extract_data['[ bondtypes ]'][each_key][each_index] for each_key in other_keys ] for each_index in equivalence_index[0]  ] )
    
    ## SORTED EQUIVALENT VALUES
    sorted_equivalent_value = np.take_along_axis(equivalent_values, atom_types_sort_index, axis=0)
    
    equivalent_values[equiv_sort_index]
    
    
    
    #np.take_along_axis( np.take_along_axis( np.take_along_axis(equivalent_values, equiv_sort_index, axis=0), 
#                                                atom_types_sort_index, axis = 0 ), 
#                                                 atom_types_sort_index_reverse, axis=0 )
    
    ## USING SORTING ALGORITHM TO 
    
    
    ## GETTING ALL THE OTHER VALUES
    # equivalent_values = for each_key in other_keys
    
    # np.where(np.in1d(equivalence_types, atom_types_from_index))
    
    # [ np.where(np.all(equivalence_types == each_type, axis=1))[0]  for each_type in atom_types_from_index ] 
    
    # np.where( equivalence_types == atom_types_from_index)
    
    
    
    ## WRITING TO FILE
    write_atom_type_list_to_file( itp_file_path = itp_file_path, 
                                  output_itp_file_path = output_itp_file_path, 
                                  atom_type_list = atom_type_list  )
    
    ## CHECKING IF WE NEED TO CORRECT THE ITP FILE
    import_tools.correct_duplicate_type_itp_file( itp_file_path = output_itp_file_path)

    #%%
    

