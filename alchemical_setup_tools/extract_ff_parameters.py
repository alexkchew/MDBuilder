# -*- coding: utf-8 -*-
"""
extract_ff_parameters.py
The purpose of this script is to extract forcefield parameters from charmm's itp files. 
We will do this using the parmed commands. These are fantastic tools that can manipulate 
itp files in a quick fashion.

Written by: Alex K. Chew (03/29/2019)


"""

## IMPORING PARMED
import parmed as pmd
from parmed import gromacs, charmm, amber, unit as u

### IMPORTING FUNCTIONS FROM MDBUILDERS
from MDBuilder.core.check_tools import check_testing

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
            
        ## DEFINING FULL PATH TO ITP FILE
        input_top_path = r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal_2.top"
        output_top_path = r"R:\scratch\SideProjectHuber\prep_mixed_solvent\input_files\solutes_alchemical\12-propanediol~propanal\alchemical_setup\itp_files\propanal\propanal_prep.top"
    
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## ITP FILE PATH
        parser.add_option('-i', '--input_top_path', dest = 'input_top_path', help = 'Full path to input top file', default = '.')
        ## DEFINING OUTPUT ITP
        parser.add_option('-o', '--output_top_path', dest = 'output_top_path', help = 'Full path to output top file', default = '.')
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ### DEFINING ARGUMENTS
        input_top_path = options.input_top_path
        output_top_path = options.output_top_path

    ### MAIN FUNCTIONS
    ## LOADING TOPOLOGY
    top = gromacs.GromacsTopologyFile(input_top_path)
    ## WRITING TOPOLOGY
    top.write(output_top_path)
    