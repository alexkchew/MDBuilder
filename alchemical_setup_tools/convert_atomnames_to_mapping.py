# -*- coding: utf-8 -*-
"""
convert_atomnames_to_mapping.py
The purpose of this code is to convert strings to mapping information. 
We ignore all 'DU' atom numbers to generate the correct mapping number. We will 
assume that all values that are not assigned will be destroyed by alchemical-setup.

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

CREATED ON: 02/21/2019

FUNCTIONS:
    convert_atomname_string: converts atomnames strings
    find_all_alphanumeric_in_string: finds all alphanumerical values in a string
    find_all_num_in_string: finds all numbers in a string
    main: main function that runs the mapping extraction
"""
from MDBuilder.core.check_tools import check_testing

## IGNORING ATOM NAMES
IGNORE_ATOM_NAMES=["DU"]

### FUNCTION TO CONVERT ATOM NAMES
def convert_atomname_string( atomname_string ):
    '''    Conversion of atom name to string, e.g. AtomName('C1') -> C1    '''
    ## FINDING INDICES
    first_quote=atomname_string.find('\'')
    last_quote=atomname_string.rfind('\'')
    ## DEFINING ATOM NAME
    atom_name=atomname_string[first_quote+1:last_quote]
    return atom_name

### FUNCTION TO FIND ALL THE ALPHANUMBERIC WITHIN A STRING
def find_all_alphanumeric_in_string( atomname ):
    ''' This function extracts all the numbers from a strong, e.g. C1 --> 1 '''
    ## LOOPING THROUGH EACH LETTER AND SEEING IF IT IS A DIGIT
    atomlabel=str(''.join(filter(str.isalpha, atomname)))
    return atomlabel

### FUNCTION TO FIND ALL THE NUMBERS WITHIN A STRING
def find_all_num_in_string( atomname ):
    ''' This function extracts all the numbers from a strong, e.g. C1 --> 1 '''
    ## LOOPING THROUGH EACH LETTER AND SEEING IF IT IS A DIGIT
    atomnum=int(''.join(filter(str.isdigit, atomname)))
    return atomnum

### MAIN FUNCTION TO CONVERT ATOMNAMES TO MAPPING INFORMATION
def main( string, output_file ):
    '''
    This is the main function to convert atomnames to mapping
    INPUTS:
        string: [str]
            string to convert, e.g. AtomName('C1') <--> AtomName('C7')
        output_file: [str]
            full path to the text file your want to save to
    OUTPUTS:
        This function will convert the string to the desired output. 
        Note: If Atomname is the same as 'DU', we ignore that mapping information.
    '''
    ## SPLITTING BY SPACE
    string_split_space = string.split()
    
    ## FINDING ATOMNAMES
    atom_names =[ convert_atomname_string(each_string) for each_string in 
                     [string_split_space[0],string_split_space[-1]] ]
    
    ## FINDING ALL ATOM LABELS
    atom_labels = [ find_all_alphanumeric_in_string(atomname = each_atomname) 
                                            for each_atomname in atom_names]
    
    ## FINDING ALL ATOM NUMBERS
    atom_numbers = [ find_all_num_in_string(atomname = each_atomname) 
                                            for each_atomname in atom_names]
    print(atom_numbers)
    ## SEEING IF ANY INTERSECTION BETWEEN ATOM NAMES AND IGNORE
    has_ignore_atom_names=bool(set(atom_labels) & set(IGNORE_ATOM_NAMES))
    
    ## RUNNING ONLY IF ATOM NAMES NOT PRESENT
    if has_ignore_atom_names is False:
        ## ADDING STRING TO A FILE
        with open(output_file, "a+") as file:
            ## WRITING IF IT IS NOT FALSE
            file.write("%d %d\n"%(atom_numbers[0],atom_numbers[1]))
    return


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
        
        ## DEFINING INPUTS
        string="AtomName('C1') <--> AtomName('C7')"
        output_file="map.txt"
        
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## PATH INFORMATION
        parser.add_option('-s', '--string', dest = 'string', help = 'string to check', default = '.')
        parser.add_option('-o', '--output', dest = 'output_file', help = 'output path to text file', default = '.')
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## REDEFINING VARIABLES
        string = options.string
        output_file = options.output_file
        
    ## RUNNING MAIN FUNCTION
    main( string = string, output_file = output_file )
    
    
    
    
    