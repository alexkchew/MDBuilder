# -*- coding: utf-8 -*-
"""
check_tools.py
The purpose of this script is to contain scripts for checking.
For example, we would like to check the paths to ensure that we are in the correct location. Here, we are distinguishing two important locations:
    SWARM
    Windows (SPYDER)

CREATED ON: 05/02/2018

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)

FUNCTIONS:
    check_server_path: checks the server path to ensure that you are loading the correct location
    check_testing: checks to see if we need to turn testing on
    check_dir: checks directory -- creates if not there!
    check_file_exist: checks if the directory exists. If not, then raise an error message
"""

### IMPORTING MODULES
import sys
import os
import time

### FUNCTION TO GET THE PATH ON THE SERVER
def check_server_path(path):
    '''
    The purpose of this function is to change the path of analysis based on the current operating system. 
    INPUTS:
        path: [str] Path to analysis directory
    OUTPUTS:
        path: [str] Corrected path (if necessary)
    '''
    ## IMPORTING MODULES
    import getpass
    
    ## CHANGING BACK SLASHES TO FORWARD SLASHES
    backSlash2Forward = path.replace('\\','/')
    
    ## CHECKING PATH IF IN SERVER
    if sys.prefix == '/usr': # At server
        ## CHECKING THE USER NAME
        user_name = getpass.getuser() # Outputs $USER, e.g. akchew, or bdallin
        
        # Changing R: to /home/akchew
        path = backSlash2Forward.replace(r'R:','/home/' + user_name)
    
    ## AT THE MAC
    elif '/Users/' in sys.prefix:
        ## CHANGING R: to /Volumes
        path = backSlash2Forward.replace(r'R:','/Volumes/akchew')
    
    ## OTHERWISE, WE ARE ON PC -- NO CHANGES
    
    return path

### FUNCTION TO SEE IF TESTING SHOULD BE TURNED ON
def check_testing():
    '''
    The purpose of this function is to turn on testing if on SPYDER
    INPUTS:
        void
    OUTPUTS:
        True or False depending if you are on the server
    '''
    ## CHECKING PATH IF IN SERVER
    if sys.prefix == '/usr' or sys.prefix.startswith("/home"): # At server
        testing = False
    else:
        print("*** TESTING MODE IS ON ***")
        testing = True
    return testing

### FUNCTION TO CREATE DIRECTORIES
def check_dir(directory):
    '''
    This function checks if the directory exists. If not, it will create one.
    INPUTS:
        directory: directory path
    OUTPUTS:
        none, a directory will be created
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)
    return

### FUNCTION TO CHECK IF DIRECTORY EXISTS
def check_file_exist( path ):
    '''
    The purpose of this function is to check if a directory exists. If not, this script will raise a system error. This script is useful for cases when you need a directory to exist!
    INPUTS:
        path: [str]
            full path to your directory        
    OUTPUTS:
        Nothing if your directory exists. If not, this function will pull up an error message
    '''
    if not os.path.exists(path):
        print("ERROR! %s does not exist!"%(path) )
        print("Stopping here to prevent further errors ...")
        time.sleep(5)
        sys.exit()
    return

