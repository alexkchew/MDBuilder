# -*- coding: utf-8 -*-
"""
count_gro_residues.py
The purpose of this function is to count the number of unique residues. The idea 
here is to update topology files given a correct count of gro residues.

INPUTS:
    - input gro file
OUTPUTS:
    - summary gro file with the number of residues
    
CREATED ON: 09/03/2019

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
"""
## KEY MODULES
import os
import numpy as np
from optparse import OptionParser # Used to allow commands within command line

## CUSTOM MODULES
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
## IMPORTING TOOLS
from MDBuilder.core.import_tools import extract_gro

### FUNCTION TO GET UNIQUE RESIDUE NAMES AND COUNTS FROM GRO FILE
def gro_get_unique_residues(gro_file):
    '''
    The puprose of this function is to get the unique residues and their 
    counts from a gro file.
    INPUT:
        gro_file: [object, gro file]
            gro file
    OUTPUTS:
        unique_residues_names: [list]
            list of unique residue names
        counts: [list]
            counts of each residue
    '''
    ## DEFINING UNIQUE RESIDUE INDEX
    unique_residue_index = gro_file.unique_resid
    
    ## LOOPING TO GET RESIDUE INDEX
    matching_residue_names = [ gro_file.ResidueName[gro_file.ResidueNum.index(each_index)] for each_index in unique_residue_index ]
    
    ## FINDING UNIQUE RESIDUE NAME
    unique_residues_names, counts = np.unique(matching_residue_names, return_counts = True)
    
    return unique_residues_names, counts

## FUNCTION TO PRINT RESIDUE NAMES
def print_residue_summary_file( summary_path,
                                unique_residues_names,
                                counts,
                                verbose = False,
                                ):
    '''
    The purpose of this function is to print a residue summary file. It should look like 
    the following:
        AUNP 1
        COO 1
        MET 3820
        SOL 6883
    where the first column is the residue name and second column is the total number of residues.
    
    INPUTS:
        summary_path: [str]
            path to summary file
        unique_residues_names: [list]
            list of unique residue names
        counts: [list]
            counts of each residue
    '''

    ## PRINTING
    if verbose is True:
        print("Writing residue summary file to %s"%(summary_path))
    ## PRINTING THE FILE
    with open(summary_path, 'w') as f:
        ## LOOPING THROUGH EACH
        for idx, residue_name in enumerate(unique_residues_names):
            ## PRINTING INTO SUMMARY
            f.write("%s %d\n"%(residue_name, counts[idx]  ) )
    return


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    # Turn test on or off
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    ## SEEING IF TESTING IS TRUE
    if testing == True:
        ## DEFINING INPUT DIR PATH
        input_dir=r"R:/scratch/nanoparticle_project/simulations/190903-Most_likely_NP_mixed_solvents/Mostlikelynp_EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1_frame_10000"
        ## DEFINING INPUT FILE
        input_gro = r"sam.gro"
        ## DEFINING SUMMARY FILE
        summary_file="sam_gro.residues"
        ## DEFINING INPUT GRO PATH
        input_gro_path = os.path.join(input_dir, input_gro )
        ## DEFINING OUTPUT 
        summary_path = os.path.join(input_dir, summary_file)
    else:
        ### ON COMMAND LINE
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ## INPUT GRO FILE
        parser.add_option("--igro", dest="input_gro_path", action="store", type="string", help="Input gro file path", default=".")
        ## SUMMARY FILE
        parser.add_option("--sum", dest="summary_path", action="store", type="string", help="Input summary file path", default=".")
        
        ## COMBINING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## DEFINING ARGUMENTS
        input_gro_path = options.input_gro_path
        summary_path = options.summary_path
    
    ## EXTRACTING GRO FILES
    gro_file = extract_gro( gro_file_path = input_gro_path,  
                            verbose = False )
    
    ## FINDING UNIQUE RESIDUE NAMES AND COUNTS    
    unique_residues_names, counts = gro_get_unique_residues( gro_file = gro_file)
    
    ## PRINTING SUMMARY FILE
    print_residue_summary_file( summary_path = summary_path,
                                unique_residues_names = unique_residues_names,
                                counts = counts,
                                verbose = True,
                                )
    

    
    
    