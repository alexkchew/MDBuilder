#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
fix_residues_gro_top.py
The purpose of this function is to fix gro and topology files for cases when you are combining a lot of gro files together. 
For example, suppose you have a lipid membrane you want to combine with the nanoparticle -- you can do this in GROMACS, 
but this program does not supply a way to make continuous GRO / top file.
The idea here is as follows:
    - You want a continuous solvent (required for the SETTLES algorithm to work correctly!)
    - You want to also replace some solvent molecule residue names
        - Highly relevant for CHARMM36 forcefield, where they renamed 'SOL' to 'TIP3' .... (how ridiculous!)
        
CREATED ON: 11/04/2018

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    - input directory
    - gro file
    - topology file
    
OUTPUTS:
    - updated gro file with residues that are rearranged
    - updated topology files with the corrected residues --- all duplicates will be added!

ALGORITHM:
    - Read gro and topology file
    - Find unique order of the residues
    - If residue replacement is required, search and replace
    - Reorder gro and topology according to the number of residues that are in it
    - Using extraction tools, print the gro and topology file
    
UPDATES:
    - 20200609 - Updated code to allow for resordering -- found issue where sorting was causing errors    
    Use "--resorder" flag to specify order you want. If you are missing residues, the code will automatically add it in. 
"""

## SYSTEM TOOLS
import sys
import os
import numpy as np

## CHECK PATH FUNCTION
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

## IMPORTING TOOLS
from MDBuilder.core.import_tools import extract_gro, extract_top

## EXPORTING TOOLS
from MDBuilder.core.export_tools import print_gro_file

## PARSER OPTIONS
from optparse import OptionParser # Used to allow commands within command line
from MDBuilder.builder.ligand_builder import get_ligand_args

## IMPORTING TIME
import time

## CORRECTING RESIDUE NUMBERS FUNCTION
from MDBuilder.math.remove_atoms import correct_residue_numbers

### FUNCTION TO GET LIGAND ARGUMENTS FROM OPTION PARSER
def get_list_of_list_attr(option, opt_str, value, parser):
    value.split(',')
    ## DEFINING UPDATED_VALUE
    updated_value = value
    if '|' in value:
        updated_value.split('|')
    ## SEEING IF VALUE IS A LIST
    if type(updated_value) == list:
        ## LOOPING THROUGH EACH AND SPLITTING
        updated_value = [each_item.split(',') for each_item in updated_value]
    else:
        updated_value = [updated_value.split(',')]    
    setattr(parser.values, option.dest, updated_value )
    return

### FUNCTION TO EXTRACT ALL ATOM NAMES FROM A GRO FILE GIVEN A RESIDUE NAME
def extract_gro_atom_names_given_residue_name( gro_file, residue_name ):
    '''
    The purpose of this function is to extract all atom names for a given residue name
    INPUTS:
        gro_file: [object]
            gro file as an object from extract_gro function
        residue_name: [str]
            name of the residue you are interested in
    OUTPUTS:
        atom_name: [list]
            atom name as a list of strings
    '''
    ## DEFINING RESIDUE NAMES IDX
    idx_residue_name = [ idx for idx,each_residue in enumerate(gro_file.ResidueName) if each_residue == residue_name ]
    
    ## CHECKING TO SEE IF RESIDUE IS THERE
    if len(idx_residue_name) == 0:
        print("Error! %s is not defined in your gro file. Please double check!"%(residue_name) )
        sys.exit()
    
    ## FINDING FIRST RESIDUE NUMBER
    residue_num = [ gro_file.ResidueNum[each_residue_index] for each_residue_index in idx_residue_name ][0]
    
    ## FINDING ATOM NAMES
    atom_names = [ gro_file.AtomName[each_idx] for each_idx in idx_residue_name if gro_file.ResidueNum[each_idx]==residue_num  ]
    
    return atom_names

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    # Turn test on or off
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    if testing == True:
    
        ### DEFINING INPUTS
        input_dir=r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200608-Pulling_other_ligs/NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT004_1"
        input_gro="nplm_1_top_with_lipids.gro"
        input_top="nplm.top"
        
        ## DEFINING CORRECTIONS (if any)
        residue_rename = [ 
                            ['TIP3', 'SOL'], # Renaming TIP3 to sol
                            ]
        
        ## DEFINING DESIRED COMBINED RESIDUES -- ensures that residues are merged together
        combined_residues = [ 
                                # Res1, Res2, .... molecule name
                                ['RO4', 'AUNP', 'sam']
                ]
        
        ## DEFINING OUTPUTS
        output_gro="nplm.gro"
        output_top="combined_fixed.top"
        
        ## DEFINING DESIRED ORDER
        desired_res_order = ['RO4', 'AUNP', 'DOPC', 'SOL', ]
        
    else:
        ### ON COMMAND LINE
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ## DEFINING INPUT FOLDER
        parser.add_option("--ifold", dest="input_dir", action="store", type="string", help="Input directory", default=".")
        ## DEFINING INPUT NAMES
        parser.add_option("--igro", dest="input_gro", action="store", type="string", help="Input gro file", default=".")
        parser.add_option("--itop", dest="input_top", action="store", type="string", help="Input top file", default=".")
        ## DEFINING OUTPUT NAMES
        parser.add_option("--ogro", dest="output_gro", action="store", type="string", help="Output gro file", default=".")
        parser.add_option("--otop", dest="output_top", action="store", type="string", help="Output top file", default=".")
        ## RESIDUE RENAMING
        parser.add_option("--resrename", dest="residue_rename", action="callback", type="string", callback=get_list_of_list_attr,
                  help="Residue rename, should be like: TIP3,SOL|XXX,YYY| <-- No spacing. | indicates a new list", default=".")
        ## COMBINED RESIDUE NAMES
        parser.add_option("--combres", dest="combined_residues", action="callback", type="string", callback=get_list_of_list_attr,
                  help="Combined residues, should be like: RO1,AUNP,sam|... <-- No spacing. | indicates a new list", default=".")
        ## COMBINED RESIDUE NAMES
        parser.add_option("--resorder", dest="desired_res_order", action="callback", type="string", callback=get_ligand_args,
                  help="Desired residue order based on GRO file -- should be separated by commas, e.g. RO1,AUNP,DOPC,SOL <-- No spacing. | indicates a new list", default=[])        
        
        
        ## COMBINING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## DEFINING ARGUMENTS
        input_dir = options.input_dir
        input_gro = options.input_gro
        input_top = options.input_top
        
        ## DEFINING OUTPUTS
        output_gro = options.output_gro
        output_top = options.output_top
        ## RESIDUE RENAME
        residue_rename = options.residue_rename
        combined_residues = options.combined_residues
        
        ## CORRECTING FOR DEFAULTS
        if residue_rename == ".":
            residue_rename = [[]]
        if combined_residues == ".":
            combined_residues = [[]]
        
        ## STORING RESORDER
        desired_res_order = options.desired_res_order
        
    ## PRINTING
    print("Residue renaming")
    print(residue_rename)
    print("Combining residues:")
    print(combined_residues)
    print("Desired residue order")
    print(desired_res_order)
    
    #%%
    
    ###################
    ### MAIN SCRIPT ###
    ###################
    
    ## DEFINING OUTPUT DIR
    output_folder = input_dir # Same output as input directory
    output_top_path = check_server_path( os.path.join( output_folder, output_top)  )
    
    #%%
    ## DEFINING FULL PATH
    gro_file_path = check_server_path( os.path.join(input_dir, input_gro) )
    top_file_path = check_server_path( os.path.join(input_dir, input_top) )
    
    ## EXTRACTING GRO FILES
    gro_file = extract_gro( gro_file_path )
    
    ## EXTRACTING TOPOLOGY
    top_file = extract_top( top_file_path )
    
    
    #%%
    
    ## FINDING ALL UNIQUE RESIDUES
    gro_unique_names = list(set(gro_file.ResidueName))
    
    ## CHECKING TO SEE IF RESIDUE NAMES NEED TO BE REPLACED
    if residue_rename != [[]]:
        ## LOOPING TO SEE IF AVAILABLE
        renaming_index = [ idx for idx, each_renaming in enumerate(residue_rename) if each_renaming[0] in gro_unique_names] # List of renaming
    else:
        renaming_index = []
    
    ## CREATING COPY OF UNIQUE NAMES
    gro_unique_names_updated = gro_unique_names[:]
    
    ## CONVERTING ATOM NAMES TO NUMPY
    atom_names = np.array(gro_file.AtomName)
    
    ## CONVERTING RESIDUE NAMES TO NUMPY
    residue_names = np.array(gro_file.ResidueName)
    
    ## IF RENAMING INDEX IS AVAILABLE, REPLACE
    if len(renaming_index) > 0:
        ## LOOPING THROUGH EACH INDEX
        for each_renaming in renaming_index:

            ## FINDING PREVIOUS AND NEW RENAMING NOMENCLATURE
            new_resname, old_resname = residue_rename[each_renaming][1], residue_rename[each_renaming][0]
            ## PRINTING
            print("Renaming residue name %s to %s"%(old_resname, new_resname) )
            ## FINDING ALL LOCATIONS FOR OLD RESIDUE NAME
            resname_locations = np.where( residue_names == old_resname )

            ## RENAMING
            residue_names[resname_locations] = new_resname
            ## REMOVING OLD NAME
            gro_unique_names_updated.remove( old_resname )
            
            ## FINDING ALL ATOM NAMES
            new_atomname = extract_gro_atom_names_given_residue_name( gro_file = gro_file, residue_name = new_resname)
            old_atomname = extract_gro_atom_names_given_residue_name( gro_file = gro_file, residue_name = old_resname)
            
            ## MAKING SURE THAT THE LENGTHS ARE CORRECT
            if len(new_atomname) != len(old_atomname):
                print("ERROR! Old and new residue have different lengths!")
                print("Length of new residue %s: %d"%(new_resname, len(new_atomname) )  )
                print("Length of old residue %s: %d"%(old_resname, len(old_atomname) )  )
                print("The differences in length may cause significant errors! Stopping here, pausing so you can see this message!")
                print("If atom names are significantly different, then we will have a big issue later where atom names cannot be identified!")
                time.sleep(5)
                sys.exit()

            ## CORRECTING FOR ATOM NAMES
            for idx, each_old_atom_name in enumerate(old_atomname):
                ## ASSUMING THE ATOM INDICES ARE MORE OR LESS THE SAME
                each_new_atom_name = new_atomname[idx]
                ## FINDING ALL ATOM POSITIONS
                idx_atom_names = np.where(atom_names == each_old_atom_name)
                ## REPLACING
                atom_names[idx_atom_names] = each_new_atom_name
    
    ## MAKING A LIST OF RESIDUE NAME ORDER
    ## CREATING EMPTY LIST
    residue_order = []
    ## CREATING EMPTY LIST FOR TOPOLOGY
    residue_order_top = []
    ## DEFINING A LIST ON TOPOLOGY TO ENSURE THAT RESIDUES ARE NOT REPLICATED
    combined_residue_top_not_avail = []
    
    ### CHECKING TO SEE IF YOU HAVE RESIDUES THAT MUST BE IN ORDER!
    if combined_residues != [[]]:
        print("Combining residues by reordering!")
        ## LOOPING THROUGH EACH ONE
        for each_combined_residue in combined_residues:
            ## MAKING SURE THE NAME IS WITHIN THE UNIQUE NAMES
            current_residues_to_add = [ each_residue for each_residue in each_combined_residue[:-1] if each_residue in gro_unique_names_updated ]
            ## EXTENDING TO RESIDUE ORDER
            residue_order.extend(current_residues_to_add)
            ## ADDING TO TOPOLOGY
            residue_order_top.append( each_combined_residue[-1] )
            combined_residue_top_not_avail.extend( each_combined_residue[:-1] )
            
    ## ADDING ALL OTHER RESIDUES
    [ residue_order.append(each_residue) for each_residue in gro_unique_names_updated if each_residue not in residue_order   ] 
    [ residue_order_top.append( each_residue ) for each_residue in gro_unique_names_updated 
                                               if each_residue not in residue_order_top and each_residue not in combined_residue_top_not_avail ]
    
    ## REORDERING BASED ON DESIRED ORDER
    if desired_res_order != []:
        ## LOOPING AND SEEING IF RES IN RESIDUE ORDER
        current_res_order = [each_res for each_res in desired_res_order if each_res in residue_order]
        ## APPENDING EACH RESIDUE
        current_res_order.extend([each_res for each_res in residue_order if each_res not in current_res_order])
        
        ## CHECK FOR COMBINED RESIDUES
        check_combined_res = [ combined[-1] if each_res in combined else each_res  for combined in combined_residues for each_res in desired_res_order]
        
        ## REMOVING DUPLICATES
        check_combined_res = list(dict.fromkeys(check_combined_res))
        
        ## DEFINING CURRENT ORDER
        current_top_res_order = [each_res for each_res in check_combined_res if each_res in residue_order_top]

        ## APPENDING EACH RESIDUE
        current_top_res_order.extend([each_res for each_res in residue_order_top if each_res not in current_top_res_order])
        
        print("Reordering residue according to the following order:")
        print(desired_res_order)
        print("New gro file order:")
        print(current_res_order)
        print("New topology file order:")
        print(current_top_res_order)
        
        ## STORING
        residue_order = current_res_order[:]
        residue_order_top = current_top_res_order[:]

        
        # desired_res_order
    #%%
    ## SORTING -- TURNED OFF SORTING -- made errors eith the gro file for nplm prepartion
    # residue_order.sort()
    # residue_order_top.sort()
    
    ## NOW LOOPING THROUGH THE GRO FILE FOR EACH RESIDUE
    '''
        Need to correct the following:
            - resids: residue ID's
            - residue_name: residue names
            - atom_names: atom names
            - geom: geometries
        We can do that by creating an updated index with the order that we want!
    '''
    ## DEFINING EMPTY LIST FOR RESIDUE ID'S
    residue_order_index = np.array([])
    
    ## LOOPING THROUGH EACH RESIDUE ORDER
    for each_residue in residue_order:
        ## FINDING LOCATION FOR ALL RESIDUES
        each_residue_index = np.where(residue_names == each_residue)
        ## APPENDING
        residue_order_index = np.append(residue_order_index,each_residue_index ).astype('int')
    
    ## NOW THAT WE HAVE THE RESIDUE ORDER, LET'S REORDER ALL THE ITEMS ON THE ITP FILE LIST
    updated_gro_resids = np.array(gro_file.ResidueNum)[residue_order_index]
    updated_gro_resname = residue_names[residue_order_index]
    # np.array(gro_file.ResidueName)[residue_order_index]
    updated_gro_atom_name = np.array(atom_names)[residue_order_index]
    updated_gro_x_coord = np.array(gro_file.xCoord)[residue_order_index]
    updated_gro_y_coord = np.array(gro_file.yCoord)[residue_order_index]
    updated_gro_z_coord = np.array(gro_file.zCoord)[residue_order_index]
    
    ## COMBINING GEOMETRY
    updated_gro_geom = np.array( [updated_gro_x_coord, updated_gro_y_coord, updated_gro_z_coord] ).T
    
    ## UPDATING RESIDUE NUMBERS
    correct_gro_resids = correct_residue_numbers( updated_gro_resids )
    
    ## PRINTING GRO FILE
    print_gro_file(output_gro = output_gro, 
                   output_folder = output_folder, 
                   resids = correct_gro_resids, 
                   residue_name = updated_gro_resname , 
                   atom_name = updated_gro_atom_name, 
                   geom = updated_gro_geom, 
                   box_dimension = gro_file.Box_Dimensions, 
                   wantSingle = False)
    #%%
    
    ## NOW, FIXING TOPOLOGY ##
    
    ## FINDING MOLECULES IN DICT
    molecule_key = [ each_dict_key for each_dict_key in top_file.bracket_extract.keys() if '[ molecules ]' in each_dict_key ][0]
    molecules_lines = top_file.bracket_extract[molecule_key]
    
    print("Residue name")
    print(residue_rename)
    
    ## CORRECTING MOLECULE LINES IN CASE THERE IS A CORRECTION
    if residue_rename != [[]]:
        ## LOOP THROUGH EACH CORRECTION
        for each_resname_correction in range(len(residue_rename)): # residue_rename: # 
            ## FINDING PREVIOUS AND NEW RENAMING NOMENCLATURE
            new_resname, old_resname = residue_rename[each_resname_correction][1], residue_rename[each_resname_correction][0]
            ## SEEING IF ANY OF THE LINES HAVE SUCH A RESIDUE NAME
            idx_within_lines = [ idx for idx,each_line in enumerate(molecules_lines) if each_line[0] == old_resname ]
            ## CORRECTING IF TRUE
            if len(idx_within_lines) > 0:
                for each_idx in idx_within_lines:
                    molecules_lines[each_idx][0]=new_resname
        
    ## DEFINING UPDATED LINES
    molecule_updated_lines = []
    
    ## LOOPING THROUGH EACH RESIDUE
    for each_residue_name in residue_order_top:
        ## SEEING LOCATION OF ALL RESIDUE NAMES
        residue_name_loc = [ idx for idx, each_line in enumerate(molecules_lines) if each_line[0] == each_residue_name]
        ## DEFINING TOTAL RES
        total_res = 0
        ## SUMMING IF LOCATION IS MORE THAN ONCE
        for each_location in residue_name_loc:
            total_res += int(molecules_lines[each_location][1])
            
        ## STORING INTO NEW LINES
        molecule_updated_lines.append( [each_residue_name, str(total_res)])
    
    ####### NOW, EDITING MOLECULES
    
    ## REMOVING ALL AFTER MOLECULES
    molecule_line_index = [ idx for idx, each_line in enumerate(top_file.topology_lines) if '[ molecules ]' in each_line ][0]
    
    ## DEFINING A COPY OF TOPOLOGY
    topology_copy_lines = top_file.topology_lines[:]
    
    ## REMOVING ALL LINES PRIOR
    topology_copy_lines = topology_copy_lines[:molecule_line_index+1] # Including molecules
    
    ## APPENDING COMMENT SECTION
    topology_copy_lines.append( '; compound   nr_mol\n' )
    
    ## ADDING TO TOPOLOGY
    for each_molecule_line in molecule_updated_lines:
        ## APPENDING TO TOPLOGY
        topology_copy_lines.append( "%s   %s \n"%( each_molecule_line[0], each_molecule_line[1] ) ) 
    
    ## PRINTING
    ## printing
    print("OUTPUTTING TOPOLOGY: %s"%(output_top_path))
    ## OPENING FILE
    with open(output_top_path, "w") as outputfile:
        ## LOOPING THROUGH AND PRINTING
        for each_line in topology_copy_lines:
            outputfile.write(each_line)
    
    