# -*- coding: utf-8 -*-
"""
callback.py

This script  contains inrepretations of iniput options from option parser

FUNCTIONS:
    interpret_options_comma: 
        interpret options as commas
"""

### FUNCTION TO INTERPRET SPACES
def interpret_options_comma(option, opt_str, value, parser):
    setattr(parser.values, option.dest, value.split(','))