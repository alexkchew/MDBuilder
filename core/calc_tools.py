# -*- coding: utf-8 -*-
"""
calc_tools.py
The purpose of this script is tocontain all calculation functions

CREATED ON: 05/02/2018

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
FUNCTIONS:
    find_atom_closest_center: for a spherical lattice, this function finds the approximate center of a geometry

"""
import numpy as np

### FUNCTION TO FIND THE ATOM CLOSEST TO THE CENTER
def find_atom_closest_center(geometry):
    '''
    The purpose of this script is to find the atom closest to the center of any lattice
    INPUTS:
        geometry: [np.array, shape=(N,3)] geometry of the entire lattice
    OUTPUTS:
        center_atom_index: [int] atom index of the center based on geometry
        center_atom_positions: [np.array, shape(1,3)] atomic positions of the center atoms
        radius: [float] radius of the lattice (found from maximum distance from center)
    '''
    ## FINDING CENTER OF THE LATTICE
    center = np.mean(geometry, axis = 0)
    ## FINDING DISTANCES OF ALL ATOMS FROM THE CENTER
    distance_from_center = np.sqrt(  np.sum( (geometry - center)**2, axis=1 ) )
    ## FINDING MAX DISTANCE (i.e. radius)
    radius = np.max(distance_from_center)
    ## FINDING CLOSEST TO CENTER
    center_atom_index = np.argmin(distance_from_center)
    center_atom_positions = geometry[center_atom_index]
    return center_atom_index, center_atom_positions, radius