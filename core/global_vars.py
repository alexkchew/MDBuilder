# -*- coding: utf-8 -*-
"""
global_vars.py
This script contains all global variables used. Global variables are defined as ALLCAPS

CREATED ON: 05/02/2018

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)
"""

## BOLTZMANN CONSTANT
k_B = 0.008314462175 # kJ/(mol K)

