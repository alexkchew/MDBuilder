# -*- coding: utf-8 -*-
"""
forcefield.py
The purpose of this file is to contain all forcefield pertinent code

CREATED ON: 05/02/2018

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)

CLASS:
    FORCEFIELD: Stores all force field information in a form of a class
    FORCEFIELD_FRAGMENT: older force field class for ligand builder fragments
    
** UPDATES **
20180503 - updated forcefield class to remove prep folders
"""
import os
###################################################
### CLASS FUNCTION TO GET FORCEFIELD PARAMETERS ###
###################################################
class FORCEFIELD:
    '''
    The purpose of this is to store all forcefield information in a form of a class. Then, we can use this class to output important information about the force fields
    INPUTS:
        prep_folder: Folder for preparation
        forcefield_folder: Force field folder, e.g.
            opls-aa
            charmm36-nov2016.ff
            pool.ff
    OUTPUTS:
        # DETAILS ABOUT LIGAND FOLDER / FRAGMENT EXTENSIONS
        self.fragment_folder: Folder where your fragments lie
        self.fragment_extension: Extension for your fragments
        self.ligand_folder: Folder where your ligands lie
        self.ligand_extension: Extension for your ligands
        
        # DETAILS ABOUT FORCEFIELD ITP INFORMATION
        self.forcefield_folder: forcefield folder
        self.forcefield_itp: location of itp file
        self.forcefield_water: location of water (tip3p) model
        self.forcefield_ions: location of ions (ions.itp)
        self.forcefield_methanol: location of methanol
        self.fragment_ending: Ending of the force field
        self.angleFunc: Angle function for angle potentials in each force field
        self.dihedralFunc: dihedral function in each force field
        
    '''
    ## INITIALIZATION
    def __init__(self, forcefield_folder):
        ## PRINTING
        print("USING THE FORCE FIELD: %s"%(forcefield_folder))
        ## FINDING FORCE FIELD FOLDERS / ANGLE FUNC / DIHEDRAL FUNC
        self.define_force_field_parameters(forcefield_folder)
        return
    
    ### FUNCTION TO FIND FORCE FIELD PARAMETERS
    def define_force_field_parameters(self, forcefield_folder ):
        '''
        Given the force field folder, define all the itp files, fragment endings, angle/dihedral functions
        INPUTS:
            forcefield_folder: Force field folder, e.g.
                opls-aa
                charmm36-nov2016.ff
                charmm36-jul2017.ff
                pool.ff
        OUTPUTS:
            self.forcefield_folder: forcefield folder
            self.forcefield_itp: location of itp file
            self.forcefield_water: location of water (tip3p) model
            self.forcefield_ions: location of ions (ions.itp)
            self.forcefield_methanol: location of methanol
            self.fragment_ending: Ending of the force field
            self.angleFunc: Angle function for angle potentials in each force field
            self.dihedralFunc: dihedral function in each force field
        '''   
        # STORING FORCE FIELD FOLDER
        self.forcefield_folder =   forcefield_folder       
        # Default names for itp files (For OPLS force field)
        if forcefield_folder == "opls-aa":
            self.forcefield_itp = forcefield_folder + '/' + 'ffoplsaa.itp'
            self.forcefield_water = forcefield_folder + '/' + 'tip3p-mod.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'OPLS'
    
            # Functions for itp files
            self.angleFunc = 1
            self.dihedralFunc = 3
    
        elif forcefield_folder == "charmm36-nov2016.ff" or forcefield_folder.startswith("charmm36-"):
            self.forcefield_itp = forcefield_folder + '/' + 'forcefield.itp'
            self.forcefield_water = forcefield_folder + '/' +  'tip3p.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'CHARMM36'
        
            # Functions for itp files
            self.angleFunc = 5
            self.dihedralFunc = 9

        elif forcefield_folder == "charmm36-jul2017.ff" or forcefield_folder == "charmm36-jul2017-mod.ff":
            self.forcefield_itp = forcefield_folder + '/' + 'forcefield.itp'
            self.forcefield_water = forcefield_folder + '/' +  'tip3p.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'CHARMM36'
        
            # Functions for itp files
            self.angleFunc = 5
            self.dihedralFunc = 9
            
        elif forcefield_folder == "pool.ff": # POOL force field is for self-assembly processes
            self.forcefield_itp = forcefield_folder + '/' + 'forcefield.itp'
            self.forcefield_water = forcefield_folder + '/' +  'tip3p.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'POOL'
            
            # Functions for itp files
            self.angleFunc = 2 # GROMOS ANGLE FUNCTION (COSINES)
            self.dihedralFunc = 3 # RYKAERT-BELLEMANS TORSION FUNCTION
            
        else:
            print("Error! No force field folder selected, stopping here!!!")
            exit()
        return
    
###################################################
### CLASS FUNCTION TO GET FORCEFIELD PARAMETERS ###
###################################################
class FORCEFIELD_FRAGMENT:
    '''
    The purpose of this is to store all forcefield information in a form of a class. Then, we can use this class to output important information about the force fields
    INPUTS:
        prep_folder: Folder for preparation
        forcefield_folder: Force field folder, e.g.
            opls-aa
            charmm36-nov2016.ff
            pool.ff
    OUTPUTS:
        # DETAILS ABOUT LIGAND FOLDER / FRAGMENT EXTENSIONS
        self.fragment_folder: Folder where your fragments lie
        self.fragment_extension: Extension for your fragments
        self.ligand_folder: Folder where your ligands lie
        self.ligand_extension: Extension for your ligands
        
        # DETAILS ABOUT FORCEFIELD ITP INFORMATION
        self.forcefield_folder: forcefield folder
        self.forcefield_itp: location of itp file
        self.forcefield_water: location of water (tip3p) model
        self.forcefield_ions: location of ions (ions.itp)
        self.forcefield_methanol: location of methanol
        self.fragment_ending: Ending of the force field
        self.angleFunc: Angle function for angle potentials in each force field
        self.dihedralFunc: dihedral function in each force field
        
    Written by: Alex K. Chew (12/22/2017)
    '''
    ## INITIALIZATION
    def __init__(self, forcefield_folder, prep_folder):
        ## PRINTING
        print("PREPARATION FOLDER LOCATION: %s"%(prep_folder))
        print("USING THE FORCE FIELD: %s"%(forcefield_folder))
        ## FINDING DEFAULT PREPARATION FOLDERS
        self.find_default_prep_folders(prep_folder)
        ## FINDING FORCE FIELD FOLDERS / ANGLE FUNC / DIHEDRAL FUNC
        self.define_force_field_parameters(forcefield_folder)
        return
    ### FUNCTION TO GET PREPARATION DIRECTORY STRUCTURE
    def find_default_prep_folders(self, prep_folder = ''):
        '''
        This function simply defines the directory structure given the preparation folder for Ligand Builder
        INPUTS:
            prep_folder: Folder for preparation
        OUTPUTS:
            self.fragment_folder: Folder where your fragments lie
            self.fragment_extension: Extension for your fragments
            self.ligand_folder: Folder where your ligands lie
            self.ligand_extension: Extension for your ligands
        '''
        ### DEFAULT PREPARATION FOLDERS
        self.fragment_folder = prep_folder + '/fragments'
        self.fragment_extension = 'frag'
        self.ligand_folder = prep_folder + '/ligands'
        self.ligand_extension = 'lig'
        return 
    
    ### FUNCTION TO FIND FORCE FIELD PARAMETERS
    def define_force_field_parameters(self, forcefield_folder ):
        '''
        Given the force field folder, define all the itp files, fragment endings, angle/dihedral functions
        INPUTS:
            forcefield_folder: Force field folder, e.g.
                opls-aa
                charmm36-nov2016.ff
                charmm36-jul2017.ff
                pool.ff
        OUTPUTS:
            self.forcefield_folder: forcefield folder
            self.forcefield_itp: location of itp file
            self.forcefield_water: location of water (tip3p) model
            self.forcefield_ions: location of ions (ions.itp)
            self.forcefield_methanol: location of methanol
            self.fragment_ending: Ending of the force field
            self.angleFunc: Angle function for angle potentials in each force field
            self.dihedralFunc: dihedral function in each force field
        '''   
        # STORING FORCE FIELD FOLDER
        self.forcefield_folder =   forcefield_folder       
        # Default names for itp files (For OPLS force field)
        if forcefield_folder == "opls-aa":
            self.forcefield_itp = forcefield_folder + '/' + 'ffoplsaa.itp'
            self.forcefield_water = forcefield_folder + '/' + 'tip3p-mod.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'OPLS'
    
            # Functions for itp files
            self.angleFunc = 1
            self.dihedralFunc = 3
    
        elif forcefield_folder == "charmm36-nov2016.ff" or forcefield_folder.startswith("charmm36-"):
            self.forcefield_itp = forcefield_folder + '/' + 'forcefield.itp'
            self.forcefield_water = forcefield_folder + '/' +  'tip3p.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'CHARMM36'
        
            # Functions for itp files
            self.angleFunc = 5
            self.dihedralFunc = 9
            
        elif forcefield_folder == "charmm36-jul2017.ff":
            self.forcefield_itp = forcefield_folder + '/' + 'forcefield.itp'
            self.forcefield_water = forcefield_folder + '/' +  'tip3p.itp' # TIP3P model
            self.forcefield_ions = forcefield_folder + '/' + 'ions.itp'
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'CHARMM36'
        
            # Functions for itp files
            self.angleFunc = 5
            self.dihedralFunc = 9
            
        elif forcefield_folder == "pool.ff": # POOL force field is for self-assembly processes
            self.forcefield_itp = os.path.join(forcefield_folder, 'forcefield.itp')
            self.forcefield_water = os.path.join(forcefield_folder,  'tip3p.itp') # TIP3P model
            self.forcefield_ions = os.path.join(forcefield_folder , 'ions.itp')
            self.forcefield_methanol = forcefield_folder + '/' + 'methanol.itp'
            self.fragment_ending = 'POOL'
            
            # Functions for itp files
            self.angleFunc = 2 # GROMOS ANGLE FUNCTION (COSINES)
            self.dihedralFunc = 3 # RYKAERT-BELLEMANS TORSION FUNCTION
            
        else:
            print("Error! No force field folder selected, stopping here!!!")
            exit()
        return
