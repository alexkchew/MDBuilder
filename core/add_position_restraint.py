# -*- coding: utf-8 -*-
"""
add_position_restraint.py
The purpose of this script is to add position restraints to a ligand

CREATED ON: 05/04/2018

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    - ligand itp file
OUTPUTS:
    - updated itp file
    
ALGORITHM:
    - load in itp file
    - check to see if position restraint is there
    - edit if necessary
TODO: Edit script to check if position restraints are there!
"""
### FROM MDBuilder
from MDBuilder.applications.nanoparticle.global_vars import SULFUR_ATOM_NAME, DEFAULT_POSITION_RESTRAINT
from MDBuilder.core.import_tools import extract_itp
from MDBuilder.builder.ligand_builder import get_ligand_args

## CHECK PATH FUNCTION
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
## SYSTEM TOOLS
import sys
from optparse import OptionParser # Used to allow commands within command line

### FUNCTION TO EDIT THE ITP FILE
def add_position_restraint( itp_files, position_restraint_atom =[ SULFUR_ATOM_NAME ], position_restraint_value = float(DEFAULT_POSITION_RESTRAINT)):
    '''
    The purpose of this function is to loop through the itp file, and check if we have a position restraint
    INPUTS:
        itp_files: [list] itp file from extract_itp class
        position_restraint_atom: [list] list of atom names to position restrain
        position_restraint_value: [float] value for position restraint
    OUTPUTS:
        itp file with the position restraint
    '''
    ## LOOPING THROUGH EACH ITP FILE
    for current_itp in itp_files:
        print("*** Working on ITP FILE: %s ***"%( current_itp.itp_file_path ))
        ## SEEING IF WE HAVE POSITION RESTRAINTS
        index_position_restraint = [ index for index,each_line in enumerate(current_itp.clean_itp) if '[ position_restraints ]' in each_line ]
        if len(index_position_restraint) > 0:
            print("Position restraint found in itp file at index: %d"%( index_position_restraint ))
        else:
            print("No position restraints found!")
        print("Attempting to add position restraint of atom name: %s"%( position_restraint_atom ))
        
        ## ASSUMING NO POSITION RESTRAINT ADDED
        if len(index_position_restraint) == 0:
            
            ## FINDING ATOM NUMBER OF THE POSITION RESTRAINT ATOMS
            try:
                atom_serial = [ current_itp.atom_atomname.index(each_atom) for each_atom in position_restraint_atom ]
            except:
                print("ERROR!!! Something may be wrong with your inputs for position_restraint_atom. Please check add_position_restraint script.")
                sys.exit()
            atom_index = [ current_itp.atom_num[each_serial] for each_serial in atom_serial ]
            ## WRITING TO FILE (APPENDING)
            with open(current_itp.itp_file_path, 'a') as edit_itp:
                edit_itp.write('\n[ position_restraints ]\n')
                ## LOOPING THROUGH EACH INDEX
                for each_index in atom_index:
                    edit_itp.write(' %d 1 %.1f %.1f %.1f\n'%(each_index,position_restraint_value, position_restraint_value, position_restraint_value   ) )

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    # Turn test on or off
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    if testing == True:
    
        ### DEFINING INPUTS
        input_dir = r"R:\scratch\nanoparticle_project\simulations\Planar_310.15_K_butanethiol_10x10_CHARMM36_intffGold"
        ligand_names = ["butanethiol"]
        
    else:
        ### ON COMMAND LINE
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ## DEFINING INPUT FOLDER
        parser.add_option("--ifold", dest="input_folder", action="store", type="string", help="Input folder", default=".")
        ## LIGAND NAMES
        parser.add_option("--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## GETTING ARGUMENTS
        input_dir = options.input_folder # Output gro file
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
        
    
    ### DEFINING FULL PATHS FOR THE ITP
    full_itp_path = [ check_server_path(input_dir + '/' + each_ligand + '.itp')  for each_ligand in ligand_names if each_ligand != 'none' ] 
    
    ### READING ITP FILE
    itp_files = [ extract_itp(each_path) for each_path in full_itp_path]
    
    ### RUNNING POSITION RESTRAINT SCRIPT
    add_position_restraint(itp_files)

    