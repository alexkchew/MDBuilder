# -*- coding: utf-8 -*-
"""
export_tools.py
This script contains exportation tools (e.g. gro, itp, etc.)

CREATED ON: 05/01/2018


FUNCTIONS:
    ## GRO FILE
        print_gro_file: prints gro file
        print_gro_file_combined_res_atom_names: prints gro files for a combined residue and atom name
    ## ITP FILE
        write_itp_type_comment: writes comments for itp files
        add_before_and_after_2_string: Adds spaces before and after string
    ## TOPOLOGY FILE
        print_top_file_ligands: prints topology file for ligands
    
AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

"""

import os

### GLOBAL VARIABLES
### DICTIONARY FOR COMMENTS ON EACH DEFINITION
ITP_COMMENT_DICT = {
        '[ atomtypes ]'      : '; name  at.num    mass    charge   ptype          sigma(nm)      epsilon (kJ/mol) ',
        '[ nonbond_params ]' : '; i     j  func     sigma       epsilon  ',
        '[ moleculetype ]'   : '; Name   nrexcl',
        '[ atoms ]'          : ';   nr    type   resnr  residue    atom    cgnr   charge    mass',
        '[ bonds ]'          : ';  ai    aj   funct  b0   kb   ; bond properties inferred from atom types',
        '[ angles ]'         : ';  ai    aj    ak  funct  ; angle properties inferred from atom types',
        '[ dihedrals ]'      : ';  ai    aj    ak   al  funct  ; dihedral properties inferred from atom types ',
        '[ pairs ]'          : ';  ai    aj ; pairs inter 1,4 interactions',
        }


### FUNCTION TO PRINT THE GRO FILE
def print_gro_file(output_gro, output_folder, resids, residue_name, atom_name, geom, box_dimension, wantSingle = False):
    '''
    The purpose of this script is to write the .gro file necessary to run GMX.
    INPUTS:
        output_gro: [str] 
            Gro file name
        output_folder: [str] 
            Directory to gro file
        resids: [list] 
            Contains list of residue numbers
        residue_name: [list] 
            Contains list of residue names
        atom_name: [list] 
            List containing atom names
        geom: [np.array, shape=(N,3)] 
            Coordinates for ligand system
        box_dimension: [list] 
            box dimensions of your system in nm
        wantSingle: [logical, default=False]
            True if you want a single monolayer SAM (hard-coded)
    '''
    ## FINDING TOTAL NUMBER OF ATOMS USING GEOMETRY
    num_atoms = len(geom)
    
    if wantSingle: # hard coded for single monolayer
        num_atoms = int( num_atoms / 2. )
    
    ## DEFINING OUTPUT PATH
    output_gro_path = output_folder + '/' + output_gro # Gives full gro path
    
    ## CREATING FILE AND OUTPUTTING
    print("OUTPUTTING GRO FILE %s WITH %d ATOMS"%(output_gro_path, num_atoms))  # Let user know what is being outputted
    
    with open(output_gro_path, "w") as outputfile: # Opens gro file and make it writable
    
        ## WRITING HEADER
        outputfile.write("GRO file developed by print_gro_file tool in MDBuilder\n") # Title for .gro, does not affect anything
        outputfile.write("%d\n"%(num_atoms)) # Total number of atoms, important! Must match topology file
        
        ## DEFINING INITIAL ATOM NUMBER
        atom_num=1; # Start at one
        for bead_index in range(0, num_atoms):
            outputfile.write("%5d%-5s%-5s%5d%8.3f%8.3f%8.3f\n"%(resids[bead_index], residue_name[bead_index], atom_name[bead_index], atom_num, geom[bead_index][0], geom[bead_index][1], geom[bead_index][2]))
            atom_num +=1
            
            # Make sure that atom_num is not greater than 99999, GROMACS cannot read more than 5 digits
            if atom_num > 99999:
                atom_num=0 # Resetting atom number
                
        ## WRITING BOX DIMENSIONS
        outputfile.write("%f %f %f\n"%(box_dimension[0], box_dimension[1], box_dimension[2]))
    return

### FUNCTION TO PRINT GRO FILE BUT WITH COMBINED RESIDUE NAME AND ATOM NAMES
def print_gro_file_combined_res_atom_names(output_gro,
                                           output_folder, 
                                           resids, 
                                           outputnames, 
                                           geom, 
                                           box_dimension, 
                                           wantSingle = False,):
    '''
    The purpose of this script is to write the .gro file necessary to run GMX.
    INPUTS:
        output_gro: [str] Gro file name
        output_folder: [str] Directory to gro file
        resids: [list] Contains list of residue numbers
        outputnames: Contains list of residue names + atom names
        geom: [np.array, shape=(N,3)] Coordinates for ligand system
        box_dimension: [list] box dimensions of your system in nm
    OUTPUTS:
        gro file
    '''
    ## FINDING TOTAL NUMBER OF ATOMS USING GEOMETRY
    num_atoms = len(geom)
    
    if wantSingle: # hard coded for single monolayer
        num_atoms = int( num_atoms / 2. )
    
    ## DEFINING OUTPUT PATH
    output_gro_path = os.path.join(output_folder, output_gro)
    
    ## CREATING FILE AND OUTPUTTING
    print("OUTPUTTING GRO FILE %s WITH %d ATOMS"%(output_gro_path, num_atoms))  # Let user know what is being outputted
    
    with open(output_gro_path, "w") as outputfile: # Opens gro file and make it writable
    
        ## WRITING HEADER
        outputfile.write("GRO file developed by print_gro_file tool in MDBuilder\n") # Title for .gro, does not affect anything
        outputfile.write("%d\n"%(num_atoms)) # Total number of atoms, important! Must match topology file
        
        ## DEFINING INITIAL ATOM NUMBER
        atom_num=1; # Start at one
        for bead_index in range(0, num_atoms):
            outputfile.write("%5d%10s%5d%8.3f%8.3f%8.3f\n"%(resids[bead_index], 
                                                            outputnames[bead_index], 
                                                            atom_num, 
                                                            geom[bead_index][0], 
                                                            geom[bead_index][1], 
                                                            geom[bead_index][2]))
            atom_num +=1
            
            # Make sure that atom_num is not greater than 99999, GROMACS cannot read more than 5 digits
            if atom_num > 99999:
                atom_num=0 # Resetting atom number
                
        ## WRITING BOX DIMENSIONS
        outputfile.write("%f %f %f\n"%(box_dimension[0], box_dimension[1], box_dimension[2]))
    return
    

### FUNCTION TO WRITE ITP COMMENTS
def write_itp_type_comment(outputfile, itp_type):
    '''
    This function takes your itp file and the interaction type to insert a comment. This function is based on "ITP_COMMENT_DICT", which is a dictionary containing all possible comments.
    INPUTS:
        outputfile: file you want to output in (this function will add at current line)
        itp_type: what is the type you have (e.g. '[ atomtypes ]')
    OUTPUTS:
        Will output comments within outputfile
    '''
    try:
        comment = ITP_COMMENT_DICT[itp_type]
        outputfile.write("%s\n"%(comment))
    except Exception: # Do nothing -- no comments
        pass
    return

### FUNCTION TO ADD SPACES TO BEGINNING AND END OF STRING
def add_before_and_after_2_string( string, additional_string=' ' ):
    '''
    This function simply adds \n to beginning and end of your string
    INPUTS:
        string (e.g. [ bonds ])
    OUTPUTS:
        new_string (e.g. '\n[ bonds ]\n')
    '''
    new_string = additional_string + string + additional_string
    return new_string

### FUNCTION TO PRINT TOPOLOGY FILE FOR LIGANDS
def print_top_file_ligands(forcefield, output_top, output_folder, ligands, ligand_nums):
    '''
    The purpose of this script is to create a topology file
    INPUTS:
        forcefield: [class] force field from forcefield class
        output_top: [str] Output topology file
        output_folder: [str] Folder where topology is at
        ligand: [list] list of ligand classes
        ligand_nums: [list] Total number of ligands for each ligand
    OUTPUTS:
        topology file
    '''
    ## DEFINING PATH
    output_folder_path = output_folder + '/' + output_top
    ## printing
    print("OUTPUTTING TOPOLOGY: %s"%(output_folder_path))
    ## OPENING FILE
    with open(output_folder_path, "w") as outputfile:
        # write basic topology information here
        outputfile.write(";  Topology file for spherical SAM, created by print_top_file_ligands in MDBuilders\n")
        # include statements
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_itp))
        # include all ligand names
        for lig in ligands:
            outputfile.write("#include \"%s.itp\"\n"%(lig.ligand_name))
        #outputfile.write("#include \"%s\"\n"%(GOLD_ITP_NAME))
        # add water here, although the molecules are added separately
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_water))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_ions))
        # outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_methanol))
        outputfile.write("\n[ system ]\n")
        outputfile.write("frame t=1.000 in water\n")
        outputfile.write("\n[ molecules ] \n")
        outputfile.write("; compound   nr_mol\n")
        ## ADDING TOTAL NUMBER OF LIGANDS
        for i in range(0, len(ligands)):
            lig=ligands[i]
            outputfile.write(" %s     %d\n"%(lig.ligand_res_name, ligand_nums[i]) )
    

