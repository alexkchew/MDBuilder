# -*- coding: utf-8 -*-
"""
pickle_funcs.py
This file contains all pickle functions

CREATED ON: 02/01/2019
    
FUNCTIONS:
    store_class_pickle: stores class as a pickle
    load_class_pickle: loads class as a pickle
CLASS:


AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
*** UPDATES ***

"""
### IMPORTING MODULES
import pickle
from datetime import datetime
import sys
import pandas

### DEFINING FUNCTION TO STORE SELF INTO A PICKLE
def store_class_pickle(my_class, output_path):
    '''
    The purpose of this function is to store all the results into a pickle file
    INPUTS:
        my_class: [obj]
            class object to store within pickle
        output_path: [str]
            output path that you want the pickle to be in. It should be a full path with name
    OUTPUTS:
        pickle file within the pickle directory under the name of the class used
    '''
    ## PRINTING TOTAL SIZE OF THE OBJECT
    # print("Total size of saving pickle is: %d bytes or %d MB"%(sys.getsizeof(self), sys.getsizeof(self) / 1000000.0 ))
    ## DEPRECIATED, see size by: https://stackoverflow.com/questions/449560/how-do-i-determine-the-size-of-an-object-in-python
    ## PRITING
    print("Creating pickle at: %s"%(output_path))
    with open(output_path, 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([my_class], f, protocol=2)  # <-- protocol 2 required for python2   # -1
    print("Pickle creation was complete at: %s\n"%(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    return

### FUNCTION TO LOAD MULTI TRAJ PICKLES
def load_class_pickle(pickle_path):
    '''
    The purpose of this script is to simply load the pickle after you have saved the results using multi_traj class
    INPUTS:
        pickle_path: [str]
            full path to pickle
    OUTPUTS:
        results: [object] 
            pickle results
    '''
    # PRINTING
    print("LOADING PICKLE FROM: %s"%(pickle_path) )    
    ## LOADING THE DATA
    with open(pickle_path,'rb') as f:
        # multi_traj_results = pickle.load(f) ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
        if sys.version_info[0] > 2:
            try:
                results = pickle.load(f, encoding='latin1') ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
            except ImportError: # Dealing with sometimes requiring pandas
                import pandas as pd
                results = pd.read_pickle(  f  )
            except:
                print("Error in importing pickle file. Check load_class_pickle function in the pickle_funcs folder of MDBuildrs")
        elif sys.version_info[0] == 2: ## ENCODING IS NOT AVAILABLE IN PYTHON 2
            results = pickle.load(f) ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
        else:
            print("ERROR! Your python version is not 2 or greater! We cannot load pickle files below Python 2!")
            sys.exit()
        ## SEE REFERENCE: https://stackoverflow.com/questions/11305790/pickle-incompatibility-of-numpy-arrays-between-python-2-and-3?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    return results[0] # Output everything within pickle --- required since it was stored as a list

    
