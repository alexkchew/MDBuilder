# -*- coding: utf-8 -*-
"""
transfer_ligand_builder.py
The purpose of this script is to take the equilibrated gold core with butanethiols and replace the ligand with desired ligands in a new trans-state. This code presents a key step of automating the nanoparticle generation based purely on MD simulations

CREATED ON: 12/19/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
INPUTS:
    - preparation gold folder
    - input gro file from gold
    - input ligand file
    - output directory
    - output gro name
    - output top name
    
OUTPUTS:
    - AUNP.itp (itp file for gold)
    - sam.gro: gro file for the system
    - sam.top: topology file for the system

CLASSES:
    self_assembled_gold: Main class to analyze the self assembly of gold

FUNCTIONS:
    transfer_ligand_builder_main: 
        Main function for transfer ligand builder
    add_gold_details: 
        function that adds gold details
    add_gold_resname_end_of_topology:
        function that adds gold residue at the end of topology file
    check_if_gold_in_topology:
        function that checks if gold is within topology
    
Author: Alex K. Chew (Created at 12/19/2017)
*** UPDATES *** 
20171222 - Updated Ligandbuilder to incorporate transfer ligands
20180220 - Updated to incorporate bonding details
20180309 - Added bonding distribution functions for self-assembly ligands
20180424 - Transformed all global variables to ALLCAPS
20180425 - Debugging -- fixing up names and correction for gold-gold bonding (did not work correctly for 4 nm case)
20180503 - Editing transfer ligand builder to incorporate MDBuilder design
            - Also updated transfer ligand builder to correctly update gold core weight for hollow shell (using spherical fcc lattice generation tool)
20200828 - Editing code to allow for double sulfur ligands
20201104 - Edited code so you could skip the addition of gold atoms
"""
## IMPORTING MODULES
import os
import numpy as np # Math functions
import numpy.matlib as npm # matrix laboratory math functions

## IMPORTING MDTRAJ
import mdtraj as md
import matplotlib.pyplot as plt

from optparse import OptionParser # Used to allow commands within command line
## LIGAND BUILDER
import MDDescriptors.core.read_write_tools as read_write_tools
from MDBuilder.builder.ligand_builder import get_ligand_args, Monolayer_Spherical, Ligand
from MDBuilder.core.forcefield import FORCEFIELD
# import ligand_builder
## SYSTEM PREPARATION
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.import_tools import extract_gro ## READING GRO FILE
from MDBuilder.builder.make_gold_planar_111 import print_itp_file_gold ## PRINTS ITP FILE FOR THE GOLD
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

### IMPORTING FUNCTIONS
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names, print_top_file_ligands
from MDBuilder.math.rotation import angle_between

### GLOBAL VARIABLES
## GOLD PROPERTIES
from MDBuilder.applications.nanoparticle.global_vars import GOLD_SULFUR_CUTOFF, AU_CHARGE, HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS, GOLD_GOLD_CUTOFF, GOLD_MOLECULE_NAME, SULFUR_ATOM_NAME, AU_TYPENAME
from MDBuilder.applications.nanoparticle.global_vars import GOLD_MASS as AU_MASS
from MDBuilder.applications.nanoparticle.global_vars import GOLD_ATOM_NAME as AU_ATOMNAME
from MDBuilder.applications.nanoparticle.global_vars import GOLD_FCC_LATTICE_CONSTANT_NM

## IMPORTING CALC TOOLS
import MDDescriptors.core.calc_tools as calc_tools
## TRACK TIME TOOLS
from MDDescriptors.core.track_time import track_time

## IMPORTING PICKLE TOOLS
import MDDescriptors.core.pickle_tools as pickle_funcs

## DEFINING DEFAULT VARIABLES
DEFAULT_GOLD_PICKLE = "skip_gold_info.pickle"

### SELF-ASSEMBLED_GOLD CLASS
class self_assembled_gold:
    '''
    The purpose of this class is to store all the information for the self-assembled gold. 
    Written by: Alex K. Chew (alexkchew@gmail.com)
    
    INPUTS:
        full_path_input_gro: [str]
            input gro file
        shape: [str]
            shape of the gold core
        assembly_lig_type: [str]
            assembly ligand types
        wantPlot: [logical, default = False]
            True if you want plot
        want_bonding: [logical]
            True if you want bonding between gold and sulfur, etc.
        sulfur_index: [np.array]
            list of sulfur indexes that you want to bind.
    
    SELF INFORMATION:
        # INITALIZATION
            self.shape: Shape of the object
                spherical: simply spherical by fcc {111} lattice (default, no specific changes)
                hollow: hollow shell, requires specific changes:
                    - Changes in minimum number of bonds (based on: HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS)
        
        # GRO INFORMATION:
            self.ResidueNum: residue numbers
            self.ResidueName: name of your residues
            self.AtomName: name of the atoms
            self.AtomNum: number of atoms
            self.xCoord, self.yCoord, self.zCoord: x, y, z coordinates
            self.Box_Dimensions: 3 x 1 vector box dimensions
        
        # GOLD/SULFUR INFO
            self.gold_index, self.gold_xyz, self.gold_num_atoms - gold index, xyz, number of atoms
            self.sulfur_index, self.sulfur_xyz, self.sulfur_num_atoms - sulfur index, xyz, number of atoms

        # CLOSEST/FAR SULFUR INFO:
            self.total_near_sulfur: Nearby sulfur atoms (total)
            self.near_sulfur_xyz: Near sulfur xyz
            self.far_sulfur_xyz: Far away sulfur xyz
        
        # BONDING BETWEEN GOLD AND SULFUR
            self.total_gold_sulfur_bonding: Total number of bonds between gold and sulfur
            self.gold_sulfur_bonding: List delineating which atoms are bonded to each other in the self-assembly case
                NOTE: If the indexes change -- this may occur if you have multiple ligands --- randomization of sulfurs are done
            self.gold_sulfur_bonding_gro_index: gold sulfur bond as a numpy array [ gold_index, sulfur_index ]
            self.gold_sulfur_bonding_values: array with gold-sulfur distances
            
        # ANGLES BETWEEN GOLD AND SULFUR
            self.sulfur_gold_angles_degrees: list of degrees to go along with the gold-sulfur-gold bonds
            self.sulfur_gold_angles_gro_index: index within gro that contains the gold, sulfur, gold -- used for [ angles ]
            
        # BONDING BETWEEN GOLD-GOLD
            self.gold_bonding: index of gold-gold bonding
            self.total_gold_gold_bonding: total number of gold bonds
            self.all_gold_bonded: True/False if all the gold atoms are bonded
            self.gold_bonding_gro_index: gold bonding as a numpy array based on the gro file
            self.gold_bonding_values: array with gold-gold distances
        
        # GOLD CORE INFO:
            self.center_coordinates: Coordinates of the gold core
            self.radius: Approximate radius of the gold core
            
        # SURFACE AREA PER LIGAND:
            self.surface_area_per_ligand: surface area per ligand in angstroms^2 / ligand
        
        # CENTERED DETAILS
            self.near_sulfur_xyz_centered: Near sulfurs centered at (0,0,0) for gold core
            self.far_sulfur_xyz_centered: Far sulfurs centered at (0,0,0) for gold core
            self.gold_xyz_centered: Gold atoms centered at (0,0,0) for gold core
            
        ### FUNCTIONS
        # PLOTTING INFO:
            self.plot_self_assembly(): Plotting gold + sulfur
            self.plot_bonding_dist(): Plots bonding distribution between S-Au and Au-Au
            self.calc_bond_occurances: [staticmethod] calculates bond occurances
            self.plot_bond_dist: [staticmethod] plots bond occurance
            self.correct_minimum_bonds: Corrects the minimum number of bonds
            self.find_sulfur_gold_angles: Finds sulfur and gold angles
            
    '''
    ## CALL FUNCTION WHEN OBJECT IS CREATED
    def __init__(self, 
                 full_path_input_gro, 
                 shape='spherical', 
                 assembly_lig_type = "SCCCC",
                 wantPlot=False,
                 want_bonding = True,
                 sulfur_index = None):
        ## SAVING SHAPE VARIABLE
        self.shape=shape
        ## SAVING ASSEMBLY LIGAND TYPE
        self.assembly_lig_type = assembly_lig_type
        self.full_path_input_gro = full_path_input_gro
        self.want_bonding = want_bonding
        
        ### LOADING TRAJECTORY
        traj = md.load(self.full_path_input_gro, self.full_path_input_gro)
        
        ## STORING TRAJ
        self.traj = traj
        
        
        ''' OLD METHOD OF USING IMPORT GRO FILE SYSTEMS (WITHOUT MDTRAJ)
        ### LOADING THE INPUT GRO FILE
        gro_file = extract_gro(self.full_path_input_gro)
        self.ResidueNum, self.ResidueName, self.AtomName, self.AtomNum, self.xCoord, self.yCoord, self.zCoord, self.Box_Dimensions =  gro_file.ResidueNum, \
                                                                                                                                      gro_file.ResidueName, \
                                                                                                                                      gro_file.AtomName, \
                                                                                                                                      gro_file.AtomNum, \
                                                                                                                                      gro_file.xCoord, \
                                                                                                                                      gro_file.yCoord, \
                                                                                                                                      gro_file.zCoord, \
                                                                                                                                      gro_file.Box_Dimensions
        ## FINDING ALL GOLD ATOMS
        self.gold_index, self.gold_xyz, self.gold_num_atoms, self.gold_atomnames = self.find_index_xyz(self.ResidueName, GOLD_MOLECULE_NAME)
    
        ## FINDING ALL SULFUR ATOMS
        self.sulfur_index, self.sulfur_xyz, self.sulfur_num_atoms, _ = self.find_index_xyz(self.AtomName, SULFUR_ATOM_NAME)
        '''
        ################################        
        ### GETTING GOLD INFORMATION ###
        ################################
        # GETTING GOLD INDEX
        self.gold_index = np.array([ atom.index for atom in traj.topology.atoms if atom.residue.name == GOLD_MOLECULE_NAME ])
        ## GETTING GOLD XYZ
        self.gold_xyz = traj.xyz[0, self.gold_index]
        ## GETTING TOTAL NUMBER
        self.gold_num_atoms = len(self.gold_index)
        ## GETTING ATOM NAME
        self.gold_atomnames = np.array([ traj.topology.atom(index).name for index in self.gold_index])
        
        ##################################
        ### GETTING SULFUR INFORMATION ###
        ##################################
        ## SULFUR INDEX
        if sulfur_index is None:
            self.sulfur_index = np.array([ atom.index for atom in traj.topology.atoms if atom.element.symbol == "S" ])
            self.check_sulfur_index = True
        else:
            self.sulfur_index = sulfur_index[:]
            self.check_sulfur_index = False # Turns off double-checking of sulfur indices
            
        
        ## GETTING XYZ
        self.sulfur_xyz = traj.xyz[0, self.sulfur_index]
        
        ## GETTING NUMBER OF SULFUR ATOMS
        self.sulfur_num_atoms = len(self.sulfur_index)
        
        ## FINDING CENTER OF GOLD CORE AND RADIUS
        self.find_core_center()
        
        ## FINDING CLOSEST SULFUR ATOMS
        self.find_closest_sulfur(traj = traj)
        ## FINDING SULFUR GOLD BONDING (DEPRECIATED, NO LONGER RESTRAIN SULFUR-GOLD BOND NAGLES)
        # self.find_sulfur_gold_angles()
        if self.want_bonding is True:
            ## FINDING GOLD-GOLD BONDS
            self.find_gold_gold_bonding()
        ## FINDING CENTERED COORDINATES WITH GOLD AS CENTER (0,0,0)
        self.find_coordinates_at_origin()
        ## FINDING SURFACE AREA PER LIGAND
        self.find_surf_area_per_lig()
        ## PLOTTING IF DESIRED
        if wantPlot:
            self.plot_self_assembly()
            
        ## PRINTING SUMMARY
        self.print_summary()
        
        return
    
    ### FUNCTION TO PRINT SUMMARY
    def print_summary(self):
        ''' This function prints the summary '''
        ## PRINTING
        print("There are %d sulfur atoms near the gold core (%d atoms)"%(self.total_near_sulfur, self.gold_num_atoms))
        print("The approximate radius using center of the gold core is: %.2f"%(self.radius))
        print("The surface area per ligand is therefore: %.2f A^2 per ligand"%(  self.surface_area_per_ligand  ))
        if self.want_bonding is True:
            print("Total number of bonds between sulfur and gold is: %s"%(self.total_gold_sulfur_bonding))
            print("Total number of gold-gold bonds is: %s"%(self.total_gold_gold_bonding))
            if self.all_gold_bonded is True:
                print("All gold atoms are bonded")
            else:
                print("*WARNING* Some gold atoms are not bonded!!")        
        else:
            print("Number of bonds are skipped!")

        return
    
    ### FUNCTION TO FIND INDICES, XYZ, AND NUMBER OF RESIDUES/ATOMS
    def find_index_xyz(self, current_list, string):
        '''
        This function finds all the index and xyz values given a specific residue name
        INPUTS:
            string: name of your residue
        OUTPUTS:
            residue_index: Index of your residue
            residue_xyz: xyz positions as a N X 3 array
            num_residues: Number of atoms/residues
        '''
        ### FINDING ALL GOLD ATOMS
        residue_index = [ index for index, res_name in enumerate(current_list) if string in res_name]
        residue_xyz = np.array([ [self.xCoord[current_index], self.yCoord[current_index], self.zCoord[current_index]] for current_index in residue_index ])
        atomname = np.array([ self.AtomName[current_index] for current_index in residue_index ])
        num_atoms = len(residue_index)
        return residue_index, residue_xyz, num_atoms, atomname
    
    ### FUNCTION TO FIND THE CORE CENTER AND APPROXIMATE RADIUS
    def find_core_center(self):
        '''
        This function finds the center of the gold core and the corresponding radius
        INPUTS:
            self: class property
        OUTPUTS:
            self.center_coordinates: Coordinates of the gold core
            self.radius: Approximate radius of the gold core
        '''
        self.center_coordinates = np.mean(self.gold_xyz, axis=0)
        self.radius = np.max( np.linalg.norm(self.gold_xyz - self.center_coordinates,axis=1), axis =0 )
    
    
    ### FUNCTION TO FIND SULFUR GOLD DISTANCES WITH TRAJ
#    This turned out to be slower than the vectorized version! Surprised that vectorized > mdtraj  (almost twice as fast)
#    @staticmethod
#    def find_sulfur_gold_distances_with_traj(traj, sulfur_index, gold_index, cutoff = GOLD_SULFUR_CUTOFF ):
#        '''
#        The purpose of this function is to find gold-sulfur distances using 
#        traj tools. The idea would be to use these distances as a way of 
#        computing which sulfurs are bound to the surface.
#        INPUTS:
#            traj: [obj]
#                trajectory object from mdtraj
#            sulfur_index: [list]
#                sulfur index of interest
#            gold_index: [list]
#                list of gold index
#            cutoff: [float]
#                cutoff distances between gold and sulfur
#        '''
#        
#        ## GETTING DISTANCES
#        distances = calc_tools.calc_pair_distances_between_two_atom_index_list(traj = traj, 
#                                                                               atom_1_index = gold_index, 
#                                                                               atom_2_index = sulfur_index, 
#                                                                               periodic=False )[0]
#        
#        ### FINDING ALL SULFUR ATOMS NEARBY THE GOLD
#        NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index = np.where(distances <= cutoff)
#        
#        return NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index
#    
    
    ### FUNCTION TO GET BONDING INFORMATION
    @staticmethod
    def get_bonding_info(NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index):
        '''
        The purpose of this function is to get the bonding information for gold-sulfur 
        bonding.
        INPUTS:
            NEAR_GOLD_Au_atom_index: [np.array]
                array of gold atom index
            NEAR_GOLD_sulfur_atom_index: [np.array]
                array of sulfur atom index
        OUTPUTS:
            gold_sulfur_bonding_index: [np.array, shape = (num_bonds, 2)]
                gold sulfur bonding index
            total_gold_sulfur_bonds: [int]
                total number of gold sulfur bonds
        '''
        ## TOTAL GOLD-SULFUR BONDS
        total_gold_sulfur_bonds = len(NEAR_GOLD_Au_atom_index)
        
        ### SORTING BASED ON SULFUR ATOM
        gold_sulfur_bonding_index = np.array([ [NEAR_GOLD_Au_atom_index[index], NEAR_GOLD_sulfur_atom_index[index]] for index in range(total_gold_sulfur_bonds) ])
        gold_sulfur_bonding_index = gold_sulfur_bonding_index[np.argsort(gold_sulfur_bonding_index[:,1],axis=0),:] ## SORTING BASED ON SULFURS
        
        return gold_sulfur_bonding_index, total_gold_sulfur_bonds
        
    ### STATIC FUNCTION TO FIND NEAREST SULFUR AND GOLD
    @staticmethod
    def find_sulfur_gold_distances( gold_coord, 
                                    sulfur_coord, 
                                    cutoff = GOLD_SULFUR_CUTOFF):
        '''
        The purpose of this function is to find the distances between gold and sulfur. It involves matrix algebra (vectorization) to speed up the calculations.
        INPUTS:
            traj: [obj]
                trajectory object
            gold_coord: [np.array, shape = N_Gold x 3 ]
                gold coordinates
            sulfur_coord: [np.array, shape = N_sulfur x 3]
                sulfur coordinates
            cutoff: [float, default = GOLD_SULFUR_CUTOFF ]
                distance cutoff between sulfur and gold
            sulfur_index: [np.array]
                sulfur indices
            assembly_lig_type: [str]
                assembly ligand type, default = 'SCCC'. If default,
                nothing changes in the algorithm. 
                
                If 'C1CCSS1': 
                    then, we will search for all sulfurs that are bound in the same residue
        OUTPUTS:
            gold_sulfur_bonding_index: [np.array]
                Index where [ AU, S ] <-- atoms should be bonded
            total_gold_sulfur_bonds: [int]
                total gold_sulfur bonds
            GOLD_2_SULFUR_DISTANCE: [np.array]
                distance between gold to sulfur
        '''
        ## FINDING TOTAL NUMBER OF ATOMS
        total_num_sulfurs = len(sulfur_coord)
        total_num_gold = len(gold_coord)
        
        ## VECTORIZATION OF GOLD XYZ
        GOLD_XYZ = npm.repmat(gold_coord, 1, total_num_sulfurs)
        SULFUR_FLATTEN_XYZ = sulfur_coord.flatten() # Flat xyz [ x1, y1, z1, x2, y2, z2, ....]
        
        ### TAKING A DIFFERENCE, SQUARING 
        GOLD_2_SULFUR = (GOLD_XYZ - SULFUR_FLATTEN_XYZ)**2
        GOLD_2_SULFUR_RESHAPED = GOLD_2_SULFUR.reshape(total_num_gold, total_num_sulfurs,3) # Now, [[ [diff_x1, diff_y1, diff_z1] ]]
        GOLD_2_SULFUR_DISTANCE = np.sqrt(np.sum(GOLD_2_SULFUR_RESHAPED,axis=2))
        
        ### FINDING ALL SULFUR ATOMS NEARBY THE GOLD
        NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index = np.where(GOLD_2_SULFUR_DISTANCE <= cutoff)
        
        return NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index, GOLD_2_SULFUR_DISTANCE
    # gold_sulfur_bonding_index, total_gold_sulfur_bonds, GOLD_2_SULFUR_DISTANCE
    
    ### STATIC FUNCTION TO FIND UNIQUE ROWS FOR N X 2 ARRAY
    @staticmethod
    def find_unique_rows(array):
        '''
        This function finds the unique rows for a given N x 2 array.
        INPUTS:
            array: N x 2 NumPy array
        OUTPUTS:
            unique_rows: M x 2 NumPy array where M is the new number of rows 
        '''
        ### REORDERING ROWS
        min_across_columns = np.argmin(array,axis=1)
        ## BLANK REORDERED
        reordered_list = []
        for index, current_vector in enumerate(array):
            current_min_index = min_across_columns[index]
            ## CHECKING WHICH ORDER IS CORRECT
            if current_min_index == 1:
                reordered_vector = current_vector[[1,0]]
            else:
                reordered_vector = current_vector
            ## APPENDING TO REORDERED LIST
            reordered_list.append( reordered_vector.tolist() )
        ## GETTING UNIQUE ROWS
        unique_rows = np.unique(reordered_list, axis=0)
        return unique_rows
    
    ### FUNCTION TO GET CLOSEST SULFUR-GOLD ATOMS IN A FORM OF AN INDEX
    def find_closest_sulfur(self, traj = None):
        '''
        This function finds the closest sulfur atom to the gold core
        INPUTS:
            self: [object]
                class property
            traj: [obj]
                trajectory object
        OUTPUTS:
            self.gold_sulfur_bonding: [np.array]
                Bonding indices for gold_sulfur
            self.total_gold_sulfur_bonding: [np.array]
                Total sulfur-gold bonds
            self.total_near_sulfur: [np.array]
                Nearby sulfur atoms (total)
            self.near_sulfur_xyz: [np.array]
                Near sulfur xyz
            self.far_sulfur_xyz: [np.array]
                Far away sulfur xyz
            self.gold_sulfur_bonding_gro_index: [np.array]
                gold sulfur bond as a numpy array [ gold_index, sulfur_index ]
            self.gold_sulfur_bonding_values: [np.array]
                array with gold-sulfur distances
            self.near_sulfur_index_same_residue: [np.array]
                sulfur index for the same residue. This is useful when you have 
                multiple sulfur atoms within self-assembly simulations.
        '''
        ## FINDING GOLD SULFUR BINDING
        NEAR_GOLD_Au_atom_index, NEAR_GOLD_sulfur_atom_index, self.GOLD_SULFUR_DISTANCES = \
                self.find_sulfur_gold_distances(
                                                gold_coord = self.gold_xyz,
                                                sulfur_coord = self.sulfur_xyz,
                                                cutoff = GOLD_SULFUR_CUTOFF,
                                                )

        ## IF SPECIFIC TYPE
        '''
        The if-statement below checks to see if all sulfur atoms are within the 
        index. If not, then it'll remove the residue. The idea would be that 
        we want all sulfur atoms bound to the gold core. If the 'check_sulfur_index' 
        is False, that means you added the bound sulfur index and removes this 
        check. Sometimes, ligands have sulfur atoms attached, which could 
        influence ligand behavior. So, in these cases, we want to specify 
        which sulfurs are actually bound to the sulfur - otherwise, we have 
        sulfurs that are weirdly bound to the gold that are far away. 
        '''
        if self.assembly_lig_type == 'C1CCSS1' and self.check_sulfur_index is True:
            print("Since assembly type is C1CCSS1, we look for all sulfurs that are within the same residue")
            ## GETTING SULFUR INDICES
            near_gold_sulfur_actual_indices = self.sulfur_index[NEAR_GOLD_sulfur_atom_index]
            ## REMOVING SULFUR INDICES
            keep_indices = []
            
            ## LOOPING THROUGH ALL NEAR SULFUR ATOM INDICES -- Checks to see if ALL sulfur atoms are within bounds
            for idx, each_sulfur_index in enumerate(near_gold_sulfur_actual_indices):
                ## FINDING ALL ATOM INDICES
                all_atom_indices = [ atom.index for atom in traj.topology.atom(each_sulfur_index).residue.atoms if atom.element.symbol == "S" ]
                ## SEEING IF ATOM INDICES ARE WITHIN
                all_indices_within = np.all(np.isin(all_atom_indices, near_gold_sulfur_actual_indices))
                ## SEEING IF WITHIN
                if all_indices_within == True:
                    keep_indices.append(idx)
            
            ## CONVERTING TO ARRAY
            keep_indices = np.array(keep_indices)

            ## PRINTING
            print("Total gold-sulfurs initially: %d"%(len(NEAR_GOLD_sulfur_atom_index)))
            print("Total gold-sulfurs after: %d"%(len(keep_indices)))
            print("Sulfur-gold bonds can be odd since multiple gold-sulfur bonds are allowed")
            
            ## STORING
            NEAR_GOLD_Au_atom_index = NEAR_GOLD_Au_atom_index[keep_indices]
            NEAR_GOLD_sulfur_atom_index = NEAR_GOLD_sulfur_atom_index[keep_indices]

        ## GETTING BONDING INFORMATION
        self.gold_sulfur_bonding, self.total_gold_sulfur_bonding = self.get_bonding_info(NEAR_GOLD_Au_atom_index = NEAR_GOLD_Au_atom_index, 
                                                                                         NEAR_GOLD_sulfur_atom_index = NEAR_GOLD_sulfur_atom_index)
        
        ## GETTING ALL UNIQUE SULFUR ATOMS
        NEAR_SULFUR_INDEX_set = set(self.gold_sulfur_bonding[:,1])
        NEAR_SULFUR_INDEX_list = list(NEAR_SULFUR_INDEX_set); NEAR_SULFUR_INDEX_list.sort() # Sorting list
        
        ## FINDING TOTAL NEARBY SULFURS
        self.total_near_sulfur = len(NEAR_SULFUR_INDEX_list) # total number of sulfurs nearby
        ## FINDING ALL SULFURS FAR AWAY
        FAR_SULFUR_INDEX_list = [index for index, x in enumerate(self.sulfur_index) 
                                        if index not in NEAR_SULFUR_INDEX_list ] 
        
        ## FINDING ALL XYZ POSITIONS OF NEAR SULFUR ATOMS
        self.near_sulfur_xyz = self.sulfur_xyz[NEAR_SULFUR_INDEX_list]
        self.near_sulfur_index = self.sulfur_index[NEAR_SULFUR_INDEX_list[:]]

        ## IF SPECIFIC TYPE
        if self.assembly_lig_type == 'C1CCSS1':
            ## GETTING ALL SULFUR INDEXES WITHIN SAME RESIDUE
            self.near_sulfur_index_same_residue = []
            
            ## LOOPING THROUGH ALL SULFUR INDICES
            for atom_index in self.near_sulfur_index :
                ## CHECKING IF ATOM INDEX IS WITHIN
                is_within_list = np.any(np.isin(atom_index, self.near_sulfur_index_same_residue))
                ## IF NOT WITHIN, THEN SORE
                if is_within_list == False:
                    ## FINDING ALL NEARBY SULFURS
                    current_residue = traj.topology.atom(atom_index).residue
                    ## GETTING ALL SULFURS
                    current_sulfur_atom_indices = \
                        [ atom.index for atom in current_residue.atoms if atom.element.symbol == "S" ]
                    
                    ## STORING
                    self.near_sulfur_index_same_residue.append(current_sulfur_atom_indices)
            ## CONVERTING TO ARRAY
            self.near_sulfur_index_same_residue = np.array(self.near_sulfur_index_same_residue)
            ## GETTING XYZ ARRAY
            self.near_sulfur_xyz_same_residue = traj.xyz[0, self.near_sulfur_index_same_residue]
            

        ## FINDING ALL XYZ POSITIONS OF FAR AWAY SULFUR ATOMS
        self.far_sulfur_xyz =  self.sulfur_xyz[FAR_SULFUR_INDEX_list]
        self.far_sulfur_index = FAR_SULFUR_INDEX_list[:]
        ## DEFINING GOLD AND SULFUR INDEX FOR GRO FILE
        self.gold_sulfur_bonding_gro_index = \
            np.array([ [ self.gold_index[eachbond[0]] +1 ,self.sulfur_index[eachbond[1]] +1 ] 
                         for eachbond in self.gold_sulfur_bonding   ]).astype('int')
        ## DEFINING THEIR CORRESPONDING DISTANCES
        self.gold_sulfur_bonding_values = \
                np.array([ self.GOLD_SULFUR_DISTANCES[each_bond[0],each_bond[1]] 
                           for each_bond in self.gold_sulfur_bonding ])
        
        return
    ### FUNCTION TO GET ANGLES BETWEEN GOLD AND SULFUR ATOMS
    def find_sulfur_gold_angles(self):
        '''
        This function finds angle constraints for the gold-sulfur-gold bonds.
        INPUTS:
            self: class property
        OUTPUTS:
            self.sulfur_gold_angles_degrees: list of degrees to go along with the gold-sulfur-gold bonds
            self.sulfur_gold_angles_gro_index: index within gro that contains the gold, sulfur, gold -- used for [ angles ]
        '''
        ## GETTING ALL UNIQUE SULFUR INDEX
        unique_sulfur_index = list(set(self.gold_sulfur_bonding[:,1]))
        
        ## CREATING EMPTY ARRAY TO STORE DETAILS
        sulfur_bonding_dict={}; sulfur_gold_angle_list=[]
        
        ## LOOPING THROUGH EACH SULFUR INDEX TO CREATE DICTIONARY
        for each_sulfur_index in unique_sulfur_index:
            ## CREATING DICTIONARY e.g. {0: array([3, 5, 13])} <-- index 0 sulfur is bonded with index 3, 5, 13 of gold
            sulfur_bonding_dict[each_sulfur_index] = self.gold_sulfur_bonding[np.where(self.gold_sulfur_bonding[:,1]==each_sulfur_index),0][0]
        
        ## LOOPING THROUGH EACH DICTIONARY TO CREATE ANGLES LIST
        for each_sulfur_key in sulfur_bonding_dict.keys():
            atoms_bonded = sulfur_bonding_dict[each_sulfur_key]
            ## FINDING TOTAL ATOMS BONDED
            total_atoms_bonded = len(atoms_bonded)
            for each_atom in range(total_atoms_bonded):
                ## LOOPING THROUGH THE NEXT SET OF ATOMS
                for next_atoms in range(len(atoms_bonded[each_atom+1:])):
                    # INDEX IS [ GOLD_INDEX, SULFUR_INDEX, GOLD_INDEX ]
                    sulfur_gold_angle_list.append([atoms_bonded[each_atom], each_sulfur_key, atoms_bonded[each_atom+1:][next_atoms]  ]) # adding [ 3, 0, 5 ] <-- indicates Au-S-Au bond
        
        ## FINDING ALL VECTORS WITH RESPECT TO SULFUR
        sulfur_gold_vectors =[ [ self.gold_xyz[sulfur_gold_angle_index[0]] - self.sulfur_xyz[sulfur_gold_angle_index[1]], 
                                self.gold_xyz[sulfur_gold_angle_index[2]] - self.sulfur_xyz[sulfur_gold_angle_index[1]] ]  for sulfur_gold_angle_index in sulfur_gold_angle_list ] 
        ## RETURNS LIST OF ARRAYS [ Gold1-S, Gold2-S ], e.g. [[array([ 0.26 , -0.158, -0.056]), array([ 0.099, -0.039, -0.273])],]
        
        ## FINDING ANGLES BETWEEN SULFUR-GOLD
        self.sulfur_gold_angles_degrees =[ angle_between(each_vectors[0], each_vectors[1])  for each_vectors in sulfur_gold_vectors ]
        
        ## FINDING GRO INDEX FOR THE ANGLES
        self.sulfur_gold_angles_gro_index =np.array([ [self.gold_index[sulfur_gold_angle_index[0]]+1,
                                                  self.sulfur_index[sulfur_gold_angle_index[1]]+1,
                                                  self.gold_index[sulfur_gold_angle_index[2]]+1,
                                                  ]  for sulfur_gold_angle_index in sulfur_gold_angle_list]).astype('int')
        return
            
    ### FUNCTION TO GET BONDING INFORMATION FOR GOLD-GOLD
    def find_gold_gold_bonding( self, cutoff=GOLD_GOLD_CUTOFF ):
        '''
        This function finds the closest gold-gold atoms, then binds them according to some cutoff.
        INPUTS:
            self: class property
        OUTPUTS:
            self.gold_bonding: index of gold-gold bonding
            self.total_gold_gold_bonding: total number of gold bonds
            self.all_gold_bonded: True/False if all the gold atoms are bonded
            self.gold_bonding_gro_index: gold bonding as a numpy array based on the gro file
            self.gold_bonding_values: array with gold-gold distances
        '''
        # USING GOLD-SULFUR DISTANCES FOR GOLD-GOLD
        gold_index_1, gold_index_2, GOLD_GOLD_DISTANCES = self.find_sulfur_gold_distances( self.gold_xyz, self.gold_xyz, cutoff = cutoff)
        self.gold_gold_index, _ = self.get_bonding_info(NEAR_GOLD_Au_atom_index = gold_index_1, 
                                                   NEAR_GOLD_sulfur_atom_index = gold_index_2)
        self.GOLD_GOLD_DISTANCES = GOLD_GOLD_DISTANCES
        ## CORRECTIONS -- GOLD ATOMS CANNOT BIND TO THEMSELVES
        gold_bonding = np.array([ index for index in self.gold_gold_index if index[0] != index[1] ])
        ## SORTING ACCORDING TO COLUMN 1
        self.gold_bonding = self.find_unique_rows(gold_bonding[np.argsort(gold_bonding[:,0],axis=0),:])  # FINDS UNIQUE ROWS
        ## CORRECTS IF THE SHAPE IS HOLLOW
        if self.shape == 'hollow':
            print("MAKING EXTRA BONDS FOR HOLLOW-TYPE SPHERE")
            self.gold_bonding = self.correct_minimum_bonds(self.gold_bonding, self.GOLD_GOLD_DISTANCES, bond_setpoint = HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS )
            ## SORTING ACCORDING TO COLUMN 1
            self.gold_bonding = self.find_unique_rows(self.gold_bonding[np.argsort(self.gold_bonding[:,0],axis=0),:])  # FINDS UNIQUE ROWS
        
        self.total_gold_gold_bonding=len(self.gold_bonding)
        ## CHECKING IF ALL GOLD IS BONDED
        num_unique_gold_atoms_bonded = len( set(self.gold_bonding.flatten() ) )
        if num_unique_gold_atoms_bonded == self.gold_num_atoms:
            self.all_gold_bonded = True
        else:
            self.all_gold_bonded = False
        ## CREATING INDEX FOR GOLD-GOLD BINDING BASED ON GRO FILE
        self.gold_bonding_gro_index = np.array([ [ self.gold_index[eachatom] + 1 for eachatom in eachbond] for eachbond in self.gold_bonding   ]).astype('int')
        ## FINDING DISTANCES
        self.gold_bonding_values = np.array([ GOLD_GOLD_DISTANCES[each_bond[0],each_bond[1]] for each_bond in self.gold_bonding ])
        return
    
    ### FUNCTION TO CORRECT FOR THE MINIMUM NUMBER OF BONDS
    @staticmethod
    def correct_minimum_bonds(gold_bonding, GOLD_GOLD_DISTANCES, bond_setpoint = HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS):
        '''
        The purpose of this script is to correct the number of bonds by setting a minimum. This is useful for cases were gold atoms are scarce, thus we need additional bonds to keep the structure in tact.
        INPUTS:
            gold_bonding: Gold bonding as a numpy array (e.g. [[1,2],[2,3]], etc.)
            GOLD_GOLD_DISTANCES: Numpy array of gold to gold distance -- should be of shape N_gold x N_gold, where N_gold is the number of gold atoms
            bond_setpoint: Minimum number of bonds desired
        OUTPUTS:
            gold_bonding: Corrected gold bonding by inclusino of additional bonds to reach the setpoint
        '''
        ## FINDING UNIQUE NUMBER OF BONDS
        gold_index, num_bonds = np.unique(gold_bonding, return_counts=True)
        ## FINDING INDICES WHERE THERE ARE FEWER GOLD BONDS
        index_of_few_bonds = np.where(num_bonds < HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS)
        gold_atoms_to_find_bonds = gold_index[index_of_few_bonds] # List of gold atoms that need more bonds!
        gold_atoms_to_find_num_atoms = num_bonds[index_of_few_bonds]
        ## SORTED DISTANCES
        # SORTED_GOLD_GOLD_DISTANCES = np.sort(GOLD_GOLD_DISTANCES,axis=1) < -- used to check
        SORTED_GOLD_INDEXES = np.argsort(GOLD_GOLD_DISTANCES,axis = 1)
        
        ### TESTING WITH SINGLE CASE
        # gold_index_of_interest = gold_atoms_to_find_bonds[0]
        ### LOOPING THROUGH EACH
        for index, gold_index_of_interest in enumerate(gold_atoms_to_find_bonds):
        
            ## CURRENT TOTAL BONDS
            num_bonds_gold_index_of_interest = gold_atoms_to_find_num_atoms[index] # Total number of bonds for each gold (tells us how much we need to add!)
            
            ## GETTING INDEXES TO ADD
            bonds_to_added = SORTED_GOLD_INDEXES[gold_index_of_interest][num_bonds_gold_index_of_interest+1:HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS+1]
            
            ## CREATING NUMPY LIST TO ADD
            bond_to_append = np.array([ [gold_index_of_interest,current_atom]  for current_atom in bonds_to_added ])
            
            ## APPENDING TO BONDING ARRAY
            gold_bonding = np.append( gold_bonding, bond_to_append, axis=0)
        
        return gold_bonding
    
    ### FUNCTION TO CALCULATE THE SURFACE AREA PER LIGAND IN ANGSTROMS^2 / LIG
    def find_surf_area_per_lig(self):
        '''
        This function calculates the surface area of a spphere divided by the total number of ligands
        INPUTS:
            self: class property
        OUTPUTS:
            self.surface_area_per_ligand: surface area per ligand in angstroms^2 / ligand
        '''
        ## CALCULATING SURFACE AREA PER LIGAND (ANGSTROMS^2 / LIG)
        self.surface_area_per_ligand = 4 * np.pi * self.radius**2 / float(self.total_near_sulfur) * 100 # Angstroms^2  / lig
        
    ###  FUNCTION TO RE-CENTER ALL THE LIGANDS ACCORDING TO THE CENTER OF THE GOLD ATOMS
    def find_coordinates_at_origin(self):
        '''
        This function takes the self information and corrects all xyz coordinates such that origin represents the center of the gold core
        INPUTS:
            self: class property
        OUTPUTS:
            self.near_sulfur_xyz_centered: Near sulfurs centered at (0,0,0) for gold core
            self.far_sulfur_xyz_centered: Far sulfurs centered at (0,0,0) for gold core
            self.gold_xyz_centered: Gold atoms centered at (0,0,0) for gold core
            
        '''
        # CENTERING ALL GEOMETRIES
        ## IF SPECIFIC TYPE
        if self.assembly_lig_type == 'C1CCSS1':
            self.near_sulfur_xyz_centered = self.near_sulfur_xyz_same_residue - self.center_coordinates
        else:
            self.near_sulfur_xyz_centered = self.near_sulfur_xyz - self.center_coordinates
        self.far_sulfur_xyz_centered = self.far_sulfur_xyz - self.center_coordinates
        self.gold_xyz_centered = self.gold_xyz - self.center_coordinates
        return                                                              
                                                                         
    ### FUNCTION TO PLOT THE SELF-ASSEMBLY
    def plot_self_assembly(self,wantDebug=False):
        '''
        This function simply plots your gold, nearby sulfur (red), and far away sulfur (gray)
        INPUTS:
            self
            wantDebug: True if you want to see the bonds slowly formed
        OUTPUTS:
            plot
        '''
        ## PLOTTING
        # import matplotlib
        # matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D # For 3D axes
        
        ## DEBUGGING INFORMATION
        Debug_type = "gold_gold_bonding"
        # sulfur_gold_bonding: sulfur_gold bonds
        # gold_gold_bonding: gold-gold bonds
        
        ## ATOM DETAILS
        line_info={'marker':'o',
                   'edgecolor':'black',
                   'linewidth':1.5,
                   }
        ### DICTIONARY FOR BONDS
        bond_dict = {
                'color': 'green',
                'linestyle': '-',
                'linewidth': 2,
                }
        if wantDebug == False:
            total_repeats = 1
        else:
            if Debug_type == "sulfur_gold_bonding":
                total_repeats = self.total_gold_sulfur_bonding
            elif Debug_type == "gold_gold_bonding":
                total_repeats = self.total_gold_gold_bonding
        
        ## LOOPING THROUGH REPEATS
        for current_repeat in range(total_repeats):

            if wantDebug == False:
                ##################################
                ### PLOTTING GOLD-SULFUR BONDS ###
                ##################################
                ## CREATING FIGURE
                fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # , aspect='equal'
                # SETTING X, Y, Z labels
                ax.set_xlabel('x (nm)')
                ax.set_ylabel('y (nm)')
                ax.set_zlabel('z (nm)')
                ## SETTING TITLE
                ax.set_title("GOLD-SULFUR BONDING")
                # ax.scatter(self.near_sulfur_xyz[:,0], self.near_sulfur_xyz[:,1], self.near_sulfur_xyz[:,2],color='red', alpha=0.1,s=100, **line_info )
                # PLOTTING ALL GOLD ATOMS
                ax.scatter(self.gold_xyz[:,0], self.gold_xyz[:,1], self.gold_xyz[:,2],color='yellow', alpha = 0.4, s=100, **line_info )
                # PLOTTING ALL FAR AWAY SULFUR ATOMS
                # ax.scatter(self.far_sulfur_xyz[:,0], self.far_sulfur_xyz[:,1], self.far_sulfur_xyz[:,2],color='gray', alpha=0.1,s=20, **line_info ) 
                # PLOTTING ALL BONDS BETWEEN GOLD AND SULFUR (assuming 3.27 angstroms as maximum bond length)
                for current_bond in range(self.total_gold_sulfur_bonding):
                    ## FINDING COORDINATES FOR SULFUR AND GOLD
                    gold_coord = self.gold_xyz[self.gold_sulfur_bonding[current_bond][0]]
                    sulfur_coord = self.sulfur_xyz[self.gold_sulfur_bonding[current_bond][1]]
                    ax.plot( [ gold_coord[0], sulfur_coord[0] ] , # x
                             [ gold_coord[1], sulfur_coord[1] ], # y
                             [ gold_coord[2], sulfur_coord[2] ], # z
                             **bond_dict
                             )
                    
                ##################################
                ### PLOTTING GOLD-GOLD BONDS ###
                ##################################
                ## CREATING FIGURE
                fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # , aspect='equal'
                # SETTING X, Y, Z labels
                ax.set_xlabel('x (nm)')
                ax.set_ylabel('y (nm)')
                ax.set_zlabel('z (nm)')
                ## SETTING TITLE
                ax.set_title("GOLD-GOLD BONDING")
                # PLOTTING ALL GOLD ATOMS
                ax.scatter(self.gold_xyz[:,0], self.gold_xyz[:,1], self.gold_xyz[:,2],color='yellow', alpha = 0.4, s=100, **line_info )
                ## PLOTTING ALL BONDS BETWEEN GOLD-GOLD 
                ''' -- IF YOU WANT TO VISUALIZE GOLD-GOLD BONDING --- '''
                ## CHANGING COLOR FOR GOLD BONDS
                bond_dict['color'] = 'red'
                bond_dict['linewidth'] = 1
                for current_bond in range(self.total_gold_gold_bonding):
                    ## FINDING COORDINATES
                    gold_coord_1 = self.gold_xyz[self.gold_bonding[current_bond][0]]
                    gold_coord_2 = self.gold_xyz[self.gold_bonding[current_bond][1]]
                    ## PLOTTING BOND
                    ax.plot( [ gold_coord_1[0], gold_coord_2[0] ] , # x
                             [ gold_coord_1[1], gold_coord_2[1] ], # y
                             [ gold_coord_1[2], gold_coord_2[2] ], # z
                             **bond_dict
                             )
            else: # DEBUGGING IS ON
                ## CREATING FIGURE
                fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # , aspect='equal'
                # SETTING X, Y, Z labels
                ax.set_xlabel('x (nm)')
                ax.set_ylabel('y (nm)')
                ax.set_zlabel('z (nm)')
                if Debug_type == "sulfur_gold_bonding":
                    ## FINDING COORDINATES FOR SULFUR AND GOLD
                    gold_coord = self.gold_xyz[self.gold_sulfur_bonding[current_repeat][0]]
                    sulfur_coord = self.sulfur_xyz[self.gold_sulfur_bonding[current_repeat][1]]
                    
                    ## FINDING DIFFERENCE
                    diff = np.sqrt( np.sum( (gold_coord - sulfur_coord)**2 ) )
                    ## PLOTTING SULFUR ATOM
                    ax.scatter(sulfur_coord[0], 
                               sulfur_coord[1], 
                               sulfur_coord[2],
                               color='red', alpha=0.1,s=100, **line_info )
                    ## PLOTTING GOLD ATOM
                    ax.scatter(gold_coord[0],
                               gold_coord[1],
                               gold_coord[2],
                               color='yellow', alpha=0.1,s=100, **line_info )
                    ## PLOTTING
                    ax.plot( [ gold_coord[0], sulfur_coord[0] ] , # x
                             [ gold_coord[1], sulfur_coord[1] ], # y
                             [ gold_coord[2], sulfur_coord[2] ], # z
                             **bond_dict
                             )
                    print("DEBUGGING SULFUR-GOLD AT BOND %s WITH DISTANCE %.3f"%(current_repeat, diff))
                elif Debug_type == "gold_gold_bonding":
                    gold_coord_1 = self.gold_xyz[self.gold_bonding[current_repeat][0]]
                    gold_coord_2 = self.gold_xyz[self.gold_bonding[current_repeat][1]]
                    ## PLOTTING SULFUR ATOM
                    ax.scatter(gold_coord_1[0], 
                               gold_coord_1[1], 
                               gold_coord_1[2],
                               color='yellow', alpha=0.1,s=100, **line_info )
                    ## PLOTTING GOLD ATOM
                    ax.scatter(gold_coord_2[0],
                               gold_coord_2[1],
                               gold_coord_2[2],
                               color='yellow', alpha=0.1,s=100, **line_info )
                    ## PLOTTING BOND
                    ax.plot( [ gold_coord_1[0], gold_coord_2[0] ] , # x
                             [ gold_coord_1[1], gold_coord_2[1] ], # y
                             [ gold_coord_1[2], gold_coord_2[2] ], # z
                             **bond_dict
                             )
                    ## CHANGING LINE COLOR
                    ax.get_lines()[-1].set_color("red")
                    
                ## PAUSING
                plt.pause(0.001) # Pause so you can see the changes
                
                if Debug_type == "gold_gold_bonding":
                    ax.get_lines()[-1].set_color(bond_dict['color'])

    ### FUNCTION TO PLOT BOND DISTRIBUTION
    def plot_bonding_dist(self):
        '''
        The purpose of this function is to plot the bonding distribution. It will plot S-Au and Au-Au bonding distribution with occurances as the y-axis and unique bonds in the x-axis
        INPUTS:
            self: class property
        OUTPUTS:
            plot of bonding distribution for S-Au and Au-Au
        FUNCTIONS:
            calc_bond_occurances: Calculates bond occurances
        '''
        ### MAIN FUNCTION SCRIPT
        ## COUNTING NUMBER S-AU BONDS
        unique_sulfur_gold_bonds, occurances_sulfur_gold =  self.calc_bond_occurances(self.gold_sulfur_bonding[:,1])
        ### COUNTING NUMBER OF AU-AU BONDS
        unique_gold_gold_bonds, occurances_gold_gold =  self.calc_bond_occurances(self.gold_bonding)
        
        ### PLOTTING FOR SULFUR GOLD
        self.plot_bond_dist(unique_sulfur_gold_bonds,occurances_sulfur_gold, title='Sulfur-gold bond distribution')
        ### PLOTTING FOR GOLD-GOLD
        self.plot_bond_dist(unique_gold_gold_bonds,occurances_gold_gold, title='Gold-gold bond distribution')

    ### FUNCTION TO COUNT THE BOND OCCURANCES
    @staticmethod
    def calc_bond_occurances(bonding_index):
        '''
        The purpose of this function is to count the occurances within an bonding index list. The idea is to get the number of bond occurances
        INPUTS:
            bonding_index: list that you want to look over. It will look over unique atoms, then counts them.
                e.g. [ [0, 1], [0,2] ] <-- In this case, we will count each 0's, 1's, 2's, etc.
        OUTPUTS:
            num_bonds_unique: Number of bonds that are unique
                e.g. [1, 2, 3, 4, 5, 6] <-- 1 bond, 2 bonds, etc.
            bond_occurances: number of times that bond was found
                e.g. [82, 35, ...] <-- 1 bond was found 82 times, etc.
        '''
        ## COUNTING NUMBER OF BONDS
        _, num_bonds = np.unique(bonding_index, return_counts=True)
        ## COUNTING BOND OCCURANCES
        num_bonds_unique, bond_occurances= np.unique(num_bonds, return_counts=True)
        return num_bonds_unique, bond_occurances

    ### FUNCTION TO FIND DISTRIBUTION OF BONDING DETAILS
    @staticmethod
    def plot_bond_dist(num_bonds_unique,bond_occurances, title=''):
        '''
        The purpose of this function is to plot the bonding distribution of the sulfur-gold and gold-gold bonds
        INPUTS:
            num_bonds_unique: x-axis: unique number of bonds
            bond_occurances: y-axis: number of occurances for the number of bonds
            title: title of the plot
        OUTPUTS:
            plot with num_bonds_unique as the x-axis and bond_occurances as the y-axis
        '''
        # import matplotlib
        # matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D # For 3D axes
        ## FINDING AVERAGE BOND
        average_bond = np.dot( num_bonds_unique, (bond_occurances/np.sum(bond_occurances).astype('float')))
        
        ## DEFINING STYLES
        font_style={'fontname':'Arial'}
        
        ## CREATING FIGURE
        fig, ax = plt.subplots()

        ## DRAWING AXIS
        ax.set_xlabel('Number of bonds', **font_style)
        ax.set_ylabel('Occurances', **font_style)
        
        ## DRAWING TITLE
        ax.set_title(title, **font_style)
        
        ## PLOTTING FIGURE
        ax.bar(num_bonds_unique, bond_occurances, align='center', color='black')
        
        ## DRAWING VERTICAL LINE TO INDICATE AVERAGE BOND
        ax.axvline(x=average_bond,color='blue',linestyle='--', label='Average bond')
        
        ## DRAWING LEGEND
        ax.legend()
        # Set the font name for axis tick labels to be Arial
        for tick in ax.get_xticklabels():
            tick.set_fontname("Arial")
        for tick in ax.get_yticklabels():
            tick.set_fontname("Arial")
        return    


### FUNCTION TO UPDATE DETAILS FOR GOLD
def add_gold_details(input_dict,
                     gold_dict,                         
                     ):
    '''
    This function adds the gold details into a data set
    INPUTS:
        input_dict: [dict]
            input dictionary containing the following:
                
            total_atoms: [int]
                total number of atoms
            total_resids: [np.array]
                total residues array
            total_geom: [np.array]
                total geometry
            total_outputnames: [np.array]
                output names
                
        gold_dict: [dict]
            dictionary containing gold information
            
            total_atoms: [int]
                total atoms
            total_resids: [np.array]
                total residue array
            total_geom: [np.array]
                gold geometry
            total_outputnames: [np.array]
                output names                
    OUTPUTS:
        total_atoms: [int]
            total number of atoms
        total_resids: [np.array]
            total residues array
        total_geom: [np.array]
            total geometry
        total_outputnames: [np.array]
            output names
    '''
    ## DEFINING INPUT VARIABLES
    total_atoms = input_dict['total_atoms']
    total_geom = input_dict['total_geom']
    total_outputnames = input_dict['total_outputnames']
    total_resids = input_dict['total_resids']
    
    ## DEFINING GOLD VARIABLES
    total_gold_atoms = gold_dict['total_atoms']
    goldArray = gold_dict['total_resids']
    gold_geom = gold_dict['total_geom']
    gold_atomnames = gold_dict['total_outputnames']
    
    ### ADDING GOLD INFORMATION
    total_atoms += total_gold_atoms
    # ADDING TO RESIDUES
    total_resids = np.append(total_resids, goldArray)
    # ADDING TO GEOMETRY
    total_geom = np.append(total_geom[:], gold_geom[:], axis=0)
    # ADDING TO OUTPUT NAMES
    GoldOutputName = []
    for currentGoldAtom in range(0, len(gold_atomnames)):
        GoldOutputName.append( "%5s%5s" %(GOLD_MOLECULE_NAME, str(gold_atomnames[currentGoldAtom]) ) )
    total_outputnames = np.append(total_outputnames[:], GoldOutputName )
    
    return total_atoms, total_resids, total_geom, total_outputnames

### FUNCTION TO ADD GOLD TO END OF FILE
def add_gold_resname_end_of_topology(path_topology,
                                     gold_resname = GOLD_MOLECULE_NAME,
                                     num_residues = 1,
                                     verbose = False):
    '''
    This function adds gold residue name at the very end of the topology.
    The idea would be to add gold at separte times.
    INPUTS:
        path_topology: [str]
            path to topology file
        gold_resname: [str]
            name of the gold residue
        num_residues: [int]
            number of residues. By default, there is 1 gold residue
        verbose: [logical]
            True if you want verbose output. By default, this is False.
    OUTPUTS:
        void, a single residue name is added to the topology. 
    '''
    ## PRINTING
    if verbose is True:
        print("ADDING GOLD RESIDUE INTO END OF TOP FILE")
    with open(path_topology, 'a' ) as topology_file: # For reading and writing
         topology_file.write(' %s     %d\n'%(gold_resname, num_residues))
    return

### FUNCTION TO ADD GOLD TO TOPOLOGY
def check_if_gold_in_topology(path_topology,
                              verbose = True,
                              skip_gold = False):
    '''
    This function checks if gold is within topology. If not, then it will add 
    to the topology.
    INPUTS:
        path_topology: [str]
            path to topology file
        verbose: [logical]
            True if you want to print details, default is True
        skip_gold: [logical]
            True if you want to skip adding gold to topology, default is False
    OUTPUTS:
        void, gold residue is added

    '''
    ### CORRECTING TOPLOGY FILE FOR GOLD
    ## DEFINING GOLD ITP FILE NAME
    gold_itp_filename = GOLD_MOLECULE_NAME + '.itp'
    ## GOLD INCLUDE STRING
    gold_itp_include_string = '#include "' + gold_itp_filename + '"\n'
    ## FIXING UP TOPOLOGY FILE TO INCLUDE GOLD ITP
    if verbose is True:
        print("WRITING ITP FILE OF GOLD (%s) TO TOPOLOGY (%s)"%(gold_itp_filename, output_top_name))
    with open(path_topology, 'r+' ) as topology_file: # For reading and writing
        ### ADDING FORCEFIELD INFO AFTER FORCEFIELD.ITP
        topology_lines = topology_file.readlines()
        ## REMOVING ALL COMMENTS
        ## SEE IF OUR ITP FILE IS ALREADY THERE
        try:
            topology_lines.index(gold_itp_include_string)
            if verbose is True:
                print("GOLD ITP FILE ALREADY IN TOPOLOGY -- NO ERROR")
        except ValueError:
            if verbose is True:
                print("GOLD ITP FILE NOT WRITTEN IN TOPOLOGY, ADDING TO IT NOW.")
            ## FINDING INDEX TO ADD
            line_index = [ index for index, eachLine in enumerate(topology_lines) if 'forcefield.itp' in eachLine ][0] + 1 # First instance, after forcefield.itp
            ## ADDING LINE
            topology_lines.insert(line_index, gold_itp_include_string)
            topology_file.seek(0) # Starting over
            topology_file.writelines(topology_lines)
            
    ## FIXING UP TOPOLOGY TO HAVE THE RESIDUE GOLD UNDER [ molecules ]
    molecule_index = [ index for index, eachLine in enumerate(topology_lines) if '[ molecules ]' in eachLine ][0]
    topology_molecules = topology_lines[molecule_index:]
    ## SEEING IF GOLD IS PART OF THE MOLECULE LIST
    if len([ currentLine for currentLine in topology_molecules if GOLD_MOLECULE_NAME in currentLine ]) == 0:
        if skip_gold is False:            
            add_gold_resname_end_of_topology(path_topology = path_topology,
                                             gold_resname = GOLD_MOLECULE_NAME,
                                             num_residues = 1,
                                             verbose = True)

    else:
        if verbose is True:
            print("GOLD RESIDUE WITHIN TOPOLOGY -- NO ERROR")    
    return

### FUNCTION TO PRINT GOLD ITP FILE
def print_itp_file_gold_with_shape(total_gold_atoms,
                                   self_assembled_gold_info,
                                   output_folder,
                                   forcefield_folder = "charmm36-nov2016.ff",
                                   shape = 'spherical'):
    '''
    Main function to print gold itp file using default information.
    INPUTS:
        total_gold_atoms: [int]
            total gold atoms
        shape: [str]
            shape of the gold atoms
        output_folder: [str]
            output folder
        forcefield_folder: [str]
            forcefield folder - could be depreciated due to interface force field 
            option. 
        self_assembled_gold_info: [obj]
            self-assembly gold object
    OUTPUTS:
        void - prints out itp file
    '''
    ### WRITING ITP FILE FOR GOLD
    ## CREATING TYPENAME, ATOMNAMES, ETC.
    gold_atomname = [AU_ATOMNAME]*total_gold_atoms
    gold_typename = [AU_TYPENAME]*total_gold_atoms
    gold_charge = [AU_CHARGE]*total_gold_atoms
    gold_mass = [AU_MASS] * total_gold_atoms
    ## FIXING MASS FOR THE CASE OF THE HOLLOW SHELL
    if shape == 'hollow':
        print("*** CORRECTING MASS FOR HOLLOW GOLD CORE! ***")
        ## CREATING A SPHERICAL FCC LATTICE OF THE SAME SIZE
        from MDBuilder.math.make_fcc_lattice import create_spherical_fcc_lattice
        sphere_fcc_lattice = create_spherical_fcc_lattice(fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM, 
                                                          radius = self_assembled_gold_info.radius)
        ## FINDING NUMBER OF ATOMS
        num_sphere_fcc_atoms = sphere_fcc_lattice.total_atoms
        ## FINDING TOTAL MASS AND DIVIDE IT BY THE CURRENT TOTAL ATOMS
        gold_hollow_mass = AU_MASS * num_sphere_fcc_atoms / float(total_gold_atoms)
        ## UPDATING GOLD MASS
        print("CURRENT TOTAL MASS: %.2f"%(np.sum(gold_mass)) )
        gold_mass = [gold_hollow_mass] * total_gold_atoms
        print("NEW TOTAL MASS: %.2f"%(np.sum(gold_mass)) )
        
    ## OUTPUTTING GOLD 
    print_itp_file_gold(molecule_name = GOLD_MOLECULE_NAME, # Name of ITP file
                        output_folder = output_folder, # Name of output folder
                        molecule_resname = GOLD_MOLECULE_NAME, # Name of gold residue
                        typename = gold_typename,
                        atomname = gold_atomname,
                        charge = gold_charge,
                        mass = gold_mass,
                        forcefield = forcefield_folder, # Type of force field
                        wantinterfaceff = True, # Interface force field enabled
                        )
    return

### MAIN FUNCTION TO TRANSFER LIGANDS
def transfer_ligand_builder_main(prep_gold_folder, 
                                 input_folder, 
                                 input_gro_file, 
                                 output_gro_name,
                                 output_top_name, 
                                 ligand_names, 
                                 ligand_fracs, 
                                 prep_folder, 
                                 forcefield_folder, 
                                 assembly_lig_type = "SCCCC",
                                 shape = None, 
                                 wantPlot=False,
                                 skip_gold = False,
                                 verbose = False,
                                 gold_surface_increment = None,
                                 output_sulfur_positions = False,
                                 max_num_ligs = None,
                                 output_sulfur_index = None):
    '''
    The purpose of this script is to take the input self-assembly gold core, then transfer new ligands ontop of the sulfur atoms. This will output a GRO, ITP FILES, TOPOLOGY FILES, etc.
    INPUTS:
        # SELF-ASSEMBLY GOLD
        prep_gold_folder: [str]
            gold folder preparation (simulations) - "R:\scratch\LigandBuilder\Prep_System\prep_gold\simulations"
        input_folder: [str]
            input folder within the simulations - "6_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol"
        input_gro_file: input gro file
        
        # LIGAND DETAILS
        ligand_names: [list]
            names of ligands as a form of a list (i.e. ['ROT_NS','butanethiol'] )
        ligand_fracs: [list]
            fraction of the ligands (i.e. ['0.5','0.5'])
        prep_folder: [str]
            folder where all preparation files are
        forcefield_folder: [str]
            force field folder (i.e. charmm36-nov2016.ff )
        output_sulfur_index: [str]
            If None, nothing will be outputted. If string is supplied, sulfurs 
            that are bound will be printed
        
        
        ## GOLD DETAILS
        shape: shape of the object, e.g.
            'spherical': assuming spherical core
            'hollow': assuming hollow core <-- We will update the hollow gold core mass!
            None: Do no changes to gold details
            
        skip_gold: [logical, default = False]
            True if you want to skip gold
        gold_surface_increment: [float, default = None]
            Place a value here with skip_gold = True to keep some surface gold 
            atoms. The idea would be that we want only SOME of the gold atoms when 
            preparing systems. This value, e.g. 0.2, would mean that we will 
            only keep the surface grid points within a 0.2 nm area radially away 
            from the center of the gold core. 
        output_sulfur_positions: [logical]
            True if you want to output sulfur positions - still working on this part.
        
        ## DEFINING MAXIMUM NUMBER OF LIGANDS
        max_num_ligs: [int]
            maximum number of ligands. By default, this is None, indicating to just 
            take the maximum possible number of ligands. If this value is 
            more than the amount of ligands available from self-assembly simulations, then 
            we will just take the maximum. If it is lower than the maxima, then 
            we will randomly remove ligands from the system. 
        
        wantPlot: 
            true if you want to see all the plots
        verbose: [logical]
            True if you want to print out details
        
        ## ASSEMBLY TYPE
            assembly_lig_type: [str]
                assembly ligand type, which is used to coordinate what 
                types of assembly ligands that you have.
                Examples:
                    "SCCCC": 1 sulfur, butanethiol-like molecule
                    "C1CCSS1": two sulfurs within a cyclopentane ring
    OUTPUTS:
        GRO FILE, TOPOLOGY FILE, ITP FILE (LIGAND & GOLD)
    '''
    print("------- transfer_ligand_builder.py -------")
    ## DEFINING FULL PATH TO THE GRO FILE
    full_path_input_gro = check_server_path(os.path.join(prep_gold_folder,
                                                         input_folder,
                                                         input_gro_file))
    
    ### EXTRACTING SELF-ASSEMBLY INFORMATION
    self_assembled_gold_info = self_assembled_gold(full_path_input_gro, 
                                                   wantPlot=wantPlot,
                                                   assembly_lig_type = assembly_lig_type,
                                                   want_bonding = False)
    
    ### USING LIGAND BUILDER TO CREATE MONOLAYER, BUT WITH THE CORRECTED SULFUR POSITIONS
    ### FINDING FORCE FIELD DETAILS            
    forcefield = FORCEFIELD(forcefield_folder = forcefield_folder)
    
    ## PRINTING
    print("Ligand types:")
    print(', '.join(ligand_names))
    print("Ligand fractions:")
    print(', '.join(ligand_fracs))

    ### CREATING LIGAND CLASSES
    # list of ligands to be loaded
    ligands = []
    # load in all ligands used; output corresponding ITP files too.
    # Loading ligands also loads fragments, etc.
    for cur_lig in ligand_names: # Uses Ligand class
        print('Working on ligand:', cur_lig)
        ligands.append(Ligand( ligand_name = cur_lig, prep_ligand_folder = prep_folder))
    
    ### CREATING SPHERICAL MONOLAYERS
    sphericalMonolayer = Monolayer_Spherical(ligands = ligands, # Ligands of interest
                                             ligand_fracs = ligand_fracs, # Ligand fractions
                                             radius = self_assembled_gold_info.radius, # Sphere radius of self-assembly
                                             assembly_lig_type = assembly_lig_type,
                                             sulfur_coord = self_assembled_gold_info.near_sulfur_xyz_centered, # xyz coordinates of gold info _centered
                                             center_coord = np.array([0,0,0]) , # self_assembled_gold_info.center_coordinates
                                             debug_mode = False,
                                             max_num_ligs = max_num_ligs,
                                             )
    
    ## PRINTING SULFUR INDEX
    if output_sulfur_index is not None:
        path_output_sulfur = os.path.join(output_folder,
                                          output_sulfur_index)
        
        ## DEFINING INDEX OBJECT
        idx_obj = read_write_tools.import_index_file(path_to_index = path_output_sulfur)
        ## READING GROUPS
        idx_obj.split_to_groups()
        
        ## WRITING INDEX OBJECT
        idx_obj.write( 
                      index_key = 'NEARBY_SULFURS', 
                      index_list = sphericalMonolayer.sulfur_bound_indices_flatten,
                      backup_file = False,
                      verbose = True,
                      n_chunks=15)
    
    ### IDENTIFYING IMPORTANT VARIABLES TO OUTPUT
    total_atoms = sphericalMonolayer.num_monolayer_atoms
    total_resids = sphericalMonolayer.monolayer_resids
    total_geom = sphericalMonolayer.monolayer_geom
    total_outputnames = sphericalMonolayer.monolayer_outputnames
    
    ### ADDING GOLD ATOMS
    # ADDING TO TOTAL ATOMS
    total_gold_atoms = self_assembled_gold_info.gold_num_atoms
    
    ### DEFINING BOX PARAMETERS
    # next, define box size based on geometry. For spherical, it will be a cubic shape determined by maximum length from the center
    # Maximum length
    maxRadius = np.max(np.linalg.norm(sphericalMonolayer.monolayer_geom, axis=1))
    maxDiameter = maxRadius * 2 # Diameter ( length of box )
    fixedDiameter = maxDiameter + 1 # Add one to PBC just in case
    
    # Shift entire nanoparticle up so it is within the box
    box_dimension = np.ones( 3 ) * fixedDiameter
    
    ## GETTING GOLD ARRAY
    currentResidueNumber = total_resids[-1] + 1 # new residue number
    
    ## DEFINING GOLD INFORMATION
    gold_geom = self_assembled_gold_info.gold_xyz_centered[:]
    gold_atomnames = self_assembled_gold_info.gold_atomnames[:]
    
    ## CHECKING
    if gold_surface_increment is not None:
        print("gold_surface_increment is not None, so we are keeping some of the gold atoms")
        print("Increment size:", gold_surface_increment)
        
        ## FINDING FARTHEST GOLD ATOM DISTANCE
        gold_geom_distance_from_center = np.linalg.norm(gold_geom, axis=1)
        
        ## GETTING MAXIMUM DISTANCE
        gold_geom_max_radius = np.max(gold_geom_distance_from_center)
        
        ## FINDING ALL INDICES WITHIN RADIUS
        gold_geom_radius_cutoff = gold_geom_max_radius - gold_surface_increment
        
        ## FINDING INDICES OF SURFACES
        gold_index_surface_atoms = np.where(gold_geom_distance_from_center >= gold_geom_radius_cutoff)
        gold_index_bulk_atoms = np.where(gold_geom_distance_from_center < gold_geom_radius_cutoff)
        
        ## GETTING GEOM
        gold_surface_geom = gold_geom[gold_index_surface_atoms]
        gold_bulk_geom = gold_geom[gold_index_bulk_atoms]
        
        ## PRINTING
        print("Total max radius: %.2f"%(gold_geom_max_radius))
        print("Total cutoff radius: %.2f"%(gold_geom_radius_cutoff))
        print("Total surface atoms: %d"%(len(gold_surface_geom)))
        print("Total bulk atoms: %d"%(len(gold_bulk_geom)))
        
        ## CHANGING TOTAL GOLD ATOMS
        total_gold_atoms = len(gold_surface_geom)
        
        ## DEFINING GOLD DICT
        gold_dict = {
                'total_atoms': len(gold_surface_geom),
                'total_resids': np.array( [currentResidueNumber] * len(gold_surface_geom) ),
                'total_geom': gold_surface_geom,
                'total_outputnames': self_assembled_gold_info.gold_atomnames[gold_index_surface_atoms],
                }
        
        ## STORING GOLD OF THE BULK GOLD ATOMS
        stored_gold_dict = {
                'total_atoms': len(gold_bulk_geom),
                'total_resids': np.array( [currentResidueNumber] * len(gold_bulk_geom) ),
                'total_geom': gold_bulk_geom,
                'total_outputnames': self_assembled_gold_info.gold_atomnames[gold_index_bulk_atoms],                
                }
        
    else:
        
        ## DEFINING GOLD ARRAY
        goldArray = np.array( [currentResidueNumber] * self_assembled_gold_info.gold_num_atoms )
        
        ## DEFINING GOLD DICT
        gold_dict = {
                'total_atoms': total_gold_atoms,
                'total_resids': goldArray,
                'total_geom': gold_geom,
                'total_outputnames': gold_atomnames,
                }
    
    ## SEEING IF GOLD SKIPPED
    if skip_gold is False or gold_surface_increment is not None:
        
        ## PERFORMING ADDITION OF GOLD
        input_dict = {
                'total_atoms': total_atoms,
                'total_resids': total_resids,
                'total_geom': total_geom,
                'total_outputnames': total_outputnames,
                }
        
        ## GETTING UPDATED OUTPUT
        total_atoms, total_resids, total_geom, total_outputnames = add_gold_details(input_dict = input_dict,
                                                                                    gold_dict = gold_dict,)                       

    ## PICKLING RESULT            
    if skip_gold is True:
        ## CHOOSING WHICH DICT TO STORE
        if gold_surface_increment is not None:
            storing_gold_dict = stored_gold_dict            
        else:
            storing_gold_dict = gold_dict            
        
        print("Adding on gold was skipped!")
        print("Storing gold results as a pickle to ensure you could add it later: %s"%(DEFAULT_GOLD_PICKLE))
        ## SHIFTING GOLD GEOMETRY ACCORDINGLY to the center
        storing_gold_dict['total_geom'] += box_dimension/2.
    
        ## DEFINING DEFAULT PICKLE
        path_to_gold_pickle = os.path.join(output_folder, DEFAULT_GOLD_PICKLE)
        ## STORING PICKLE
        pickle_funcs.pickle_results(results=storing_gold_dict,
                                    pickle_path = path_to_gold_pickle)
            
    #### DEBUGGING ####
    ## MOVING NANOPARTICLE TO THE EDGE OF THE BOX!
    # total_geom = total_geom[:] + 3*box_dimension/4. # <-- Shifting entire molecule!
    ### BELOW IS DEBUGGING FOR PBC
    # total_geom = total_geom[:] + box_dimension # <-- Shifting entire molecule! -- at the edge
    ### BELOW IS THE REAL JOB
    total_geom = total_geom[:] + box_dimension/2. # <-- Shifting entire molecule!
    box_dimension = ( box_dimension ).tolist()

    ### OUTPUTTING GRO AND TOPOLOGY FILE                
    # OUTPUTTING GRO FILE
    print_gro_file_combined_res_atom_names(output_gro = output_gro_name, 
                                           output_folder = output_folder, 
                                           resids = total_resids, 
                                           geom = total_geom, 
                                           outputnames = total_outputnames, 
                                           box_dimension = box_dimension
                                           )
    # OUTPUTTING TOPOLOGY FILE
    print_top_file_ligands(forcefield = forcefield,
                           output_top = output_top_name,
                           output_folder = output_folder,
                           ligands = ligands,
                           ligand_nums = sphericalMonolayer.ligandDistribution
                           )
    
    ## PRINTING ITP GOLD FF
    print_itp_file_gold_with_shape(total_gold_atoms = total_gold_atoms,
                                   self_assembled_gold_info = self_assembled_gold_info,
                                   output_folder = output_folder,
                                   forcefield_folder = forcefield_folder,
                                   shape = shape)
    

    
    ## DEFINING PATH TO TOPOLOGY
    path_topology = os.path.join(output_folder, output_top_name)
    
    ## CHECKING IF YOU WANT TO ADD GOLD
    if skip_gold is True:
        if gold_surface_increment is not None and total_gold_atoms > 0:
            skip_gold_in_topology = False
        else:
            skip_gold_in_topology = True
    else:
        skip_gold_in_topology = False
    
    ## CHECKING TOPOLOGY FOR GOLD ATOMS
    check_if_gold_in_topology(path_topology,
                              verbose = verbose,
                              skip_gold = skip_gold_in_topology)


    return self_assembled_gold_info, ligands


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    ## TESTING IS ON
    if testing is True:
        #### INPUTS
        ### DEFINING INPUT GRO FILE
        prep_gold_folder = r"/Volumes/akchew/scratch/nanoparticle_project/prep_system/prep_gold/self_assembly_simulations_nonfrag/spherical" # Preparation gold folder
        # check_server_path(r"R:\scratch\nanoparticle_project\prep_system\prep_gold\self_assembly_simulations\spherical") # Preparation gold folder
        # input_folder = "spherical_5.00_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_bidente_Trial_1"
        input_folder = "spherical_2.00_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_1"
        # "spherical_6.40_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_bidente_Trial_1" 
        # "spherical_5.00_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_bidentate_Trial_1"
        # "hollow_4_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_1" # Input folder (i.e. equilibrated values)
        input_gro_file = "gold_ligand_equil.gro" # Input GRO files
        
        ### DEFINING OUTPUT FILES
        output_gro_name="sam.gro" # GRO File
        output_top_name="sam.top" # Toplogy file
        
        ### DEFINING LOGICALS
        wantPlot = False
        
        ### DEFINING DESIRED LIGANDS
        ligand_names=['LIG103'] # Name of the ligand 'ROT_NS','butanethiol'
        # 'LIG57'
        # 'dodecanethiol'
        ligand_fracs=['1.00'] # '0.5','0.5' # '1.0'
        
        ### DEFINING OUTPUT DIRECTORY INFORMATION
        output_folder=check_server_path(r"/Volumes/akchew/scratch/nanoparticle_project/simulations/20210111-Rerun_GNPs_newGNPs/spherical_300.00_K_GNP286_CHARMM36jul2020_Trial_1")
        prep_folder=check_server_path(r"/Volumes/akchew/scratch/MDLigands/database_ligands/final_structures/charmm36-jul2020.ff")
        # check_server_path(r"R:/scratch/MDLigands/final_ligands/charmm36-jul2017.ff")
        
        ## DEFINING SHAPE
        shape="spherical"
        # 'hollow' # Important for shapes that need adjustments to mass, etc.
        
        ### DEFINING FORCE FIELD FOLDER
        forcefield_folder = "charmm36-jul2020.ff"
        
        ### DEFINING TYPE
        assembly_lig_type = "SCCCC"
        # "C1CCSS1"
        # "single" - means that you have one sulfur atom
        # "C1CCSS1" - means that you have cyclopentane with two sulfurs
        
        ## SELECTING NONE
        gold_surface_increment = 0.3
        
        ## DEFINING SKIP GOLD
        skip_gold = True
        
        ## DEFINING MAXIMUM NUMBER OF LIGANDS
        max_num_ligs = 80
        # 595
        
        # "charmm36-nov2016.ff" # default
        
        ## DEFINING OUPTUT SULFUR
        output_sulfur_index = "nearby_sulfurs.ndx"
        
    ## TESTING IS OFF
    else: 
        ### DEFINING PARSER OPTIONS
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ### GOLD PREPARATION FOLDER
        ## PREPARATION SIMULATION GOLD FOLDER
        parser.add_option("--gfold", dest="prep_gold_folder", action="store", type="string", help="Input gold preparation folder", default=".")
        ## INPUT FOLDER WITHIN THE GOLD SIMULATION
        parser.add_option("--inputgoldfold", dest="input_folder", action="store", type="string", help="Input folder within gold preparation folder", default=".")
        ## INPUT GRO FILE
        parser.add_option("--goldgro", dest="input_gro_file", action="store", type="string", help="Input gro file within gold input", default=".")        
        
        ### OUTPUT FILES / INPUT FILES
        ## OUTPUT GRO FILE
        parser.add_option("-g", "--ogro", dest="output_gro_name", action="store", type="string", help="Output GRO filename", default="double_sam.gro")
        ## OUTPUT TOP FILE
        parser.add_option("-t", "--otop", dest="output_top_name", action="store", type="string", help="Output TOP filename", default="double_sam.top")
        ## OUTPUT FOLDER
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        ## FORCE FIELD TYPE
        parser.add_option("-m", "--ff", dest="forcefield", action="store", type="string", help="Force field, typically a directory", default=".")
        ## PREPARATION FOLDER
        parser.add_option("-i", "--prep", dest="Prep_Folder", action="store", type="string", help="Preparation folder", default=".")
        
        ## SHAPE
        parser.add_option("--geom", dest="shape", type="string",help="Geometric shape of the core (e.g. spherical, hollow, etc.)", default="spherical")
        
        ## ASSEMBLY TYPE
        parser.add_option("--assembly", dest="assembly_lig_type", type="string",help="assembly type desired", default="SCCCC")
        
        ### LIGAND DETAILS
        ## LIGAND NAMES
        parser.add_option("-n", "--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        ## LIGAND FRACTIONS
        parser.add_option("-f", "--fracs", dest="ligand_fracs", action="callback", type="string", callback=get_ligand_args,
                          help="Fraction of monolayer for each ligand specified by ligand_names. For multiple ligands, separate each ligand name by comma (no whitespace)")
        
        ## ADDING OPTION TO SKIP GOLD ADD-ON
        parser.add_option("--skip_gold", dest="skip_gold", action="store_true", default=False, 
                          help="This option will skip gold adding onto gro file")        
        
        ## ADDING OPTIONS TO GOLD SURFACE INCREMENT
        parser.add_option("--gold_surface_increment", dest="gold_surface_increment", 
                          type="string", default=None, 
                          help="This option will add gold surface increments relative to the bulk")   
        
        ## MAXIMUM LIGANDS
        parser.add_option("--max_num_ligs", dest="max_num_ligs", type="string",
                          help="Maximum number of ligands on surface. If this is None, then we will use maxima available", default="None")
        
        ## GETTING SUFUR INDEX
        parser.add_option("--output_sulfur_index", dest="output_sulfur_index", type="string",
                          help="Ouptut sulfur index", default="None")


        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"

        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
    
        if (options.ligand_fracs): # Checking if ligand fraction exists
            ligand_fracs = options.ligand_fracs
        else:
            print("ERROR! Must specify at least one ligand fraction with -f or --fracs")
            exit()

        ## GOLD INPUT FILES
        prep_gold_folder = options.prep_gold_folder
        input_folder = options.input_folder
        input_gro_file = options.input_gro_file
        
        ## OUTPUT / INPUT FILES
        output_gro_name = options.output_gro_name # Output gro file
        output_top_name = options.output_top_name # Output top file
        output_folder = options.output_folder # Output folder (for itps)
        prep_folder = options.Prep_Folder # Preparation folder
        forcefield_folder = options.forcefield # Force field
        
        ## DEFINING ASSEMBLY TYPE
        assembly_lig_type = options.assembly_lig_type
    
        ## SHAPE
        shape = options.shape
        
        ## SKIP GOLD
        skip_gold = options.skip_gold
        gold_surface_increment = options.gold_surface_increment
        
        ## DEFINING MAXIMA
        max_num_ligs = options.max_num_ligs
        
        ## DEFINIG SULFUR
        output_sulfur_index = options.output_sulfur_index
        
        ## CORRECTING
        if max_num_ligs == "None":
            max_num_ligs = None
        else:
            max_num_ligs = int(float(max_num_ligs))
            
        if gold_surface_increment == "None":
            gold_surface_increment = None
        else:
            gold_surface_increment = float(gold_surface_increment)
            
        if output_sulfur_index == "None":
            output_sulfur_index = None
        
    
    ## RUNNING MAIN SCRIPT
    self_assembled_gold_info, ligands = transfer_ligand_builder_main(prep_gold_folder = prep_gold_folder, 
                                                            input_folder = input_folder, 
                                                            input_gro_file = input_gro_file, 
                                                            output_gro_name = output_gro_name, 
                                                            output_top_name = output_top_name, 
                                                            ligand_names = ligand_names,
                                                            ligand_fracs = ligand_fracs, 
                                                            prep_folder = prep_folder, 
                                                            forcefield_folder = forcefield_folder, 
                                                            shape = shape, 
                                                            wantPlot = False,
                                                            assembly_lig_type = assembly_lig_type,
                                                            skip_gold = skip_gold,
                                                            gold_surface_increment = gold_surface_increment,
                                                            max_num_ligs = max_num_ligs,
                                                            output_sulfur_index = output_sulfur_index)
    
    #%%
    '''
    # TESTING
    ## DEFINING FULL PATH TO THE GRO FILE
    full_path_input_gro = check_server_path(os.path.join(prep_gold_folder,
                                                         input_folder,
                                                         input_gro_file))
    
    ### EXTRACTING SELF-ASSEMBLY INFORMATION
    self_assembled_gold_info = self_assembled_gold(full_path_input_gro, 
                                                   wantPlot=wantPlot,
                                                   assembly_lig_type = assembly_lig_type)
    #%%
    
    #  DEBUGGING 
    ### USING LIGAND BUILDER TO CREATE MONOLAYER, BUT WITH THE CORRECTED SULFUR POSITIONS
    ### FINDING FORCE FIELD DETAILS            
    forcefield = FORCEFIELD(forcefield_folder = forcefield_folder)
    
    ## PRINTING
    print("Ligand types:")
    print(', '.join(ligand_names))
    print("Ligand fractions:")
    print(', '.join(ligand_fracs))

    ### CREATING LIGAND CLASSES
    # list of ligands to be loaded
    ligands = []
    # load in all ligands used; output corresponding ITP files too.
    # Loading ligands also loads fragments, etc.
    for cur_lig in ligand_names: # Uses Ligand class
        print('Working on ligand:', cur_lig)
        ligands.append(Ligand( ligand_name = cur_lig, prep_ligand_folder = prep_folder))
    
    #%%
    
    plt.close('all')
    ### CREATING SPHERICAL MONOLAYERS
    sphericalMonolayer = Monolayer_Spherical(ligands = ligands, # Ligands of interest
                                             ligand_fracs = ligand_fracs, # Ligand fractions
                                             radius = self_assembled_gold_info.radius, # Sphere radius of self-assembly
                                             assembly_lig_type = assembly_lig_type,
                                             sulfur_coord = self_assembled_gold_info.near_sulfur_xyz_centered, # xyz coordinates of gold info _centered
                                             center_coord = np.array([0,0,0]) , # self_assembled_gold_info.center_coordinates
                                             debug_mode = True,
                                             max_num_ligs = max_num_ligs,
                                             )
    
    #%%
    
    sphericalMonolayer.monolayer_geom.reshape(
            sphericalMonolayer.num_ligands
            )
    
    
    #%%
    
    radius = sphericalMonolayer.sphere_radius
    center = sphericalMonolayer.center_coord
    
    ## GETTING DISTANCES
    distances = np.linalg.norm(sphericalMonolayer.monolayer_geom - center, axis = 1).reshape(sphericalMonolayer.num_ligands, ligands[0].ligand_num_atoms)
    
    ## FINDING WHEN DISTANCES IS
    less_than_radius = np.where(distances < radius)[0]
    residues_overlap = np.unique(less_than_radius)
    
    print(len(residues_overlap))
    
    '''