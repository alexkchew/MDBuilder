#/usr/bin/env python
'''
ligand_builder_fragments
The main idea in this script is that we are building ligands via fragments. Therefore, we will need a fragment file for each ligand part.
So far, this is used more often in the case of the self-assembly process for the Pool force field


# v1 by Reid Van Lehn, December 2016
# v2 by Alex K. Chew, March 2017
# Based on library of molecular fragments with specified atom/bonding properties,
# generates topology and geometry for ligands that are defined as combinations of
# fragments. Further populates a double self-assembled monolayer system with
# combinations of user-specified ligands in preparation for further simulations.
# Note that this was written for use in Python v2.7.6, which is now legacy.

INPUTS:
    1. num_lig_x: Number of ligands in the x-direction
    2. num_lig_y: Number of ligands in the y-direction
    3. ligand_names: Names of the ligands (as a list)
    4. ligand_fracs: Fraction of ligands (as a list)
    5. output_dir: Where do you want the output file to be in?
    6. output_gro: Where do you want your gro output to be in?
    7. output_top: Where do you want your top output to be in?
    8. layer_separation: Separation between the layers
    9. forcefield: What force field are we using?
    10. prepfolder: where is the preparation folder
    11. wantGold: Do you want gold or not?
    
GLOBAL VARIABLES:
    GOLDITPNAME: name of the gold itp file
    GOLD_DIAMETER_NM: gold diameter in nm
    LIGANDPERNM2: ligand per nm^2
    DEFAULT_POSITION_RESTRAINT: default position restraints on the sulfur and the gold atoms
    
FUNCTIONS:
    extractDataType: function to extract data from an ITP file
    get_ligand_args: gets back ligand arguments (used for interpreting multiple ligands)
    
CLASSES:
    FORCEFIELD: Stores all information about the force field
    Fragment: Creates a fragment
    Ligand: Creates a ligand
    Monolayer: Creates a planar monolayer
    Monolayer_Spherical: Creates spherical monolayer assuming fcc 111 grafting density
    
    
** UPDATES **
171121 - Added functionality to read entire fragment file without doing while looping
171122 - Fixing up functionality of printing "additional" dihedral angles
171122 - Merging spherical ligand builder to current ligand builder
171125 - Debugging spherical ligand builder with planar ligand builder
171130 - Created function to simply define the force field parameters
171222 - Working on spherical monolayer class to accept transfer ligands
180221 - Fixed bug for spherical monolayer rotating without correcting for position
180424 - Updating script to correctly account for monolayer distances between gold and sulfur
'''
#%% 

### IMPORTING MODULES
import numpy as np # Mathematical tool for arrays, etc.
import os

## CORRECTION FOR QUEUING BETWEEN PYTHON 2 & 3
import sys
is_py2 = sys.version[0] == '2'
if is_py2:
    import Queue as queue
else:
    import queue as queue

from optparse import OptionParser # Used to allow commands within command line
from random import shuffle
from MDBuilder.core.forcefield import FORCEFIELD_FRAGMENT as FORCEFIELD
from MDBuilder.core.check_tools import check_server_path as checkPath2Server

### GLOBAL VARIABLES
### DEFAULT GOLD DIAMETER & SULFUR RESTRAINT
# obtain number of ligands from 1/3 number of gold atoms; assuming
# root 3 root R30 hexagonal overlayer (simplification, but fine)
## GOLD PARAMETER
GOLDITPNAME = 'Au_111' # Gold itp name
GOLD_DIAMETER_NM = 0.2884 # in nm; covalent bond diameter <-- 0.2884 nm???
LIGANDPERNM2 = 4.62 # Number of ligands per nm^2 -- for a planar 111 surface, equivalent to ~21.6 A^2/ligand
DEFAULT_POSITION_RESTRAINT ="100000.0"  # "10000.0" # very high to keep sulfur in place
GOLD_SULFUR_CUTOFF = 0.327 # nms (3.27 angstroms)
#################
### FUNCTIONS ###
#################

### FUNCTION TO EXTRACT DATA FROM A CLEAN ITP DATA
def extractDataType( no_comment_data, desired_type ):
    '''
    The purpose of this function is to take your itp file and the desired type (i.e. bonds) and get the information from it. It assumes your itp file has been cleaned of comments
    INPUTS:
        no_comment_data: itp data as a list without comments (semicolons)
        desired_type: Types that you want (i.e. [bonds])
    OUTPUTS:
        DataOfInterest: Data for that type as a list of list
    '''
    # Finding bond index
    IndexOfExtract = no_comment_data.index(desired_type)
    
    # Defining the data we are interested in
    DataOfInterest = []
    currentIndexCheck = IndexOfExtract+1 
    
    # Using while loop to go through the list to see when this thing ends
    while ('[' in no_comment_data[currentIndexCheck]) is False and currentIndexCheck != len(no_comment_data) - 1: # Checks if the file ended
        # Appending to the data set
        DataOfInterest.append(no_comment_data[currentIndexCheck])
        # Incrementing the index
        currentIndexCheck+=1     
        
    # Removing any blanks and then splitting to columns
    DataOfInterest = [ currentLine.split() for currentLine in DataOfInterest if len(currentLine) != 0 ]
    
    return DataOfInterest

###$ OUTPUT FUNCTIONS FOR WHOLE SYSTEM

#######********************** CLASSES FOR SYSTEM COMPONENTS **********************########


############################## FRAGMENT CLASS ##############################
# defines a Fragment, including geometry/atom properties
class Fragment:
    # Variables defined:
    # num_fragment_atoms
    # invert_y
    # fragment geometry (array)
    # fragment atom names (array)
    # fragment atom types (array)
    # fragment atom charges (array)
    # fragment atom masses (array)

    # read in atoms for fragment
    def read_num_atoms(self, file):
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            elif (len(splitline) > 0 and splitline[0] == 'NUM_ATOMS'):
                # record number of atoms as integer
                self.num_fragment_atoms = int(splitline[1])
        # reset file to read again
        file.seek(0)

    # read in bonds for fragment
    def read_num_bonds(self, file):
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            elif (len(splitline) > 0 and splitline[0] == 'NUM_BONDS'):
                # record number of atoms as integer
                self.num_fragment_bonds = int(splitline[1])
        # reset file to read again
        file.seek(0)

    # read in atoms for fragment
    def read_invert_y(self, file):
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            elif (len(splitline) > 0 and splitline[0] == 'INVERT_Y'):
                # record number of atoms as integer
                self.invert_y = int(splitline[1])
        # reset file to read again
        file.seek(0)
        

    # read full fragment file -- looks for each bracket
    def read_fragment_data(self, file):
        '''
        This function reads the fragment file by partitioning it into sub-data values based on brackets '[]'
        INPUTS:
            self: self
            file: fragment file
        OUTPUTS:
            self.fragment_atomtypes - atomtypes of the fragment
            self.fragment_atomnames - atom names of the fragment
            self.fragment_charges - charges of the fragment
            self.fragment_massses - masses of the fragment
            self.fragment_bonds - bond of the fragment
            self.fragment_geom - geometry of the fragment
            self.fragment_dihedrals - dihedrals (additional) of the fragment
        '''
        #Reading current data
        current_data = file.read().splitlines() # Reading the data while splitting the lines
        clean_data = [  eachLine.replace('\t', ' ') for eachLine in current_data ] # Cleaning the data -- may not be necessary sometimes
        # Removing all comments
        no_comment_data =[ eachLine for eachLine in clean_data if not eachLine.startswith("#") ]
        # Finding dihedrals
        allTypes = [ eachLine for eachLine in no_comment_data if '[' in eachLine ]
        # Extracting the data by type
        data_by_type = [ extractDataType( no_comment_data=no_comment_data, desired_type=eachType) for eachType in allTypes ]
        # Creating dictionary for each one
        frag_dict = {}
        for currentIndex in range(len(allTypes)):
            frag_dict[allTypes[currentIndex]] = data_by_type[currentIndex]
            
        ### STORING ATOM TYPES ###
        for i in range(0, self.num_fragment_atoms):
            current_atom_type = frag_dict['[ ATOMTYPES ]'][i]
            # s4 attributes - type, name, charge, mass
            self.fragment_atomtypes.append(current_atom_type[1])
            self.fragment_atomnames.append(current_atom_type[2])
            self.fragment_charges[i] = float(current_atom_type[3])
            self.fragment_masses[i] = float(current_atom_type[4])
            
        ### STORING GEOMETRY ###
        for i in range(0, self.num_fragment_atoms+1): # + 1 saves the next atom
            current_geom = frag_dict['[ GEOMETRY ]'][i]
            # store geometry
            self.fragment_geom[i][0] = float(current_geom[1])
            self.fragment_geom[i][1] = float(current_geom[2])
            self.fragment_geom[i][2] = float(current_geom[3])
        
        ### STORING FRAGMENT BONDS ###
        for i in range(0, self.num_fragment_bonds):
            current_bonds = frag_dict['[ BONDS ]'][i]
            # store bonds
            if (len(current_bonds) > 1):
                self.fragment_bonds[i][0] = int(current_bonds[0])
                self.fragment_bonds[i][1] = int(current_bonds[1])
            else:
                self.fragment_bonds[i][0] = int(current_bonds[0])
                self.fragment_bonds[i][1] = 0
                
        ### STORING DIHEDRALS ###
        try: # See if dihedrals is there
            num_dihedrals=len(frag_dict['[ DIHEDRALS ]'])
            if num_dihedrals > 0: # If true, then we have dihedrals we need to account for
                for current_dihedral_index in range(num_dihedrals):
                    self.fragment_dihedrals.append([ int(x) for x in frag_dict['[ DIHEDRALS ]'][current_dihedral_index]])
        except:
            self.fragment_dihedrals = []
            # print("No additional fragment dihedrals defined")
        return

    # called when object created
    def __init__(self, fragment_filename):        
        ## OPEN FILE
        f = open(fragment_filename, 'r')
        ## READ NUMBER OF ATOMS IN FILE
        self.read_num_atoms(f)
        ## READ NUMBER OF BONDS IN FILE
        self.read_num_bonds(f)
        ## READ INVERT_Y
        self.read_invert_y(f)
        ## CREATING EMPTY LIST  FOR BONDS, MASSES, ETC.
        self.fragment_geom = np.zeros((self.num_fragment_atoms+1, 3), dtype='float')
        self.fragment_bonds = np.zeros((self.num_fragment_bonds, 2), dtype='int')
        self.fragment_atomnames = []
        self.fragment_atomtypes = []
        self.fragment_charges = np.zeros((self.num_fragment_atoms, 1), dtype='float')
        self.fragment_masses = np.zeros((self.num_fragment_atoms, 1), dtype='float')
        self.fragment_dihedrals = [] # May not be available -- if so, let's disregard
        
        ## READING ALL FRAGMENT INFORMATION
        self.read_fragment_data(f)
        ## Closing file
        f.close()

############################## LIGAND CLASS ##############################
# defines a ligand, consisting of multiple fragments
# ligand geometries are created by combining individual fragment geometries
# Ligand bonding/angle/dihedral potentials are inferred from bond connectivity
# by creating a ligand "graph" and performing breadth-first searches
class Ligand:
    '''
    This class defines a ligand entirely (i.e. xyz coordinates, etc.) and outputs an ITP file
    INPUTS:
        lig_file_name: File name of your ligand
        output_folder: Where do you want your output to be?
        forcefield: forcefield class that includes all important information about folders, etc.
        want_position_restraint: [logical]
            True if you want position restraints at the first atom. 
            This is useful for restraining the first sulfur atom. 
    OUTPUTS:
        *** STILL NEED TO INCLUDE OUTPUTS FOR LIGAND CLASS ***
        ITP file with your ligand, etc.
    '''

    # OUTPUTTING ITP FILE FOR GROMACS -READIBLE FILES
    def print_itp_file(self, 
                       ligname, 
                       output_folder, 
                       ligresname, 
                       angleFunc,
                       dihedralFunc,
                       want_position_restraint = True):
        output_itp_name = os.path.join(output_folder, ligname + '.itp' ) # Output .itp file name
        print("Outputting %s"%(output_itp_name))
        with open(output_itp_name, "w") as outputfile:
            # MOLECULE INFO
            # write molecule type info; for GROMOS force field
            outputfile.write("[ moleculetype ]\n")
            outputfile.write("; Name   nrexcl\n")
            #now molecule name + 3 for exclusions
            outputfile.write("%s    3\n\n"%(ligresname))
            # ATOM PROPERTIES
            outputfile.write("[ atoms ]\n")
            outputfile.write(";   nr    type   resnr  residu    atom    cgnr   charge    mass\n") # assume charge, mass already defined by atomtypes
            for i in range(0, self.ligand_num_atoms):
                # Charge groups don't matter for recent
                outputfile.write("%4d %15s  %d %s %s%d %4d  %.3f   %.4f\n"%(i+1, self.ligand_atomtypes[i], 1, self.ligand_name.rjust(10, ' '), self.ligand_atomnames[i].rjust(5, ' '), i+1, i+1, self.ligand_charges[i], self.ligand_masses[i]))
            # BONDS
            outputfile.write("\n[ bonds ]\n")
            outputfile.write(";  ai    aj   funct  ; bond properties inferred from atom types \n")
            for i in range(0, len(self.ligand_bonds)):
                # add 1 for correct indexing
                outputfile.write("%d   %d   1\n"%(self.ligand_bonds[i][0]+1, self.ligand_bonds[i][1]+1))
            # ANGLES
            outputfile.write("\n[ angles ]\n")
            outputfile.write(";  ai    aj    ak  funct  ; angle properties inferred from atom types \n")
            for i in range(0, len(self.ligand_angles)):
                # add 1 for correct indexing
                outputfile.write("%d   %d   %d   %d\n"%(self.ligand_angles[i][0]+1, self.ligand_angles[i][1]+1, self.ligand_angles[i][2]+1, angleFunc)) # 1 for function type
            # DIHEDRALS
            outputfile.write("\n[ dihedrals ]\n")
            outputfile.write(";  ai    aj    ak   al  funct  ; dihedral properties inferred from atom types \n")
            for i in range(0, len(self.ligand_dihedrals)):
                # add 1 for correct indexing
                outputfile.write("%d   %d   %d   %d    %d\n"%(self.ligand_dihedrals[i][0]+1, self.ligand_dihedrals[i][1]+1, self.ligand_dihedrals[i][2]+1, self.ligand_dihedrals[i][3]+1, dihedralFunc)) # 3 for function type
            ## ADDING EXTRA DIHEDRALS IF NECESSARY
            # Checking if there is extra dihedrals
            if np.array(self.ligand_extra_dihedrals).size > 0:
                outputfile.write("; extra dihedral constraints specified from .frag file\n")
                for i in range(0, len(self.ligand_extra_dihedrals)):
                    outputfile.write("%d   %d   %d   %d   %d\n"%(self.ligand_extra_dihedrals[i][0],
                                                            self.ligand_extra_dihedrals[i][1], 
                                                            self.ligand_extra_dihedrals[i][2], 
                                                            self.ligand_extra_dihedrals[i][3],
                                                            self.ligand_extra_dihedrals[i][4], # Dihedral function type
                                                            )) # 1 for function type
                
                
            # pairs
            outputfile.write("\n[ pairs ]\n")
            outputfile.write(";  ai    aj  \n")
            # infer pairs from all dihedrals
            for i in range(0, len(self.ligand_dihedrals)):
                # add 1 for correct indexing
                outputfile.write("%d   %d   1\n"%(self.ligand_dihedrals[i][0]+1, self.ligand_dihedrals[i][3]+1))
                
            # end with simple position restraints on the sulfur atoms
            if want_position_restraint is True:
                outputfile.write("\n; Position restraints for sulfur atoms\n")
                outputfile.write("[ position_restraints ]  \n")
                outputfile.write(" %d  1   %s   %s   %s\n"%(1, DEFAULT_POSITION_RESTRAINT, DEFAULT_POSITION_RESTRAINT, DEFAULT_POSITION_RESTRAINT))
        return

    # READING FORM FILE: FRAGMENT LIST
    def read_fragment_list(self, file, folder, forcefield_folder, fragment_extension):
        fragment_list = []
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            if (len(splitline) > 0 and splitline[1] == 'FRAGMENTS'):
                break
        # record data from next 3 non-commented lines
        for i in range(0, self.num_fragments):
            # read lines, ignoring comments
            while True:
                line=file.readline()
                splitline = line.split()
                if (len(splitline)>0 and splitline[0] != '#'):
                    break
            # add fragment name to list with correct forcefield extension, folder
            cur_fragment = folder + '/' + line.strip('\n\r') + '_' + forcefield_folder + '.' + fragment_extension
            fragment_list.append(cur_fragment)
        file.seek(0)
        return fragment_list

    ## READING FROM FILE: LIGAND NAME -- RESIDUE
    def read_ligname(self, file):
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            elif (len(splitline) > 0 and splitline[0] == 'LIGAND_NAME'):
                # record number of atoms as integer
                self.ligand_name = splitline[1]
        # reset file to read again
        file.seek(0)

    ## READING FROM FILE: NUMBER OF FRAGMENTS
    def read_num_fragments(self, file):
        while True:
            line = file.readline()
            splitline=line.split()
            if not line:
                break
            elif (len(splitline) > 0 and splitline[0] == 'NUM_FRAGMENTS'):
                # record number of atoms as integer
                self.num_fragments = int(splitline[1])
        # reset file to read again
        file.seek(0)

    ## DETERMINING GEOMETRY OF THE LIGAND
    # combines geometry only based on fragments in list
    def combine_fragment_geom(self, fragment_list):
        # set flag for iteratively flipping geometry with respect to y-axis
        invert_y = 1.0
        for i in range(0, self.num_fragments):
            # load current fragment info from file
            cur_fragment = Fragment(fragment_list[i])
            # used to mirror geometries of fragements along chain
            # mirror geometry first if necessary
            cur_fragment_geom = cur_fragment.fragment_geom * [1, invert_y, 1]
            # translate current fragment to place at end of chain
            cur_fragment_geom = cur_fragment_geom + self.ligand_geom[-1]
            # Remove last line of ligand geom (for "next" atom)
            self.ligand_geom = np.delete(self.ligand_geom, -1, 0)
            # add new geometry
            self.ligand_geom = np.append(self.ligand_geom, cur_fragment_geom, axis=0)
            # update y-inversion flag
            invert_y = invert_y * cur_fragment.invert_y
        # delete last line, whicch points to next atom that doesn't exist
        self.ligand_geom = np.delete(self.ligand_geom, -1, 0)
    
    ## DETERMINING ATOM PROPERTIES
    # concatenates atom properties (charge, mass, etc0)
    def combine_fragment_atom_properties(self, fragment_list):
        for i in range(0, self.num_fragments): # Looping through each fragment
            cur_fragment = Fragment(fragment_list[i]) # Defining current fragment
            # Adding any additional dihedral restraints 
            current_extra_dihedral = np.array(cur_fragment.fragment_dihedrals) # Converting to numpy  array
            if current_extra_dihedral.size > 0: # There is extra dihedral angels!
                # Correcting for atom numbers
                current_extra_dihedral[:,0:4] = current_extra_dihedral[:,0:4] + self.ligand_num_atoms
                # Storing the extra dihdrals
                self.ligand_extra_dihedrals.extend(current_extra_dihedral)
            self.ligand_num_atoms += cur_fragment.num_fragment_atoms # Calculates total number of atoms
            self.ligand_atomtypes.extend(cur_fragment.fragment_atomtypes) # Attaching on atomtypes (force field dependent)
            self.ligand_atomnames.extend(cur_fragment.fragment_atomnames) # Attaching on atomnames (i.e. S, C)
            self.ligand_charges = np.append(self.ligand_charges, cur_fragment.fragment_charges) # Attaching on ligand charges
            self.ligand_masses = np.append(self.ligand_masses, cur_fragment.fragment_masses) # Attaching on ligand masses
            
    ## DETERMINING CONNECTIVITY OF ATOMS -- BONDS
    # defines adjacency matrix based on bonds - determines connectivity of atoms
    def make_adjacency_matrix(self, fragment_list):
        # set flag for combining bonds by defining "offset" = to maximum atom index
        cur_bond_offset = 0
        # offset used to determine bonding to previous fragment
        prev_frag_bond_offset = 0
        for i in range(0, self.num_fragments):
            cur_fragment = Fragment(fragment_list[i])
            for j in range(0, cur_fragment.num_fragment_bonds):
                # iterative over all bonds; get single bond from fragment
                cur_bond = cur_fragment.fragment_bonds[j]
                # check if there is a '0' in the array - if so, this bond connects to previous/next fragment
                if (0 in cur_bond):
                    if (j == 0):
                        # connect to previous fragment
                        if (prev_frag_bond_offset > 0):
                            new_bond = cur_bond[0] + cur_bond_offset-1
                            # add symmetrically
                            self.ligand_adjacency_matrix[prev_frag_bond_offset-1][new_bond] = 1
                            self.ligand_adjacency_matrix[new_bond][prev_frag_bond_offset-1] = 1
                        else:
                            print("ERROR: Trying to add bond with no previous fragment bond found")
                    elif (j == cur_fragment.num_fragment_bonds-1):
                        # Save this bond index as the new prev fragment offset
                        prev_frag_bond_offset = cur_bond[0] + cur_bond_offset
                    else:
                        print("ERROR: Unsatisfied bond in centrally located fragment")
                else:
                    # add as a bond; add offset to each
                    # note that I have to make this correct size in order to append
                    new_bond_one = cur_bond[0] + cur_bond_offset-1
                    new_bond_two = cur_bond[1] + cur_bond_offset-1
                    self.ligand_adjacency_matrix[new_bond_one][new_bond_two] = 1
                    self.ligand_adjacency_matrix[new_bond_two][new_bond_one] = 1
            # find largest value in adjacency matrix that is non zero; this is new offset
            for i in range(0, self.ligand_num_atoms):
                for j in range(0, self.ligand_num_atoms):
                    if self.ligand_adjacency_matrix[i][j] > 0 and i >= cur_bond_offset:
                        cur_bond_offset = i+1

    # returns list of all paths to given distance max_distance via breadth-first search
    # of given matrix. Root is the starting point, max-distance is the largest number
    # of edges to traverse during search
    def find_paths_bfs_search(self, matrix, root, max_distance):
        bfs_queue = queue.Queue()
        # put in root element used to initiate search
        bfs_queue.put(root)
        node_distances = np.zeros(self.ligand_num_atoms, dtype='int')
        # list that keeps track of paths
        all_paths=[[root]]
        while (not bfs_queue.empty()):
            # get the top of the queue
            current = bfs_queue.get()
            # iterate across row for this element
            for i in range(0, self.ligand_num_atoms):
                if (matrix[current][i] == 1):
                    # if distance = 0, then distances have not yet been set
                    # so this node hasn't been visited (must check to avoid infinite loops)
                    if (node_distances[i] == 0):
                        # set distance to this node by incrementing distance of parent
                        node_distances[i] = node_distances[current] + 1
                        # If below maximum desired distance, continue search from this child
                        if (node_distances[i] <= max_distance):
                            # adds child; update paths as well
                            bfs_queue.put(i)
                            # for each path currently being tracked, add this node
                            # to all paths that end in parent node
                            for cur_path in all_paths:
                                # append if last entry is "current" and entry not already in this path
                                if cur_path[-1] == current and not i in cur_path:
                                    # need to create actual new entry before appending
                                    new_path = cur_path[:]
                                    new_path.append(i)
                                    all_paths.append(new_path)
        return all_paths


    # Reads in ligand information
    def __init__(self, lig_file_name, output_folder, forcefield,
                 want_position_restraint = True):
        
        ## STORING SELF
        self.want_position_restraint = want_position_restraint
        
        ### DEFINING IMPORTANT FORCE FIELD INFORMATION
        lig_folder = forcefield.ligand_folder # Location of all ligands
        lig_extension = forcefield.ligand_extension # Force field extension
        fragment_folder = forcefield.fragment_folder # Fragment folder
        fragment_ending = forcefield.fragment_ending # Fragment ending for a particular force field
        fragment_extension = forcefield.fragment_extension # fragment extension
        angleFunc = forcefield.angleFunc # Angle function for GROMACS
        dihedralFunc = forcefield.dihedralFunc # Dihedral function for GROMACS
        
        # combine ligand information to read in file
        lig_comboname = os.path.join(lig_folder, lig_file_name + '.' + lig_extension) # Direct flow to ligand file
        
        ## READING LIG FILE
        with open(lig_comboname, 'r') as lig_file:
            # read number of fragemnets and ligand name
            self.read_num_fragments(lig_file)
            self.read_ligname(lig_file)
            # read list of fragments from file
            fragment_list = self.read_fragment_list(lig_file, fragment_folder, fragment_ending, fragment_extension)
        # initialize empty array for all ligand properties; will be
        # generated from fragment properties
        # ATOM PROPERTIES
        self.ligand_num_atoms = 0
        self.ligand_geom = np.zeros((1, 3), dtype='float') # 1 by 3 matrix
        self.ligand_charges = np.zeros((0, 1), dtype='float') # Storage for ligand charges
        self.ligand_masses = np.zeros((0,1), dtype='float') # Storage for masses
        self.ligand_atomtypes = [] # Storage for atomtypes (force field dependent)
        self.ligand_atomnames = [] # Storage for atomnames
        self.ligand_extra_dihedrals = [] # Extra dihedral constraints that is within the fragment
        # read in / combine atom properties
        self.combine_fragment_geom(fragment_list)
        self.combine_fragment_atom_properties(fragment_list)
        # now set up bond properties
        self.ligand_num_bonds = 0
        self.ligand_bonds = np.zeros((0, 2), dtype='int')
        # set up adjacency matrix
        self.ligand_adjacency_matrix = np.zeros((self.ligand_num_atoms, self.ligand_num_atoms), dtype='int')
        # Combine all fragment bond information to create adjacency matrix
        self.make_adjacency_matrix(fragment_list)
        
        print("Adjacency matrix")
        print(self.ligand_adjacency_matrix)
        print("----")

        # get ALL paths, representing all bonds/angles/dihedrals based on their distance
        all_paths = []
        for i in range(0, self.ligand_num_atoms):
            all_paths.extend(self.find_paths_bfs_search(self.ligand_adjacency_matrix, i, 3))
        
        ## PRINTING ALL PATHS
        print("All paths")
        print(all_paths)
        print("---")
        
        # determine all bonds
        self.ligand_bonds = []
        self.ligand_angles = []
        self.ligand_dihedrals = []
        for path in all_paths:
            # check for duplicates and for reversed duplicates via [::-1] syntax
            if len(path) == 2 and not path in self.ligand_bonds  and not path[::-1] in self.ligand_bonds :
                # increment by [1 1] to match correct syntax when outputting
                self.ligand_bonds .append(path)
            elif len(path) == 3 and not path in self.ligand_angles and not path[::-1] in self.ligand_angles:
                self.ligand_angles.append(path)
            elif len(path) == 4 and not path in self.ligand_dihedrals and not path[::-1] in self.ligand_dihedrals:
                self.ligand_dihedrals.append(path)

        # Set height based on maximum z-value (approximately) by finding max of
        # each coordinate and returning only z-coord.
        self.ligand_length = np.amax(self.ligand_geom, axis=0)[2]
        # combine ligand name with atom name /number to create output name for gro file
        self.ligand_outputnames = []
        for i in range(0, self.ligand_num_atoms):
            # convert ligname to 5 characters
            ligname = self.ligand_name.rjust(5, ' ')
            # make atom name + id as well
            atomname = self.ligand_atomnames[i] + str(i+1)
            comboname = ligname + atomname.rjust(5, ' ')
            self.ligand_outputnames.append(comboname)
        # write output file for itp
        # Note: this might get done multiple times right now for multiple monolayers, oh well.
        self.print_itp_file(ligname = lig_file_name, 
                            output_folder = output_folder, 
                            ligresname = self.ligand_name, 
                            angleFunc = angleFunc, 
                            dihedralFunc = dihedralFunc,
                            want_position_restraint = want_position_restraint)
        return

############################## MONOLAYER CLASS ##############################
class Monolayer:
    '''
    builds sqrt 3 x sqrt3 R30 hexagonal overlayer superimposed over gold lattice
    It's just hexagonally close packed with 3x the spacing, basically.
    Definitely a simplification given more recent data on superstuctures
    Orientation is either +1 or -1, and determines z-direction. trans_z
    is added directly to the z-coordinate as well.
    INPUTS:
        forcefield: Force field from forcefield class
        num_lig_x: Number of ligands in the x-direction
        num_lig_y: Number of ligands in the y-direction
        orientation: +1 for pointing up, -1 for pointing down
        trans_z: Layer separation in nm between the two layers
        ligands: Ligand class as a list
        ligand_nums: Number of ligands in each list entry
    OUTPUTS:
        *** STILL NEED TO WORK ON OUTPUTS ***
    '''
    
    # Initializes monolayer with num_lig_x by num_lig_y ligands, oriented in either +1 or -1 direction, offset_y
    # vertically by trans_z, with the list of ligand names denoting the ligands randomly distributed in the monolayer.
    def __init__(self, num_lig_x, num_lig_y, orientation, trans_z, ligands, ligand_nums):
        
        # Create list of all ligand objects, then shuffle to get random positions
        ligand_objects = []
        for i in range(0, len(ligands)):
            cur_lig = ligands[i]
            for j in range(0, int(ligand_nums[i])):
                ligand_objects.append(cur_lig) # Appends different ligands with respect to the number of ligand (e.g. butanethiol, butanethiol, decanethiol, etc.)
        # Randomize list
        shuffle(ligand_objects)

        # generate geometry by combining ligand geometries
        self.monolayer_geom = np.zeros((0, 3), dtype='float')
        # make list of atomnames
        self.monolayer_atomnames = []
        # make list of resids
        self.monolayer_resids = np.zeros((0, 1), dtype='int')
        # list of all ligand output naems for gro file
        self.monolayer_outputnames = []

        # choose geometric parameters based on xsqrt(3) packing for AU (111) plane
        sam_incr_x = 1.5*GOLD_DIAMETER_NM # distance between columns
        sam_incr_y = 1.5*np.tan(np.pi/6)*2*GOLD_DIAMETER_NM # 1.5 diameters * tan 30 degrees * 2 <-- Point far away, if ligand = 2, add this, if xligand = 1, add half of this
        offset_x = 0.5*GOLD_DIAMETER_NM # D/2 <-- Offset assuming gold at center
        offset_y = 0.5*GOLD_DIAMETER_NM*np.tan(np.pi/6) # D / 2 tan 30 <-- Assumes gold at (0,0)
        # first build top layer
        ligand_counter = 1
        # Add ligands to monolayer at random geometric positions, but list so that
        # we have blocks of ligands in gro/itp file. A little bit complicated
        # but massively simplifies the output files for gromacs
        for lig in ligands:
            for lig_x in range(0,num_lig_x):
                for lig_y in range(0,num_lig_y):
                    cur_ligand = ligand_objects[lig_x*num_lig_y + lig_y]
                    # Add to list if this ligand is of correct type, so that
                    # we have blocks of ligands in output eventually.
                    if (cur_ligand == lig):
                        # Define geometry of monolayer based on presumed gold positions
                        trans_x = lig_x*sam_incr_x + offset_x
                        # here we change the offset in alternating rows to achieve hexagonal lattice
                        if (lig_x % 2):
                            trans_y = lig_y*sam_incr_y + offset_y + 0.5*sam_incr_y
                        else:
                            trans_y = lig_y*sam_incr_y + offset_y
                        # multiply all z-dimension geometry by orientation, first
                        lig_geom_orient = cur_ligand.ligand_geom * [1, 1, orientation]
                        # add ligand geometry translated by this amount
                        lig_geom_trans = lig_geom_orient + [trans_x, trans_y, trans_z]
                        # add ligand geometry to monolayer geometry
                        self.monolayer_geom = np.append(self.monolayer_geom, lig_geom_trans, axis=0)
                        # add atomnames as well
                        self.monolayer_atomnames.extend(cur_ligand.ligand_atomnames)
                        # assign resid for each of these atoms
                        self.monolayer_resids = np.append(self.monolayer_resids, ligand_counter*np.ones(cur_ligand.ligand_num_atoms))
                        self.monolayer_outputnames.extend(cur_ligand.ligand_outputnames)
                        ligand_counter += 1
        #self.build_monolayer(num_lig_x, num_lig_y, cur_ligand, orientation, trans_z)
        self.num_monolayer_atoms = np.shape(self.monolayer_geom)[0]
    
    @staticmethod
    def print_top_file(forcefield, output_top, output_folder, num_lig_x, num_lig_y, ligand_names, ligands, ligand_nums, wantGold, GOLDITPNAME, gold_resname):
        '''
        The purpose of this script is to create a topology file
        INPUTS:
            forcefield: forcefield class
            output_top : topology name
            output_folder: folder for topology
            num_lig_x: Number of ligands in the x-direction
            num_lig_y: Number of ligands in the y-direction
            ligand_names: Names of the ligands (list)
            ligands: Ligand class
            ligand_nums: Numbers for the ligands
            wantGold: True if you want gold
            GOLDITPNAME: Name of the gold itp file
            gold_resname: Name of the residue of gold
        OUTPUTS:
            TOPOLOGY FILE
        '''
        output_top_name = output_folder + '/' + output_top
        outputfile = open(output_top_name, "w")
        print("Outputting %s"%(output_top_name))
        # write basic topology information here
        outputfile.write(";  Topology file for %d x %d SAM\n\n"%(num_lig_x, num_lig_y))
        # include statements
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_itp))
        if wantGold==True:
            outputfile.write("#include \"%s\"\n"%(GOLDITPNAME + ".itp"))
        # include all ligand names
        for ligname in ligand_names:
            outputfile.write("#include \"%s.itp\"\n"%(ligname))
        #outputfile.write("#include \"%s\"\n"%(GOLD_ITP_NAME))
        # add water here, although the molecules are added separately
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_water))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_ions))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_methanol))
        outputfile.write("\n[ system ]\n")
        outputfile.write("frame t=1.000 in water\n")
        outputfile.write("\n[ molecules ] \n")
        outputfile.write("; compound   nr_mol\n")
        # assume double monolayer for now; output for each ligand in order
        for j in range(0, 2):
            for i in range(0, len(ligand_names)):
                lig=ligands[i]
                outputfile.write(" %s     %d\n"%(lig.ligand_name, ligand_nums[i]))
        if wantGold == True:
            # Inputting gold atoms
            outputfile.write(" %s     %d\n"%(gold_resname, 1))
        outputfile.close()

    ### PRINTING GRO FILE FUNCTION
    @staticmethod
    def print_gro_file(output_gro, output_folder, num_atoms, resids, geom, outputnames, box_dimension):
        '''
        The purpose of this script is to write the .gro file necessary to run GMX.
        Inputs:
            1. output_gro: Gro file name
            2. output_folder: Directory to gro file
            3. num_atoms: Total number of atoms
            4. resids: Contains list of residue numbers for monolayer
            5. outputnames: Contains list of residue names
            6. geom: Coordinates for ligand system
        '''
        output_gro_name = output_folder + '/' + output_gro # Gives full gro path
        outputfile = open(output_gro_name, "w") # Opens gro file and make it writable
        print("Outputting %s"%(output_gro_name)) # Let user know what is being outputted
        # heading
        outputfile.write("GRO file for SAM\n") # Title for .gro, does not affect anything
        outputfile.write("%d\n"%(num_atoms)) # Total number of atoms, important! Must match topology file
        # will have to modify eventually but fine for now.
        atomNum=1; # Start at one
        for bead_index in range(0, num_atoms):
            outputfile.write("%5d%10s%5d%8.3f%8.3f%8.3f\n"%(resids[bead_index], outputnames[bead_index], atomNum, geom[bead_index][0], geom[bead_index][1], geom[bead_index][2]))
            atomNum +=1
    
            # Make sure that AtomNum is not greater than 99999, GROMACS cannot read more than 5 digits
            if atomNum > 99999:
                atomNum=0 # resetting atomNum
    
        # finish with box dimensions
        outputfile.write("%f %f %f\n"%(box_dimension[0], box_dimension[1], box_dimension[2]))
        outputfile.close()

############################## SPHERICAL_MONOLAYER CLASS ##############################
## FOR SPHERICAL MONOLAYERS
class Monolayer_Spherical:
    '''
    Written by: Alex K. Chew
    Builds spherical nanoparticle.
    INPUTS:
        ligands, expecting a list of classes (From ligands class)
        ligand_fracs: Fraction of ligands.(Number between 0 and 1) The larger fraction occurs for the first ligand
        sphereRadius: Total sphere radius in nms
        sulfur_coord: Coordinates of sulfur atoms (if available) -- IF NOT AVAILABLE -- THIS WILL ASSUME PERFECTLY SPHERICAL        
    OUTPUTS:
        Class Type: Monolayer_Spherical, Attributes:
            monolayer_geom: N x 3 array containing the entire coordinate structure file for the gro file <-- From ligand_geom
            monolayer_atomname: N x 1 array containing all atoms (e.g. S, C, ....) <-- From ligand_atomnames
            monolayer_resids: N x 1 array containing residue numbers (e.g. 1, 1, 1, 1, ... ) <-- For each molecule, unique residue number! <-- array created given you know total number of ligands and size of ligand
            monolayer_outputnames: N x 1 string array containing 3 letter residue code and the atom type (e.g. ['BUT C3', etc.]) <-- uses ligand_outputnames
            num_monolayer_atoms: Total number of atoms <-- outputted on top of gro file (second line)
    Miscellaneous definitions:
        print_gro_file - prints gro file
        print_top_file - prints top file
    '''
    
    ### INITIALIZING
    def __init__(self, ligands, ligand_fracs,  sphereRadius = None, sulfur_coord = None):
        '''
        INPUTS:
            ligands: From ligand class
            ligand_fracs: Fraction of ligands.(Number between 0 and 1) The larger fraction occurs for the first ligand
            sphereRadius: Total sphere radius in nms
            sulfur_coord: Coordinates of sulfur atoms (if available)
        '''
        print("\n---- CLASS: MONOLAYER_SPHERICAL ----")
        ### DEFINING VARIABLES INTO SELF
        self.sphere_radius = sphereRadius
        
        ### PRE-DEFINING GEOMETRY, ATOMNAMES, ETC.
        # generate geometry by combining ligand geometries
        self.monolayer_geom = np.empty((0, 3), dtype='float')
        # make list of atomnames
        self.monolayer_atomnames = []
        # make list of resids
        self.monolayer_resids = []
        # list of all ligand output names for gro file
        self.monolayer_outputnames = []
        # total number of atoms
        self.num_monolayer_atoms = 0
        
        ## CHECKING IF THERE IS ANY PRE-DEFINED SULFUR COORDINATES. IF SO, THEN DROP THE RANDOMIZATION SPHERICAL LIGAND SCRIPT
        if sulfur_coord is None:
            ## CALCULATING TOTAL LIGANDS
            self.monolayer_calc_total_lig(LIGANDPERNM2)
            # First, we need spherical geometry, which we can generate using Brownian motion
            self.initialSphericalCoord = self.monolayer_initialize_spherical(self.num_ligands)
            ## PRINTING
            print("Building spherical monolayer (uniform) with the ligands")
            print("Nanoparticle radius: %f nm"%(self.sphere_radius))
            print("Total ligands per nm^2 is %f, totalling up to %d ligands"%(LIGANDPERNM2, self.num_ligands))
        ## SULFUR COORDINATES ARE AVAILABLE    
        else:
            print("--- USING PRE-DEFINED SULFUR ATOMS ---")
            ## CALCULATING TOTAL LIGANDS
            self.num_ligands = len(sulfur_coord) # Each sulfur atom is another ligand
            ## FINDING SPHERICAL GEOMETRY
            self.initialSphericalCoord = sulfur_coord[:] # Copying sulfur coordinates
        
        ## FINDING CENTER OF THE SULFURS
        self.center_coord = np.mean(self.initialSphericalCoord,axis=0)                                              
        ## FINDING GEOMETRY BASED ON CENTER (ZEROED)
        # GEOMETRY WITH THE SULFURS BASED ON (0,0,0) <-- center
        self.initial_spherical_coord_center_zero = self.initialSphericalCoord - self.center_coord

        # Calculate total number of ligands for each fraction
        self.ligandDistribution = []
        for currentFracCount in range(len(ligand_fracs)):
            self.ligandDistribution.append( int(self.num_ligands*float(ligand_fracs[currentFracCount] ) ) )        
        # Correct if we have too many ligands!
        while np.sum(self.ligandDistribution) != self.num_ligands:
            if np.sum(self.ligandDistribution) < self.num_ligands:
                self.ligandDistribution[-1] += 1
            else: # Greater than total number
                self.ligandDistribution[-1] -= 1    
            
        ## CREATING RANDOMIZED SPOTS
        availableSpots =  np.arange(self.num_ligands)
        ## RANDOMIZE IF NON-UNIFORM LIGAND FRACTIONS
        if len(ligand_fracs) != 1: # Indicates multiple ligand fractions
            np.random.shuffle(availableSpots) # Shuffles spots
        
        ## CREATING COUNTER
        sphericalSpotCounter = 0 # Counts the total spots
        upToSpherical = 0 # First up to spherical point
        ligandcounter = 0 # Counts current ligand
        totalResidueLigandCount = 1 # For counting residues
        
        ## LOOPING THROUGH EACH LIGAND
        for lig in ligands:
            # First start by generating a random number to place my first ligand
            randInteger = availableSpots[sphericalSpotCounter]
        
            # Then, I need to place my ligand on the specified random integer. 
            # At the same time, I need to rotate the molecule to correctly align with the chosen direction
            # Directional vector of the ligand
            directionalVector = np.average(lig.ligand_geom,axis=0) # Average along the rows
        
            ## RE-SCALING DIRECTIONAL VECTOR OF THE LIGAND ACCORDING TO RADIUS
            from make_spherical import return_spherical 
            fixedDirectionalVector = return_spherical(directionalVector[:], radius=self.sphere_radius)
        
            # Selecting a random directional
            randDirection = self.initial_spherical_coord_center_zero[randInteger]
        
            # Rotating entire molecule to start off at this new random direction
            from make_spherical import findRotatedCoordinates, angle_between
            initialGeom = findRotatedCoordinates( Position1 = fixedDirectionalVector,
                                                 Position2 = randDirection,
                                                 Angle = angle_between(fixedDirectionalVector, randDirection ),
                                                  LineData = lig.ligand_geom
                                                  )
            # Translating the ligand to the surface of sphere
            sulfurIndex = lig.ligand_atomnames.index('S') # Find sulfur index ** FIRST INDEX **
            translationVector = self.initial_spherical_coord_center_zero[randInteger] - initialGeom[sulfurIndex]
            self.translatedGeom = initialGeom[:] + translationVector[:] # Brings ligand to the surface you care about
        
            # Now, we can find random integers to selectively place our ligands
            # Start by creating new spherical position array
            upToSpherical += self.ligandDistribution[ligandcounter]
            self.sphericalPositions = np.array([ self.initial_spherical_coord_center_zero[x] for x in availableSpots[sphericalSpotCounter:upToSpherical] ] )

            # Creating geometry with rotations
            geometrySpherical = self.monolayer_rotate_spherical(ReferenceIndex=0, # Referenced at 0
                                                                  sphericalCoord=self.sphericalPositions,
                                                                  LigandData=self.translatedGeom,
                                                                  sulfur_index = sulfurIndex)
            
            ## ADDING BACK VECTOR TO GET THE CORRECT CENTER
            geometrySpherical = geometrySpherical + self.center_coord
            
            ## APPENDING TO MONOLAYER GEOMETRY
            self.monolayer_geom = np.append( self.monolayer_geom, geometrySpherical, axis=0 ) # Appending to monolayer_geom                        
                                           
            # Fixing other variables, like atom names, number of atoms, etc.
            self.num_monolayer_atoms += len(geometrySpherical)
            self.monolayer_atomnames.extend(lig.ligand_atomnames * self.ligandDistribution[ligandcounter]) # S1, ...
            self.monolayer_outputnames.extend(lig.ligand_outputnames * self.ligandDistribution[ligandcounter]) # String list ' RES C1 ' etc.
        
            # Creating residues
            for currentLigand in range(totalResidueLigandCount, upToSpherical+1): # Starting at 1
                newResidue = np.ones( (1, lig.ligand_num_atoms ), dtype=int ) * currentLigand
                self.monolayer_resids.extend(newResidue)
            
            # Updating ligand counter
            totalResidueLigandCount += self.ligandDistribution[ligandcounter]
            sphericalSpotCounter += self.ligandDistribution[ligandcounter]
            ligandcounter += 1
            
        # Fixing residues according to output GRO format
        self.monolayer_resids =  np.concatenate(self.monolayer_resids).tolist()
    
    
    # output in GRO format (.gro)
    @staticmethod
    def print_gro_file(output_gro, output_folder, num_atoms, resids, geom, outputnames, box_dimension):
        '''
        The purpose of this script is to write the .gro file necessary to run GMX.
        Inputs:
            1. output_gro: Gro file name
            2. output_folder: Directory to gro file
            3. num_atoms: Total number of atoms
            4. resids: Contains list of residue numbers for monolayer
            5. outputnames: Contains list of residue names
            6. geom: Coordinates for ligand system
        '''
        output_gro_name = output_folder + '/' + output_gro # Gives full gro path
        outputfile = open(output_gro_name, "w") # Opens gro file and make it writable
        print("Outputting %s"%(output_gro_name))  # Let user know what is being outputted
        # heading
        outputfile.write("GRO file for SAM\n") # Title for .gro, does not affect anything
        outputfile.write("%d\n"%(num_atoms)) # Total number of atoms, important! Must match topology file
        # will have to modify eventually but fine for now.
        atomNum=1; # Start at one
        for bead_index in range(0, num_atoms):
            outputfile.write("%5d%-10s%5d%8.3f%8.3f%8.3f\n"%(resids[bead_index], outputnames[bead_index], atomNum, geom[bead_index][0], geom[bead_index][1], geom[bead_index][2]))
            atomNum +=1
            
            # Make sure that AtomNum is not greater than 99999, GROMACS cannot read more than 5 digits
            if atomNum > 99999:
                atomNum=0 # resetting atomNum
                
        # finish with box dimensions
        outputfile.write("%f %f %f\n"%(box_dimension[0], box_dimension[1], box_dimension[2]))
        outputfile.close()
    
    # output top file using ligand names, etc.
    #def print_top_file(output_top, output_folder, num_lig_x, num_lig_y, ligand_names, ligands, ligand_nums):
    @staticmethod
    def print_top_file(forcefield, output_top, output_folder, ligand_names, ligands, ligand_nums):
        '''
        The purpose of this script is to create a topology file
        Inputs:
            1. output_top: Output topology file
            2. output_folder: Folder where topology is at
            3. ligand_names: Names of ligands
            4. ligands: From ligand class
            5. ligand_nums: Total number of ligands
        '''
        output_top_name = output_folder + '/' + output_top
        outputfile = open(output_top_name, "w")
        print("Outputting %s"%(output_top_name))
        # write basic topology information here
        outputfile.write(";  Topology file for spherical SAM \n")
        # include statements
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_itp))
        # include all ligand names
        for ligname in ligand_names:
            outputfile.write("#include \"%s.itp\"\n"%(ligname))
        #outputfile.write("#include \"%s\"\n"%(GOLD_ITP_NAME))
        # add water here, although the molecules are added separately
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_water))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_ions))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_methanol))
        outputfile.write("\n[ system ]\n")
        outputfile.write("frame t=1.000 in water\n")
        outputfile.write("\n[ molecules ] \n")
        outputfile.write("; compound   nr_mol\n")
        
        # Adding total number of ligands to the [molecules] section
    #    outputfile.write( " %s     %d\n"%(ligands.ligand_name, ligand_nums) )
        # assume double monolayer for now; output for each ligand in order
    
        for i in range(0, len(ligand_names)):
            lig=ligands[i]
            outputfile.write(" %s     %d\n"%(lig.ligand_name, ligand_nums[i]) )
        
        outputfile.close()
    
    def monolayer_initialize_spherical(self, totalNumLigands):
        '''
        This function takes functions from "make_spherical.py" and creates a sphere
        INPUTS:
            totalNumLigands: Total number of ligands
        OUTPUTS:
            coordinates: coordinates in N x 3 array
        '''
        from make_spherical import makeSphericalCoord
        return makeSphericalCoord( sphereRad = self.sphere_radius, numAtoms = totalNumLigands )
        
    def monolayer_rotate_spherical(self, ReferenceIndex, sphericalCoord, LigandData, sulfur_index):
        '''
        This function takes functions from "make_spherial.py" to rotate the ligand.
        Inputs:
            1. ReferenceIndex: Which point on the sphere do you want to start from?
            2. sphericalCoord: Coordinates of all initial spheres
            3. LigandData: Coordinates for the initial ligand (should already be correctly translated)
            
        '''
        from make_spherical import findAllRotatedCoord
        
        return findAllRotatedCoord( ReferenceIndex, sphericalCoord, LigandData, sulfur_index )

    ### FUNCTION TO CALCULATE TOTAL LIGANDS GIVEN RADIUS AND LIGAND PER NM^2
    def monolayer_calc_total_lig(self, LIGANDPERNM2):
        '''
        INPUTS:
            self: self class
            LIGANDPERNM2: Number of ligands per nm^2
        OUTPUTS:
            self.num_ligands: Number of ligands
        '''   
        self.num_ligands = int(np.ceil(4 * np.pi * self.sphere_radius**2 * LIGANDPERNM2 )) # Rounding up
        return
        
# Callback function for interpreting ligand
def get_ligand_args(option, opt_str, value, parser):
    setattr(parser.values, option.dest, value.split(','))

#%% MAIN SCRIPT
if __name__ == "__main__": # ALLOWS TESTING OF THE SCRIPT

    # Send user some output
    print('')
    print('**** RUNNING LIGAND BUILDER ****')

    # Turn test on or off
    testing = False # False if you're running ligand builder!!!!

    ### CHECK IF YOU ARE TESTING
    if testing == False:
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        #### DEFINING PARSER OPTIONS
        ### MONOLAYER TYPE
        parser.add_option('-z',"--type", dest="monolayer_type", action="store", type="string", 
                          help="String with the type of monolayer you want (planar, spherical, etc.)")
        
        ### OUTPUT FILES / INPUT FILES
        ## OUTPUT GRO FILE
        parser.add_option("-g", "--ogro", dest="output_gro_name", action="store", type="string", help="Output GRO filename", default="double_sam.gro")
        ## OUTPUT TOP FILE
        parser.add_option("-t", "--otop", dest="output_top_name", action="store", type="string", help="Output TOP filename", default="double_sam.top")
        ## OUTPUT FOLDER
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        ## FORCE FIELD TYPE
        parser.add_option("-m", "--ff", dest="forcefield", action="store", type="string", help="Force field, typically a directory", default=".")
        ## PREPARATION FOLDER
        parser.add_option("-i", "--prep", dest="Prep_Folder", action="store", type="string", help="Preparation folder", default=".")

        ### LIGAND DETAILS
        ## LIGAND NAMES
        parser.add_option("-n", "--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        ## LIGAND FRACTIONS
        parser.add_option("-f", "--fracs", dest="ligand_fracs", action="callback", type="string", callback=get_ligand_args,
                          help="Fraction of monolayer for each ligand specified by ligand_names. For multiple ligands, separate each ligand name by comma (no whitespace)")
        
        ### PLANAR PARSER OPTIONS
        ## NUMBER OF LIGANDS IN THE X AND Y DIRECTION
        parser.add_option("-x", "--ligx", dest="num_lig_x", action="store", type="int", help="Number of ligands in x-dimension on SAM", default="8")
        parser.add_option("-y", "--ligy", dest="num_lig_y", action="store", type="int", help="Number of ligands in y-dimension on SAM", default="7")
        ## LIGAND SEPARATION DISTANCE (nms)
        parser.add_option("-s", "--sep", dest="layer_separation", action="store", type="string", help="Separation in nm between two monolayers", default=None)
        ## GOLD FORCE FIELDS
        parser.add_option("-a", "--gold", dest="wantGold", action="store", type="string", help="True if you want gold", default=".")
        parser.add_option("-v", "--vir", dest="wantVirtualSites", action="store", type="string", help="True Or False, Want Virtual Sites?", default=".")
        parser.add_option("-p", "--pol", dest="wantPolarization", action="store", type="string", help="True Or False, Want Polarization (Rigid rod dipole)?", default=".")
        # Interface Force field
        parser.add_option("-w", "--int", dest="wantinterfaceff", action="store", type="string", help="True Or False, Want Interface Force Field Parameters?", default=".") 
        
        ### SPHERICAL PARSER OPTIONS
        ## SPHERE RADIUS
        parser.add_option("-r", "--rad", dest="sphereRadius", action="store", type="int", help="Radius in nm of your nanoparticle", default="2")
        
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        #### GETTING ARGUMENTS
        ## MONOLAYER TYPE
        monolayer_type=options.monolayer_type
        
        ## OUTPUT / INPUT FILES
        output_gro_name = options.output_gro_name # Output gro file
        output_top_name = options.output_top_name # Output top file
        output_folder = options.output_folder # Output folder (for itps)
        prep_folder = options.Prep_Folder # Preparation folder
        forcefield_folder = options.forcefield # Force field
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
    
        if (options.ligand_fracs): # Checking if ligand fraction exists
            ligand_fracs = options.ligand_fracs
        else:
            print("ERROR! Must specify at least one ligand fraction with -f or --fracs")
            exit()
            
        #### CHECKING CONSISTENCY
        # ensure consistency of output for number of ligands and fractions specified
        if (len(ligand_fracs) != len(ligand_names)):
            print("ERROR: Different number of ligand names/fractions specified")
            exit()               
        # ensure fractions correctly sum
        frac_sum = 0.0
        for i in range(0, len(ligand_fracs)):
            frac_sum+=float(ligand_fracs[i])
        if (frac_sum > 1.01 or frac_sum < 0.99):
            print("ERROR: Ligand fractions do not sum to 1.0")
            exit()
        # Checking force fields
        possibleForceFields = [ 'opls-aa', 'charmm36-nov2016.ff']
        if forcefield_folder not in possibleForceFields:
            print("Error! Force field selection is not one of the following: %s"%(str(possibleForceFields)))
            exit()
        
        #### PLANAR MONOLAYER TYPE INPUT
        if monolayer_type == "planar":
            # assign input parameters
            num_lig_x = options.num_lig_x
            num_lig_y = options.num_lig_y
            
            #### EXTRACTING ADDITIONAL ARGUMENTS
            try:
                layer_separation = float(options.layer_separation) # Layer separation
            except:
                layer_separation = None
            wantGold=bool(options.wantGold=="True") # True if you want gold!
            wantVirtualSites=bool(options.wantVirtualSites=="True") # True if you want virtual sites
            wantPolarization=bool(options.wantPolarization=="True") # True if you want rigid rod dipoles
            wantinterfaceff=bool(options.wantinterfaceff=="True") # True if you want interface force field
    
            #### CHECKING INPUT CORRECTNESS
            # check output for correctness
            if num_lig_x % 2: # Checking if num_lig_x is an even number
                print("WARNING! num_lig_x should be even for proper treatment of PBCs")
            # Check gold
            if wantGold == False:
                if wantVirtualSites == True or wantPolarization == True:
                    print("Error, how can there be no gold with virtual sites and polarization???")
                    print("Stopping here!")
                    exit()
        #### SPHERICAL MONOLAYER TYPE INPUT
        elif monolayer_type == "spherical":            
            #### EXTRACTING ADDITIONAL ARGUMENTS
            sphereRadius = options.sphereRadius # Sphere radius
            
        else:
            print("Error, no monolayer type specified! Stopping here!")
            exit()
            
    ### DEBUGGING PARAMETERS
    else: # Testing is on!
        # *-*- Testing parameters *-*- #
        ### DEFINING DIRECTORY STRUCTURE
        prep_folder=checkPath2Server( r'R:\scratch\LigandBuilder\Prep_System' )
        
        # ligand names to load in ligand_builder; see options in ligands folder
#        ligand_names=["butanethiol", "decanethiol"]
        ligand_names=["butanethiol"]
        # specifies ligand fractions in mixed monolayer
#        ligand_fracs=['0.5','0.5']
        ligand_fracs=['1.0']

        # number of rows/columns of ligands in SAM; determines box size; x-value should be even
        num_lig_x=8
        num_lig_y=8
        
        # Defining layer separation
        layer_separation = None # nms

        # size of z-dimension of water box (in nm) to add on top of SAM
        # Since each box added has same cross-sectional area, can control volume simply through ratio of the two z-dimensions.
        water_box_z=2.0 # nm of water in box
        methanol_box_z=3.0 # nm of methanol in box

        # Force field
        forcefield_folder = "charmm36-nov2016.ff"

        # Output Directory & Files
        output_top="sam_8x7_6C_amine_methanol.top" # Topology file
        output_gro_name="sam_8x7_6C_amine_methanol.gro" # GRO File
        output_top_name="sam_8x7_6C_amine_methanol.top"
        output_folder=checkPath2Server( r"R:\scratch\LigandBuilder/Simulations/testFolder" )

        # Want Gold?
        wantGold=True # True if you want gold!
        wantVirtualSites=False # Virtual sites, True if you want
        wantPolarization=False # Polarization Sites, True if you want
        wantinterfaceff = True
        
        ## DEFINING MONOLAYER TYPE
        monolayer_type = 'planar' # spherical
        
        # SPHERE RADIUS
        sphereRadius = 1
        
    ########################################
    #### MAIN SCRIPT FOR LIGAND BUILDER ####
    ########################################
    
    ### FINDING FORCE FIELD DETAILS            
    forcefield = FORCEFIELD(prep_folder = prep_folder,
                            forcefield_folder = forcefield_folder
                            )
    

    ## PRINTING
    print("Ligand types:")
    print(', '.join(ligand_names))
    print("Ligand fractions:")
    print(', '.join(ligand_fracs))
    
    ################################
    ### CREATING LIGAND CLASSES ####
    ################################
    # set number of ligands based on input
    num_ligand_types = len(ligand_names)
    ### LOADING LIGANDS AND FRAGMENTS
    # list of ligands to be loaded
    ligands = []
    # load in all ligands used; output corresponding ITP files too.
    for cur_lig in ligand_names: # Uses Ligand class
        print('Working on ligand:', cur_lig)
        ligands.append(Ligand(cur_lig, output_folder, forcefield))

    #######################
    ### MONOLAYER TYPE ###
    #######################
    if monolayer_type == "planar":
        
        ## PRINTING
        print("Building double planar monolayer with the ligands")
        print(" SAM size: %d by %d"%(num_lig_x, num_lig_y))
        
        ### INTRODUCTION OF GOLD
        ## Currently hard coded for one single monolayer -- still working on it!
        if wantGold == True:
            print("*** BUILDING GOLD LATTICE ***")
            # Now inserting gold atoms
            from make_gold_planar_111 import au_111_expansion_itp_CHARMM_GolP #imports function for expanding 111 facet
    
            gold_resname, goldCoordinates, atomname_list = au_111_expansion_itp_CHARMM_GolP(num_x_ligands =3 + int( 3/2.*(num_lig_x-2) ) - 1,
                                                                                             num_y_ligands = num_lig_y - 1,
                                                                                             num_z_spacing=0,
                                                                                             output_folder= output_folder,
                                                                                             molecule_name = GOLDITPNAME,
                                                                                             wantVirtualSites=wantVirtualSites,
                                                                                             wantPolarization=wantPolarization,
                                                                                             forcefield = forcefield_folder,
                                                                                             wantinterfaceff = wantinterfaceff)
            ## FINDING THICKNESS OF THE MONOLAYER
            gold_geom_z_min = np.amin(goldCoordinates, axis=0)[2] # finds minimum of each column and outputs z-minimum
            gold_geom_z_max = np.amax(goldCoordinates, axis=0)[2] # finds maximum
            gold_thickness = gold_geom_z_max - gold_geom_z_min
            print("Gold thickness: %.3f nm"%(gold_thickness))
            
            ## IF NO LAYER THICKNESS IS SPECIFIED, WE WILL SIMPLY RUN THIS ASSUMING BOND LENGTHS AND GOLD DISTANCES
            if layer_separation is None:
                layer_separation = gold_thickness + 2 * GOLD_SULFUR_CUTOFF
                ## BOTTOM MONOLAYER --- GOLD --- TOP MONOLAYER
                ## DISTANCE OF GOLD-SULFUR MUST BE ACCOUNTED TWICE
            print("Layer separation between monolayers: %.3f nm"%(layer_separation))
            
        ### FIND THE TOTAL NUMBER OF LIGANDS THAT WILL BE ADDED
        # calculate number of ligands of each type
        tot_num_ligs = num_lig_x*num_lig_y
        ligand_nums = []
        # count number of each ligand
        for i in range(0, len(ligand_fracs)):
            ligand_nums.append(round(tot_num_ligs * float(ligand_fracs[i])))
            print("  Adding %f %s ligands to monolayers"%(round(tot_num_ligs * float(ligand_fracs[i])), ligand_names[i]))
    
        ### CREATING TWO MONOLAYERS
        top_monolayer = Monolayer(num_lig_x, num_lig_y, 1, layer_separation, ligands, ligand_nums) # Creates class for top monolayer
        bottom_monolayer = Monolayer(num_lig_x, num_lig_y, -1, 0.0, ligands, ligand_nums) # Creates class for bottom monolayer
    
        ## CONCATENATE ALL MONOLAYER PROPERTIES TOGETHER
        total_atoms = top_monolayer.num_monolayer_atoms + bottom_monolayer.num_monolayer_atoms # sums all the atoms in the top and bottom monolayer
        # need to offset resids in bottom monolayer (arbitrarily chosen; could be top monolayer instead)
        top_resids = top_monolayer.monolayer_resids
        max_resid = np.amax(top_resids) # Maximum numbere of residues for the top
        bot_resids = bottom_monolayer.monolayer_resids + max_resid
        total_resids = np.append(top_resids, bot_resids) # Putting residue List together
    
        ## FIXING TOP LAYER GEOMETRY TO MATCH THAT OF GOLD
        offset_x = GOLD_DIAMETER_NM*np.tan(np.pi/6) # Offsetting x because of the crystal gold structure
        offset_y = 0.5*  GOLD_DIAMETER_NM * np.tan(np.pi/3)*0.5 # Offsetting y because of crystal structure D/2 tan 60
        top_monolayer.monolayer_geom = top_monolayer.monolayer_geom + np.array( [0,  offset_y , 0] )# + np.array( [offset_x, 0, 0] ) +
        
        ### GETTING TOTAL GEOMETRY AND OUTPUT NAMES
        total_geom = np.append(top_monolayer.monolayer_geom, bottom_monolayer.monolayer_geom, axis=0) # Adding top and  bottom monolayer residues
        total_outputnames = top_monolayer.monolayer_outputnames + bottom_monolayer.monolayer_outputnames # Adding top and bottom RESIDUE name and atom type
    
        ### FINDING BOX SIZE BASED ON GEOMETRY
        box_dimension = np.zeros(3, dtype='float')
        # set x/y dimensions based on number of ligands and presumed gold diameter
        box_dimension[0] = num_lig_x * 1.5 * GOLD_DIAMETER_NM
        box_dimension[1] = num_lig_y * GOLD_DIAMETER_NM * np.tan(np.pi/6)*1.5*2
        #For z-dimension, take min/max, and adjust geometry so all atoms are within box.
        geom_z_min = np.amin(total_geom, axis=0)[2] # finds minimum of each column and outputs z-minimum
        geom_z_max = np.amax(total_geom, axis=0)[2] # finds maximum
        # could pad with gold diameter, but no real reason to.
        box_dimension[2] = geom_z_max - geom_z_min
        # adjust all positions now
        total_geom = total_geom - [0.0, 0.0, geom_z_min]
    
        if wantGold == True:
            # goldThickness = 4.7091001410/10.0 # nm
            # Shifting coordinates
            shiftedGoldCoordinates = np.array(goldCoordinates[:]) - [0.0, 0.0, geom_z_min] + [0.0, 0.0, GOLD_SULFUR_CUTOFF] # Bringing gold up, sulfur  bond, GOLD_DIAMETER_NM--offsetting  + GOLD_DIAMETER_NM/4.0
            totalGoldAtoms = len(shiftedGoldCoordinates)
    
            # Adding to total atoms
            total_atoms = total_atoms +  totalGoldAtoms # Adding total gold atoms
    
            # Adding to total residues
            currentResidueNumber = total_resids[-1] + 1 # new residue number
            goldArray = np.array( [currentResidueNumber] * totalGoldAtoms )
            total_resids = np.append(total_resids,goldArray )
    
            # Adding to geometry
            total_geom = np.append(total_geom[:], shiftedGoldCoordinates[:], axis=0)
    
            # Adding to output name
            GoldOutputName = []
            for currentGoldAtom in range(0, len(atomname_list)):
                GoldOutputName.append( "%5s%5s" %(gold_resname, str(atomname_list[currentGoldAtom]) ) )
            total_outputnames = np.append(total_outputnames[:], GoldOutputName )
        else:
            gold_resname=None
        
        ### PRINTING GRO / TOP FILE
        # output gro file with system geometry
        Monolayer.print_gro_file(output_gro_name, output_folder, total_atoms, total_resids, total_geom, total_outputnames, box_dimension)
        # Output topology file too
        Monolayer.print_top_file(forcefield, output_top_name, output_folder, num_lig_x, num_lig_y, ligand_names, ligands, ligand_nums, wantGold, GOLDITPNAME, gold_resname )
            
    elif monolayer_type == "spherical":
        
        ### CREATING SPHERICAL MONOLAYERS
        sphericalMonolayer = Monolayer_Spherical(ligands = ligands, sphereRadius = sphereRadius, ligand_fracs = ligand_fracs)
        
        ## Identifying important variables to output
        total_atoms = sphericalMonolayer.num_monolayer_atoms
        total_resids = sphericalMonolayer.monolayer_resids
        total_geom = sphericalMonolayer.monolayer_geom
        total_outputnames = sphericalMonolayer.monolayer_outputnames
    
        # next, define box size based on geometry. For spherical, it will be a cubic shape determined by maximum length from the center
        # Maximum length
        maxRadius = np.max(np.linalg.norm(sphericalMonolayer.monolayer_geom, axis=1))
        maxDiameter = maxRadius * 2 # Diameter ( length of box )
        fixedDiameter = maxDiameter + 1 # Add one to PBC just in case
        
        # Shift entire nanoparticle up so it is within the box
        box_dimension = np.ones( 3 ) * fixedDiameter
        total_geom = total_geom[:] + box_dimension/2 # <-- Shifting entire molecule!
        box_dimension = ( box_dimension ).tolist()
    
        # OUTPUTTING GRO FILE
        Monolayer_Spherical.print_gro_file(output_gro_name, output_folder, total_atoms, total_resids, total_geom, total_outputnames, box_dimension)
        # OUTPUTTING TOPOLOGY FILE
        Monolayer_Spherical.print_top_file(forcefield,output_top_name, output_folder, ligand_names, ligands, ligand_nums = sphericalMonolayer.ligandDistribution )
        
    ### PRINTING SUMMARY
    print("Done! Next, energy minimize, then add water to the output file.")
    print('')
    print(' **** FUTURE IMPROVEMENTS ****')
    print('  Need support for improper dihedrals (e.g. for arginine)')
    print("  Should add support for multiple force fields, different monolayer geometry, more exotic ligand architectures.")
    print('')