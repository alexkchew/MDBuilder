#/usr/bin/env python
'''
ligand_builder.py
# v1 by Reid Van Lehn, December 2016
# v2 by Alex K. Chew, March 2017
# v3 by Alex K. Chew, May 2018

This script has been re-written to avoid fragments. We will be focusing now on ligands that are draw-able. Fragment-based approaches may be supported in the future.

# Based on library of molecular fragments with specified atom/bonding properties,
# generates topology and geometry for ligands that are defined as combinations of
# fragments. Further populates a double self-assembled monolayer system with
# combinations of user-specified ligands in preparation for further simulations.
# Note that this was written for use in Python v2.7.6, which is now legacy.

INPUTS:
    
GLOBAL VARIABLES:
    GOLDITPNAME: name of the gold itp file
    GOLD_DIAMETER_NM: gold diameter in nm
    LIGANDPERNM2: ligand per nm^2
    DEFAULT_POSITION_RESTRAINT: default position restraints on the sulfur and the gold atoms
    
FUNCTIONS:
    find_atom_plot_properties: function that reats properties for an atomic plot
    plot_ligand: plots the ligand
    get_ligand_args: gets back ligand arguments (used for interpreting multiple ligands)
    
CLASSES:
    Ligand: Extracts information for a ligand
    Monolayer_Spherical: Builders a monolayer based on sphericity

** UPDATES **
20180508 - AKC - Found bug in Monolayer_Spherical where the sulfurs are not initially aligned at (0,0,0). As a result, we have directional ligands that are not correctly aligned to the radial vector.

Installing open mm for energy minization:
    conda install -c omnia openmm -n py36_mdd

'''
#%% 

### IMPORTING MODULES
import numpy as np # Mathematical tool for arrays, etc.
import random
import os

## CORRECTION FOR QUEUING BETWEEN PYTHON 2 & 3
import sys
is_py2 = sys.version[0] == '2'
if is_py2:
    import Queue as queue
else:
    import queue as queue

## IMPORTING MATPLOTLIB
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D # For 3D axes

## importing options
from optparse import OptionParser # Used to allow commands within command line

## FROM MDBUILDERS
from MDBuilder.core.import_tools import extract_gro, extract_itp ## EXTRACTION OF GRO AND ITP FILES
from MDBuilder.applications.nanoparticle.global_vars import DEFAULT_POSITION_RESTRAINT, LIGANDPERNM2, SULFUR_ATOM_NAME
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS

## IMPORTING MATH TOOLS
from MDBuilder.math.make_perfect_sphere import return_spherical
from MDBuilder.math.rotation import find_rotated_coordinates, angle_between, \
    rotation_matrix, vector_projection_u_onto_v, find_multiple_rotation_translation_coordinates, \
    get_angle_cw_direction_btn_two_vecs

### GLOBAL VARIABLES
## GOLD PARAMETER
GOLDITPNAME = 'Au_111' # Gold itp name

ATOM_DICT = { 
        'C': {'color': 'black'},
        'S': {'color': 'yellow'},
        'O': {'color': 'red'},
        'H': {'color': 'gray'},
        'N': {'color': 'blue'},
        }    

###################
### DEFINITIONS ###
###################

### FUNCTION TO CHECK ATOM NAME AND FIND ATOM COLOR
def find_atom_plot_properties(atom_name, full_atom_dictionary = ATOM_DICT):
    '''
    The purpose of this function is to find atom plot properties
    INPUTS:
        atom_name: [str] atom name, e.g. 'S1'
        full_atom_dictionary: [dict] dictionary for each atom type
    OUTPUTS:
        atom_dict: [dictionary] dictionary for the atom plot
    '''
    atom_dict = {}
    for each_key in full_atom_dictionary.keys():
        if each_key in atom_name:
            atom_dict = full_atom_dictionary[each_key]
    if atom_dict == {}:
        atom_dict = {'color': 'purple'}
    return atom_dict


### FUNCTION TO PLOT LIGAND
def plot_ligand(geometry, atomnames, fig = None, ax = None):
    '''
    The purpose of this function is to plot the ligand in an xyz coordinate space
    INPUTS:
        geometry: [np.array, shape=(N,3)] 
            geometry of the ligand
        atomnames: [list] 
            list of strings of atom names (e.g. 'S1')
    OUTPUTS:
        fig, ax for the plot of the ligand
    '''
    ## IMPORTING TOOLS
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D # For 3D axes
    ## CREATING PLOT
    if fig is None or ax is None:
        fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') 
        # , aspect='equal'
        ## SETTING X, Y, Z labels
        ax.set_xlabel('x (nm)')
        ax.set_ylabel('y (nm)')
        ax.set_zlabel('z (nm)')
        
    ## PLOTTING POINTS
    for index, each_atom_name in enumerate(atomnames):
        atom_dict = find_atom_plot_properties( each_atom_name )
        ax.scatter( geometry[index, 0], geometry[index,1], geometry[index,2], s = 100, **atom_dict )
    return fig, ax

### FUNCTION TO GET LIGAND ARGUMENTS FROM OPTION PARSER
def get_ligand_args(option, opt_str, value, parser):
    setattr(parser.values, option.dest, value.split(','))


### FUNCTION TO REFLECT POSITIONS
def reflect_positions_along_axis(positions,
                                 reflection_vec,
                                 center_vec = np.array([0,0,0]),
                                 reflect_angle = np.radians(180)):
    '''
    The purpose of this function is to reflect positions along an axis. The idea 
    would be that we wnat to rotate a ligand 180 degrees. 
    INPUTS:
        positions: [np.array, shape = (N,3)]
            position array in x,y,z coordinates
        reflection_vec: [np.array]
            vector axis to reflect from
        center_vec: [np.array]
            center vector to re-position the actual positions. We will just set to origin by default.
        reflect_angle: [float]
            reflection angle in radians. By default, this is 180 degrees / pi/2
    OUTPUTS:
        reflected_positions: [np.array, shape = (N,3)]
            reflected positions acros an axis
    '''    
    ## REFLECTING
    reflect_matrix = rotation_matrix(reflection_vec, reflect_angle)
    
    ## REALIGNING
    # Then, we need to multiply the LineData, assuming it is in n x 3 format
    reflected_positions = np.dot( reflect_matrix, (positions - center_vec).T ).T + center_vec

    return reflected_positions





################################################
### CLASS FUNCTION TO GET LIGAND INFORMATION ###
################################################
class Ligand:
    '''
    The purpose of this function is to extract ligand information so that we can use it for subsequent calculations (e.g. )
    INPUTS:
        ligand_name: name of the ligand
        prep_ligand_folder: folder where all the final structures of the ligands are located
    OUTPUTS:
        ## FROM INPUTS
            self.prep_ligand_folder, self.ligand_name
            
        ## DEFINING PATHS
            self.path_ligand_folder: [str] full path to ligand folder
            self.path_gro_file: [str] path to gro file
            self.path_itp_file: [str] path to itp file

        ## FROM GRO FILE
            self.ligand_num_atoms: [int] number of atoms in the ligand
            self.ligand_geom: [np.array, shape=(N,3)] ligand geometry
        ## FROM ITP FILE
            self.ligand_res_name: [str] ligand residue name
            ## ATOMIC INFORMATION
                self.ligand_charges: atomic charges on the ligands
                self.ligand_masses: masses on the ligands
                self.ligand_atomtypes: atomtypes for the ligands
                self.ligand_atomnames: atom names on the ligands
            ## BONDING INFORMATION
                self.ligand_bonds: bonds on the ligands
                self.ligand_bonds_func: bonding function on the ligands
                self.ligand_angles: angles on the ligands
                self.ligand_angles_func: angle function on the ligands
                self.ligand_dihedrals: dihedrals on the ligands
                self.ligand_dihedrals_func: dihedral function on the ligands
        ## MISCELLANEOUS
        self.ligand_outputnames: output names for ligands in gro files
    ## ACTIVE FUNCTIONS
        self.plot_ligand: plots the ligand in xyz coordinates
    self.print_itp_file <-- prints itp file (DEPRECIATED!)
    '''
    ### INITIALIZATION
    def __init__(self, prep_ligand_folder, ligand_name):
        ## STORING INITIAL INFORMATION
        self.prep_ligand_folder = prep_ligand_folder
        self.ligand_name = ligand_name
        
        ## DEFINING FULL PATH TO LIGAND FOLDER
        self.path_ligand_folder = self.prep_ligand_folder + '/' + self.ligand_name
        
        ## DEFINING GRO AND ITP FILE
        self.path_gro_file = os.path.join(self.path_ligand_folder,
                                          self.ligand_name + '.gro')
        self.path_itp_file = os.path.join(self.path_ligand_folder, 
                                          self.ligand_name + '.itp')
        
        ## EXTRACTING GRO AND ITP FILE
        ligand_gro_file = extract_gro(self.path_gro_file)
        ligand_itp_file = extract_itp(self.path_itp_file)
        
        ## READING GRO FILE
        self.read_gro(ligand_gro_file)
        
        ## READING ITP FILE
        self.read_itp(ligand_itp_file)
        
        ## GETTING OUTPUT NAMES: combine ligand name with atom name /number to create output name for gro file
        self.ligand_outputnames = []
        for i in range(0, self.ligand_num_atoms):
            # convert ligname to 5 characters
            ligname = self.ligand_res_name.rjust(5, ' ')
            # make atom name + id as well
            atomname = self.ligand_atomnames[i] # + str(i+1)
            comboname = ligname + atomname.rjust(5, ' ')
            self.ligand_outputnames.append(comboname)
        
        return
    ### FUNCTION TO READ THE GRO FILE
    def read_gro(self,gro_file):
        '''
        The purpose of this function is to read the gro file
        INPUTS:
            gro_file: extracted gro file from extract_gro class
        OUTPUTS:
            self.ligand_num_atoms: [int] number of atoms in the ligand
            self.ligand_geom: [np.array, shape=(N,3)] ligand geometry
        '''
        ## GETTING TOTAL NUMBER OF ATOMS
        self.ligand_num_atoms = len(gro_file.AtomNum)
        ## GETTING GEOMETRY
        self.ligand_geom = np.array( [gro_file.xCoord, gro_file.yCoord, gro_file.zCoord] ).T
        return
    
    ### FUNCTION TO READ ITP FILE
    def read_itp(self, itp_file):
        '''
        The purpose of this function is to read the itp file
        INPUTS:
            itp_file: extracted itp file from extract_itp class
        OUTPUTS:
            self.ligand_res_name: [str] residue name of ligand
            ## ATOMIC INFORMATION
                self.ligand_charges: [list] atomic charges on the ligands
                self.ligand_masses: [list] masses on the ligands
                self.ligand_atomtypes: [list] atomtypes for the ligands
                self.ligand_atomnames: [list] atom names on the ligands
            ## BONDING INFORMATION
                self.ligand_bonds:          [np.array, shape=(N,2)] bonds on the ligands
                self.ligand_bonds_func:     [np.array, shape=(N,1)] bonding function on the ligands
                self.ligand_angles:         [np.array, shape=(N,3)] angles on the ligands
                self.ligand_angles_func:    [np.array, shape=(N,1)] angle function on the ligands
                self.ligand_dihedrals:      [np.array, shape=(N,4)] dihedrals on the ligands
                self.ligand_dihedrals_func: [np.array, shape=(N,1)] dihedral function on the ligands
                self.ligand_pairs:          [np.array, shape=(N,2)] pairs on the ligands
                self.ligand_pairs_func:     [np.array, shape=(N,1)] function on the pairs
        '''
        ## FINDING RESIDUE NAME
        self.ligand_res_name    = itp_file.residue_name
        
        ## EXTRACTING ATOM INFORMATION
        self.ligand_charges     = itp_file.atom_charge
        self.ligand_masses      = itp_file.atom_mass
        self.ligand_atomtypes   = itp_file.atom_type
        self.ligand_atomnames   = itp_file.atom_atomname
        ## EXTRACTING BONDING, ANGLE, DIHEDRAL, AND PAIR INFORMATION
        self.ligand_bonds, self.ligand_bonds_func           = itp_file.bonds, itp_file.bonds_func               # BONDS
        self.ligand_angles, self.ligand_angles_func         = itp_file.angles, itp_file.angles_func             # ANGLES
        self.ligand_dihedrals, self.ligand_dihedrals_func   = itp_file.dihedrals, itp_file.dihedrals_func       # DIHEDRALS
        self.ligand_pairs, self.ligand_pairs_func           = itp_file.pairs, itp_file.pairs_func               # PAIRS
        return
    
    ### FUNCTION TO PLOT LIGAND
    def plot_ligand(self):
        '''
        The purpose of this function is to plot the ligand in an xyz coordinate space
        INPUTS:
            void
        OUTPUTS:
            plot of the ligand
        '''
        fig, ax = plot_ligand(geometry=self.ligand_geom[:], atomnames = self.ligand_atomnames[:] )
        return

    ## OUTPUTTING ITP FILE FOR GROMACS
    def print_itp_file(self, itp_name, output_folder, ligresname, angleFunc, dihedralFunc):
        '''
        The purpose of this function is to print an itp file for the ligand
        '''
        output_itp_path = output_folder + '/' + itp_name # Output .itp file name
        print("OUTPUTTING ITP FILE TO: %s"%(output_itp_path))
        outputfile = open(output_itp_path, "w") # Now, writing in output file
        # MOLECULE INFO
        # write molecule type info; for GROMOS force field
        outputfile.write("[ moleculetype ]\n")
        outputfile.write("; Name   nrexcl\n")
        #now molecule name + 3 for exclusions
        outputfile.write("%s    3\n\n"%(ligresname))
        # ATOM PROPERTIES
        outputfile.write("[ atoms ]\n")
        outputfile.write(";   nr    type   resnr  residu    atom    cgnr   charge    mass\n") # assume charge, mass already defined by atomtypes
        for i in range(0, self.ligand_num_atoms):
            # Charge groups don't matter for recent
            outputfile.write("%4d %15s  %d %s %s%d %4d  %.3f   %.4f\n"%(i+1, self.ligand_atomtypes[i], 1, self.ligand_name.rjust(10, ' '), self.ligand_atomnames[i].rjust(5, ' '), i+1, i+1, self.ligand_charges[i], self.ligand_masses[i]))
        # BONDS
        outputfile.write("\n[ bonds ]\n")
        outputfile.write(";  ai    aj   funct  ; bond properties inferred from atom types \n")
        for i in range(0, len(self.ligand_bonds)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   1\n"%(self.ligand_bonds[i][0]+1, self.ligand_bonds[i][1]+1))
        # ANGLES
        outputfile.write("\n[ angles ]\n")
        outputfile.write(";  ai    aj    ak  funct  ; angle properties inferred from atom types \n")
        for i in range(0, len(self.ligand_angles)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   %d   %d\n"%(self.ligand_angles[i][0]+1, self.ligand_angles[i][1]+1, self.ligand_angles[i][2]+1, angleFunc)) # 1 for function type
        # DIHEDRALS
        outputfile.write("\n[ dihedrals ]\n")
        outputfile.write(";  ai    aj    ak   al  funct  ; dihedral properties inferred from atom types \n")
        for i in range(0, len(self.ligand_dihedrals)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   %d   %d    %d\n"%(self.ligand_dihedrals[i][0]+1, self.ligand_dihedrals[i][1]+1, self.ligand_dihedrals[i][2]+1, self.ligand_dihedrals[i][3]+1, dihedralFunc)) # 3 for function type
        ## ADDING EXTRA DIHEDRALS IF NECESSARY
        # Checking if there is extra dihedrals
        if np.array(self.ligand_extra_dihedrals).size > 0:
            outputfile.write("; extra dihedral constraints specified from .frag file\n")
            for i in range(0, len(self.ligand_extra_dihedrals)):
                outputfile.write("%d   %d   %d   %d   %d\n"%(self.ligand_extra_dihedrals[i][0],
                                                        self.ligand_extra_dihedrals[i][1], 
                                                        self.ligand_extra_dihedrals[i][2], 
                                                        self.ligand_extra_dihedrals[i][3],
                                                        self.ligand_extra_dihedrals[i][4], # Dihedral function type
                                                        )) # 1 for function type
            
            
        # pairs
        outputfile.write("\n[ pairs ]\n")
        outputfile.write(";  ai    aj  \n")
        # infer pairs from all dihedrals
        for i in range(0, len(self.ligand_dihedrals)):
            # add 1 for correct indexing
            outputfile.write("%d   %d   1\n"%(self.ligand_dihedrals[i][0]+1, self.ligand_dihedrals[i][3]+1))
            
        # end with simple position restraints on the sulfur atoms
        outputfile.write("\n; Position restraints for sulfur atoms\n")
        outputfile.write("[ position_restraints ]  \n")
        outputfile.write(" %d  1   %s   %s   %s\n"%(1, DEFAULT_POSITION_RESTRAINT, DEFAULT_POSITION_RESTRAINT, DEFAULT_POSITION_RESTRAINT))
        outputfile.close()

### FUNCTION TO PLOT LIGAND
def plot_ligand_with_bonds(geom,
                           atomnames,
                           bonds,
                           fig = None,
                           ax = None,
                           verbose = False):
    '''
    This function plots all the ligands with bonding information.
    INPUTS:
        geom: [np.array]
            geometry of the ligand with bonds
        atomnames: [np.array]
            atomnames of each atom
        bonds: [np.array, shape = (N_bonds, 2)]
            bonds between the atoms
        verbose: [logical]
            True if you want verbose printing
    OUTPUTS:
        fig, ax: [obj]
            figure and axis of object
    '''
    ## CREATING PLOT
    fig, ax = plot_ligand( geometry = geom, atomnames = atomnames, fig = fig, ax = ax )
    
    ## ADDING BONDS
    ## PLOTTING BONDING INFORMATION
    for each_bond in bonds:
        ## GETTING INDICES BACK TO PYTHON
        indices_in_python = np.array(each_bond) - 1
        if verbose is True:
            print("Plotting bond: %s"%(indices_in_python))

        ## PLOTTING
        geom_1 = geom[indices_in_python[0]]
        geom_2 = geom[indices_in_python[1]]
        ## CREATING ARRAY
        vector_array = np.array([ geom_1, geom_2]).T
        
        ## PLOTTING
        ax.plot(vector_array[0],
                vector_array[1],
                vector_array[2],
                linestyle ='-',
                color = 'k',
                linewidth = 1,
                )

    # MATCHING AXIS LIMITS
    xyzlim = np.array([ax.get_xlim3d(),ax.get_ylim3d(),ax.get_zlim3d()]).T
    XYZlim = [min(xyzlim[0]),max(xyzlim[1])]
    ax.set_xlim3d(XYZlim)
    ax.set_ylim3d(XYZlim)
    ax.set_zlim3d(XYZlim)
    
    return fig, ax

### FUNCTION TO PLOT THE SPHERICAL POINTS
def plot_sphere(center, radius, fig = None, ax = None):
    '''
    The purpose of this script is to visualize the sphere in three-dimensions
    INPUTS:
        center: [np.array]
            center of your sphere
        radius: [float] 
            radius of your sphere, which is used to plot a wireframe
        fig: [figure] 
            figure to plot on
        ax: [axis] 
            axis to plot on
    OUTPUTS:
        Coordinates plotted on a figure
    '''    
    ## CREATING FIGURE
    if fig is None or ax is None:
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') 
        ## DEFINING X, Y, Z AXIS
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
    
    ## GENERATING WIREFRAME
    phi = np.linspace(0, np.pi, 20)
    theta = np.linspace(0, 2 * np.pi, 40)
    x = center[0] + radius * np.outer(np.sin(theta), np.cos(phi))
    y = center[1] + radius * np.outer(np.sin(theta), np.sin(phi))
    z = center[2] + radius * np.outer(np.cos(theta), np.ones_like(phi))
    
    ## PLOTTING WIREFRAME
    ax.plot_wireframe(x, y, z, color='k', rstride=1, cstride=1, alpha=0.2) # Wireframe, alpha is transparency
      
    return fig, ax

#####################################################
### CLASS FUNCTION CREATE SPHERICAL NANOPARTICLES ###
#####################################################
class Monolayer_Spherical:
    '''
    The purpose of this class is to builds spherical nanoparticles (i.e. attaching ligands onto a gold particle)
    Written by: Alex K. Chew
    INPUTS:
        ligands: [list] 
            ligands from Ligand class
        ligand_fracs: [list] 
            Fraction of ligands.(Number between 0 and 1) The larger fraction occurs for the first ligand
        sphereRadius: [float] 
            Total sphere radius in nms
        sulfur_coord: [np.array,shape=(N,3) 
            Coordinates of sulfur atoms 
        debug_mode: [logical] 
            True if you want to see plots for debugging
        assembly_lig_type: [str]
            assembly ligand types
                assembly ligand type, which is used to coordinate what 
                types of assembly ligands that you have.
                Examples:
                    "SCCCC": 1 sulfur, butanethiol-like molecule
                    "C1CCSS1": two sulfurs within a cyclopentane ring
        center_coord: [np.array]
            center coordinate that you are using fo r gold core
            
        ## DEFINING MAXIMUM NUMBER OF LIGANDS
        max_num_ligs: [int]
            maximum number of ligands. By default, this is None, indicating to just 
            take the maximum possible number of ligands. If this value is 
            more than the amount of ligands available from self-assembly simulations, then 
            we will just take the maximum. If it is lower than the maxima, then 
            we will randomly remove ligands from the system. 
        optimize: [logical]
            True if you want to try to optimize ligand structure such that it is 
            not within the gold core. For the C1CCSS1 - type ligands, we will 
            rotate the sulfur-sulfur bond to minimize contact with the gold core. 
    OUTPUTS:
        Class Type: Monolayer_Spherical, Attributes:
            monolayer_geom: [np.array, shape=(N,3)] array containing the entire coordinate structure file for the gro file <-- From ligand_geom
            monolayer_atomname: [list] containing all atoms (e.g. S, C, ....) <-- From ligand_atomnames
            monolayer_resids: [list] containing residue numbers (e.g. 1, 1, 1, 1, ... ) <-- For each molecule, unique residue number! <-- array created given you know total number of ligands and size of ligand
            monolayer_outputnames: [list] string array containing 3 letter residue code and the atom type (e.g. ['BUT C3', etc.]) <-- uses ligand_outputnames
            num_monolayer_atoms: [int] Total number of atoms <-- outputted on top of gro file (second line)
    ALGORITHM:
        - Define empty variables for storage
        - Calculate total number of ligands
        - Re-center sulfur coordinates to the center (Makes the math easier!!!)
        - Figure out how many ligands are required to be distributed
        - Randomize spots
        - Loop through each ligand:
            - Find a random spot on the sulfur atom
            - Get the current ligand directional vector
            - Reorients directional vector
            - Copies that directional vector across all other sulfur positions
    FUNCTIONS:
        monolayer_rotate_spherical: rotates ligands spherically across the nanoparticle
        calc_ligand_dist: calculates ligand distribution for multiple ligand types
    '''
    
    ### INITIALIZING
    def __init__(self, 
                 ligands, 
                 ligand_fracs, 
                 radius, 
                 sulfur_coord, 
                 center_coord = None,
                 assembly_lig_type = "SCCCC",
                 debug_mode=False,
                 max_num_ligs = None,
                 verbose = True,
                 optimize = False,):
        if verbose is True:
            print("\n---- CLASS: %s ----"%(self.__class__.__name__))
        ### DEFINING VARIABLES INTO SELF
        self.sphere_radius = radius
        self.ligands = ligands
        self.ligand_fracs = ligand_fracs
        self.sulfur_coord = sulfur_coord
        self.debug_mode = debug_mode
        self.assembly_lig_type = assembly_lig_type
        
        ## STORING CENTER COORDINATES
        if center_coord is not None:
            self.center_coord = center_coord[:]
        
        ### PRE-DEFINING GEOMETRY, ATOMNAMES, ETC.
        # generate geometry by combining ligand geometries
        self.monolayer_geom = np.empty((0, 3), dtype='float')
        # make list of atomnames
        self.monolayer_atomnames = []
        # make list of resids
        self.monolayer_resids = []
        # list of all ligand output names for gro file
        self.monolayer_outputnames = []
        # TOTAL MONOLAYER ATOMS
        self.num_monolayer_atoms = 0
        
        ## CALCULATING TOTAL LIGANDS
        self.num_ligands = len(sulfur_coord) # Each sulfur atom is another ligand

        ## REMOVING INITIAL LIGANDS
        if max_num_ligs is not None:
            if verbose is True:
                print("Checking if maximum number of ligands is too high")
            ## NUMBER OF LIGANDS LESS THAN TOTAL
            if max_num_ligs <= self.num_ligands:
                if verbose is True:
                    print("%d is less than maxima of %d, lowering sulfur indices"%(max_num_ligs,self.num_ligands))
                ## GETTING RANDOM NUMBER
                idx_to_keep = np.sort(np.array(random.sample(range(0, self.num_ligands), max_num_ligs)))
            else:
                if verbose is True:
                    print("%d is more than maxima of %d, keeping maxima"%(max_num_ligs,self.num_ligands))
                idx_to_keep = np.arange(0, self.num_ligands)
            
            ## STORING new coordinates
            self.num_ligands = len(idx_to_keep)
            self.initial_sulfur_coord = sulfur_coord[idx_to_keep]
        else:
            ## FINDING SPHERICAL GEOMETRY
            self.initial_sulfur_coord = sulfur_coord[:] # Copying sulfur coordinates

        ## RECENTERING
        if self.assembly_lig_type != 'C1CCSS1':
        
            ## FINDING CENTER OF THE SULFURS
            self.center_coord = np.mean(self.initial_sulfur_coord,axis=0)                                              
            ## FINDING GEOMETRY BASED ON CENTER (ZEROED)
            # GEOMETRY WITH THE SULFURS BASED ON (0,0,0) <-- center
            self.initial_spherical_coord_center_zero = self.initial_sulfur_coord - self.center_coord
        else:
            self.initial_spherical_coord_center_zero = self.initial_sulfur_coord[:]
        
        ## FINDING LIGAND DISTRIBUTION
        self.calc_ligand_dist()
            
        ## CREATING RANDOMIZED SPOTS
        availableSpots =  np.arange(self.num_ligands)
        ## RANDOMIZE IF NON-UNIFORM LIGAND FRACTIONS
        if len(ligand_fracs) != 1: # Indicates multiple ligand fractions
            np.random.shuffle(availableSpots) # <-- Shuffles spots
        
        ## CREATING COUNTER
        sphericalSpotCounter = 0 # Counts the total spots
        upToSpherical = 0 # First up to spherical point
        ligandcounter = 0 # Counts current ligand
        totalResidueLigandCount = 1 # For counting residues
        
        ## STORING SULFUR INDICES
        self.lig_atom_indices = []
        self.sulfur_bound_indices = []
        
        ## LOOPING THROUGH EACH LIGAND
        for lig in ligands:
            # First start by generating a random number to place my first ligand
            randInteger = availableSpots[sphericalSpotCounter]
            
            ## LOCATING ALL SULFUR ATOMS THAT STARTS WITH 'S' AND ARE BONDED WITH EACH OTHER
            all_sulfur_index = np.array([ idx for idx, each_atom in enumerate(lig.ligand_atomnames)
                                        if each_atom.startswith('S')])
            
            ## DEFINING THE SULFUR INDEX (USED FOR CENTERING, ETC)
            ## IF SPECIFIC TYPE
            if self.assembly_lig_type == 'C1CCSS1':

                ## SULFUR INDEX + 1 TO MATCH GROMACS
                all_sulfur_index_plus_one = all_sulfur_index + 1
                
                ## GETTING SULFUR INDEX
                sulfurIndex = []
                
                ## FINDING ALL BONDS
                for each_index in all_sulfur_index_plus_one:
                    ## FINDING ALL CURRENT BONDS
                    current_bonds = lig.ligand_bonds[np.where(lig.ligand_bonds == each_index)[0]]
                    ## FLATTENING AND REMOVING CURRENT ATOM INDEX
                    current_bonds = current_bonds.flatten()
                    current_atoms_bonded = current_bonds[current_bonds!=each_index]
                    ## FINDING ALL SULFURS
                    current_atoms_bonded_sulfur = [each_atom for each_atom in current_atoms_bonded if each_atom in all_sulfur_index_plus_one]

                    ## SEEING IF CURRENT ATOMS ARE WITHIN SULFUR INDEX
                    is_sulfur_bonded = np.any(np.isin(current_atoms_bonded_sulfur, all_sulfur_index_plus_one))
                    if is_sulfur_bonded == True:
                        ## DEFINING ATOM INDICES
                        include_atom_indices = [ each_index ] + list(current_atoms_bonded_sulfur)
                        ## STORING
                        for included_atom in include_atom_indices:
                            if included_atom not in sulfurIndex:
                                sulfurIndex.append(included_atom)
                
                ## CHECKING IF LENGTH IS > 2
                if len(sulfurIndex) != 2:
                    print("Warning! Sulfur indices not equal to 2.")
                    print("Current assembly type: %s"%(self.assembly_lig_type))
                    print("This type assumes a double-sulfur within a cyclopentane ring, it looks for two sulfurs bonded")
                    print("Current sufur index:")
                    print(sulfurIndex)
                
                ## SUBTRACTING TO 1 (Since Python starts by counting zero)
                sulfurIndex = np.array(sulfurIndex) - 1
                
                ### CENTERING FIRST SULFUR INDEX
                initial_sulfur_geom = lig.ligand_geom[sulfurIndex[0]]

            else:
                
                ## SEEING IF THERE IS ONE SULFUR
                if len(all_sulfur_index) == 1:
                    sulfurIndex = all_sulfur_index[0]
                else:
                    ## LOCATING SULFUR BASED ON "S1" SULFUR ATOM NAME
                    sulfurIndex = lig.ligand_atomnames.index(SULFUR_ATOM_NAME) # Find sulfur index ** FIRST INDEX **
            
                ### CENTERING LIGAND GEOMETRY WITH RESPECT TO SULFUR ATOM (TRANSLATION)
                ## FINDING SULFUR ATOM GEOMETRY
                initial_sulfur_geom = lig.ligand_geom[sulfurIndex]
            
            ## GETTING NUMBER OF LIGAND ATOMS
            num_lig_atoms = lig.ligand_num_atoms
                
            ## DEFINING INITIAL GEOMETRY BASED ON INITIAL SULFUR (Now, sulfur is at 0, 0, 0)
            initial_lig_geom = lig.ligand_geom - initial_sulfur_geom
            
            # Then, I need to place my ligand on the specified random integer. 
            # At the same time, I need to rotate the molecule to correctly align with the chosen direction
            # Directional vector of the ligand
            directionalVector = np.average(initial_lig_geom,axis=0) # Average along the rows -- average direction of the ligand
            
            ## DEBUG: SEEING DIRECTIONAL VECTOR
            if debug_mode is True:

                ## CREATTING PLOT WITH BONDS
                fig, ax = plot_ligand_with_bonds(geom = initial_lig_geom,
                                                 atomnames = lig.ligand_atomnames,
                                                 bonds = lig.ligand_bonds)
                
                # fig, ax = plot_ligand( geometry = initial_lig_geom, atomnames = lig.ligand_atomnames )
                ax.set_title("INITIAL GEOMETRY FOR %s"%(lig.ligand_res_name) )
                
                ## PLOTTING
                ax.plot( [0, directionalVector[0] ], 
                        [0, directionalVector[1] ], 
                        [0, directionalVector[2] ],
                        linestyle = '-',
                        color = 'blue',
                        linewidth = 3)

            
            ## RE-SCALING DIRECTIONAL VECTOR OF THE LIGAND ACCORDING TO RADIUS
            fixedDirectionalVector = return_spherical(directionalVector[:], 
                                                      radius=self.sphere_radius) ## SIMPLY RE-SCALED DIRECTIONAL VECTOR OF THE LIGAND
            
            ## GETTING DIRECTION OF THE RANDOM
            randDirection = self.initial_spherical_coord_center_zero[randInteger]
        
            ## IF SPECIFIC TYPE
            if self.assembly_lig_type == 'C1CCSS1':
                current_rand_direction = randDirection[0]
            else:
                current_rand_direction = randDirection[:]
            
            # ROTATING ENTIRE MOLECULE IN THE NEW DIRECTION
            initialGeom = find_rotated_coordinates( vec1 = fixedDirectionalVector,
                                                    vec2 = current_rand_direction,
                                                    angle_btn_vector = angle_between(fixedDirectionalVector, current_rand_direction ),
                                                    positions = initial_lig_geom,
                                                  )
            
            ############################
            ### TRANSLATING GEOMETRY ###
            ############################
            ## IF SPECIFIC TYPE
            if self.assembly_lig_type == 'C1CCSS1':
                ## GETTING FIRST SULFUR INDEX
                current_sulfur_index = sulfurIndex[0]                
            else:
                current_sulfur_index = sulfurIndex
            
            ## DEFINING CURRENT GEOMETRY
            initial_sulfur_geom = initialGeom[current_sulfur_index]
            
            # Translating the ligand to the surface of sphere
            translationVector = current_rand_direction - initial_sulfur_geom
            self.translatedGeom = initialGeom[:] + translationVector[:] # Brings ligand to the surface you care about
        
            # Now, we can find random integers to selectively place our ligands
            # Start by creating new spherical position array
            upToSpherical += self.ligandDistribution[ligandcounter]
            
            ## DEFINING AVAILABLE SPOTS VECT
            available_spots_array = availableSpots[sphericalSpotCounter:upToSpherical]
            
            ## IF SPECIFIC TYPE
            if self.assembly_lig_type == 'C1CCSS1':
                ## FINDING ALL SPHERICAL POSITIONS (FIRST TYPE)
                self.sphericalPositions = np.array([ self.initial_spherical_coord_center_zero[x][0] 
                                                     for x in available_spots_array ] )

            else:
                ## FINDING ALL SPHERICAL POSITIONS
                self.sphericalPositions = np.array([ self.initial_spherical_coord_center_zero[x] 
                                                     for x in available_spots_array ] )
            
            ## GETTING NUMBER OF LIGANDS
            num_ligands = len(self.sphericalPositions)
            print("Total number of ligs: %d"%(num_ligands))
            
            ## CREATING GEOMETRIES WITH ROTATIONS
            geometrySpherical = self.monolayer_rotate_spherical(ReferenceIndex=0, # Referenced at 0 (arbitrary starting point -- should not influence results)
                                                                sphericalCoord=self.sphericalPositions,
                                                                LigandData=self.translatedGeom,
                                                                sulfur_index = current_sulfur_index)
            
            ## ADDING BACK VECTOR TO GET THE CORRECT CENTER
            geometrySpherical = geometrySpherical + self.center_coord
            
            ## GETTING ATOM INDICES
            lig_atom_indices = [ list(np.arange(num_lig_atoms) + num_lig_atoms*idx)
                                 for idx, x in enumerate(available_spots_array) ]
            
            ## GETTING MORE SULFUR INDICES
#            lig_sulfur_bound_indices = [ list(np.array(lig_atom_indices[idx])[sulfurIndex] + len(self.monolayer_geom)) 
#                                         for idx, x in enumerate(available_spots_array)] 
            
            lig_sulfur_bound_indices = [ [np.array(lig_atom_indices[idx])[sulfurIndex] + len(self.monolayer_geom)]
                                         for idx, x in enumerate(available_spots_array)] 
            # Added + len(self.monolayer_geom) to account for the fact that ligand indices start from 0
            
            self.sulfur_bound_indices.extend(lig_sulfur_bound_indices)

            ## STORING SULFUR INDICES
            self.lig_atom_indices.append(lig_atom_indices)

            ## NOW, EDIT THE GEOMETRY SUCH THAT SECOND SULFUR IS ALIGNED
            if self.assembly_lig_type == 'C1CCSS1':
                '''
                For this section, we now have ligands that are aligned to 
                one of the sulfurs. The idea is then to:
                    1- rotate ligand so the second sulfur is aligned. Then, we will 
                    2- position the second sulfur to correctly align to second position
                    3- Using the fix S-S bond as axis of rotation, rotate entire molecule based on radial distance from center
                '''
                
                ## GETTING LIGAND INDEX
                lig_indexes = np.arange(len(geometrySpherical)).reshape(num_ligands, num_lig_atoms)
                
                ## STORING NEW GEOMETRY
                new_geom_spherical = []
                
                ## LOOPING THROUGH EACH INDICES
                for idx_lig, each_lig_indices in enumerate(lig_indexes):
                    
                    ## PRINTING
                    if idx_lig % 100 == 0:
                        print("Assembly type %s - Realigning %d of %d ligands"%(self.assembly_lig_type, idx_lig, num_ligands ))
                    
                    ## DEFINING CURRENT LIG GEOM
                    current_lig_geom = geometrySpherical[each_lig_indices]
                    
                    ## DEFINING CURRENT SPHERICAL POSITION
                    current_spherical_position = available_spots_array[idx_lig]
                    
                    ## GETTING REAL SULFUR POSITIONS
                    real_sulfur_positions = self.initial_spherical_coord_center_zero[current_spherical_position] + self.center_coord
                    
                    #####################################
                    #### ALIGNING SECOND SULFUR ATOM ####
                    #####################################
                    
                    ## GETTING CURRENT ALIGNMENT
                    vec1 = current_lig_geom[sulfurIndex[1]] - current_lig_geom[sulfurIndex[0]]
                    
                    ## GETTING ALIGNMENT CORRECTED BY V1 AND V2 VECTOR
                    vec2 = real_sulfur_positions[1] - real_sulfur_positions[0]
                    
                    ## GETTING ANGLE IN BETWEEN 
                    current_angle = angle_between(v1 = vec1,
                                                  v2 = vec2)
                    
                    ## GETTING NEW SULFUR POSITIONS BY ROTATING
                    new_positions = find_rotated_coordinates(vec1 = vec1, 
                                                             vec2 = vec2, 
                                                             angle_btn_vector = current_angle, 
                                                             positions = current_lig_geom - real_sulfur_positions[0]) + real_sulfur_positions[0]
                    
                    ## FINDING DISTANCE
                    dist_btn_real_and_new = np.sqrt( np.sum( (real_sulfur_positions[1] - new_positions[sulfurIndex[1]])**2 ) )
                    
                    ## CHECKING DISTANCE
                    dist_tol = 0.5
                    if dist_btn_real_and_new > dist_tol:
                        print("Warning! Second sulfur position tolerance is greater than %.3f"%(dist_tol))
                        print("This may cause errors when simulating because sulfur positions should not be too far from setpoint")
                        print("Current distance: %.3f"%(dist_btn_real_and_new))
                    
                    #####################################
                    #### ROTATING ABOUT THE S-S BOND ####
                    #####################################
                    
                    ## STORING NEW POSITIONS
                    new_positions[sulfurIndex[1]] = real_sulfur_positions[1]
                    
                    ### ROTATING ALONG AXIS TO MOVE ALL LIGAND ATOMS RADIALLY AWAY
                    vector_S_S = new_positions[sulfurIndex[1]] - new_positions[sulfurIndex[0]]
                    
                    ## GETTING VECTOR WITH RESPECT TO THE ORIGIN
                    avg_S_S_position = np.mean( new_positions[sulfurIndex], axis = 0)
                    
                    ## GETTING VECTOR FROM CENTER
                    vector_S_avg_to_center = avg_S_S_position - self.center_coord
                    
                    ## GETTING CURRENT LIGAND VECTOR                    
                    
                    ## GETTING AVERAGE VEC
                    average_lig_vec = np.mean(new_positions - vector_S_avg_to_center, axis = 0)
                    
                    ## GETTING ANGLE
                    angle_btn_avg_to_vec = angle_between(v1 = average_lig_vec,
                                                         v2 = vector_S_S)
                    
                    ## GETTING V1 AND V2
                    S_v0 = new_positions[sulfurIndex[0]] - self.center_coord
                    S_v1 = new_positions[sulfurIndex[1]] - self.center_coord
                    
                    ## FINDING NORMAL VECTOR
                    norm_S_S = np.cross(S_v0,
                                        S_v1)
                    
                    ## GETTING VECTOR NORMAL
                    norm_desired = np.cross(
                                            vector_S_S,
                                            norm_S_S)
                    
                    ## PROJECTING LIGAND VECTOR ONTO NORMALIZED
                    projection_lig_vec_to_norm = vector_projection_u_onto_v(u = average_lig_vec,
                                                                            v = vector_S_S)
                    
                    ## GETTING VECTOR IN PLANE
                    vec_in_plane = average_lig_vec - projection_lig_vec_to_norm
                    
                    ## GETTING ANGLE
                    angle_btn_avg_to_vec, quad = get_angle_cw_direction_btn_two_vecs(x_vec = norm_desired,
                                                                               y_vec = norm_S_S,
                                                                               vec = vec_in_plane)
                    
                    ## ROTATING ALONG AN AXIS
                    RotationMatrix = rotation_matrix(vector_S_S, angle_btn_avg_to_vec)
                    
                    # Then, we need to multiply the LineData, assuming it is in n x 3 format
                    positions_after_realignment = np.dot( RotationMatrix, (new_positions - vector_S_avg_to_center).T ).T + vector_S_avg_to_center
                    
                    
                    ## RANDOMLY ROTATE LIGAND (DUE TO SYMMETRY)
                    # random_boolean = False
                    random_boolean = bool(random.getrandbits(1))
                    # False
                    # 
                    # False
                    # 
                    
                    ## ROTATING
                    if random_boolean is True:
                        if debug_mode is True:
                            print("Randomly rotating for %d"%(idx_lig))
                        ## REFLECTING
                        positions_after_realignment = reflect_positions_along_axis(positions = positions_after_realignment,
                                                                                   reflection_vec = norm_desired[:],
                                                                                   center_vec = vector_S_avg_to_center,
                                                                                   reflect_angle = np.radians(180))
                    
                    ## CHECKING IF POSITIONS ARE LESS THAN DISTANCE
                    distances_from_center = np.linalg.norm(positions_after_realignment, axis = 1).reshape(1, num_lig_atoms)    
                    is_within_radius = np.any(distances_from_center < self.sphere_radius)
                    
                    ## CHECKING 
                    if is_within_radius == True:
                        if debug_mode is True:
                            print("Correcting: Reflecting orientation of ligand index: %d"%(idx_lig))
                        ## REFLECTING
                        positions_after_realignment = reflect_positions_along_axis(positions = positions_after_realignment,
                                                                                   reflection_vec = norm_desired[:],
                                                                                   center_vec = vector_S_avg_to_center,
                                                                                   reflect_angle = np.radians(180))
                        

                    ## STORING NEW POSITIONS
                    new_geom_spherical.extend(positions_after_realignment)
                    
                    ## GETTING VECTOR
                    '''
                    print("Positions before realignment:")
                    print(new_positions[sulfurIndex])
                    print("Positions after realignment:")
                    print(positions_after_realignment[sulfurIndex])
                    '''
                    
                    ## FOR FIRST INDEX
                    if debug_mode is True and idx_lig in [200]: # 0,4 14 0 14 , 37 3
                        ## PRINTING DISTANCE DIFFERENCE
                        print("Index of ligand:", idx_lig)
                        print(average_lig_vec)
                        ## PRINTING ANGLE
                        print("Angle:", np.degrees(angle_btn_avg_to_vec) )
                        print("Vector S-S:", vector_S_S)
                        print("Vector to S from center:", vector_S_avg_to_center)
                        print("Avg. of ligand:", average_lig_vec)
                        
                        labels = ['1: Initial rotation', '2: Alignment of sulfur 2', '3: Direct outward']
                        geom_to_plot = [ current_lig_geom, new_positions, positions_after_realignment ]
                        
                        ## GETTING AVERAGE VEC
                        new_average_lig_vec = np.mean(positions_after_realignment - vector_S_avg_to_center, axis = 0)
                        
                        ## PLOTTING EACH
                        for (each_label, each_geom) in zip(labels,geom_to_plot):
                        
                            ## CREATING PLOT WITH BONDS
                            fig, ax = plot_ligand_with_bonds(geom = each_geom,
                                                             atomnames = lig.ligand_atomnames,
                                                             bonds = lig.ligand_bonds,
                                                             fig = None,
                                                             ax = None,)
                            ## SETTING TITLE
                            ax.set_title("%s"%(each_label))
                            ## GETTING SPHERE
                            fig, ax = plot_sphere(center = self.center_coord,
                                                  radius = self.sphere_radius,
                                                  fig = fig,
                                                  ax = ax)
    
                            ## TIGHT LAYOUT
                            fig.tight_layout()
                            
                            ## DEFINING COLORS
                            colors = ['purple', 'orange']
                            
                            ## PLOTTING
                            for idx,each_sulfur in enumerate(real_sulfur_positions):
                                ax.scatter( each_sulfur[0], 
                                            each_sulfur[1],
                                            each_sulfur[2], 
                                            s = 200, 
                                            color = colors[idx],
                                            label = "Real-S-%d"%(idx),
                                            )
                            
                            ## PLOTTING LIGN
                            if each_label in ['2: Alignment of sulfur 2', '3: Direct outward'] :
                                ## PLOTTING
                                ax.plot([0, vector_S_avg_to_center[0] ], 
                                        [0, vector_S_avg_to_center[1] ], 
                                        [0, vector_S_avg_to_center[2] ],
                                        linestyle = '-',
                                        color = 'purple',
                                        linewidth = 3,
                                        label = "Center to sulfur bond")
                                
                                ## PLOTTING
                                ax.plot([vector_S_avg_to_center[0], average_lig_vec[0] + vector_S_avg_to_center[0] ], 
                                        [vector_S_avg_to_center[1], average_lig_vec[1] + vector_S_avg_to_center[1] ], 
                                        [vector_S_avg_to_center[2], average_lig_vec[2] + vector_S_avg_to_center[2]],
                                        linestyle = '-',
                                        color = 'blue',
                                        linewidth = 3,
                                        label = "Avg. ligand vec")
                                
                                ## PLOTTING NEW VECTOR
                                ax.plot([vector_S_avg_to_center[0], new_average_lig_vec[0] + vector_S_avg_to_center[0] ], 
                                        [vector_S_avg_to_center[1], new_average_lig_vec[1] + vector_S_avg_to_center[1] ], 
                                        [vector_S_avg_to_center[2], new_average_lig_vec[2] + vector_S_avg_to_center[2]],
                                        linestyle = '-',
                                        color = 'red',
                                        linewidth = 3,
                                        label = "New ligand vec")
                                
                                ## PLOTTING NORMAL VECTOR
                                ax.plot([vector_S_avg_to_center[0], norm_S_S[0] + vector_S_avg_to_center[0] ], 
                                        [vector_S_avg_to_center[1], norm_S_S[1] + vector_S_avg_to_center[1] ], 
                                        [vector_S_avg_to_center[2], norm_S_S[2] + vector_S_avg_to_center[2]],
                                        linestyle = '-',
                                        color = 'brown',
                                        linewidth = 3,
                                        label = "Normal_S_S_center_vec")
                            
                                ## PLOTTING VECTOR DESIRED
                                ax.plot([vector_S_avg_to_center[0], norm_desired[0] + vector_S_avg_to_center[0] ], 
                                        [vector_S_avg_to_center[1], norm_desired[1] + vector_S_avg_to_center[1] ], 
                                        [vector_S_avg_to_center[2], norm_desired[2] + vector_S_avg_to_center[2]],
                                        linestyle = '-',
                                        color = 'black',
                                        linewidth = 3,
                                        label = "Desired vector")
                            
                            
                                ## PLOTTING VECTOR IN PLANE
                                ax.plot([vector_S_avg_to_center[0], vec_in_plane[0] + vector_S_avg_to_center[0] ], 
                                        [vector_S_avg_to_center[1], vec_in_plane[1] + vector_S_avg_to_center[1] ], 
                                        [vector_S_avg_to_center[2], vec_in_plane[2] + vector_S_avg_to_center[2]],
                                        linestyle = '-',
                                        color = 'green',
                                        linewidth = 3,
                                        label = "Vector in plane")
                            
                            
                            
                            ## ADDING LEGENDS
                            ax.legend()
                        
                ## AFTER FOR LOOP, RESTORING TO NUMPY
                geometrySpherical = np.array(new_geom_spherical)
                            
            #######################################
            #### FINALIZING MONOLAYER GEOMETRY ####
            #######################################
            
            ## APPENDING TO MONOLAYER GEOMETRY
            self.monolayer_geom = np.append( self.monolayer_geom, geometrySpherical, axis=0 ) # Appending to monolayer_geom                        
                                           
            # Fixing other variables, like atom names, number of atoms, etc.
            self.num_monolayer_atoms += len(geometrySpherical)
            self.monolayer_atomnames.extend(lig.ligand_atomnames * self.ligandDistribution[ligandcounter]) # S1, ...
            self.monolayer_outputnames.extend(lig.ligand_outputnames * self.ligandDistribution[ligandcounter]) # String list ' RES C1 ' etc.
        
            # Creating residues
            for currentLigand in range(totalResidueLigandCount, upToSpherical+1): # Starting at 1
                newResidue = np.ones( (1, lig.ligand_num_atoms ), dtype=int ) * currentLigand
                self.monolayer_resids.extend(newResidue)
            
            # Updating ligand counter
            totalResidueLigandCount += self.ligandDistribution[ligandcounter]
            sphericalSpotCounter += self.ligandDistribution[ligandcounter]
            ligandcounter += 1
            
        ## COMBINING SULFUR BOUND INDICES
        self.sulfur_bound_indices_flatten = np.concatenate(self.sulfur_bound_indices).ravel()
            
        # Fixing residues according to output GRO format
        self.monolayer_resids = np.concatenate(self.monolayer_resids).tolist()
    
    ### FUNCTION TO ROTATE THE LIGANDS
    def monolayer_rotate_spherical(self, ReferenceIndex, sphericalCoord, LigandData, sulfur_index):
        '''
        This function takes functions from "make_spherial.py" to rotate the ligand.
        INPUTS:
            ReferenceIndex: Which point on the sphere do you want to start from?
            sphericalCoord: Coordinates of all initial spheres
            LigandData: Coordinates for the initial ligand (should already be correctly translated)
            sulfur_index: [int] index of the sulfur atom in the ligand
            
        '''
        return find_multiple_rotation_translation_coordinates(reference_index = ReferenceIndex, 
                                                              reference_positions = sphericalCoord, 
                                                              positions = LigandData, 
                                                              positions_ref_index = sulfur_index )

    ## FUNCTION TO FIND LIGAND DISTRIBUTION
    def calc_ligand_dist(self):
        '''
        The purpose of this function is to calculate the ligand distribution 
        for mixed-monolayers. This will correct if the number of ligands are incorrect.
        The incorrectness comes from counting error, where you might have a % ligand, but the number 
        of ligands do not match one-to-one, resulting in underestimating or overestimating 
        ligand types. 
        INPUTS:
            void
        OUTPUTS:
            self.ligandDistribution: [list] 
                number of ligands of each type
        '''
        # Calculate total number of ligands for each fraction
        self.ligandDistribution = []
        for currentFracCount in range(len(self.ligand_fracs)):
            self.ligandDistribution.append( int(self.num_ligands*float(self.ligand_fracs[currentFracCount] ) ) )
        
        # Correct if we have too many ligands!
        while np.sum(self.ligandDistribution) != self.num_ligands:
            if np.sum(self.ligandDistribution) < self.num_ligands:
                self.ligandDistribution[-1] += 1
            else: # Greater than total number
                self.ligandDistribution[-1] -= 1    

#%% MAIN SCRIPT
if __name__ == "__main__":
    from MDBuilder.builder.transfer_ligand_builder import self_assembled_gold
    ### DEFINING INPUT GRO FILE
    prep_gold_folder = r"R:\scratch\nanoparticle_project\prep_system\prep_gold\self_assembly_simulations\hollow" # Preparation gold folder
    input_folder = "hollow_4_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_1" # Input folder (i.e. equilibrated values)
    input_gro_file = "gold_ligand_equil.gro" # Input GRO files
    
    ### DEFINING DESIRED LIGANDS
    ligand_names=['dodecanethiol'] # Name of the ligand 'ROT_NS','butanethiol'
    ligand_fracs=['1.0'] # '0.5','0.5'
    
    ### DEFINING OUTPUT DIRECTORY INFORMATION
    output_folder=check_server_path(r'R:\scratch\nanoparticle_project\simulations\test_transfer_lig')
    prep_folder=check_server_path(r"R:\scratch\nanoparticle_project\prep_system\prep_ligand\final_ligands")
    
    ## DEFINING SHAPE
    shape='hollow' # Important for shapes that need adjustments to mass, etc.
    
    ## DEFINING FULL PATH TO THE GRO FILE
    full_path_input_gro = check_server_path( os.path.join(prep_gold_folder, input_folder, input_gro_file ))
    
    ### EXTRACTING SELF-ASSEMBLY INFORMATION
    self_assembled_gold_info = self_assembled_gold(full_path_input_gro, wantPlot=False)
    
    ## LOADING LIGANDS
    ligands = []
    for cur_lig in ligand_names: # Uses Ligand class
        print('Working on ligand:', cur_lig)
        ligands.append(Ligand( ligand_name = cur_lig, prep_ligand_folder = prep_folder))
    
    ligands[0].plot_ligand()
    
    #%%
    ## CREATING SPHERICAL MONOLAYERS 
    ### CREATING SPHERICAL MONOLAYERS
    spherical_monolayer = Monolayer_Spherical(ligands = ligands, # Ligands of interest
                                                ligand_fracs = ligand_fracs, # Ligand fractions
                                                radius = self_assembled_gold_info.radius, # Sphere radius of self-assembly
                                                sulfur_coord = self_assembled_gold_info.near_sulfur_xyz_centered, # xyz coordinates of gold info _centered
                                                debug_mode=True # Debug mode is on
                                                )