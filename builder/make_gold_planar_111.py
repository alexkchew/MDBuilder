#/usr/bin/env python
"""
make_gold_planer_111.py
The purpose of this script is to take the {111} crystal lattice structure of gold and generate a planar surface.
Requirements:
    1. Crystal lattice structure (Taken from Interface Force Field)
    2. Amount of unit cells required in x, y, and z directions

CREATED ON: 03/05/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

"""
#%%

## SYSTEM TOOLS
import os

### MATH TOOLS
import numpy as np

### IMPORTING MODULES
from MDBuilder.math.rotation import angle_clockwise
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

### IMPORTING IMPORTANT GLOBAL VARIABLES
from MDBuilder.applications.nanoparticle.global_vars import GOLD_FCC_LATTICE_CONSTANT_NM, GOLD_DIAMETER_NM, GOLD_MASS, PLANARGOLDRESNAME
from MDBuilder.applications.nanoparticle.global_vars import PLANAR_SURFACEAU_TYPENAME, PLANAR_BULK_AU_TYPENAME, VIRTUAL_TYPENAME, VIRTUAL_ATOMNAME, PLANAR_SURFACEAU_ATOMNAME, PLANAR_BULK_AU_ATOMNAME, GOLD_PLANAR_POSITION_RESTRAINT
# from ligand_builder import define_force_field_parameters


### FUNCTIONS / DEFINITIONS        
def findAllGoldVirtual(fullLattice_BotSurfaceAu_GoldOnly,GOLD_DIAMETER_NM = GOLD_DIAMETER_NM, bot=True):
    '''
    The purpose of this function is to take all your gold and predict which gold atoms can be used together to generate virtual sites in your ITP file. Note that if they are not aligned, we will need to use virtual-site 3
    
    INPUTS:
        1. fullLattice_BotSurfaceAu_GoldOnly: Full lattice coordinates for gold
        2. GOLD_DIAMETER_NM: gold diameter in angstroms
        3. bot: Are you on top of the gold or on the bottom? ** Important! Need to specify due to geometry -- Will fix later
    
    OUTPUTS:
        1. storeVirtualConnectors: List of list of connections
    '''
    import numpy as np
    # Defining tolerance (Hard coded)
    tolerance = 1E-5
    
    # Separating according to residue/gold
    totalGold = len(fullLattice_BotSurfaceAu_GoldOnly)
    goldArray = np.arange(0, totalGold)

    # Finding matching residues
    if bot==True:
        increment_x = np.array( [1.5*GOLD_DIAMETER_NM, 0, 0] )
        increment_y = np.array( [0, 0.5*GOLD_DIAMETER_NM*np.sqrt(3), 0] )
    else: # bot == False -- moving down
        increment_x = np.array( [1.5*GOLD_DIAMETER_NM, 0, 0] )
        increment_y = -np.array( [0, 0.5*GOLD_DIAMETER_NM*np.sqrt(3), 0] )
    # Storage array
    storeVirtualConnectors = []

    while np.size(goldArray) > 0: # Keep going until we run out of gold!
        indexOfZero_pos = np.zeros(2) # Creating an nonzero array for the while loop
        indexOfZero_neg = np.zeros(2)
    
        # Storing first values
        currentGold_VirtualStorage = [ goldArray[0] ]
        currentGoldCoordinates_pos = np.array(fullLattice_BotSurfaceAu_GoldOnly[ goldArray[0] ])
        currentGoldCoordinates_neg = np.array(fullLattice_BotSurfaceAu_GoldOnly[ goldArray[0] ])
        
        while np.size(indexOfZero_pos) > 0 or np.size(indexOfZero_neg) > 0: 
            # Increment, both directions
            currentGoldCoordinates_pos = currentGoldCoordinates_pos + increment_x + increment_y
            currentGoldCoordinates_neg = currentGoldCoordinates_neg - increment_x - increment_y # Captures negative portion
            
            # Checking in the positive side
            checkForZeros_pos = np.sum(np.array(fullLattice_BotSurfaceAu_GoldOnly[:]) - currentGoldCoordinates_pos[:], axis = 1) # Across columns
            indexOfZero_pos = np.where( checkForZeros_pos*checkForZeros_pos < tolerance)

            # Checking in the negative side
            checkForZeros_neg = np.sum(np.array(fullLattice_BotSurfaceAu_GoldOnly[:]) - currentGoldCoordinates_neg[:], axis = 1) # Across columns
            indexOfZero_neg = np.where( checkForZeros_neg*checkForZeros_neg < tolerance)
        
            if np.size(indexOfZero_pos) > 0: 
                # For positive
                currentGold_VirtualStorage.extend( [ int(indexOfZero_pos[0]) ] )
                goldArray = np.delete(goldArray[:], np.where( goldArray[:] == indexOfZero_pos )[-1] ) # Deleting when it's equal
            if np.size(indexOfZero_neg) > 0:
                # For negative            
                currentGold_VirtualStorage.extend( [ int(indexOfZero_neg[0]) ] )
                goldArray = np.delete(goldArray[:], np.where( goldArray[:] == indexOfZero_neg )[-1] ) # Deleting when it's equal
                
        # Deleting first value in array
        goldArray = np.delete(goldArray[:], 0) 
        
        # Storing all connections
        storeVirtualConnectors.append(currentGold_VirtualStorage)
        
        # Re-organizing storage connectors so they are sequential! (Only for top layer!)
        if bot==False: # Need to reorganize storage connectors so they are sequential!!!
            reOrganized_Connectors = []
            increment_x = np.array( [1.5*GOLD_DIAMETER_NM, 0, 0] )
            increment_y = -np.array( [0, 0.5*GOLD_DIAMETER_NM*np.sqrt(3), 0] )
            for eachLine in storeVirtualConnectors:
                if len(eachLine) > 1: # More than one
                    # Defining fixed line
                    fixedLine = []
        
                    # Defining locations
                    currentAtomLocations = [ fullLattice_BotSurfaceAu_GoldOnly[x] for x in eachLine ] # N x 3 Array
                    
                    # Find the smallest, which from geometry is the right-most and bottom-most value
                    smallestIndex = np.argmin( [ x[1] for x in currentAtomLocations ]  )# Y-values
                    
                    # Find next index given that you have the smallest index. We can do this by vectorization!
                    diffCoordinates = np.array(currentAtomLocations) - np.array(currentAtomLocations[ smallestIndex ])
                    checkCoordinates = np.sum(diffCoordinates*diffCoordinates,axis = 1) # Across columns
                    
                    # Sort and give back index
                    mySortedIndex = sorted(range(len(checkCoordinates)), key=lambda k: checkCoordinates[k])   
                    fixedLine = [ eachLine[x] for x in mySortedIndex ]
                    
                else:
                    fixedLine = list(eachLine)
                    
                reOrganized_Connectors.append(fixedLine) 
                storeVirtualConnectors = list(reOrganized_Connectors) # copying over if we have top layer
        
    return storeVirtualConnectors    
    
def generateVirtualItems_itp(storeVirtualConnectors, fullLattice_SurfaceAu_GoldOnly, virtual_increment_x, virtual_increment_y, virt2_a_frac, GOLD_DIAMETER_NM, bot=True):
    '''
    The purpose of this script is to to take the virtual connectors from "findAllGoldVirtual" script and transform them into itp readable lists
    Inputs:
        1. storeVirtualConnectors: virtual connectors given by findAllGoldVirtual function
        2. fullLattice_SurfaceAu_GoldOnly: Full lattice of gold -- used to generate virtual sites
        3. virtual_increment_x: How much increment do you need in x-direction for virtual site? (Angstroms)
        4. virtual_increment_y: How much increment do you need in y-direction for virtual site? (Angstroms)
        5. virt2_a_frac: What is "a" in your virtual_sites 2? 
        6. GOLD_DIAMETER_NM: what is the gold diameter?
        7. bot: Are we at the bottom monolayer or at the top? Put False if top of gold layer!
    Outputs:
        1. virtual_geom2: Geometry for virtual sites 2
        2. virtual_geom3fad: Geometry for virtual sites 3fad
        3. virtual_sites2_list: Input for itp file virtual sites 2
        4. virtual_sites3fad_list: Input for itp file virtual sites 3
    '''
    import numpy as np
    
    # Checking for top or bottom
    if bot == False:
        virtual_increment_y = -virtual_increment_y # Fixing increments for y
    
    # Calculating distance to virtual -- Used for virtual site 3
    dist2virtual = np.sqrt(np.sum(virtual_increment_x*virtual_increment_x + virtual_increment_y*virtual_increment_y)) # in Angstroms -- will convert later
    
    # Creating storage
    virtual_geom2 = []
    virtual_geom3fad = []
    virtual_sites2_list = []
    virtual_sites3fad_list = [] # Uses virtual site 3fad
    
    current_virtual_site_2 = 1
    current_virtual_site_3fad = 1
    for eachGoldList in storeVirtualConnectors:
        temp_geom_hold = []
        if len(eachGoldList) > 1: # For correctly interacting ones
            for goldIndex in range(len(eachGoldList)):
                currentGoldAtom = eachGoldList[goldIndex]        
                currentGoldLocation = fullLattice_SurfaceAu_GoldOnly[ currentGoldAtom ] # Getting current location
                
                # Generating virtual sites (geometry)
                upperGoldVirtual = np.array(currentGoldLocation) + virtual_increment_x + virtual_increment_y # Virtual atom #1
                lowerGoldVirtual = np.array(currentGoldLocation) - virtual_increment_x - virtual_increment_y # Virtual atom #2
                
                # Storing the geometry information
                temp_geom_hold.append(upperGoldVirtual.tolist())
                temp_geom_hold.append(lowerGoldVirtual.tolist())
                
                # Storing the current virtual sites
                # Finding next gold atom for increments in itp file (virtual_sites2)
                if goldIndex+1 < len(eachGoldList):
                    nextGoldAtom = eachGoldList[goldIndex+1]
                    virtual_sites2_list.append( [current_virtual_site_2, currentGoldAtom, nextGoldAtom, virt2_a_frac ] ); current_virtual_site_2+=1
                    virtual_sites2_list.append( [current_virtual_site_2, currentGoldAtom, nextGoldAtom, -virt2_a_frac ] ); current_virtual_site_2+=1
                else: # gold index is equal to the length!
                    prevGoldAtom = eachGoldList[goldIndex-1] # Need previous to reference!
                    virtual_sites2_list.append( [current_virtual_site_2, currentGoldAtom, prevGoldAtom, virt2_a_frac ] ); current_virtual_site_2+=1
                    virtual_sites2_list.append( [current_virtual_site_2, currentGoldAtom, prevGoldAtom, -virt2_a_frac ] ); current_virtual_site_2+=1
                
            virtual_geom2.extend(temp_geom_hold) # Storing geometries
            
        else: # For singly ones -- need to be third virtual site!! <-- Corner locations
            # Getting current Gold location
            currentGoldLocation = fullLattice_SurfaceAu_GoldOnly[ eachGoldList[0] ] # Getting current location
            
            # First, I need to find the 2 closest gold atoms (I choose the ones on the edge of parllelogram)
            diff2golds = np.array(fullLattice_SurfaceAu_GoldOnly) - np.array(currentGoldLocation)
            dist2golds = np.sqrt(np.sum( diff2golds*diff2golds, axis =1 )) - GOLD_DIAMETER_NM  # Summing and finding distance (r^2) 
            
            # Finding minimum
            minIndex = np.argsort( abs(dist2golds) )[0:3:2] # array 2 x 1 <-- Gets you two gold atoms
            
            # Generating virtual sites (geometry)
            upperGoldVirtual = np.array(currentGoldLocation) + virtual_increment_x + virtual_increment_y # Virtual atom #1
            lowerGoldVirtual = np.array(currentGoldLocation) - virtual_increment_x - virtual_increment_y # Virtual atom #2
            
            # Getting angles, first by using vectors. --Dot product helps to get vectors
            vec2Gold =  np.array( fullLattice_SurfaceAu_GoldOnly[minIndex[1]] ) - currentGoldLocation# Array pointing 
            # For upper virtual site
            vec2UpperVirt = upperGoldVirtual - currentGoldLocation
            
            # For lower virtual site
            vec2LowerVirt = lowerGoldVirtual - currentGoldLocation
            
            if bot == False:
                upperAngle = angle_clockwise( vec2Gold,vec2UpperVirt ) # in degrees (CCW)
                lowerAngle = angle_clockwise( vec2Gold,vec2LowerVirt ) # in degreES (CCW)
            else:
                upperAngle = 360 - angle_clockwise( vec2Gold,vec2UpperVirt ) # in degrees
                lowerAngle = 360 - angle_clockwise( vec2Gold,vec2LowerVirt ) # in degrees
            # Now that I have angles and distances, I can add to virtual_sites3fad
            # [ virtual_sites3 ]
            # ; Site from funct theta d
            # ; 5 1 2 3 3 120 0.5
            # Upper virtual site
            virtual_sites3fad_list.append( [current_virtual_site_3fad, eachGoldList[0], minIndex[1], minIndex[0], upperAngle, dist2virtual ] ); current_virtual_site_3fad+=1
            # Lower virtual site
            virtual_sites3fad_list.append( [current_virtual_site_3fad, eachGoldList[0], minIndex[1], minIndex[0], lowerAngle, dist2virtual ] ); current_virtual_site_3fad+=1
            
            # Storing the geometry information
            temp_geom_hold.append(upperGoldVirtual.tolist())
            temp_geom_hold.append(lowerGoldVirtual.tolist())
            
            virtual_geom3fad.extend(temp_geom_hold)
            
    return virtual_geom2, virtual_geom3fad, virtual_sites2_list, virtual_sites3fad_list        

### FUNCTION TO PRINT ITP FILE FOR GOLD ATOMS
def print_itp_file_gold(molecule_name, 
                        output_folder, 
                        molecule_resname, 
                        typename, 
                        atomname, 
                        charge, 
                        mass,
                        forcefield, 
                        constraint_matrix = None,
                        wantinterfaceff = False, 
                        wantVirtualSites = False, 
                        wantPolarization = False, 
                        virtual_sites2 = None, 
                        virtual_sites3 = None,
                        position_restraint = GOLD_PLANAR_POSITION_RESTRAINT):
    '''
    The purpose of this function is to print an ITP file for gold. 
    INPUTS:
        molecule_name: Name of molecule ( *.itp )
        output_folder: Folder you want to output the itp file
        molecule_resname: Residue name (5 letters max)
        typename: name type of the atom, typically opls_ something for OPLS-aa
        atomname: atom name
        charge: list of charges
        mass: list of masses
        virtual_sites2: virtual site list outputted from generateVirtualItems_itp
        virtual_sites3: virtual site list outputted from generateVirtualItems_itp
        wantVirtualSites: True if you want virtual sites (CHARMM-GolP)
        forcefield: What force field are you using? ( opls-aa or charmm36-nov2016.ff )
        wantinterfaceff: Want interface force field? (Polarization off + virtual sites off)
        position_restraint: [str] position restraint
    OUTPUTS:
        itp file
    '''
    ## DEFINING ITP FILE NAME
    output_itp_name = os.path.join(output_folder, molecule_name + '.itp') # Output .itp file name
    print("Outputting %s"%(output_itp_name))
    
    # Creating output file
    outputfile = open(output_itp_name, "w") # Now, writing in output file
    
    # Finding total number of atoms
    totalAtoms = len(typename)
    
    # -- MOLECULE INFO -- #
    outputfile.write("; Gold itp file: %s \n\n"%(output_itp_name))
    
    # write molecule type info; for GROMOS force field
    # ATOMTYPES -- Curently hard coded
    if forcefield == "charmm36-nov2016.ff" or forcefield == "opls-aa":
        outputfile.write("; Atomtypes describing LJ interactions \n")
    if forcefield == "charmm36-nov2016.ff":
        if wantinterfaceff != True:
            outputfile.write("; CHARMM-GolP Parameters")
        else: # Interface force field is on!
            outputfile.write("; Interface Force Field CHARMM Parameters")
    elif forcefield == "opls-aa":
        if wantinterfaceff != True:
            outputfile.write("; OPLS-GolP Parameters")
        else: # Interface force field is on!
            outputfile.write("; Interface Force Field OPLS-AA Parameters")
    if forcefield != "pool.ff":
        outputfile.write("\n[ atomtypes ]\n")
        outputfile.write("; name  at.num    mass    charge   ptype          sigma(nm)      epsilon (kJ/mol) \n")        
    
    ## Atom types: Virtual Sites ##
    if wantVirtualSites==True:
        if forcefield == "charmm36-nov2016.ff":
            outputfile.write("  MAU   79          0.000   0.000    A              0.380      0.480 ; Virtual sites on the surface\n")
        elif forcefield == "opls-aa":
            outputfile.write("  MAU   79          0.000   0.000    A              0.320    0.650 ; Virtual sites on the surface\n")
        if forcefield != "pool.ff":
            outputfile.write("  SAu   79          196.9965   0.000    A              0.000      0.000 ; No LJ potential for blank golds (surface) \n")
        
    else: # No virtual sites, the Surface Au needs VDWs!!!    
        if forcefield == "charmm36-nov2016.ff" and wantinterfaceff != True:
            outputfile.write("  SAu   79          196.9965   0.000    A              0.380      0.480 ; LJ Potential for surface only if there is no virtual sites \n")        
        elif forcefield == "opls-aa" and wantinterfaceff != True:
            outputfile.write("  SAu   79          196.9965   0.000    A              0.320    0.650 ; LJ Potential for surface only if there is no virtual sites \n")
        elif wantinterfaceff == True: # Interface force field on!
#            outputfile.write("  SAu   79          196.9965   0.000    A              0.2951      22.13336 ; LJ Potential for surface only if there is no virtual sites \n")
            outputfile.write("  SAu   79          196.9965   0.000    A              0.2951      0.001 ; LJ Potential for surface only if there is no virtual sites \n")
            
    ## Atom Types: Bulk Atoms
    if forcefield == "charmm36-nov2016.ff" and wantinterfaceff != True:
        outputfile.write("  BAu   79         196.9965   0.000    A              0.380      0.480 ; Bulk gold interactions \n")
    elif forcefield == "opls-aa" and wantinterfaceff != True:
        outputfile.write("  BAu   79         196.9965   0.000    A              0.320    0.650 ; Bulk gold interactions \n")
    elif wantinterfaceff == True: # Interface force field on!
#        outputfile.write("  BAu   79         196.9965   0.000    A              0.2951      22.13336 ; Bulk gold interactions \n")
        outputfile.write("  BAu   79         196.9965   0.000    A              0.2951      0.001 ; Bulk gold interactions \n")
        
    ## Atom types: Polarization, rigid rod dipole ##
    if wantPolarization==True:
        outputfile.write("  RAu   0         0.5   0.3    A   0.0 0.0; Special dummy atoms (Rotating golds) \n")
    
    if forcefield != "pool.ff":
        # NONBOND_PARAMS -- used to prevent gold atoms from interacting with itself!  
        outputfile.write("\n[ nonbond_params ]\n")
        outputfile.write("; i     j  func     sigma       epsilon  \n")  # Sigma and epsilon?
        if wantVirtualSites==True:
            outputfile.write("  MAU   MAU  1          0.000   0.000  ; No virtual to virtual site interaction\n")
            outputfile.write("  MAU   SAu  1          0.000   0.000  ; No virtual site to blank gold surface interaction\n")
            outputfile.write("  SAu   MAU  1          0.000   0.000  ; No virtual site to blank gold surface interaction\n")
            outputfile.write("  MAU   BAu  1          0.000   0.000  ; No virtual site to bulk gold interaction\n")
            outputfile.write("  BAu   MAU  1          0.000   0.000  ; No virtual site to bulk gold interaction\n")
            if forcefield == "charmm36-nov2016.ff": # Sulfur to virtual site
                outputfile.write("  SG311   MAU  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
                outputfile.write("  MAU   SG311  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            elif forcefield == "opls-aa":
                outputfile.write("  opls_200   MAU  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
                outputfile.write("  MAU   opls_200  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
                
        outputfile.write("  BAu   BAu  1          0.000   0.000  ; No bulk to bulk gold interaction \n")
        outputfile.write("  BAu   SAu  1          0.000   0.000  ; No bulk to surface gold interaction \n")
        outputfile.write("  SAu   BAu  1          0.000   0.000  ; No bulk to surface gold interaction \n")
        outputfile.write("  SAu   SAu  1          0.000   0.000  ; No Surface to surface gold interactions\n")
        # Gold to sulfur interactions
        if forcefield == "charmm36-nov2016.ff":
            outputfile.write("  SG311   SAu  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  SAu   SG311  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  SG311   BAu  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  BAu   SG311  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
        elif forcefield == "opls-aa":
            outputfile.write("  opls_200   SAu  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  SAu   opls_200  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  opls_200   BAu  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
            outputfile.write("  BAu   opls_200  1          0.000   0.000  ; No Gold to Sulfur interactions\n")
    
    outputfile.write("\n[ moleculetype ]\n")
    outputfile.write("; Name   nrexcl\n")
    #now molecule name + 3 for exclusions
    outputfile.write("%s    3\n\n"%(molecule_resname))
    # ATOM PROPERTIES [ atoms ]
    outputfile.write("[ atoms ]\n")
    outputfile.write(";   nr    type   resnr  residu    atom    cgnr   charge    mass\n") # assume charge, mass already defined by atomtypes
    for i in range(0, totalAtoms):
        # Charge groups don't matter for recent
        outputfile.write("%4d %10s  %d %s    %s %4d  %.3f   %.4f\n"%(i+1, typename[i], 1, molecule_resname.rjust(10, ' '), atomname[i].rjust(5, ' '), 1, charge[i], mass[i]))
    if wantVirtualSites==True:
        # VIRTUAL SITES 2
        outputfile.write("\n[ virtual_sites2 ]\n")
        outputfile.write(";  site  ai  aj  funct   a \n")
        for i in range(0, len(virtual_sites2)):
            outputfile.write("     %d   %d   %d      %d   %.7f\n"%(virtual_sites2[i][0], virtual_sites2[i][1], virtual_sites2[i][2], 1, virtual_sites2[i][3])) # 3 for function type
        # VIRTUAL SITES 3
        outputfile.write("\n[ virtual_sites3 ]\n")
        outputfile.write(";  Site  from            funct    theta      d \n")            
        for i in range(0, len(virtual_sites3)): # Possible error with virtual sites 3 --- Order of numbering incorrect
            outputfile.write("%5d%4d%4d%4d %5d     %.2f      %.7f\n"%(virtual_sites3[i][0], virtual_sites3[i][1], virtual_sites3[i][2], virtual_sites3[i][3], 3, virtual_sites3[i][4], virtual_sites3[i][5] )) # 3 for function 
            
    # POSITION RESTRAINTS
    if forcefield != "pool.ff":
        outputfile.write("\n; Position restraints for all atoms, except dummy atoms\n")
        outputfile.write("[ position_restraints ]  \n")
        if wantPolarization==True:
            totalAtoms_Constrainted = totalAtoms + 1 - len(constraint_matrix)
        else:
            totalAtoms_Constrainted = totalAtoms + 1
        
        for i in range(1, totalAtoms_Constrainted):
            outputfile.write(" %d  1   %s   %s   %s\n"%(i, position_restraint, position_restraint, position_restraint))
    
    # CONSTRAINTS 
    if wantPolarization==True:
        outputfile.write("\n ; CONSTRAINTS for all dummy atoms \n")
        outputfile.write("[ constraints ]  \n")
        outputfile.write(";  i  j    funct   distance \n")
        for currentConstraint in constraint_matrix:
            outputfile.write(" %d   %d  %d   %.4f \n"%(currentConstraint[0], currentConstraint[1], currentConstraint[2], currentConstraint[3] ) )
        
    # -- MOLECULE INFO -- #
    outputfile.close()   
    return

### FUNCTION TO EXPAND THE GOLD, AND PRINT AN ITP FILE FOR THAT
def au_111_expansion_itp_CHARMM_GolP(itp_file, 
                                     repeat_x, 
                                     repeat_y, 
                                     repeat_z, 
                                     forcefield, 
                                     output_folder, 
                                     wantVirtualSites = False, 
                                     wantPolarization = False, 
                                     wantinterfaceff = True):
    '''
    The purpose of this function is to take the expansion requirements and generate virtual atoms according to CHARMM GolP (two virtual sites per gold). This will print an itp file as required
    INPUTS:
        repeat_x: [int] Number of repeats in the x-dimension
        repeat_y: [int] Number of repeats in the y-dimension
        repeat_z: [int] Number of repeats in the z-dimension
        output_folder: [float] Where do you want the itp file?
        molecule_name: [str] what do you want to call the itp file?
        wantVirtualSites: [logical] True if you want virtual sites (CHARMM-GolP)
        wantPolarization: [logical] True if you want rigid rod dipoles
        forcefield: [logical] What force field are you using?
        wantinterfaceff: [logical] Want interface force field?
    OUTPUTS:
        Residue name 
        Coordinates of the gold + virtual atoms
        Atom names for the gold + virtual atoms
        itp file
    '''
    # Defining gold coordinates (hardcoded) -- in Angstrom units
    gold2goldDist_diag = GOLD_DIAMETER_NM*np.sqrt(3) # Total Diagonal distance
    virt2_a_dist = GOLD_DIAMETER_NM*np.tan(np.pi/6) # D tan 30 for "a" constant in gold -- Virtual site #2
    virt2_a_frac = virt2_a_dist / gold2goldDist_diag
    
    ## FOR INTERFACE FF -- NO POLARIZATON & VDW
    if wantinterfaceff == True:
        wantVirtualSites=False # Turn off virtual sites
        wantPolarization=False # Turn off polarization
    ## FOR CHARMM-GOLP -- POLARIZATION IS ON (DUMMY ATOMS)
    if wantPolarization == True:
        dist2dummy = 0.7 # In angstroms
        massOfdummy = 0.5 # amus
        constraint_func=1 # Function of bond
        AU_CHARGE = -0.300 # New Au charge
        chargeOfdummy=0.300 # Charge of dummy atom
        dummy_typename="RAu"
        dummy_atomname="RAu"
    else:
        from MDBuilder.applications.nanoparticle.global_vars import AU_CHARGE
    
    ## DEFINING VIRTUAL ATOM PARAMETERS
    virtual_increment_x = np.array( [ GOLD_DIAMETER_NM/2., 0, 0 ] ) # Angstroms
    virtual_increment_y = np.array( [ 0, np.tan(np.pi/6)*GOLD_DIAMETER_NM/2., 0 ] ) # Angstroms
    
    ## FINDING COORDINATES FOR PURE GOLD
    from MDBuilder.math.make_fcc_lattice import create_gold_fcc_111_lattice
    fcc_lattice = create_gold_fcc_111_lattice(
                                     repeat_x = repeat_x,
                                     repeat_y = repeat_y,
                                     repeat_z = repeat_z
                                     )
    ## FINDING SURFACE AND BULK GOLD ATOMS
    fullLattice_TopSurfaceAu_GoldOnly, fullLattice_BulkAu_GoldOnly, fullLattice_BotSurfaceAu_GoldOnly = fcc_lattice.find_surface_bulk_atoms()

    # Total gold atoms on each surface
    totalGoldAtoms_Surface = len(fullLattice_BotSurfaceAu_GoldOnly) # Finds total gold atoms
    total_gold_atoms_bulk = len(fullLattice_BulkAu_GoldOnly)
    totalGoldAtoms = len(fullLattice_TopSurfaceAu_GoldOnly) + len(fullLattice_BotSurfaceAu_GoldOnly) + len(fullLattice_BulkAu_GoldOnly)
    print("Total Gold Atoms: %d"%(totalGoldAtoms))
    
    # Creating full geometry
    full_geom_gold= np.concatenate( (fullLattice_BotSurfaceAu_GoldOnly, fullLattice_TopSurfaceAu_GoldOnly, fullLattice_BulkAu_GoldOnly ) )
    # full_geom_gold= fullLattice_BotSurfaceAu_GoldOnly + fullLattice_TopSurfaceAu_GoldOnly + fullLattice_BulkAu_GoldOnly 
    full_geom = [ list(coords) for coords in full_geom_gold ] # Copying gold geometry
    
    # Creating type list, etc.
    typename_list = [PLANAR_SURFACEAU_TYPENAME] * totalGoldAtoms_Surface + [ PLANAR_SURFACEAU_TYPENAME ] * totalGoldAtoms_Surface + [PLANAR_BULK_AU_TYPENAME] * total_gold_atoms_bulk
    atomname_list = [PLANAR_SURFACEAU_ATOMNAME] * totalGoldAtoms_Surface + [ PLANAR_SURFACEAU_ATOMNAME ] * totalGoldAtoms_Surface + [PLANAR_BULK_AU_ATOMNAME] * total_gold_atoms_bulk
    print("length of atom list: %d"%(len(atomname_list)) )
    mass_list = [GOLD_MASS] * totalGoldAtoms # Only Au has mass
    charge_list = [AU_CHARGE] * totalGoldAtoms # Creating of charge list
    
    if wantVirtualSites==True:
        # Finding "bonds" between gold that link the virtual atoms -- required for ITP file
        storeVirtualConnectors_Bot = findAllGoldVirtual(fullLattice_BotSurfaceAu_GoldOnly,GOLD_DIAMETER_NM, bot=True)
        storeVirtualConnectors_Top = findAllGoldVirtual(fullLattice_TopSurfaceAu_GoldOnly,GOLD_DIAMETER_NM, bot=False)
        
        # Now that we have the correct virtual connectors for top and bottom, we need the virtual atoms associated with them.
        # The goal is to have gold atoms first, then have virtual sites. Then, we will "bind" the virtual sites to the gold!
        # Alternatively, we can re-create the virtual sites given that we know each gold atom and the correct distances
        virtual_geom2_Bot, virtual_geom3fad_Bot, virtual_sites2_list_Bot, virtual_sites3fad_list_Bot = generateVirtualItems_itp(storeVirtualConnectors_Bot, fullLattice_BotSurfaceAu_GoldOnly, virtual_increment_x, virtual_increment_y,virt2_a_frac=virt2_a_frac, GOLD_DIAMETER_NM = GOLD_DIAMETER_NM, bot=True)
        virtual_geom2_Top, virtual_geom3fad_Top, virtual_sites2_list_Top, virtual_sites3fad_list_Top = generateVirtualItems_itp(storeVirtualConnectors_Top, fullLattice_TopSurfaceAu_GoldOnly, virtual_increment_x, virtual_increment_y,virt2_a_frac=virt2_a_frac, GOLD_DIAMETER_NM = GOLD_DIAMETER_NM, bot=False)
        
        # Now, conglomerating it all and creating an ITP file
    
        # Start with the bottom
        totalVirtualAtoms = len(virtual_geom2_Bot) # Finds total virtual atoms of type 2
        # Creating full geometry (Lists, not numpy)
        full_geom = [ list(coords) for coords in full_geom_gold ] + virtual_geom2_Bot + virtual_geom2_Top + virtual_geom3fad_Bot + virtual_geom3fad_Top
        
        # Creating virtual_sites2 -- Note: we will need to shift gold / virtual sites accordingly
        
        # Start by correcting for the bottom
        firstVirtualSite2_bot = 3*totalGoldAtoms_Surface # Since we had gold first
        cor_virtual_sites2_list_Bot = [] # Corrected virtual sites 2
        for virtual_sites2_bot in virtual_sites2_list_Bot:
            current_virtual_sites2 = virtual_sites2_bot[:]
            current_virtual_sites2[0] = current_virtual_sites2[0] + firstVirtualSite2_bot # Correcting for current virtual site
            current_virtual_sites2[1] = current_virtual_sites2[1] + 1 # Correcting for the fact that we count from 1 for gold
            current_virtual_sites2[2] = current_virtual_sites2[2] + 1 # Correcting for the fact that we count from 1 for gold
            cor_virtual_sites2_list_Bot.append( current_virtual_sites2 )
        
        # Then for the top
        firstVirtualSite2_top = 3*totalGoldAtoms_Surface + totalVirtualAtoms # Since we had gold first and virtual sites for bottom
        cor_virtual_sites2_list_Top = [] # Corrected virtual sites 2
        for virtual_sites2_top in virtual_sites2_list_Top:
            current_virtual_sites2 = virtual_sites2_top[:]
            current_virtual_sites2[0] = current_virtual_sites2[0] + firstVirtualSite2_top # Correcting for current virtual site
            current_virtual_sites2[1] = current_virtual_sites2[1] + 1 + totalGoldAtoms_Surface  # Correcting for the fact that we count from 1 for gold and initial gold layer
            current_virtual_sites2[2] = current_virtual_sites2[2] + 1 + totalGoldAtoms_Surface  # Correcting for the fact that we count from 1 for gold and initial gold layer
            cor_virtual_sites2_list_Top.append( current_virtual_sites2 )
        
        # Summing virtual sites
        virtual_sites_2 = cor_virtual_sites2_list_Bot + cor_virtual_sites2_list_Top
            
        # Correcting virtual_sites3 -- Will need to shift gold + virtual atoms
        # Start with bottom
        firstVirtualSite3_bot = firstVirtualSite2_top + totalVirtualAtoms
        cor_virtual_sites3_list_Bot = [] # Corrected virtual sites 3
        for virtual_sites3_bot in virtual_sites3fad_list_Bot:
            current_virtual_sites3 = virtual_sites3_bot[:]
            current_virtual_sites3[0] = current_virtual_sites3[0] + firstVirtualSite3_bot # Correcting for current virtual site
            current_virtual_sites3[1] = current_virtual_sites3[1] + 1 # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[2] = current_virtual_sites3[2] + 1 # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[3] = current_virtual_sites3[3] + 1 # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[-1] = current_virtual_sites3[-1] / 10 # in nms
            cor_virtual_sites3_list_Bot.append( current_virtual_sites3 )
    
        # Correcting virtual_sites3 -- Will need to shift gold + virtual atoms
        firstVirtualSite3_top = firstVirtualSite3_bot + len( virtual_sites3fad_list_Bot ) 
        cor_virtual_sites3_list_Top = [] # Corrected virtual sites 3
        for virtual_sites3_top in virtual_sites3fad_list_Top:
            current_virtual_sites3 = virtual_sites3_top[:]
            current_virtual_sites3[0] = current_virtual_sites3[0] + firstVirtualSite3_top # Correcting for current virtual site
            current_virtual_sites3[1] = current_virtual_sites3[1] + 1 + totalGoldAtoms_Surface  # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[2] = current_virtual_sites3[2] + 1 + totalGoldAtoms_Surface  # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[3] = current_virtual_sites3[3] + 1 + totalGoldAtoms_Surface  # Correcting for the fact that we count from 1 for gold
            current_virtual_sites3[-1] = current_virtual_sites3[-1] / 10 # in nms
            cor_virtual_sites3_list_Top.append( current_virtual_sites3 )
            
        virtual_sites_3fad = cor_virtual_sites3_list_Bot + cor_virtual_sites3_list_Top
    
        # Adding to typelist, atomlist, etc.
        typename_list = typename_list[:] + [VIRTUAL_TYPENAME] * (totalVirtualAtoms + len( virtual_sites3fad_list_Bot ) )*2
        atomname_list = atomname_list[:]+ [VIRTUAL_ATOMNAME] * (totalVirtualAtoms + len( virtual_sites3fad_list_Bot ) )*2
    
        # Creating charges and masses
        mass_list = mass_list[:] + [0] * (totalVirtualAtoms + len( virtual_sites3fad_list_Bot ) )*2 # Only Au has mass
        charge_list = charge_list[:] + [0]*(totalVirtualAtoms + len( virtual_sites3fad_list_Bot ) )*2 # No charge
    
    else:
        virtual_sites_2 = None
        virtual_sites_3fad = None

    
    # Adding polarization dummy atoms
    if wantPolarization==True:
        
        ## Constraint matrix ##
        nextIndex= len(atomname_list) + 1 # Next index, necessary for creating constraints file
        constraint_matrix = [] # creating empty matrix for constraints
        dist2dummy_nm=dist2dummy/10.0 # in nm
        for currentGoldIndex in range(totalGoldAtoms):
            constraint_matrix.append( [currentGoldIndex+1,nextIndex+currentGoldIndex, constraint_func, dist2dummy_nm  ])
            
        # Finding the geometry
        polar_geom = ( np.array(full_geom_gold[:]) + np.array( [dist2dummy, 0, 0]) ).tolist() # Geometry of polar atoms (assumed into the x-direction, arbitrary)
        
        ## With geometry and constraints, we can add to the list! ##
        
        # Adding to geometry
        full_geom= full_geom[:] + polar_geom[:]

        # Adding to typelist, atomlist, etc
        typename_list = typename_list[:] + [dummy_typename] * totalGoldAtoms
        atomname_list = atomname_list[:]+ [dummy_atomname] * totalGoldAtoms

        # Adding to mass and charges
        mass_list = mass_list[:] +  [ massOfdummy ] * totalGoldAtoms
        charge_list = charge_list[:] + [chargeOfdummy] * totalGoldAtoms

    else:
        constraint_matrix = None

    ## Converting everything from angstroms to nm
    # full_geom_nm = (np.array(full_geom[:]) / 10.).tolist() # in nm
    full_geom_nm = full_geom[:]  # (np.array(full_geom[:]) / 10.).tolist() # in nm
    
    ## printing gold itp file name
    print_itp_file_gold(molecule_name= itp_file,
                        output_folder= output_folder, 
                        molecule_resname= PLANARGOLDRESNAME, 
                        typename = typename_list, 
                        atomname = atomname_list, 
                        charge = charge_list, 
                        mass = mass_list, 
                        virtual_sites2 = virtual_sites_2, 
                        virtual_sites3 = virtual_sites_3fad,
                        wantVirtualSites = wantVirtualSites,
                        constraint_matrix = constraint_matrix,
                        wantPolarization = wantPolarization,
                        forcefield = forcefield,
                        wantinterfaceff=wantinterfaceff)
    
    # Returning molecule residue name, geometry, and atom list -- Used for GRO file
    return PLANARGOLDRESNAME, full_geom_nm, atomname_list
    
def print_top_file(output_top, output_folder, golditpname, gold_resname, forcefield_folder):
    '''
    The purpose of this script is to create a topology file
    INPUTS:
        output_top: Output topology folder
        output_folder: Output folder
        golditpname: name of your gold itp file
        gold_resname: residue name for your gold
        forcefield_folder: Force field folder (i.e. opls-aa, etc.)
    OUTPUTS:
        output_top.top file
    '''
    forcefield_itp, forcefield_water, forcefield_ions, forcefield_methanol, _, _, _ = define_force_field_parameters( forcefield_folder )
    output_top_name = output_folder + '/' + output_top
    outputfile = open(output_top_name, "w")
    print("Outputting %s"%(output_top_name))
    # write basic topology information here
    outputfile.write(";  Topology file for Gold \n" )
    # include statements
    outputfile.write("#include \"%s\"\n"%(forcefield_itp))
    outputfile.write("#include \"%s\"\n"%(golditpname + ".itp"))
    # include all ligand names
#    for ligname in ligand_names:
#        outputfile.write("#include \"%s.itp\"\n"%(ligname))
    #outputfile.write("#include \"%s\"\n"%(GOLD_ITP_NAME))
    # add water here, although the molecules are added separately
    outputfile.write("#include \"%s\"\n"%(forcefield_water))
    outputfile.write("#include \"%s\"\n"%(forcefield_ions))
    outputfile.write("#include \"%s\"\n"%(forcefield_methanol))
    outputfile.write("\n[ system ]\n")
    outputfile.write("frame t=1.000 in water\n")
    outputfile.write("\n[ molecules ] \n")
    outputfile.write("; compound   nr_mol\n")
    # assume double monolayer for now; output for each ligand in order
#    for j in range(0, 2):
#        for i in range(0, len(ligand_names)):
#            lig=ligands[i]
#            outputfile.write(" %s     %d\n"%(lig.ligand_name, ligand_nums[i]))
    # Inputting gold atoms
    outputfile.write(" %s     %d\n"%(gold_resname, 1))
    outputfile.close()
    
if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt # matlab plotting tools
    from optparse import OptionParser # Used to allow commands within command line
#    plt.close('all')
    
    # Setting testing parameter
    testing = check_testing() # False if you are running from a bash script

    if testing == False:
    
        # Including parse options
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        parser.add_option("-i", "--prep", dest="Prep_Folder", action="store", type="string", help="Preparation folder", default=".")
        parser.add_option("-v", "--vir", dest="wantVirtualSites", action="store", type="string", help="True Or False, Want Virtual Sites?", default=".")
        parser.add_option("-p", "--pol", dest="wantPolarization", action="store", type="string", help="True Or False, Want Polarization (Rigid rod dipole)?", default=".")
        # Force field
        parser.add_option("-m", "--ff", dest="forcefield", action="store", type="string", help="Force field, typically a directory", default=".")
        # Interface Force field
        parser.add_option("-ff", "--int", dest="wantinterfaceff", action="store", type="string", help="True Or False, Want Interface Force Field Parameters?", default=".")        
        # Extracting parser
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        output_folder = options.output_folder # Output folder (for itps)
        prep_folder = options.Prep_Folder # Preparation folder
        wantVirtualSites=bool(options.wantVirtualSites=="True") # True if you want virtual sites
        wantPolarization=bool(options.wantPolarization=="True") # True if you want rigid rod dipoles
        wantinterfaceff=bool(options.wantinterfaceff=="True") # True if you want interface force field
        forcefield_folder = options.forcefield # Preparation folder
    else:
        output_folder="R:/scratch/LigandBuilder/Simulations/Gold_CHARMM_Test_Virt_correction_virt3"
        prep_folder = "R:/scratch/LigandBuilder/Prep_System"
        # True/False for flexibility
        wantVirtualSites=True
        wantPolarization=False
        wantinterfaceff=False
        forcefield_folder="charmm36-nov2016.ff"
        
    # File output names
    golditpname = 'Au_111_CHARMM-GolP'
    output_gro_name = "gold.gro"
    output_top = "gold.top"

    # User defined variables
    num_lig_x = 8 # Number of x-ligands -- Tested with 4
    num_lig_y = 8 # Number of y-ligands -- Tested with 2
    num_z_spacing = 0 # Number of z- multiplier
    
    # Gold periodic boundary
    periodicBoundary = np.array([2.8837, 4.9948, 7.0637])/10 # in nms
    xDist = (3 + int( 3/2.*(num_lig_x-2) )) * periodicBoundary[0]
    yDist= (num_lig_y) * periodicBoundary[1]

    # Offset in z-direction for gold. Prevent PBC from cutting it apart
    offset_z=periodicBoundary[0]/2.0 + 0.5 # Diameter divided by two added for safety
    box_dimension=[xDist, yDist, periodicBoundary[2] + offset_z+0.5]

    # Finding coordinates, residue names, etc.
    gold_resname, goldCoordinates, atomname_list = au_111_expansion_itp_CHARMM_GolP(num_x_ligands =3 + int( 3/2.*(num_lig_x-2) ) - 1,
                                                                                     num_y_ligands = num_lig_y - 1,
                                                                                     num_z_spacing=0, 
                                                                                     output_folder= output_folder, 
                                                                                     molecule_name = golditpname,
                                                                                     wantVirtualSites=wantVirtualSites,
                                                                                     wantPolarization=wantPolarization,
                                                                                     forcefield = forcefield_folder,
                                                                                     wantinterfaceff = wantinterfaceff)
   
    goldCoordinates = [ [x[0], x[1], x[2]+offset_z]  for x in goldCoordinates ] # offsetting by z
    totalGoldAtoms_Surface = len(goldCoordinates)
    
    # Adding to total atoms
    total_atoms =  totalGoldAtoms_Surface # Adding total gold atoms
    
    # Adding to total residues
    goldArray =[1] * totalGoldAtoms_Surface 
    total_resids = goldArray[:]
    
    # Adding to geometry
    total_geom = goldCoordinates[:]
    
    # Adding to output name
    GoldOutputName = []
    for currentGoldAtom in range(0, len(atomname_list)):
        GoldOutputName.append( "%5s%5s" %(gold_resname, str(atomname_list[currentGoldAtom]) ) )

    total_outputnames =  GoldOutputName[:]
    
    print_gro_file(output_gro_name, output_folder, total_atoms, total_resids, total_geom, total_outputnames, box_dimension)
    print_top_file(output_top, output_folder, golditpname, gold_resname, forcefield_folder)
    

