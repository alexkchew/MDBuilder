# -*- coding: utf-8 -*-
"""
make_gold_spherical_111.py
The purpose of this script is to make a 111 block and truncate the block into a spherical shape. 
PREREQUISITES:
    Lattice constant
    
CREATED ON: 11/29/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

FUNCTIONS:
    main: Main function for this script

### UPDATES ###
171130 - Added functionality to output top, itp files
180503 - Updated script to improve flexibiity


"""
#%%
### IMPORTING FUNCTIONS
import numpy as np # Math

### IMPORTING FROM PLANAR CASE
from MDBuilder.builder.make_gold_planar_111 import print_itp_file_gold
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

### IMPORTING FROM SELF ASSEMBLY SCRIPT
import MDBuilder.ligand.prep_self_assembly_ligands as prep_self_assembly_ligands

## IMPORTING OPTION PARSER TOOLS
import sys
#is_py2 = sys.version[0] == '2'
#if is_py2:
#    import Queue as queue
#else:
#    import queue as queue
from optparse import OptionParser # Used to allow commands within command line

### DEFINING GLOBAL VARIABLES
# ATOM TYPES FOR GOLD
from MDBuilder.applications.nanoparticle.global_vars import BULKAU_TYPENAME, GOLD_MASS, AU_CHARGE
from MDBuilder.applications.nanoparticle.global_vars import GOLD_ATOM_NAME as ATOM_NAME
from MDBuilder.applications.nanoparticle.global_vars import GOLD_FCC_LATTICE_CONSTANT_NM

### MAIN FUNCTION TO MAKE GOLD SPHERICAL NPS
def main( geometry, radius, distance_from_edge, force_field_type, output_folder, output_gro, output_top, molecule_name, molecule_res_name, num_lig_x = None, num_lig_y = None, length = None,
          input_dir = None, input_file = None, wantPlot = False    ):
    '''
    Main function for making spherical gold cores. 
    Assumptions:
        - the Au atom type is "Au" and the name is 'BAu', representing bulk gold atoms
        - Mass of gold is 196.9965 
        - Zero charge for Au
    INPUTS:
        geometry: type of geometry you want, for example:
            'spherical': spherical cut from a {111} facet
            'hollow': perfect sphere that is hollow
            'cylindrical_curved_ends': cylinder with curved ends
        radius: [float]
            radius of the gold sphere in nm
        distance_from_edge: [float]
            How far you want the edge of the  box to be from the sphere?
        force_field_type: [str]
            Type of forcefield (i.e. "charmm36-nov2016.ff")
        output_folder: [str]
            Where to output all the files
        output_gro: [str]
            output gro file name
        output_top: [str]
            Output top file name
        molecule_name: [str] 
            Name of the itp file
        molecule_res_name: [str]
            Name of the gold residue
        num_lig_x: [int, default = None]
            number of ligands in the x-dimension
        num_lig_y: [int, default = None]
            number of ligands in the y-dimension
        length: [float, default = None] 
            length of cylinder
        input_dir: [str] input directory (OPTIONAL, default = None)
        input_file: [str] input file (OPTIONAL, default = None)
        wantPlot: Logical -- true if you want to see the sphere
    OUTPUT:
        GRO, ITP, TOP FILE in the output_folder
    '''

    if geometry == 'spherical':
        ### GETTING SPHERICAL COORDINATES BASED ON AU-111 FACET
        from MDBuilder.math.make_fcc_lattice import create_spherical_fcc_lattice
        sphere_fcc_lattice = create_spherical_fcc_lattice(fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM,  radius = radius )
        sphere_coord, center_of_box = sphere_fcc_lattice.geometry, sphere_fcc_lattice.center_atom_position
        #  find_spherical_au_111_coordinates(radius,wantPlot)
    elif geometry == 'hollow':
        ### IMPORTING GLOBAL VARIABLES
        from MDBuilder.applications.nanoparticle.global_vars import LIGANDPERNM2
        from MDBuilder.builder.make_gold_hollow import create_hollow_core, METAL_TO_LIGAND_RATIO, TOTAL_STEPS
        hollow_core = create_hollow_core( ligand_per_nm_squared = LIGANDPERNM2,
                                      radius = radius,
                                      num_runs = TOTAL_STEPS,
                                      metal_to_ligand_ratio = METAL_TO_LIGAND_RATIO,
                                     )
        sphere_coord, center_of_box = hollow_core.coordinates, hollow_core.center_of_box
        
    ######### CYLINDRICAL GEOMETRY
    elif geometry == 'cylindrical_curved_ends':
        ### IMPORTING GLOBAL VARIABLES
        from MDBuilder.math.make_fcc_lattice import create_cylindrical_curved_fcc_lattice
        ## CREATING CYLINDER WITH SPHERICAL ENDS
        cylin = create_cylindrical_curved_fcc_lattice(
                                            fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM,
                                            radius = radius,
                                            length = length,
                                            )
        ## STORING COORDINATES AND CENTER OF BOX
        sphere_coord, center_of_box = cylin.geometry, cylin.center_atom_position
    
    ######### PLANAR GEOMETRY
    elif geometry == 'planar':
        ### IMPORTING IMPORTANT VARIABLES
        from MDBuilder.applications.nanoparticle.global_vars import GOLD_DIAMETER_NM
        from MDBuilder.math.make_fcc_lattice import create_gold_fcc_111_lattice
        
        ## FINDING NUMBER OF REPEATS (Hardcoded by math)
        repeat_x = 3 + int( 3/2.*(num_lig_x-2)) - 1
        repeat_y = num_lig_y - 1
        ## DEFAULTING REPEAT_Z TO BE ZERO (generally num_layers - 1)
        repeat_z = 0
        ## FINDING COORDINATES FOR PURE GOLD
        fcc_lattice = create_gold_fcc_111_lattice(
                                         repeat_x = repeat_x,
                                         repeat_y = repeat_y,
                                         repeat_z = repeat_z,
                                         )
        ## DEFINING GEOMETRY
        sphere_coord = fcc_lattice.geometry
        new_sphere_coord = sphere_coord
        
        ### FINDING BOX SIZE BASED ON GEOMETRY
        box_dim = np.zeros(3, dtype='float')
        # set x/y dimensions based on number of ligands and presumed gold diameter
        box_dim[0] = num_lig_x * 1.5 * GOLD_DIAMETER_NM
        box_dim[1] = num_lig_y * GOLD_DIAMETER_NM * np.tan(np.pi/6)*1.5*2
        
        ## FINDING MAX AND MIN GEOMETRY
        gold_geom_z_min = np.amin(new_sphere_coord, axis=0)[2] # finds minimum of each column and outputs z-minimum
        gold_geom_z_max = np.amax(new_sphere_coord, axis=0)[2] # finds maximum
        ### DEFINING BOX DIMENSION FOR Z
        box_dim[2] = gold_geom_z_max - gold_geom_z_min
        
    elif geometry == 'EAM':
        if input_dir is None or input_file is None:
            print("ERROR! Geometry selected to be EAM, which requires an input directory / file")
            print("STOPPING HERE! CHECK INPUTS!")
            sys.exit()
        ## IMPORTING TOOLS FROM MDBUILDER
        from MDBuilder.applications.nanoparticle.read_tools import read_vcsgc_xyz
        vcsgc_xyz = read_vcsgc_xyz( input_dir + '/' +  input_file)
        
        ## GETTING GEOMETRY
        sphere_coord = vcsgc_xyz.extract_geometry[0] / 10.0 # Converting Angstroms to nms
        
        ## FINDING CENTER
        center_of_box = np.mean(sphere_coord, axis=0)
        
    else:
        print("ERROR! No geometry selected! Check the main script in make_gold_spherical_111.py!")
        print("STOPPING HERE. There is something wrong with your input!")
        sys.exit()
    
    ### GETTING THINGS FOR THE GRO FILE ###
    # Finding number of atoms
    numAtoms = len(sphere_coord)
    
    # Creating a residue number -- assume all residue as one
    resids = [1] * numAtoms
    
    # Creating atom names
    output_names = [BULKAU_TYPENAME] * numAtoms
    
    if geometry != 'planar':
        # Finding box dimensions (assume that we will get center +- )
        box_dim = [distance_from_edge*2 + radius *2]*3
        
        # Finding new center and moving sphere there
        new_center = np.array(box_dim)/2
        coord_shift = new_center - center_of_box
        new_sphere_coord = sphere_coord + coord_shift
    
    
    ### PRINTING GRO FILE ###
    print_gro_file_combined_res_atom_names(output_gro = output_gro,
                   output_folder = output_folder,
                   resids = resids,
                   geom = new_sphere_coord,
                   outputnames = output_names,
                   box_dimension = box_dim
                   )
    
    ### PRINTING TOPOLOGY FILE
    # Getting default force field parameters
    if force_field_type == "pool.ff":
        ## PRINTING FORCE FIELD ITP FILE
        prep_self_assembly_ligands.print_force_field_itp( output_folder )
        # PRINT TOP FILE       
        prep_self_assembly_ligands.print_top_file(output_top = output_top,
                   output_folder = output_folder,
                   itp_file_name = molecule_name,
                   res_name = molecule_res_name,
                   forcefield_folder = force_field_type)
    else:
        print("Error! Force field type (%s) is not an available force field type, please check make_gold_spherical_111.py"%(force_field_type))
        sys.exit()
    
    
    ### GETTING THINGS FOR THE ITP FILE ###
    # Getting charges
    typename_list = [BULKAU_TYPENAME]*numAtoms
    charge = [AU_CHARGE]*numAtoms
    mass = [GOLD_MASS]*numAtoms
    ATOM_NAME_list = [ATOM_NAME]*numAtoms
    
    ### PRINTING ITP FILES
    if force_field_type != "pool.ff":
        wantinterfaceff = True
    else:
        wantinterfaceff = False
        
    print_itp_file_gold(molecule_name = molecule_name, # Name of the file
                        output_folder = output_folder, # Output folder
                        molecule_resname = molecule_res_name, # Residue name
                        typename = typename_list, # Types like "BAu" as a list
                        atomname = ATOM_NAME_list, # Atom name like "Au" as a list
                        charge = charge, # Charges as a list
                        mass = mass, # Masses as a list
                        virtual_sites2 = None,
                        virtual_sites3 = None,
                        wantVirtualSites = False,
                        constraint_matrix = False,
                        wantPolarization = False,
                        forcefield = force_field_type,
                        wantinterfaceff = wantinterfaceff
                        )        

    return


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    # Turn test on or off
    testing = check_testing() # False if you are running from a bash script
    
    ### IF YOU ARE TESTING
    if testing is True:
    
        ### DEFINING INPUT PARAMETERS
        radius=1 # nm
        wantPlot=True # True if you want to see a plot of your points
        
        ### DEFINING GEOMETRY
        geometry = 'planar' # 'spherical', 'hollow'
        
        ### DEFAULT INPUT PARAMETERS
        distance_from_edge = 2 #nm <-- distance from the edge of the box you would like your sphere to be in

        ### DEFINING FORCE FIELD FOLDER
#        force_field_type= "charmm36-nov2016.ff"
        force_field_type="pool.ff"
        
        ### DEFINING OUTPUT VARIABLES
        output_folder=r"R:\scratch\nanoparticle_project\prep_system\prep_gold\self_assembly_simulations\test_folder"
        output_gro="sphere_gold.gro"
        output_top="sphere_gold.top"
        
        ### DEFINING ITP SPECIFIC PARAMETERS ###
        # Molecule name
        molecule_name="Au_111_sphere" # name of the itp file
        # Molecule residue name
        molecule_res_name="AUNP"
        
        ## DEFINING INPUT DIRECTORY AND FILE
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\final_structures\2\2nm_from_transfer_2nm_PHITRANS-0.1_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc'
        input_file = 'vcsgc_em_final.xyz'
        
        ## DEFINING LENGTH
        length = 10
        
        ## DEFINING NUMBER OF X AND Y
        num_lig_x = 10
        num_lig_y = 10
    
    else: # Molecule name, residue name, and distance from the edge needs to be added
        # Turning off plotting
        wantPlot = False
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
    
        ### DEFINING PARSER OPTIONS
        
        ### OUTPUT FILES / INPUT FILES
        ## OUTPUT GRO FILE
        parser.add_option("-g", "--ogro", dest="output_gro_name", action="store", type="string", help="Output GRO filename", default="double_sam.gro")
        ## OUTPUT TOP FILE
        parser.add_option("-t", "--otop", dest="output_top_name", action="store", type="string", help="Output TOP filename", default="double_sam.top")
        ## OUTPUT FOLDER
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        
        ## FORCE FIELD TYPE
        parser.add_option("-m", "--ff", dest="forcefield", action="store", type="string", help="Force field, typically a directory", default=".")
        
        ## GEOMETRY
        ## SPHERE RADIUS
        parser.add_option("--geom", dest="geometry", action="store", type="string", 
                          help="Geometry of the nanoparticle (e.g. spherical)", default="2")
                
        ## SPHERE RADIUS
        parser.add_option("-r", "--rad", dest="sphereRadius", action="store", type="float", help="Radius in nm of your nanoparticle", default="2")
        
        ## CYLINDER LENGTH
        parser.add_option("-l", "--len", dest="length", action="store", type="float", help="Length in nm of your cylinder nanoparticle", default="2")
    
        ## PLANAR CASE
        parser.add_option("--numligx", dest="num_lig_x", action="store", type="int", help="Number of ligands in the x-dimension", default="2")
        parser.add_option("--numligy", dest="num_lig_y", action="store", type="int", help="Number of ligands in the y-dimension", default="2")
        
        ## DISTANCE FROM THE EDGE
        parser.add_option("-d", "--dist", dest="distance_from_edge", action="store", type="int", help="Distance from the edge of box", default="2")
        
        ## ITP FILE NAME
        parser.add_option("-p", "--itp", dest="molecule_name", action="store", type="string", help="Name of molecule for gold", default=".")
        
        ## RESIDUE NAME
        parser.add_option("-n", "--resname", dest="molecule_res_name", action="store", type="string", help="Residue name of molecule for gold", default=".")
        
        ## INPUT DIRECTORIES
        parser.add_option("--idir", dest="input_dir", action="store", type="string", help="input directory (optional", default=None)
        parser.add_option("--ifile", dest="input_file", action="store", type="string", help="input file(optional)", default=None)
        
        
        #### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## OUTPUT / INPUT FILES
        output_gro = options.output_gro_name # Output gro file
        output_top = options.output_top_name # Output top file
        output_folder = options.output_folder # Output folder (for itps)
        force_field_type = options.forcefield # Force field
        
        ## SPHERICAL PARAMETERS
        geometry = options.geometry
        radius = options.sphereRadius # Sphere radius
        distance_from_edge = options.distance_from_edge
        
        ## CYLINDER PARAMETERS
        length = options.length
        
        ## PLANAR PARAMETERS
        num_lig_x = options.num_lig_x
        num_lig_y = options.num_lig_y
        
        ## ADDITIONAL PARAMETERS
        molecule_name = options.molecule_name
        molecule_res_name = options.molecule_res_name
        
        ## INPUT FILES
        input_dir = options.input_dir
        input_file = options.input_file

        
    ### RUNNING MAIN SCRIPT
    main( geometry, radius, distance_from_edge, force_field_type, output_folder, output_gro, output_top, molecule_name, molecule_res_name, 
         input_dir = input_dir, input_file = input_file, length = length, num_lig_x = num_lig_x, num_lig_y = num_lig_y,
         wantPlot = wantPlot )
        
  