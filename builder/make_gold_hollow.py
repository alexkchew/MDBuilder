# -*- coding: utf-8 -*-
"""
make_gold_hollow.py
The purpose of this script is to make hollow gold cores. 
INPUTS:
    1. Gold core diameter
    2. Ligand per nm^2 
    3. Force field information
    
OUTPUTS:
    1. GRO file with coordinates
    2. Gold ITP file (?) for the GRO
    3. Updated topology file
    
ALGORITHM:
    1. CLASS: hollow_core
        - Calculate total number of atoms required for the hollow core
        - Using coulombic repulsion, generate simulation for gold atoms
        - Output geometry

Written by: Alex K. Chew (alexkchew@gmail.com, 02/26/2018)

** UPDATES **
180304 - Creating functions to make spherical coordinates
"""
### IMPORTING MODULES
from MDBuilder.math.make_perfect_sphere import make_perfect_sphere
import numpy as np
### DEFINING GLOBAL VARIABLES
from MDBuilder.applications.nanoparticle.global_vars import LIGANDPERNM2
# LIGANDPERNM2: ligand per nm^2, which will be used to determine the number of atoms required
METAL_TO_LIGAND_RATIO = 3 # Ratio between metal atoms and ligand molecules
TOTAL_STEPS = 100 # Number of steps you want for your simulation <-- This simulation equilibrates ~ 40 steps. >40 steps recommended!

##########################################################
### CLASS FUNCTION TO DEVELOP A HOLLOW GOLD CORE SHELL ###
##########################################################
class create_hollow_core:
    '''
    The purpose of this class is to generate a hollow core
    INPUTS:
        radius: size of the sphere
        ligand_per_nm_squared: Ligand per nanometers squared (i.e. grafting density of ligands for a 111 surface)
        num_runs: Number of simulation steps for equilibration
        gold_to_ligand_ratio: gold to ligand ratio in a {111} planar lattice (assumed to be 3 to 1 for gold to ligand ratio)
    OUTPUTS:
        ## INTIALIZATION VARIABLES
        self.ligand_per_nm_squared, self.radius, self.num_runs, self.metal_to_ligand_ratio
        
        ## INFORMATION ABOUT NUMBER OF ATOMS
        self.surface_area: surface area occupied by the specified radius >>> floating point value
        self.num_atoms: number of atoms for the hollow shell >>> integer value
        
        ## COORDINATES AND BOX INFORMATION
        self.coordinates: NumPy array with the final coordinates from the spherical coordinations script ( N_ATOMS X 3 )
        self.center_of_box: center of the spherical coordinates found by averaging
        
    FUNCTIONS:
        self.calc_total_atoms_hollow_shell: Calculates the total number of atoms within a hollow shell
    '''
    ### INITIALIZATION
    def __init__(self,ligand_per_nm_squared, radius, num_runs, metal_to_ligand_ratio = 3 ):
        ## STORING INITIALIZATION VARIABLES
        self.ligand_per_nm_squared = ligand_per_nm_squared
        self.radius = radius
        self.num_runs = num_runs
        self.metal_to_ligand_ratio = metal_to_ligand_ratio
        
        ## CALCULATING TOTAL NUMBER OF ATOMS FOR THE HOLLOW SHELLS
        self.calc_total_atoms_hollow_shell()
        
        ## RUN SPHERICAL COORDINATES SCRIPT
        perfect_sphere = make_perfect_sphere(radius=self.radius, num_atoms=self.num_atoms, total_steps = self.num_runs,
                                              want_plots = False, debug_mode = False)
        
        self.coordinates = perfect_sphere.geometry[:]
        
        ## FINDING CENTER OF THE BOX
        self.center_of_box = np.mean(self.coordinates, axis=0)
        
    ### FUNCTION TO CALCULATE THE TOTAL NUMBER OF ATOMS
    def calc_total_atoms_hollow_shell(self):
        '''
        The purpose of this function is to calculate the total number of atoms available for a hollow shell
        INPUTS:
            self: class property
        OUTPUTS:
            self.surface_area: surface area occupied by the specified radius >>> floating point value
            self.num_atoms: number of atoms for the hollow shell >>> integer value
        '''
        ## CALCULATING SURFACE AREA OF THE CORE
        self.surface_area = 4 *np.pi*self.radius**2
        ## FINDING NUMBER OF GOLD ATOMS
        self.num_atoms = np.round( self.ligand_per_nm_squared * self.metal_to_ligand_ratio * self.surface_area, decimals=0 ).astype('int')            
        return


#%% MAIN SCRIPT
if __name__ == "__main__":

    ### DEFINING INPUTS
    radius = 2 # in nms
    
    ### FINDING HOLLOW CORE PARAMETERS
    hollow_core = create_hollow_core( ligand_per_nm_squared = LIGANDPERNM2,
                                      radius = radius,
                                      num_runs = TOTAL_STEPS,
                                      metal_to_ligand_ratio = METAL_TO_LIGAND_RATIO,
                                     )