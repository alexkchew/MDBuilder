#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyRosetta_Tutorial_3_Scoring.py
This code goes over computing the score of a biomolecule:
    https://graylab.jhu.edu/pyrosetta/downloads/documentation/pyrosetta4_online_format/PyRosetta4_Workshop3_Scoring.pdf

Written by: Alex K. Chew (alexkchew@gmail.com, 09/28/2019)


"""

## MAIN FUNCTION
if __name__ == "__main__":
    #%%
    ## DEFINING SCORING FUNCTION
    from pyrosetta.teaching import *
    scorefxn = get_fa_scorefxn()
    
    ## PRINTING SCORE FUNCTION
    print(scorefxn)
    
    
    #%%
    
    ## GENERATING YOUR OWN CUSTOM SCORING FUNCTION
    # INCLUDES vdw, SOLVATION, HYDROGEN BONDING TERMS, WITH ALL WEIGHTS 1
    scorefxn2 = ScoreFunction() 
    scorefxn2.set_weight(fa_atr, 1.0)
    scorefxn2.set_weight(fa_rep, 1.0)
    
    #%%
    
    ## LOADING RAS PROTEIN, PDB ID: 6Q21
    from pyrosetta.toolbox import pose_from_rcsb
    ras = pose_from_rcsb("6Q21")
    
    #%%
    
    ## PRINTIGN SCORE FUNCTION OF RAS
    print(scorefxn(ras))
    
    ## PRINTING INDIVIDUAL PEICES
    scorefxn.show(ras)
    
    ## BREAKING DOWN INTO EACH RESIDUES CONTRIBUTION USE
    print(ras.energies().show(24))
    
    #%%
    
    ## ATOMIC PAIRWISE ENERGIES - DOES NOT WORK
    r1 = ras.residue(24)
    r2 = ras.residue(20)
    a1 = r1.atom("N")
    a2 = r2.atom("O")
    etable_atom_pair_energies(atom_index_1 = a1, atom_index_2 = a2, sfxn = scorefxn, res1 = r1, res2 = r2)
    
    #%%
    
    ## HYDROGEN BONDING SCORING
    from rosetta.core.scoring.hbonds import HBondSet
    hbond_set = hbonds.HBondSet()
    ras.update_residue_neighbors()
    hbonds.fill_hbond_set(ras, False, hbond_set)
    hbond_set.show(ras)
    
    #%%
    ## ALTERNATIVE - DOES NOT WORK
    from pyrosetta.toolbox import get_hbonds
    hbond_set = get_hbonds(ras)
    hbond_set.show(ras)
    
    #%%
    
    ## FINDING NUMBER OF HYDROGEN BONDS OF RESIDUE 24
    hbond_set.show(ras, 24)
    
    #%%
    ## LOADING CETUXIMAB (1YY9)
    pose = pose_from_rcsb("1YY9")
    
    #%%
    print(pose.pdb_info().pdb2pose('D', 102))
    print(pose.pdb_info().pdb2pose('A', 408))
    
    #%%
    
    rsd1 = pose.residue(pose.pdb_info().pdb2pose('D', 102))
    rsd2 = pose.residue(pose.pdb_info().pdb2pose('A', 408))
    
    #%%
    
    emap = EMapVector()
    scorefxn.eval_ci_2b(rsd1, rsd2, pose, emap)
    print(emap[fa_atr])
    print(emap[fa_rep])
    print(emap[fa_sol])
    
    #%%
    ## PYMOL
    ## RUNNING IN PYMOL!
    from pyrosetta import PyMOLMover
    pymol = PyMOLMover()
    pymol.apply(pose)
    
    #%%
    ## SENDING ENERGY
    pymol.send_energy(pose)
    #%%
    pymol.send_energy(pose, fa_atr)
    
    
    
    #%%
    
    pymol.update_energy(True)
    pymol.energy_type(fa_atr)
    pymol.apply(pose)
    
    
    #%%
    pymol.label_energy(pose, fa_rep) # did not work
    
    #%%
    pymol.send_hbonds(pose)