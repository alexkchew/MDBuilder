#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyRosetta_Tutorial_4_Folding.py
This code goes over protein folding
    https://graylab.jhu.edu/pyrosetta/downloads/documentation/pyrosetta4_online_format/PyRosetta4_Workshop4_Folding.pdf

Written by: Alex K. Chew (alexkchew@gmail.com, 09/28/2019)

"""

from pyrosetta import *
init() # Initializes

#%%
## MAIN FUNCTION
if __name__ == "__main__":
    
    ## GETTING POSES FOR 10 ALANINES
    pose = pose_from_sequence('A'*10)
    
    #%%
    
    from pyrosetta import PyMOLMover
    pmm = PyMOLMover()
    pmm.apply(pose)