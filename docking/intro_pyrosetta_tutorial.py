#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
intro_pyrosetta_tutorial.py
This code goes through the Workshop2 of introducing PyRosetta
Link: https://graylab.jhu.edu/pyrosetta/downloads/documentation/pyrosetta4_online_format/PyRosetta4_Workshop2_PyRosetta.pdf

Created by: Alex K. Chew (09/28/2019)
"""
from pyrosetta import *
init() # Initializes

#%%
## MAIN FUNCTION
if __name__ == "__main__":
    
    ## GETTING CLEAN ATOM TOOL
    from pyrosetta.toolbox import cleanATOM
    cleanATOM("1YY8.pdb")
    
    ## DOWNLOAD 1YYB: https://www.rcsb.org/structure/1YY8
    #%%
    
    ## LOADING PROTEIN FROM CLEAN PDB FILE
    pose = pose_from_pdb("1YY8.clean.pdb")
    
    #%%
    ## FROM THE INTERNET
    from pyrosetta.toolbox import pose_from_rcsb
    pose = pose_from_rcsb("1YY8")
    
    #%%
    
    ## QUERY FUNCTIONS
    print(pose)
    print(pose.sequence())
    print("Protein has", pose.total_residue(), "residues.")
    print(pose.residue(500).name())
    
    #%%
    
    ## 500 RESIDUE NUMBER
    print(pose.pdb_info().chain(500))
    print(pose.pdb_info().number(500))
    ## ROSETTA INTENRAL NUMBER OF RESIDUE 100
    print(pose.pdb_info().pdb2pose('A', 100)) 
    
    #%%
    ## GETTING SECONDARY STRUCTURE
    from pyrosetta.toolbox import get_secstruct
    get_secstruct(pose)
    
    #%%
    
    ## FINDING DIHEDRAL ANGLES O RESIDUE 5
    print(pose.phi(5))
    print(pose.psi(5))
    print(pose.chi(1, 5))
    
    #%%
    #3 finding bond lengths of residue 5
    R5N = AtomID(1, 5)
    R5CA = AtomID(2, 5)
    R5C = AtomID(3, 5) 
    
    ## PRINTING BOND LENGTH
    print(pose.conformation().bond_length(R5N, R5CA))
    print(pose.conformation().bond_length(R5CA, R5C))
    #%%
    
    ## USE VECTOR CLASS TO GET NORM OF DISPLACEMENT VECTOR
    N_xyz = pose.residue(5).xyz("N")
    CA_xyz = pose.residue(5).xyz("CA")
    N_CA_vector = CA_xyz - N_xyz
    print(N_CA_vector.norm())
    
    #%%
    
    ## MANIPULATING PROTEIN GEOMETRY
    pose.set_phi(5, -60)
    pose.set_psi(5, -43)
    pose.set_chi(1, 5, 180)
    pose.conformation().set_bond_length(R5N, R5CA, 1.5)
    pose.conformation().set_bond_angle(R5N, R5CA, R5C,110./180.*3.14159)
    
    #%%
    ## VIAULIZATION USING PYMOL
    pose.dump_pdb("filename.pdb")
    
    #%%
    
    ## RUNNING IN PYMOL!
    from pyrosetta import PyMOLMover
    pymol = PyMOLMover()
    pymol.apply(pose)