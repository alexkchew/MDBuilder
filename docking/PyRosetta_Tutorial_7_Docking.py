#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyRosetta_Tutorial_7_Docking.py
This goes through Rosetta's code to dock protein-protein interactions
    https://graylab.jhu.edu/pyrosetta/downloads/documentation/pyrosetta4_online_format/PyRosetta4_Workshop7_Docking.pdf


Written by: Alex K. Chew (alexkchew@gmail.com, 09/28/2019)
"""


from pyrosetta import *
init() # Initializes
#%%


## MAIN FUNCTION
if __name__ == "__main__":
    
    ## GETTING COMPLEX OF: colicin D and ImmD (1V74)
    ## LOADING RAS PROTEIN, PDB ID: 6Q21
    from pyrosetta.toolbox import pose_from_rcsb
    pose = pose_from_rcsb("1V74")
    
    #%%
    
    ## VISUALIZING POSE
    from pyrosetta import PyMOLMover
    pmm = PyMOLMover()
    pmm.apply(pose)
    #%%
    
    ## FUNDAMENTAL DOCKING MOVE IS RIGID-BODY TRANSFOMRATION WITH TRANSLATION / ROTATION
    print(pose.fold_tree())
    '''
    The fold tree printout looks as below:
        FOLD_TREE  EDGE 1 107 -1  EDGE 1 108 1  EDGE 108 194 -1 
        
    Here, each three number sequence following the word eDGE is the beginning and 
    ending of a residue number. The codes are -1 for stretches of protein and any positive 
    interget for a jmp, which represents a jump number. 
    
    '''
    #%%
    
    from pyrosetta.teaching import *
    setup_foldtree(pose, "A_B", Vector1([1]))
    print(pose.fold_tree())
    '''
    Outputs:
        FOLD_TREE  EDGE 1 88 -1  EDGE 88 107 -1  EDGE 88 158 1  EDGE 158 108 -1  EDGE 158 194 -1 
    
    "A_B" tells Rosetta to make chain A the rigid chain and allow chain B to move. 
    If there were more chains, supplying "AB_C" would hold chians A and B rigid 
    together and allows chain C to move. 
    
    Victor([1]) is required, it creates a Rosetta vector object indexed from 1. 
    We past a list with one elemnt that identifies the first jump in the fold tree 
    for docking use.
    '''
    
    #%%
    ## DEFINING THE FIRST JUMP
    jump_num = 1
    print(pose.jump(jump_num).get_rotation())
    print(pose.jump(jump_num).get_translation())
    
    #%%
    
    ## BASIC MANIPULATIONS ARE TRANSLATIONS AND ROTATIONS.
    '''
    Trnaslation, change of x, y, z coordinates are needed as well as the jump number. 
    Rotation requires a center and an axis about which to rotate. 
    
    For structure prediction calculations, we have a mover that is preconfigured 
    to make random movements around set magnitudes (meaning 8 degree rotations 
    and 3 A translation, located in rosetta.protocols.rigid namespace, which we make 
    as an alias rigid_moves for our convenience).
    '''
    
    import pyrosetta.rosetta.protocols.rigid as rigid_moves
    pert_mover = rigid_moves.RigidBodyPerturbMover(jump_num, 8, 3)
    
    #%% 
    ## GLOBAL PERTURBATIONS USEFUL TO MAKING COMPLETELY RANDOMIZED STARTING STRUCTURES
    randomize1 = rigid_moves.RigidBodyRandomizeMover(pose,
                                                    jump_num,
                                                    rigid_moves.partner_upstream)
    randomize2 = rigid_moves.RigidBodyRandomizeMover(pose,
                                                    jump_num,
                                                    rigid_moves.partner_downstream)
    
    #%%
    
    ## SLIDING PROTEINS UNTIL THEY ARE IN CONTACT
    from rosetta.protocols.docking import *
    slide = DockingSlideIntoContact(jump_num) # for centroid mode
    slide = FaDockingSlideIntoContact(jump_num) # for fullatom mode
    slide.apply(pose)
    
    #%%
    ## DEFINING ANY SCORING FUNCTION
    scorefxn = get_fa_scorefxn()
    
    movemap = MoveMap()
    movemap.set_jump(jump_num, True)
    min_mover = MinMover()
    min_mover.movemap(movemap)
    min_mover.score_function(scorefxn) # use any scorefxn
    scorefxn(pose)
    min_mover.apply(pose)
    

    #%%
    
    ## NEED TO BE LOW RESOLUTION (CENTROID) MODE
    scorefxn_low = create_score_function("interchain_cen")
    
    ## RANDOMIZE CENTROID, THEN CREATE LOW-RESOLUTION DOCKINING STRUCTURES
    dock_lowres = DockingLowRes(scorefxn_low, jump_num)
    dock_lowres.apply(pose)
    
    #%%
    
    
    #%%
    ## JOB DISTRIBUTOR
    from pyrosetta.teaching import *
    jd = PyJobDistributor("output", 10, scorefxn_low)
    
    #%%
    ## WAY TO FIND POSES FROM PDB -- SKIPPING
    native_pose = pose_from_rcsb("1V74")
    jd.native_pose = native_pose
    
    #%%
    ## STARTING POSE, ETC
    pose.assign(pose) # starting_pose
    dock_lowres.apply(pose)
    jd.output_decoy(pose)
    
    #%%
    
    ## STARTING POSE
    starting_pose = pose_from_rcsb("1V74")
    
    #%%
    while not jd.job_complete: # Check if 10 poses created
        pose.assign(starting_pose) # starting_pose
        dock_lowres.apply(pose)
        jd.output_decoy(pose)
    
    #%%
    
    ## High-Resolution Docking
    scorefxn_high = create_score_function_ws_patch(
    "talaris2013", "docking")
    dock_hires = DockMCMProtocol()
    dock_hires.set_scorefxn(scorefxn_high)
    dock_hires.set_partners("A_B")
    
    
    
    
    