# -*- coding: utf-8 -*-
"""
edit_dhdl_files.py
The purpose of this code is to read in dhdl*.xvg files, then attempt to split it if necessary
The idea here is that we want to use alchemical analysis tool for specific trajectories. However, 
the tool itself does not have a good way to split trajectories aside from it's equiltime. So, 
as an alternative, we will regenerate the xvg files in partitions, allowing us to run alchemical 
analysis without having to modify the original alchemical-analysis code.

INPUTS:
    - xvg file
    - initial time (equil time)
    - number of splits (e.g. 2)
OUTPUTS:
    - xvg file for the number of splits

CREATED BY: Alex K. Chew (alexkchew@gmail.com)

CREATED ON: 01/14/2019

CLASSES:
    edit_dhdl_file: edits dhdl file, prints out a new one
    
FUNCTIONS:
    edit_multiple_dhdl_files: uses "edit_dhdl_file" class to edit multiple dhdl files

"""
import numpy as np
import os
import decimal
import glob

## FROM MDBUILDERS
from MDBuilder.core.import_tools import load_and_clean_text_file
from MDBuilder.core.check_tools import check_testing

## FROM MDDESCRIPTORS
from MDDescriptors.core.calc_tools import split_list

####################################################
### CLASS METHOD TO EXTRACT AND REPLACE XVG FILE ###
####################################################
class edit_dhdl_file:
    '''
    The purpose of this class is to edit dhdl files. Here, we are interested in splitting 
    dhdl files to separate dhdl files, then we can run alchemical-analysis to get the FEP of
    split trajectories.
    
    INPUTS:
        parent_path: [str]
            parent path where you have your xvg files
        xvg_file_name: [str]
            file name of the xvg file (e.g. 'md0.xvg')
        output_dir: [str]
            path you want to output your edited xvg fles
        output_prefix: [str, default = "split"]
            output prefix you want to attach in beginning of xvg_file_name, e.g. 'split_0_md0.xvg'
        equil_time: [float, default = 0 ps]
            equilibration time you want truncated from your xvg file
        num_split: [int, default = 1]
            number of splits desired
        renumber: [lofical, default=False]
            True if you want the end result to be renumbered in terms of 0.0000 ps
        verbose: [logical, default = False]
            True if you want step by step output
    OUTPUTS:
        ## STORED INPUTS
            self.parent_path, self.xvg_file_name, self.output_dir, self.output_prefix, self.equil_time, self.num_split, 
            self.verbose, self.renumber
        ## COMPUTED INFORMATION
            self.xvg_file_path: [str]
                full path to xvg file
        ## LOADING XVG INFO
            self.xvg_lines: [list]
                xvg lines based on imported xvg file
            self.header_lines: [list]
                list of strings of the header lines based on whether or not the line has # or @ in the beginning
                of the string
            self.header_last_index: [int]
                index of the last header, used to denote the beginning of the data
        ## EXTRACTED XVG INFO
            self.data_lines: [list]
                list of strings of the data after the header
            self.split_prod_data_extract: [list]
                list of splitted production extracted (split lines into strings)
            self.split_prod_data: [list]
                list of splitted post equilibrated data
            self.split_prod_times: [list]
                list of splitted production times in ps
            self.decimal_places: [int]
                number of decimal places for time
    FUNCTIONS:
        load_xvg: loads xvg file
        extract_xvg_data: extracts xvg data based on header information
        print_split_dhdl: prints dhdl into file
    '''
    ### INITIALIZING
    def __init__( self, parent_path, xvg_file_name, output_dir, 
                 output_prefix = "split", equil_time=0, num_split=1, 
                 renumber=False, verbose=False ):
        ## STORING INPUTS
        self.parent_path = parent_path
        self.xvg_file_name = xvg_file_name
        self.output_dir = output_dir
        self.output_prefix = output_prefix
        self.equil_time = equil_time
        self.num_split = num_split
        self.verbose = verbose
        self.renumber = renumber

        ## DEFINING XVG FILE PATH
        self.xvg_file_path = os.path.join(self.parent_path, self.xvg_file_name)
        
        ## LOADING XVG FILE
        self.load_xvg()
        
        ## EXTRACTING XVG FILE
        self.extract_xvg_data()
        
        ## PRINTING
        self.print_split_dhdl()
        
        return

    ### FUNCTION TO LOAD XVG FILE
    def load_xvg(self):
        '''
        The purpose of this function is to load the xvg file and extract the header information
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
            self.xvg_lines: [list]
                xvg lines based on imported xvg file
            self.header_lines: [list]
                list of strings of the header lines based on whether or not the line has # or @ in the beginning
                of the string
            self.header_last_index: [int]
                index of the last header, used to denote the beginning of the data
        '''
        ## LOADING XVG FILE
        self.xvg_lines = load_and_clean_text_file(text_file_path = self.xvg_file_path)

        ## DEFINING HEADER LINES
        self.header_lines = [ each_line for each_line in self.xvg_lines if each_line[0] == '#' or each_line[0] == '@' ]
        self.header_last_index =[i for i, j in enumerate(self.header_lines) if '@' in j][-1] 
        
        ## PRINTING
        if self.verbose is True:
            print("\n----------------------------")
            print("Loading xvg file: %s"%( self.xvg_file_path )  )
            print("Total header lines: %d"%(len(self.header_lines) ) )
            print("Final header index (e.g. last @ and #): %d"%(self.header_last_index)  )
        
        return
    
    ### FUNCTION TO EXTRACT THE DATA
    def extract_xvg_data(self):
        '''
        The purpose of this function is to extract the xvg data. Here, we assume that the xvg data is already 
        loaded and converted into lines. We will first define the extracted data, then split the data accordingly.
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
            self.data_lines: [list]
                list of strings of the data after the header
            self.split_prod_data_extract: [list]
                list of splitted production extracted (split lines into strings)
            self.split_prod_data: [list]
                list of splitted post equilibrated data
            self.split_prod_times: [list]
                list of splitted production times in ps
            self.decimal_places: [int]
                number of decimal places for time
        '''
        ## DEFINING DATA LINES
        self.data_lines = self.xvg_lines[self.header_last_index+1:] # Plus one to go after heading ended
        data_extract = [ x.split() for x in self.data_lines ]
        
        ## COMPUTING DECIMAL PLACES OF TIME
        if self.renumber is True:
            self.decimal_places = abs(decimal.Decimal(data_extract[0][0]).as_tuple().exponent) # Returns 4 for 4 decimal places
        
        ## FINDING TIME STAMPS
        data_time_stamps = np.array([ float(each_line[0]) for each_line in data_extract ])
    
        ## SEEING WHEN EQUILIBRATION TIME EXISTS
        equil_index = np.argwhere( data_time_stamps == self.equil_time )[0][0]
        
        ## DEFINING POST EQUIL DATA
        prod_equil_data_extract = data_extract[equil_index:]
        post_equil_data = self.data_lines[equil_index:]
        post_equil_time_stamps = data_time_stamps[equil_index:]
        # post_equil_index = range(len(post_equil_data)) ** DEPRECIATED **
        
        ## SPLITTING THE LIST
        self.split_prod_data_extract = split_list( prod_equil_data_extract, wanted_parts = self.num_split  )
        self.split_prod_data  = split_list( post_equil_data,  wanted_parts = self.num_split  )
        self.split_prod_times = split_list( post_equil_time_stamps,  wanted_parts = self.num_split  )
        # self.split_prod_index = split_list( post_equil_index,  wanted_parts = self.num_split  )
        
        ## PRINTING
        if self.verbose is True:
            print("----------------------------")
            print("Skipping %.3f ps equilibration data..."%( self.equil_time ) )
            print("Starting production data from line: %d"%( equil_index + self.header_last_index+1 ) )
            print("Number of splits desired: %d"%(self.num_split) )
            print("Production split times:")
            for each_split in range(self.num_split):
                print("   Split %d, %.3f ps to %.3f ps"%( each_split, self.split_prod_times[each_split][0],\
                                                 self.split_prod_times[each_split][-1]    ) )
        return
    
    ### FUNCTION TO PRINT DHDL DATA
    def print_split_dhdl(self):
        '''
        The purpose of this function is to print the splitted dhdl data to separate files. After printing this,
        we can run alchemical-analysis to compute FEP of the specified split data. 
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
            null; this function simply prints split dhdl files
        '''
        ## ACCOUNTING FOR RENUMBERING OF TIME
        if self.renumber is True:
            ## COMPUTING THE PRODUCTION TIMES RELATIVE TO FIRST VALUE
            self.renumber_split_prod_time = [ np.array(each_split_times) - each_split_times[0] 
                                                for each_split_times in self.split_prod_times ]
        ## PRINTING
        if self.verbose is True:
            print("----------------------------")
            if self.renumber is True:
                print("Renumbering has been turned on! Renumbering each text file starting from 0.000 ps")
        ## LOOPING THROUGH EACH SPLIT
        for each_split in range(self.num_split):
            ## DEFINING CURRENT NAME
            split_file_name=self.output_prefix + '_' + str(each_split) + '_' + self.xvg_file_name
            split_file_path=os.path.join(self.output_dir, split_file_name)
            ## PRINTING
            if self.verbose is True:
                print("Writing to file: %s"%( split_file_name ) )
            ## CREATING A FILE
            with open(split_file_path, 'w' ) as f: # For reading and writing
                ## WRITING THE HEADER
                for each_header_line in self.header_lines:
                    f.write("%s\n"%(each_header_line))
                ## WRITING EACH SPLIT
                if self.renumber is False:
                    for each_prod_line in self.split_prod_data[each_split]:
                        f.write("%s\n"%(each_prod_line))
                else:
                    for idx, each_split_line in enumerate(self.split_prod_data_extract[each_split]):
                        ## REPLACING THE FIRST STRING BASED ON RENUMBERED LINE
                        each_split_line[0] = format( self.renumber_split_prod_time[each_split][idx], 
                                                       '.' + str(self.decimal_places) +'f') # Formating, e.g. .10f
                        ## JOINING LINES
                        joined_lines = ' '.join(each_split_line)
                        ## PRINTING
                        f.write("%s\n"%(joined_lines))
        return

### FUNCTION TO RUN EXTRACTION OF DHDL FOR MULTIPLE FILES
def edit_multiple_dhdl_files( parent_path, dhdl_file_dict , xvg_file_prefix = "md", xvg_file_suffix = ".xvg" ):
    '''
    The purpose of this function is to use glob to generate a list of dhdl files.
    The idea here is that we will edit each of these files consecutively. 
    INPUTS:
        parent_path: [str]
            parent path to the dhdl files
        dhdl_file_dict: [dict]
            dictionary for the extraction protocal of dhdl
        xvg_file_prefix: [str, default="md"]
            prefix for the xvg file
        xvg_file_suffix: [str, default=".xvg"]
            suffix for the xvg files
    OUTPUTS:
        null; multiple dhdl files will be created baased on inputs
    '''
    ## ATTEMPTING TO GLOB ALL THE FILE NAMES
    file_list=glob.glob( parent_path + '/' + xvg_file_prefix + '*' + xvg_file_suffix )
    
    ## FINDING ONLY THE BASENAME
    file_names = [ os.path.basename( each_file ) for each_file in file_list ]
    
    ## FINDING TOTAL NUMBER OF FILES
    n_files = len(file_names)
    
    print("Running multiple dhdl files, totaling up to: %d"%( n_files ) )
    print("dhdl file list: (divided by //)")
    print( " // ".join( file_names ) )
    
    ## LOOPING THROUGH EACH FILE NAME
    for idx, xvg_file_name in enumerate(file_names):
        ## PRINTING
        print("*** Editing %s file, file %d out of %d ***"%( xvg_file_name, idx, n_files  ) ) 
        ## EDITING DICTIONARY
        dhdl_file_dict['xvg_file_name'] = xvg_file_name
        ## RUNNING DHDL FILE EDITOR
        edit_dhdl_file( **dhdl_file_dict )
    return


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
        ## DEFINING INPUTS
        equil_time=1000.000 # equilibrated time in ps
        num_split=2 # number of splits
        
        ## DEFINING PARENT PATH
        parent_path=r"R:\scratch\SideProjectHuber\Simulations\190111-Extended_HYD_FEP_Pure_Solvents\FEP_300.00_6_nm_HYD_100_WtPercWater_spce_Pure\5_Analysis"
        ## DEFINING XVG FILE NAME AND PATH
        xvg_file_name="md0.xvg"
        xvg_file_prefix="md"
        xvg_file_suffix=".xvg"
        
        ## DEFINING OUTPUT DIRECTORY
        output_prefix="split"
        output_dir=r"R:\scratch\SideProjectHuber\Simulations\190111-Extended_HYD_FEP_Pure_Solvents\FEP_300.00_6_nm_HYD_100_WtPercWater_spce_Pure\5_Analysis"
        
        ## DEFINING LOGICAL
        renumber = True
        verbose = False
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## PATH INFORMATION
        parser.add_option('-p', '--parentpath', dest = 'parent_path', help = 'Full parent path to xvg files', default = '.')
        ## XVG FILE INFORMATION
        parser.add_option('-x', '--xvgprefix', dest = 'xvg_file_prefix', help = 'Prefix for xvg file', default = 'md')
        parser.add_option('-z', '--xvgsuffix', dest = 'xvg_file_suffix', help = 'Suffix for xvg file', default = '.xvg')
        ## OUTPUT INFORMATION
        parser.add_option('-o', '--outputdir', dest = 'output_dir', help = 'Output directory for xvg files', default = '.')
        parser.add_option('-m', '--outputprefix', dest = 'output_prefix', help = 'Output prefix for xvg files', default = '.')
        ## VARIABLE INFORMATION
        parser.add_option('-n', '--numsplits', dest = 'num_split', help = 'Number of splits for the production xvg files', default = 2, type=int)
        parser.add_option('-e', '--equiltime', dest = 'equil_time', help = 'Equilibration time in ps for xvg file, which will be truncated', default = 0, type=int)
        ## LOGICAL
        parser.add_option('-r', '--renumber', dest = 'renumber', help = 'Renumber time stamps for output xvg starting from 0.000 ps option. Default: False.',
                          default = False, action = 'store_true')
        parser.add_option('-v', '--verbose', dest = 'verbose', help = 'Verbose option. Default: False.', default = False, action = 'store_true')
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ### DEFINING ARGUMENTS
        parent_path = options.parent_path
        xvg_file_prefix = options.xvg_file_prefix
        xvg_file_suffix = options.xvg_file_suffix
        output_dir = options.output_dir
        output_prefix = options.output_prefix
        num_split = options.num_split
        equil_time = options.equil_time
        renumber = options.renumber
        verbose = options.verbose
        
    
    #%%
    
    ## DEFINING DHDL FILE VARIABLES
    dhdl_file_dict={
            'parent_path'    : parent_path,
            # 'xvg_file_name'  : xvg_file_name,
            'output_prefix'  : output_prefix,
            'output_dir'     : output_dir,
            'equil_time'     : equil_time,
            'num_split'      : num_split,
            'renumber'       : renumber,
            'verbose'        : verbose
            }
    
    #%%

#    ### RUNNING DHDL EDITING SOFTWARE
#    dhdl_file = edit_dhdl_file( **dhdl_file_dict ) 
    
    #%%
    
    ## RUNNING MULTIPLE DHDL FILE EDITOR
    edit_multiple_dhdl_files( parent_path = parent_path, 
                              dhdl_file_dict = dhdl_file_dict, 
                              xvg_file_prefix = xvg_file_prefix, 
                              xvg_file_suffix = xvg_file_suffix, 
                              )
    
    
    