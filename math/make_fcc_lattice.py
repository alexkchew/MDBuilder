# -*- coding: utf-8 -*-
"""
make_fcc_lattice.py
This script contains code to create fcc lattice

FUNCTIONS:
    plot_lattice: plots lattice
    
CLASS:
    create_spherical_fcc_lattice: creates spherical fcc lattice
    create_fcc_lattice: creates planar fcc lattice
    create_cylindrical_curved_fcc_lattice: creates cylindrical surface with curved ends

CREATED ON: 03/05/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
UPDATES:
    20180712-Added function to generate cylinders with curved ends

"""
## IMPORTING MODULES
import numpy as np
from MDBuilder.core.calc_tools import find_atom_closest_center

### FUNCTION TO PLOT A LATTICE OF POINTS
def plot_lattice(coordinates, fig = None, ax = None):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        import matplotlib.pyplot as plt # matlab plotting tools
        from mpl_toolkits.mplot3d import Axes3D # For 3D axes
        
        ## CREATING FIGURE
        if fig is None or ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d', aspect='equal')
        
        ## SETTING X,Y,Z LABELS
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        
        ## PLOTTING SCATTER PLOT
        ax.scatter(coordinates[:,0], 
                   coordinates[:,1],
                   coordinates[:,2],
                   color='r', s=20)
        return fig, ax
    
### FUNTION TO FIND SURFACE AND BULK ATOMS FOR A PLANAR SLAB
def find_planar_surface_bulk_atoms(geometry):
    '''
    The purpose of this function is to distinguish surface atoms and bulk atoms. 
    INPUTS:
        geometry: [np.array, shape=(N,3)] geometry of the planar atoms
    OUTPUTS:
        top_surface_coord: [np.array, shape=(N,3)] coordinates of the top surface
        bulk_coord: [np.array, shape=(N,3)] coordinates of the bulk surface
        bot_surface_coord: [np.array, shape=(N,3)] coordinates of the bottom surface
    '''
    ## START BY FINDING THE MAXIMUM AND MINIMUM Z VALUES
    max_z_values = np.max(geometry[:,2])
    min_z_values = np.min(geometry[:,2])
    
    ## FINDING ALL INDEX WHERE WE HAVE THE MAXIMUM / MINIMUM
    index_top_atoms = np.where( geometry[:,2] == max_z_values )
    index_bot_atoms = np.where( geometry[:,2] == min_z_values )
    index_surf_atoms = np.where( (geometry[:,2] != min_z_values) & (geometry[:,2] != max_z_values) )
    
    ## FINDING COORDNATES
    top_surface_coord = geometry[ index_top_atoms ]
    bulk_coord = geometry[index_surf_atoms]
    bot_surface_coord = geometry[index_bot_atoms]
    
    return top_surface_coord, bulk_coord, bot_surface_coord
    

##########################################################
### CLASS TO CREATE SPHERICAL LATTICE FROM FCC LATTICE ###
##########################################################
class create_spherical_fcc_lattice():
    '''
    The purpose of this script is to create a spherical nanoparticle based on fcc lattice. It will truncate based on a sphere radius.
    INUTS:
        fcc_lattice_constant:[float] lattice constant in nm
        radius: [float] radius of sphere in nm
        debug_mode: True if you want to debug
    OUTPUTS:
        ## INPUTS
            self.radius, self.lattice_constant
        ## GEOMETRY
            self.geometry: [np.array,shape=(sphere_pts, 3) geometry of just a spherical lattice
            self.total_atoms: [int] total number of atoms
        ## CENTER ATOM INFORMATION
            self.center_atom_index: [int] index of self.geometry that is the center
            self.center_atom_position: [np.array, shape=(1,3)] position of the center atom    
    FUNCTIONS:
        print_summary: prints the summary
        cut_lattice_spherical: cuts spherical lattice
    ACTIVE FUNCTION:
        plot_lattice: plots the current lattice given the coordinates
    ALGORITHM:
        - Find basis vectors
        - Expansion of basis vectors in all three-dimensions
        - Truncation of the lattice into a sphere
    '''
    def __init__(self, fcc_lattice_constant, radius, debug_mode = False):
        ## DEFINING INPUT VARIABLES
        self.radius = radius
        self.lattice_constant = fcc_lattice_constant
        
        ## FINDING NUMBER OF TIMES WE NEED TO REPEAT
        self.num_expansion = np.ceil( self.radius*2 / self.lattice_constant )
        
        ## DEFINING PLANAR INPUTS
        fcc_lattice_inputs = {
                                'fcc_lattice_constant'  :   self.lattice_constant,
                                'repeat_x'              :   self.num_expansion,
                                'repeat_y'              :   self.num_expansion,
                                'repeat_z'              :   self.num_expansion,
                                }
        
        ## CREATING LATTICE
        fcc_lattice = create_fcc_lattice(**fcc_lattice_inputs )
        
        ## DEFINING GEOMETRY
        self.geometry = fcc_lattice.geometry
        ## FINDING ATOMS CLOSEST TO THE CENTER
        self.center_atom_index, self.center_atom_position, _ = find_atom_closest_center(self.geometry)
        ## FINDING GEOMETRY BY CUTTING A SPHERE
        self.cut_lattice_spherical()
        ## PRINTING SUMMARY
        self.print_summary()
        
        ## PLOTTING IF DEBUG MODE IS ON
        if debug_mode is True:
            self.plot_lattice()
            
            
    ### FUNCTION TO PRINT SUMMARY
    def print_summary(self):
        ''' This function prints a summary '''
        print("**** CLASS: %s ****"%(self.__class__.__name__))
        print('Radius: %.2f nm'%(self.radius) )
        print("Center of sphere: %s"%(self.center_atom_position))
        return
    ### FUNCTION TO PLOT THE LATTICE
    def plot_lattice(self):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        plot_lattice(self.geometry)
        return
        
    ### FUNCTION TO CUT SPHERICAL LATTICE
    def cut_lattice_spherical(self):
        '''
        The purpose of this function is to cut the lattice into a sphere
        INPUTS:
            self: class object
        OUTPUTS:
            self.spherical_geometry: [np.array, shape=(n,3)] geometry of a spherical cut fcc lattice
            self.total_atoms: [int] total number of atoms
        '''
        ## FINDING DISTANCES FROM THE SPHERE CENTER
        distance_to_center_atom = np.sqrt(  np.sum( (self.geometry - self.center_atom_position)**2, axis=1 ) )
        ## FIND ALL ATOMS WITHIN THE SPHERE
        self.geometry = self.geometry[np.where(distance_to_center_atom <= self.radius)]
        ## FINDING TOTAL ATOMS
        self.total_atoms = len(self.geometry)
        return

###################################
### CLASS TO CREATE FCC LATTICE ###
###################################
class create_fcc_lattice():
    '''
    The purpose of this function is to create fcc lattice geometry as a rectangular shape, then truncate with spherical radius
    INPUTS:
        lattice_constant: lattice constant
        repeat_x, repeat_y, repeat_z: number of repeat groups for x, y, z
            NOTE #1: if 0.5, then that is equivalent to 1 layer of gold.
            NOTE #2: only whole numbers and half integers allowed (e.g. 1, or 1.5)
            NOTE #3: Repeat 1 means we duplicate 1 more -- we start counting at zero!
            
        debug_mode: True if you want to see the plot 
    OUTPUTS:
        ## INPUTS:
            self.lattice_constant, self.repeat_x, self.repeat_y, self.repeat_z
        ## LATTICE
            self.basis_vectors: [np.array, shape=(4, 3)] basis vectors (4 atoms) that can be translated to create a fcc lattice
            self.num_expansion: number of times to expand the lattice in all three dimensions
        ## GEOMETRY
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
            self.total_atoms: [int] total number of atoms
    FUNCTIONS:
        find_basis_vectors: locates basis vectors based on the fcc lattice
        expand_lattice: expands the lattice in x, y, and z dimensions
        check_lattice: enables the checking of lattice to make sure that the dimensions match up (fixes .5 increments!)
        print_summary: prints summary
    ACTIVE FUNCTION:
        plot_lattice: plots the current lattice given the coordinates
        find_surface_bulk_atoms: finds all surface atoms and bulk atoms
    ALGORITHM:
        - Find basis vectors
        - Expansion of basis vectors in all three-dimensions
    '''
    ### INITIALIZATION
    def __init__(self, fcc_lattice_constant, repeat_x = 1, repeat_y = 1, repeat_z = 1, debug_mode=False):
        ## DEFINING INPUT VARIABLES
        self.lattice_constant = fcc_lattice_constant
        self.repeat_x = repeat_x
        self.repeat_y = repeat_y
        self.repeat_z = repeat_z
    
        ## FINDING BASIS VECTORS
        self.find_basis_vectors()
        ## EXPANDING LATTICE
        self.expand_lattice()
        ## CHECK LATTICE
        self.check_lattice()            
        ## PRINTING SUMMARY
        self.print_summary()
        ## PLOTTING IF DEBUG MODE IS ON
        if debug_mode is True:
            self.plot_lattice()
        
    ### FUNCTION TO PRINT SUMMARY
    def print_summary(self):
        ''' This function prints the summary '''
        print("**** CLASS: %s ****"%(self.__class__.__name__))
        print("Number of x, y, z expansions: %.1f, %.1f, %.1f"%(self.repeat_x, self.repeat_y, self.repeat_z))
        print("Total number of atoms: %d"%(len(self.geometry) ) )
        print("Lattice constant: %.3f nm"%(self.lattice_constant))
        return
    
    ### FUNCTION TO DEFINE BASIS VECTORS
    def find_basis_vectors(self):
        '''
        The purpose of this function is to get the basis vectors for a fcc lattice.
        INPUTS:
            self: class object
        OUTPUTS:
            self.basis_vectors: [np.array, shape=(4, 3)] basis vectors (4 atoms) that can be translated to create a fcc lattice
        '''
        ## MAGNITUDE OF VECTOR
        mag_vector = self.lattice_constant/2.0
        ## FCC basis vectors defined as a/2(i + k), a/2(i + j), and a/2 (j+k), etc.
        self.basis_vectors = np.array([
                                [0, 0, 0 ],
                                [1, 0, 1 ],
                                [0, 1, 1 ],
                                [1, 1, 0 ],
                ]) * mag_vector
        return
        
    ### FUNCTION TO PLOT THE LATTICE
    def plot_lattice(self):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        plot_lattice(self.geometry)
        return
    
    ### FUNCTION TO EXPAND THE LATTICE
    def expand_lattice(self):
        '''
        The purpose of this function is to expand the lattice
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
        '''
        ## CREATING GEOMETRY, APPENDING BASIS VECTORS
        self.geometry = self.basis_vectors[:]
        ## DEFINING EXPANSION VECTOR
        self.expansion_vector = [ np.arange(1,self.repeat_x + 1)*self.lattice_constant,
                                 np.arange(1,self.repeat_y + 1)*self.lattice_constant,
                                 np.arange(1,self.repeat_z + 1)*self.lattice_constant ]
                                 
                                 # 0, 1, 2, .. and so on * lattice constant
        ## LOOPING THROUGH EACH DIMENSION (X, Y, Z)
        for each_dimension in range(3):
            ## DEFINING CURRENT GEOMETRY
            current_geometry = np.copy(self.geometry[:])
            ## DEFINING EXPANSION VECTOR
            expansion_vector = self.expansion_vector[each_dimension]
            ## NOW, LOOPING THROUGH EACH EXPANSION AND APPENDING
            for each_expansion in expansion_vector:
                ## NEW GEOMETRY
                new_geometry = np.copy(current_geometry[:])
                new_geometry[:, each_dimension] += each_expansion
                ## STORING INTO GEOMETRY
                self.geometry = np.concatenate( (self.geometry, new_geometry) )
        return
    
    ### FUNCTION TO CHECK THE LATTICE
    def check_lattice(self):
        '''
        The purpose of this function is to check the lattice, in the case of repeat units that are not whole numbers
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
            self.total_atoms: [int] total number of atoms
        '''
        ## CREATING GENERAL LIST
        repeats = [ self.repeat_x, self.repeat_y, self.repeat_z ]
        
        ## LOOPING THROUGH THE REPEATS
        for index, each_dim_repeat in enumerate(repeats):
            if not float(each_dim_repeat).is_integer():
                ## FINDING MAX VALUE TO LOOK AT
                cutoff_length = (each_dim_repeat + 0.5) * self.lattice_constant + self.lattice_constant/4.0 # 1/4 lattice constant addition just in-case
                ## FINDING ALL INDEXES THAT HAVE ADDITIONAL REPEAT
                atoms_above_cutoff_index = np.where( self.geometry[:, index] > cutoff_length)
                print("For dimension %s (0 is x, 1 is , 2 is z), we are removing %d atoms with a cutoff length of %.2f"%(index, 
                                                                                                                          len(atoms_above_cutoff_index[0]  ),
                                                                                                                          cutoff_length,
                                                                                                                          ))
                ## FINDING ALL ATOMS BELOW CUTOFF
                atoms_below_cutoff_index = np.where( self.geometry[:, index] <= cutoff_length)
                self.geometry = self.geometry[atoms_below_cutoff_index]
        ## FINDING TOTAL ATOMS
        self.total_atoms = len(self.geometry)
        return
    ### ACTIVE FUNCTION: FINDS ALL SURFACE GOLD ATOMS AND BULK GOLD ATOMS
    def find_surface_bulk_atoms(self):
        '''
        The purpose of this function is to distinguish surface atoms and bulk atoms. 
        INPUTS:
            void
        OUTPUTS:
            top_surface_coord: [np.array, shape=(N,3)] coordinates of the top surface
            bulk_coord: [np.array, shape=(N,3)] coordinates of the bulk surface
            bot_surface_coord: [np.array, shape=(N,3)] coordinates of the bottom surface
        '''
        top_surface_coord, bulk_coord, bot_surface_coord = find_planar_surface_bulk_atoms(self.geometry[:])
        return top_surface_coord, bulk_coord, bot_surface_coord
        

###################################################
### CLASS TO CREATE PLANAR GOLD FCC 111 LATTICE ###
###################################################
class create_gold_fcc_111_lattice:
    '''
    The purpose of this function is to create a fcc {111} lattice. We make the assumption of 6 basis points, which essentially is required to develop this lattice.
    INPUTS:
        repeat_x, repeat_y, repeat_z: number of repeat groups for x, y, z
    OUTPUTS:
        ## INPUTS
            self.repeat_x, self.repeat_y, self.repeat_z
        ## BASIS VECTORS
            self.basis_vectors: [np.array, shape=(6,3)] basis vectors for the fcc {111} lattice
            self.periodicity: [np.array, shape=3,1] periodicity of each dimension
        ## GEOMETRY
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
        
    FUNCTIONS:
        define_basis_vectors: gets the basis vectors for the gold fcc {111} lattice
        expand_lattice: expand lattice in the x, y, z direction
    ACTIVE FUNCTION:
        plot_lattice: plots the current lattice given the coordinates
        find_surface_bulk_atoms: finds all surface atoms and bulk atoms
    '''
    ### INITIALIZATION
    def __init__(self, repeat_x, repeat_y, repeat_z):
        ## STORING INITIAL VARIABLES
        self.repeat_x = repeat_x
        self.repeat_y = repeat_y
        self.repeat_z = repeat_z
        
        ## GETTING THE BASIS VECTORS
        self.define_basis_vectors()
        
        ## EXPANDING THE GEOMETRY
        self.expand_lattice()
        
    ### ACTIVE FUNCTION: FINDS ALL SURFACE GOLD ATOMS AND BULK GOLD ATOMS
    def find_surface_bulk_atoms(self):
        '''
        The purpose of this function is to distinguish surface atoms and bulk atoms. 
        INPUTS:
            void
        OUTPUTS:
            top_surface_coord: [np.array, shape=(N,3)] coordinates of the top surface
            bulk_coord: [np.array, shape=(N,3)] coordinates of the bulk surface
            bot_surface_coord: [np.array, shape=(N,3)] coordinates of the bottom surface
        '''
        top_surface_coord, bulk_coord, bot_surface_coord = find_planar_surface_bulk_atoms(self.geometry[:])
        return top_surface_coord, bulk_coord, bot_surface_coord
        
    ### FUNCTION TO PLOT THE LATTICE
    def plot_lattice(self):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        plot_lattice(self.geometry)
        return
    
    ### FUNCTION TO EXPAND THE LATTICE
    def expand_lattice(self):
        '''
        The purpose of this function is to expand the lattice
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
        '''
        ## CREATING GEOMETRY, APPENDING BASIS VECTORS
        self.geometry = self.basis_vectors[:]
        ## DEFINING EXPANSION VECTOR
        self.expansion_vector = [ np.arange(1,self.repeat_x + 1)*self.periodicity[0],
                                 np.arange(1,self.repeat_y + 1)*self.periodicity[1],
                                 np.arange(1,self.repeat_z + 1)*self.periodicity[2] ]
                                 
                                 # 0, 1, 2, .. and so on * lattice constant
        ## LOOPING THROUGH EACH DIMENSION (X, Y, Z)
        for each_dimension in range(3):
            ## DEFINING CURRENT GEOMETRY
            current_geometry = np.copy(self.geometry[:])
            ## DEFINING EXPANSION VECTOR
            expansion_vector = self.expansion_vector[each_dimension]
            ## NOW, LOOPING THROUGH EACH EXPANSION AND APPENDING
            for each_expansion in expansion_vector:
                ## NEW GEOMETRY
                new_geometry = np.copy(current_geometry[:])
                new_geometry[:, each_dimension] += each_expansion
                ## STORING INTO GEOMETRY
                self.geometry = np.concatenate( (self.geometry, new_geometry) )
        return
        
    ### FUNCTION TO DEFINE THE BASIS VECTORS
    def define_basis_vectors(self):
        '''
        The purpose of this function is to generate the basis vectors -- defaulted by FCC (111) lattice from the Interface force field
        INPUTS:
            void
        OUTPUTS:
            self.basis_vectors: [np.array, shape=(6,3)] basis vectors for the fcc {111} lattice
            self.periodicity: [np.array, shape=3,1] periodicity of each dimension
        '''
        self.basis_vectors = np.array([ [ 0.000000000, 0.000000000, 0.000000000 ], # Bottom Au1
                                        [ 1.441860000, 2.497377000, 0.000000000 ], # Bottom Au2
                                        [-0.000000000, 1.664917901, 2.354550070 ], # Bulk Au3
                                        [ 1.441860000, 4.162294901, 2.354550070 ], # Bulk Au4
                                        [ 1.441860000, 0.832459025, 4.709100141 ], # Top Au5
                                        [-0.000000000, 3.329836099, 4.709100141 ], # Top Au6
                                        ]) / 10.0 # in nms
        self.periodicity = np.array([2.8837, 4.9948, 7.0637]) / 10.0 # in nms
        return
        
    
####################################################################
### CLASS TO CREATE CYLINDRICAL, CURVED LATTICE FROM FCC LATTICE ###
####################################################################
class create_cylindrical_curved_fcc_lattice():
    '''
    The purpose of this script is to create a nanoparticle based on a cylinder. The ends of the cylinder is curved. It will truncate spheres at the ends.
    INPUTS:
        fcc_lattice_constant:[float] lattice constant in nm
        radius: [float] radius of the cylinder in nm at the ends
        length: [float] length of the cylinder in nm
        debug_mode: True if you want to debug
    OUTPUTS:
        self.radius: [float] radius of the edges of the cylinder in nm
        self.length: [float] total length of cylinder in nm
        self.lattice_constant: [float] lattice constant used
        self.geomtry: [np.array, shape=(n,3)] geometry of a fcc lattice
    FUNCTIONS:
        cut_lattice_cylinder: cuts lattice into a cylinder
        cut_lattice_round_ends_z_dir: cuts round edge of the cylinder
    ACTIVE FUNCTIONS:
        plot_lattice: plots the lattice
    ALGORITHM:
        - Create fcc lattice
        - Truncate into cylinder
        - Truncate edges
    '''
    ## INITIALIZATION
    def __init__(self, fcc_lattice_constant, radius, length, debug_mode = False):
        ## DEFINING INPUT VARIABLES
        self.radius = radius
        self.length = length
        self.lattice_constant = fcc_lattice_constant
        
        ## FINDING NUMBER OF TIMES WE NEED TO REPEAT
        num_expansion_z = np.ceil( self.length / self.lattice_constant )
        num_expansion_xy = np.ceil( self.radius*2 / self.lattice_constant )
        
        ## DEFINING PLANAR INPUTS
        fcc_lattice_inputs = {
                                'fcc_lattice_constant'  :   self.lattice_constant,
                                'repeat_x'              :   num_expansion_xy,
                                'repeat_y'              :   num_expansion_xy,
                                'repeat_z'              :   num_expansion_z,
                                }
        ## CREATING LATTICE
        fcc_lattice = create_fcc_lattice(**fcc_lattice_inputs )
        
        ## DEFINING GEOMETRY
        self.geometry = fcc_lattice.geometry

        ## FINDING ATOMS CLOSEST TO THE CENTER
        self.center_atom_index, self.center_atom_position, _ = find_atom_closest_center(self.geometry)
        
        ## CUTTING LATTICE INTO A CYLINDER
        self.cut_lattice_cylinder()
        
        ## TODO: Get lattice into spherical cuts
        self.cut_lattice_round_ends_z_dir()
        
        ## PLOTTING IF DEBUG MODE IS ON
        if debug_mode is True:
            self.plot_lattice()

    ### FUNCTION TO CUT LATTICE INTO A CYLINDER
    def cut_lattice_cylinder(self):
        '''
        The purpose of this function is to cut the lattice into a cylinder along the z-dimension
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(n,3)] geometry of a fcc lattice
        '''
        ## FINDING DISTANCES FROM THE SPHERE CENTER
        distance_to_center_atom = np.sqrt(  np.sum( (self.geometry[:,[0,1]] - self.center_atom_position[ [0,1]] )**2, axis=1 ) )
        ## FIND ALL ATOMS WITHIN THE SPHERE
        self.geometry = self.geometry[np.where(distance_to_center_atom <= self.radius)]
        
        return
    
    ### FUNCTION TO TRIM TOP AND BOTTOM OF THE CYLINDER
    def cut_lattice_round_ends_z_dir(self):
        '''
        The purpose of this function is to cut a cylinder into rounded edges. This function will truncate the top portion, then the bottom portion of the cylinder.
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(n,3)] geometry of a fcc lattice
        '''
        ## FINDING Z-CUTOFF POSITION
        z_cutoff_position_from_center = np.array( [ 0, 0, (self.length - 2*self.radius)/2.0 ] ) ## z-position = (L-2r)/2
        
        ## FINDING TOP AND BOTTOM POSITIONS
        center_atom_position_z_cutoff_top = self.center_atom_position + z_cutoff_position_from_center ## CENTER POSITION ON TOP
        center_atom_position_z_cutoff_bot = self.center_atom_position - z_cutoff_position_from_center ## CENTER POSITION ON BOTTOM
        ## TRUNCATING TOP
        distance_to_upper_z = np.sqrt(  np.sum( (self.geometry[:] - center_atom_position_z_cutoff_top )**2, axis=1 ) )
        ## FIND ALL ATOMS WITHIN THE CYLINDER
        self.geometry = self.geometry[np.where( (distance_to_upper_z <= self.radius) | (self.geometry[:,2] <= center_atom_position_z_cutoff_top[2]) )] ## TRUNCATING TOP
        ## TRUNCATING BOTTOM
        distance_to_lower_z = np.sqrt(  np.sum( (self.geometry[:] - center_atom_position_z_cutoff_bot )**2, axis=1 ) )
        self.geometry = self.geometry[np.where( (distance_to_lower_z <= self.radius) | (self.geometry[:,2]>=center_atom_position_z_cutoff_bot[2]) )] ## TRUNCATING BOTTOM
        
        return

    ### FUNCTION TO PLOT THE LATTICE
    def plot_lattice(self):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        plot_lattice(self.geometry)
        return
        
    
#%% DEBUGGING CODE
if __name__ == "__main__": 
    from MDBuilder.applications.nanoparticle.global_vars import GOLD_FCC_LATTICE_CONSTANT_NM
    # fcc_lattice = create_fcc_lattice( fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM, 
    #                                     repeat_x = 0.5,
    #                                     repeat_y = 0.5,
    #                                     repeat_z = 0.5,
    #                                     debug_mode = True
    #                                     )
    # spherical_fcc_lattice = create_spherical_fcc_lattice(fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM, radius = 2, debug_mode = True)
    
    planar_111_fcc_lattice = create_gold_fcc_111_lattice(repeat_x = 10, repeat_y = 10, repeat_z = 1)
    planar_111_fcc_lattice.plot_lattice()
    
    
    #%%
    from MDBuilder.applications.nanoparticle.global_vars import GOLD_FCC_LATTICE_CONSTANT_NM

    
    ## CREATING CYLINDERICAL LATTICE
    cylin = create_cylindrical_curved_fcc_lattice( fcc_lattice_constant = GOLD_FCC_LATTICE_CONSTANT_NM , radius = 1, length = 10, debug_mode = True   )
