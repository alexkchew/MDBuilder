#/usr/bin/python
"""
make_sphere.py
The purpose of this script is to take the number of atoms and radius of particle to generate a spherical nanoparticle.
This uses Brownian dynamics to describe the motion of molecules.

CREATED ON: 02/15/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    Nanoparticle diameter
    Total number of ligands
OUTPUTS:
    geometry for a perfectly hollow sphere

FUNCTIONS:
    initial_spherical: creates initial sphere lattice based on random numbers
    return_spherical: returns the coordinates 
    calc_dist_matrix: calculates distance matrix
    calc_force_matrix: calculates a force matrix
    calc_velocities: calculates velocities

CLASSES:
    make_perfect_sphere: class that makes a perfect sphere

*** UPDATES ***
180304 - Fixing up the script due to multiple mains, changed some variable names
180503 - converted the script to a class -- moving all rotation functions out of this script
"""
#%% 

### IMPORTING MODULES
import numpy as np

##################################################################
### FUNCTIONS TO CREATE SPHERE BASED ON COULOMBIC INTERACTIONS ###
##################################################################

### FUNCTION TO INITIALIZE THE SPHERICAL LATTICE BY GENERATING RANDOM VALUES
def initial_spherical(npoints, radius=1, ndim=3):
    '''
    The purpose of this script is to initialize the spherical lattice
    INPUTS:
        npoints: Number of points
        Radius of points
        ndim: Dimensions (3D, etc.)
    OUTPUTS:
        Numpy array containing all the points
    '''
    vec = np.random.randn( int(ndim), int(npoints)) # Create random numbers in 3D for a uniform distribution
    vec /= np.linalg.norm(vec, axis=0) # Normalize the vector
    vec *= radius
    return vec.T # Tranposing to get (xi, yi, zi)

### FUNCTION TO RE-SCALE ALL THE COORDINATES ACCORDING TO THE RADIUS (PREVENTS ATOMS FROM RUNNING AWAY!)
def return_spherical(coordinates, radius=1,sc=None):
    '''
    The purpose of this script is to take the coordinates and re-scale according to the radius of sphere
    INPUTS:
        coordinates: List of coordinates as numpy array (N x 3)
        radius: radius of sphere
    OUTPUTS:
        Numpy array containing all the points within sphere radius
    '''    
    fixedCoordinates = coordinates[:].T
    fixedCoordinates /= np.linalg.norm(fixedCoordinates, axis=0) # Normalize the vector
    fixedCoordinates *= radius
    return fixedCoordinates.T # Transposing to get (xi, yi, zi)
    
### FUNCTION TO GET THE DISTANCE MATRIX OF THE COORDINATES
def calc_dist_matrix(coordinates):
    '''
    The purpose of this script is to take coordinates and find the difference between i and j
    Inputs:
        1. Coordinates - Numpy array
    Outputs:
        1. deltaXarray - Array for x-differences (j-i)
        1. deltaYarray - Array for y-differences (j-i)
        1. deltaZarray - Array for z-differences (j-i)
    '''    
    def makeDistArray(Vector):
        '''
        This script simply takes a vector of x's, y's, or z's and creates a distance matrix for them
        Input:
            1. Vector - A list of x coordinates for example
        Outputs:
            1. Array - Distance matrix j - i type
        '''
        vectorSize = len(Vector)
        
        # Start by creating a blank matrix
        myArray = np.zeros( (vectorSize, vectorSize ) )
        
        # Use for-loop to input into array and find distances
        for i in range(0,vectorSize-1):
            for j in range(i,vectorSize):
                myArray[i,j] = Vector[j]-Vector[i]
        
        return myArray # - myArray.T

    deltaXarray = makeDistArray(coordinates.T[0])  # X-values
    deltaYarray = makeDistArray(coordinates.T[1])  # Y-values
    deltaZarray = makeDistArray(coordinates.T[2])  # Z-values
    return deltaXarray, deltaYarray, deltaZarray
    
### FUNCTION TO USE THE DISTANCE MATRIX TO GET THE FORCES BETWEEN PARTICLES
def calc_force_matrix(deltaXarray,deltaYarray,deltaZarray):
    '''
    The purpose of this script is to get forces between i and j given distance matrix, rij = rj - ri
    INPUTS:
        deltaXarray: [np.array, shape=(N,N)] distances in N x N in x-direction
        deltaYarray: [np.array, shape=(N,N)] distances in N x N in y-direction
        deltaZarray: [np.array, shape=(N,N)] distances in N x N in z-direction
    OUTPUTS:
        force_matrix: [np.array, shape=(N,3) Forces for each particle in x, y, z dimensions
    '''
    # Finding total distance^2
    dist2 = deltaXarray*deltaXarray + deltaYarray*deltaYarray + deltaZarray*deltaZarray
    
    # Taking inverse, ignore the zeros
    def div0( a, b ): # Allows you to divide arrays
        """ ignore / 0, div0( [-1, 0, 1], 0 ) -> [0, 0, 0] """
        with np.errstate(divide='ignore', invalid='ignore'):
            c = np.true_divide( a, b )
            c[ ~ np.isfinite( c )] = 0  # -inf inf NaN
        return c
    ## INVERSE DISTANCES SQUARED
    invdist2 = div0(1, dist2) # Inverse r^2
    
    ## CALCULATING FORCES
    forceX = invdist2 * deltaXarray # Force in x-direction
    forceY = invdist2 * deltaYarray # Force in y-direction
    forceZ = invdist2 * deltaZarray # Force in z-direction
    
    ## SUMMING ALL THE FORCES
    forceXsum = np.sum(forceX - forceX.T, axis=1) # sum along the rows
    forceYsum = np.sum(forceY - forceY.T, axis=1) # sum along the rows
    forceZsum = np.sum(forceZ - forceZ.T, axis=1) # sum along the rows
    
    # Since forces are symmetrical
    force_matrix = np.array([ forceXsum, forceYsum, forceZsum ]).T # Creating list
    return force_matrix

### FUNCTION TO GET VELOCITIES (Note that Brownian motion included to speed up the interactions)
def calc_velocities(force_matrix, diff_coeff = 1, fric_fact = 1, wantBrownian = True):
    '''
    The purpose of this script is to get velocities via Brownian dynamics using the force matrix
    INPUTS:
        force_matrix: [np.array, shape=(N,3)] force matrix Numpy matrix in N x 3
        diff_coeff: [float, default = 1] Diffusion coefficient
        fricFactor: [float, default = 1] Friction factor
        wantBrownian: [logical] True if you want Brownian motion
    OUTPUTS:
        velocityMatrix: [np.array, shape=(N,3)] velocities of each particle
    '''
    if wantBrownian == True: # If you want Brownian motion
        # First, we need to get R(t), the random number generator
        ranVec = np.random.normal(loc=0,scale=1.0,size=3)

        velocityMatrix = -force_matrix/fric_fact + np.sqrt(2*diff_coeff) * ranVec 
    else:
        velocityMatrix = -force_matrix/fric_fact

    return velocityMatrix

######################################################
### CLASS FUNCTION TO MAKE PERFECTLY HOLLOW SPHERE ###
######################################################
class make_perfect_sphere:
    '''
    The purpose of this script is to take the sphere radius and total number of particles to create a sphere that distances all particles using Brownian motion. 
    After Brownian motion, pure coulombic effect takes over to fully equilibrate the particles.
    INPUTS:
        radius: [float] Radius of sphere
        num_atoms: [int] Total number of atoms
        total_steps: [int] Total steps you want to run this script for
        diff_coeff: [float, default = 1] diffusion coefficient for Brownian motion
        fric_fact: [float, default = 1] frictional factor for Brownian motion
        delta_t: [float, default=0.1] arbitrary time step
        want_plots: [logical] True if you want to get printed plots of average force and final force
        debug_mode: [logical] True if you want to debug and see the particles as snapshots of time
    OUTPUTS:
        ## INITIALIZATION VARIABLES
            self.radius, self.num_atoms, self.total_steps, self.diff_coeff, self.fric_fact, self.delta_t
            self.final_opt_step: [int] final step to start optimization
        ## INITIAL SPHERE LATTICE
            self.initial_sphere: [np.array, shape=(N,3)] initial geometry of the spherical lattice
            self.geometry: [np.array, shape=(N,3)] final geometry that is continuously updated in the simulation
        ## STORAGE OF FORCES
            self.max_force_storage: [list] stores maximum forces per frame
            self.avg_force_storage: [list] stores average forces per frame
    FUNCTIONS:
        initialize_sphere: function to randomly select points at a given radius
        plot_spherical_lattice: [staticmethod] plots the spherical lattice
        plot_xy: [staticmethod] plots x, y
    ACTIVE FUNCTIONS:
        plot_force_vs_time: function that plots force versus time
        dump_coordinates_csv: dumps csv file (if you'd like!)
    '''
    def __init__(self, radius=2, num_atoms=100, total_steps = 100, diff_coeff = 1, fric_fact = 1, delta_t=0.1, want_plots = False, debug_mode = False,):
        ## STORING INITIAL VARIABLES
        self.radius = radius
        self.num_atoms = num_atoms
        self.total_steps = total_steps
        self.diff_coeff = diff_coeff
        self.fric_fact = fric_fact
        self.delta_t = delta_t
        ## CREATING INITIAL SPHERICAL LATTICE
        self.initialize_sphere()
        
        ## CALCULATING FINAL OPTIMIZATION STEP
        self.final_opt_step = round(self.total_steps/3) # Optimize 200 steps before totalSteps
        
        ## GETTING INITIAL DISTANCES
        xDist, yDist, zDist = calc_dist_matrix(self.initial_sphere)
        
        ## CALCULATING INITIAL FORCES
        force_matrix = calc_force_matrix(xDist, yDist, zDist)
        
        ## CALCULATING INITIAL VELOCITIES (BROWNIAN TURNED ON)
        velocities = calc_velocities(force_matrix, diff_coeff=self.diff_coeff,fric_fact=self.fric_fact, wantBrownian = True )
        ###########################
        ### START MD SIMULATION ###
        ###########################
        ## FINDING NEW POSITIONS
        self.geometry = self.initial_sphere + velocities * self.delta_t
        ## RETURNING ALL POSITIONS BACK TO THE SPHERE
        self.geometry = return_spherical(self.geometry, radius=self.radius)
        ## CREATING FIGURE
        if want_plots == True:
            import matplotlib
            matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
            import matplotlib.pyplot as plt
            from mpl_toolkits.mplot3d import axes3d
            # Closing all extraneous figures
            plt.close("all") # Close all figures
            fig, ax = plt.subplots(1, 1, subplot_kw={'projection':'3d'}) # , 'aspect':'equal'
            fig, ax = self.plot_spherical_lattice(self.geometry,radius, fig, ax)
            # plt.show(fig)
            
        ## CREATING VARIABLES TO STORE FORCES
        self.max_force_storage, self.avg_force_storage = [], []
        
        ## LOOPING THROUGH TIME STEPS
        for time_index in range(self.total_steps):
            ## FINDING DISTANCES
            xDist, yDist, zDist = calc_dist_matrix(self.geometry[:])
            ## FINDING FORCES
            force_matrix = calc_force_matrix(xDist[:], yDist[:], zDist[:])
            ## FINDING VELOCITIES
            if time_index < self.final_opt_step:
                # Print current step
                print('Running Brownian step: %d'%(time_index))
                # Getting velocities
                velocities = calc_velocities(force_matrix[:], diff_coeff=self.diff_coeff,fric_fact=self.fric_fact, wantBrownian = True  )
                
            else: # Optimization step -- Necessary in case diffusion coefficient is not correct!
                print('Running Optimization step: %d'%(time_index))
                velocities = calc_velocities(force_matrix[:], diff_coeff=self.diff_coeff,fric_fact=self.fric_fact, wantBrownian = False )
            
            ## FINDING NEW POSITIONS
            self.geometry = self.geometry + velocities * self.delta_t
            ## RETURNING POSITIONS BACK TO THE SPHERICAL LATTICE
            self.geometry = return_spherical(self.geometry, radius=radius)
            
            ## STORING FORCES
            forces_squared = np.sum(force_matrix * force_matrix,axis=1) # fx^2 + fy^2 + fz^2
            self.max_force_storage.append( np.max(forces_squared) )
            self.avg_force_storage.append( np.mean(forces_squared) )
            
            ## IF DEBUG MODE: ON
            if debug_mode is True and time_index%10 == 0: # Every 10 steps:
                ## CONTINUOUSLY UPDATE THE COORDINATES
                plt.cla()
                fig, ax = self.plot_spherical_lattice(self.geometry,radius, fig, ax)
                plt.pause(0.01) # Pause so you can see the changes

        ################################
        ### POST-SIMULATION ANALYSIS ###
        ################################
        if want_plots is True:
            plt.cla()
            fig, ax = self.plot_spherical_lattice(self.geometry,radius, fig, ax)
            fig.savefig('sphericalplot.png', format='png', dpi=1000, bbox_inches='tight')            
            ## CREATING PLOTS
            self.plot_force_vs_time()
        
    ### FUNCTION TO INITIALIZE A SPHERICAL LATTICE (RANDOMLY)
    def initialize_sphere(self, ):
        '''
        The purpose of this function is to create an initial lattice of points randomly
        INPUTS:
            void
        OUTPUTS:
            self.initial_sphere: [np.array, shape=(N,3)] initial geometry of the spherical lattice
        '''
        self.initial_sphere = initial_spherical( npoints = self.num_atoms, radius = self.radius  )
        return
    
    ### FUNCTION TO PLOT FORCE VS TIME
    def plot_force_vs_time(self, save_fig=False):
        '''
        The purpose of this function is to plot forces vs. time
        INPUTS:
            save_fig: [logical] True if you want to save the figures
                This will save two figures:
                    1. maxforce_vs_timestep: maximum force versus time step
                    2. avgforce_vs_timestep: average force versus time step
        OUTPUTS:
            Two figures:
                - max force versus time step
                - average force versus time step
        '''
        # Plotting max force vs. steps
        self.plot_xy(xValues = range(self.total_steps), 
                   yValues = np.sqrt(self.max_force_storage[:]), 
                   xLabel='Time Step', 
                   yLabel='Max Force', 
                   figName='maxforce_vs_timestep', 
                   wantPlots=save_fig)
        
        # Plotting average force vs. steps
        self.plot_xy(xValues = range(self.total_steps), 
                   yValues = np.sqrt(self.avg_force_storage[:]), 
                   xLabel='Time Step', 
                   yLabel='Average Force', 
                   figName='avgforce_vs_timestep', 
                   wantPlots=save_fig)
    
    ### FUNCTION TO PLOT THE SPHERICAL POINTS
    @staticmethod
    def plot_spherical_lattice(coordinates, radius, fig, ax):
        '''
        The purpose of this script is to visualize the sphere in three-dimensions
        INPUTS:
            coordinates: [np.array, shape=(N,3)] coordinates of the ponits
            radius: [float] radius of your sphere, which is used to plot a wireframe
            fig: [figure] figure to plot on
            ax: [axis] axis to plot on
        OUTPUTS:
            Coordinates plotted on a figure
        '''
        import matplotlib
        matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import axes3d
        ## TAKING IN THE COORDINATES
        xi, yi, zi = coordinates.T # Transposing to fix coordinates
        
        ## DEFINING X, Y, Z AXIS
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        
        ## GENERATING WIREFRAME
        phi = np.linspace(0, np.pi, 20)
        theta = np.linspace(0, 2 * np.pi, 40)
        x = radius * np.outer(np.sin(theta), np.cos(phi))
        y = radius * np.outer(np.sin(theta), np.sin(phi))
        z = radius * np.outer(np.cos(theta), np.ones_like(phi))
        
        ## PLOTTING WIREFRAME
        ax.plot_wireframe(x, y, z, color='k', rstride=1, cstride=1, alpha=0.2) # Wireframe, alpha is transparency
        ## PLOTING COORDINATES
        ax.scatter(xi, yi, zi, s=100, c='r', zorder=10)
        plt.draw()        
        return fig, ax
    
    ### FUNCTION TO CHECK THE PLOT
    @staticmethod
    def plot_xy(xValues, yValues, xLabel='x', yLabel='y', figName='plot_check', wantPlots=False):
        '''
        The purpose of this script is to check the configurations via plot
        Inputs:
            1. xValues: x-values for plot
            2. yValues: y-values for plot
            3. xLabel: x-labels for plot
            4. yLabel: y-labels for plot
            5. figName: Name of figure that will be exported, PNG only
        Outputs
            1. Plot of y vs. x
        '''
        import matplotlib
        matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
        import matplotlib.pyplot as plt
        
        # Plotting force vs. steps
        fig = plt.figure() 
        plt.plot(xValues,yValues,'-',color='blue')
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        # plt.show(fig)
        
        if wantPlots==True:
            fig.savefig(figName + '.png', format='png', dpi=1000, bbox_inches='tight')
    
    ### FUNCTION TO COPY THE COORDINATES INTO A TEXT FILE
    def dump_coordinates_csv(self, output_csv='make_perfect_sphere_coordinates.csv'):
        '''
        The purpose of this script is simply to copy the coordinates into a textfile.
        INPUTS:
            output_csv - Textfile name you would like to call your output
        OUTPUTS:
            outputted csv file
        '''
        # Outputting coordinates as a text file
        def myFormat(value): # Formats decimal places
            return "%.3f" % value # 3 Decimal places -- GROMACS style
        with open(output_csv,'w+') as coordinatesFile:
            formattedList = [[myFormat(v) for v in r] for r in self.geometry]
            for eachCoordinate in formattedList:
                coordinatesFile.write(', '.join(str(coord) for coord in eachCoordinate))
                coordinatesFile.write('\n')
    
if __name__ == "__main__": # allows us to test script
    ### RUNNING MAIN SCRIPT
    sphere = make_perfect_sphere(radius=2, num_atoms=100, total_steps = 100, want_plots = True, debug_mode = True)

