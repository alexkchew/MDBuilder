# -*- coding: utf-8 -*-
"""
rotation.py
The purpose of this script is to contain all 3D rotation scripts

Created on: 05/02/2018

Written by: Alex K. Chew (alexkchew@gmail.com)

FUNCTIONS:
    unit_vector: gets a unit vector given a vector
    angle_between: finds the angle between two vectors in radians
    rotation_matrix: creates a rotation matrix based on an axis and an angle
    rotate_coord: 
        rotates coordinates according to some direction vector
    angle_clockwise: 
        calculates angles between two vectors in clockwise
    vector_projection_u_onto_v:
        projects vector u onto v
    get_angle_cw_direction_btn_two_vecs:
        gets angles in a clockwise fashion
    
"""
import numpy as np
import math

### FUNCTIONS USED TO ROTATE
def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
        Uses dot product , where theta = arccos ( unitVec(A) dot unitVec(B))
    """
    ## GETTING UNIRT VECDTORS
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

# Rotating such that the ligand is facing the y-axis
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians. The axis is found by cross product between two vectors, theta is the angle between the vectors (dot product)
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis)) # Normalizing the axis
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]]) # 3 x 3 rotation
    
### FUNCTION TO PROJECT VECTOR
def vector_projection_u_onto_v(u, v):
    '''
    This function projects u ont o v
    INPUTS:
        u: [np.array, shape = 3]
            vector in x,y,z coordinates. This is the vector you are interested 
            in projecting
        v: [np.array, shape = 3]
            vector in x,y,z coordinates. This is the vector you a projected 
            towards. 
    
    REFERENCE:
        https://www.geeksforgeeks.org/vector-projection-using-python/
    '''
    ## GETTING NORM OF V
    v_norm = np.sqrt(np.sum(v**2))
    
    # Apply the formula as mentioned above 
    # for projecting a vector onto another vector 
    # find dot product using np.dot() 
    proj_of_u_on_v = (np.dot(u, v)/v_norm**2)*v 

    return proj_of_u_on_v

### FUNCTION TO GET ANGLE BASED ON CCW DIRECTION
def get_angle_cw_direction_btn_two_vecs(x_vec, y_vec, vec):
    '''
    This function computes the angle in a clockwise fashion using 
    vec1 and vec2. We assume that vec1 and vec2 are perpendicular vectors, 
    allowing us to define the quadrants I - IV. We could then add angles 
    and subtract angles to get the correct angle in CW fashion. The angle 
    will be relative to the x_vec. 
    
    y vec
    |   /
    |  /
    | / angle
    |/____ x vec
    
    INPUTS:
        x_vec: [np.array]
            vector in x direction
        y_vec: [np.array]
            vector in y direction
        vec: [np.array]
            vector you want the angle relative to the x-axis
    OUTPUTS:
        angle_cw: [radians]
            angle in clockwise direction
        quad: [str]
            quadrant that your vector is in relative to x_vec and y_vec
    
    '''
    ## GETTING ANGLE BETWEEN
    angle_btn_avg_to_vec = np.degrees( angle_between(v1 = vec,
                                                     v2 = x_vec) )
    
    ## GETTING DOT PRODUCT
    x_dot = np.dot(vec, x_vec)
    y_dot = np.dot(vec, y_vec)
    
    ## GETTING QUADRANT
    if x_dot > 0 and y_dot > 0:
        quad = "I"
        angle_cw = angle_btn_avg_to_vec
    elif x_dot < 0 and y_dot >0:
        quad = "II"
        angle_cw = angle_btn_avg_to_vec
    elif x_dot < 0 and y_dot <0:
        quad = "III"
        angle_cw = 360 -  angle_btn_avg_to_vec
    else:
        quad = "IV"
        angle_cw = 360 - angle_btn_avg_to_vec
    
    ## CONVERTING ANGLE BACK TO RADIANS
    angle_cw = np.radians(angle_cw)

    return angle_cw, quad

### FUNCTION TO ROTATE YOUR LIGAND
def rotate_coord(avg_vector_dir, desired_dir, rotated_data ):
    '''
    Given x, y, z, the current direction vector and the desired direction vector, rotate all xyz positions -- uses a rotation matrix
    INPUTS:
        avg_vector_dir: Current direction that you are at (1 x 3 vector)
        desired_dir: Vector direction you would like to be
        rotated_data: N x 3 numpy array that will be transformed
    OUTPUTS:
        rotated_data: data that has been rotated
    '''
    # Finding the axis to rotate on
    axis = np.cross(avg_vector_dir, desired_dir) # cross-product
    # Now, finding angle to rotate to
    angle = angle_between(avg_vector_dir, desired_dir) # in radians
    # Finding rotation matrix
    rot_matrix = rotation_matrix(axis, angle)
    # Rotating the xyz coordinates
    rotated_data =np.dot( rot_matrix, rotated_data.T).T

    return rotated_data

### FUNCTION TO FIND THE ANGLE BETWEEN TWO VECTORS CLOCKWISE
def angle_clockwise(A, B):
    '''
    The purpose of this script is to simply find the angle between two vectors clockwise
    Looks like the angle is based on 
    Inputs:
        1. A, B -- Vectors
    Outputs:
        1. Angle in degrees
    '''
    from math import acos
    from math import sqrt
    from math import pi

    def length(v):
        return sqrt(v[0]**2+v[1]**2)
    def dot_product(v,w):
        return v[0]*w[0]+v[1]*w[1]
    def determinant(v,w):
        return v[0]*w[1]-v[1]*w[0]
    def inner_angle(v,w):
       cosx=dot_product(v,w)/(length(v)*length(w))
       rad=acos(cosx) # in radians
       return rad*180/pi # returns degrees
    inner=inner_angle(A,B)
    det = determinant(A,B)
    if det<0: #this is a property of the det. If the det < 0 then B is clockwise of A
        return inner
    else: # if the det > 0 then A is immediately clockwise of B
        return 360-inner
    
#########################################################
### FUNCTIONS TO ROTATE LIGANDS ACCORDING TO A SPHERE ###
#########################################################
    
### FUNCTION TO GET ANGLES BETWEEN POINTS IN THE SPHERE
def sphere_find_all_angles(Positions, index_of_reference=0):
    '''
    The purpose of this function is to generate angles between the points of the sphere using dot products
    INPUTS:
        Positions: [np.array, shape=(N,3) Matrix containing position of points
        point_of_reference: [int] Point to initially reference
    OUTPUTS:
        Angles: All angles in radians between each point with respect to reference (N x 1)
    '''
    # Using for-loop to create angle matrix -- Vectorization may be better, but may improve later
    
    # First finding total number of atoms
    totalAtoms = len(Positions)
    
    # Creating array to store your angles
    Angles = np.zeros( (totalAtoms, 1) ) # N x 1
    
    for currentAtom in range(totalAtoms):
        Angles[currentAtom] = angle_between( Positions[index_of_reference,:], Positions[currentAtom,:] )
    
    return Angles # in radians

### FUNCTION TO GENERALLY FIND ROTATED COORDINATES BETWEEN TWO POSITIONS AND ANGLES
def find_rotated_coordinates(vec1, vec2, angle_btn_vector, positions):
    '''
    The purpose of this function is to take the positions, angle between positions, and data from a line to simply rotate all the lines according to spherical coordinates.
    For example, suppose you had one line:
        -->
    Another line:
        ^
        |
        |
    Therefore, this script will update the positions in the correct rotation
    INPUTS:
        vec1: [np.array, shape=(1,3)] 
            Vector of the reference
        vec2: [np.array, shape=(1,3)] 
            Vector of the new location
        angle_btn_vector: [float] 
            Angle between vectors 1 and 2
        positions: [np.array, shape=(N,3)]  
            data you want to rotate
    OUTPUTS:
        positions: [np.array, shape=(N,3)] 
            Transformed positions
    '''
    ## FINDING AXIS OF ROTATION
    myAxis = np.cross(vec1, vec2) # Gives us the axis of rotation    
    ## DEVELOPING THE ROTATION MATRIX GIVEN AXIS AND ANGLE
    # Then, we need rotation matrix given we know the angle between the vectors
    RotationMatrix = rotation_matrix(myAxis, angle_btn_vector)
    # Then, we need to multiply the LineData, assuming it is in n x 3 format
    positions = np.dot( RotationMatrix, positions.T )
    return positions.T

### ROTATE ALL THE DATA ACCORDING TO A REFERENCE
def find_multiple_rotation_translation_coordinates(reference_index, reference_positions, positions, positions_ref_index):
    '''
    The purpose of this script is to take some data and rotate it according to positions around a sphere. It will also translate the rotated line to the correct reference point
    INPUTS:
        reference_index: [int] 
            Which one do you want the LineData to reference to? Should be your initial point of reference
        reference_positions: [np.array, shape=(N x 3)] 
            atom positions where you want your line data to align to
        positions: [np.array, shape=(Nx3) ]  
            Data that you want to rotate (e.g. ligand positions)
        positions_ref_index: [int] 
            Index of the data that you want to recenter to the positions
    OUTPUTS:
        Final coordinates of all rotated data as a numpy array
    '''
    ## DEFINING REFERENCE POINT
    referencePoint = reference_positions[reference_index,:]
    
    # Finding all angles between reference points and all other points
    AnglesToRef = sphere_find_all_angles(Positions = reference_positions, index_of_reference = reference_index)
    
    ## GENERATING HOLDER FOR ROTATED COORDINATES
    RotatedCoordinates = []
    ## STORING INITIAL ROATATED POSITIONS
    RotatedCoordinates.extend(positions[:]) 

    # Using for-loop to rotate line data around each coordinate with respect to reference
    for currentAtom in range(len(reference_positions)):
        if currentAtom != reference_index: # Ensures no rotation to itself!
            nextPoint=reference_positions[currentAtom,:]
            rotated_line_data = find_rotated_coordinates(vec1 = referencePoint, 
                                                         vec2 = nextPoint, 
                                                         angle_btn_vector = AnglesToRef[currentAtom], 
                                                         positions = positions[:] )
            ## RE-ALIGNING THE POSITIONS
            translation_vector = nextPoint - rotated_line_data[positions_ref_index]
            fixed_data = rotated_line_data + translation_vector
            RotatedCoordinates.extend( fixed_data )
        
    return np.array(RotatedCoordinates)
    
