#/usr/bin/python
"""
remove_atoms.py
Given a gro file, we would like to remove all the atoms within a certain radius given that you know the atoms that are your "center" and the residues that you would like to remove.
This works in conjunction of spherical nanoparticle builder

CREATED ON: 02/16/2017

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    - GRO File
    - Radius of removal
OUTPUTS:
    - Should make a corrected gro file
    - Should fix topology file's total molecule count
ALGORITHM:
    - Import the gro file
    - Find all gold atoms
    - Find center of gold atoms
    - Translate the coordinates to the center of the gold atoms
    - Look for all residues within the radius that we care about
    - Get index for the residues
    - Find all atoms within those specific residues
    - Remove those atoms by discluding them in the new gro file
    - Find all residues that were removed
    - Update topology with changes to the gro file:
        - Find all residues that were changed
        - Change the values of those residue numbers
        - Re-output topology file

FUNCTIONS:
    correct_residue_numbers: corrects residue number list
    fix_gro_remove_within_sphere: fixes gro file within sphere
    fix_topology_residue_number: fixes topology residue numbers
    
*** UPDATES ***
20170220 - Added GRO file definition
20180305 - Fixed script to remove anything (including water!) -- based on gold atoms indexes! --- needing new center since the coordinates are messy using half box lengths
20180503 - Edited script for be MDBuilder enabled

"""
### IMPORTING MODULES
import numpy as np ## MATH FUNCTIONS
import time ## RECORDING TIME
import os
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names
from MDBuilder.applications.nanoparticle.global_vars import BULKAU_TYPENAME
from MDBuilder.core.import_tools import extract_gro
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

### FUNCTION TO CORRECT FOR RESIDUE NUMBERS
def correct_residue_numbers(residue_num_list):
    '''
    The purpose of this function is to correct residue numbers. This will iteratively go through the residue numbers and check to see if there is a jump. If so, it will update the residue numbers. This will also go through the numbers to ensure that the residue number does not go > 99999.
    INPUTS:
        residue_num_list: List of residue numbers (e.g. [ 1, 1, 3, 3, 5, 5,  etc. ] )
    OUTPUTS:
        fixed_residue_num_list: Corrected list of residue numbers (e.g. [ 1, 1, 2, 2, etc. ] )
    '''
    #### CORRECTIONS FOR INCORRECT VALUES
    
    # Correcting: Residue Numbers
    # Hard-code checking
    firstNum = 1
    nextNum = firstNum + 1
    checkNum = None # Nothing at first
    correct_residue_num_list = [] # Used to store the corrected residue numbers
    
    ## ADDING A CHECKING TO ENSURE THAT YOUR LIST STARTS WITH 1. IF NOT, THEN WE WILL 
    ## CORRECT THE FIRST INDICES SUCH THAT IT IS 1. 
    if residue_num_list[0] != 1:
        print("Starting residue number from 1! (original start was %d)"%(residue_num_list[0]))
        copied_array = residue_num_list[:]
        current_num = residue_num_list[0]
        desired_num = 1
        residue_num_list = [each_res_index if each_res_index != current_num else desired_num 
                            for each_res_index in copied_array]
    
    for checkResidueNum in residue_num_list:        
        # Check if the residue number is not the current number or the next number
        if checkResidueNum == firstNum or checkResidueNum == checkNum:
            # Append the correct number
            correct_residue_num_list.append(firstNum)
            
        elif checkResidueNum == nextNum:
            # Append the correct number
            correct_residue_num_list.append(nextNum)
            
            # Adding to correct
            firstNum +=1
            nextNum +=1
            
        else: # For big leaps
            # Append the correct number
            correct_residue_num_list.append(nextNum)
            
            # Adding to correct
            firstNum +=1
            nextNum +=1
            checkNum = checkResidueNum # Used for checking if the pattern is sequential
        
    #### CORRECTIONS FOR >99999 VALUES
    # Going through the list and fixing
    fixed_residue_num_list=[]
    for currentResidNum in correct_residue_num_list:
        if currentResidNum <= 99999:
            fixed_residue_num_list.append( currentResidNum )
        else: # Greater than 99999
            fixed_residue_num_list.append( int(currentResidNum - 100000*np.floor(currentResidNum/100000)) )

    return fixed_residue_num_list

### FUNCTION TO REMOVE ATOMS WITHIN THE SPHERE AND UPDATE THE GRO FILE
def fix_gro_remove_within_sphere(inputGROPath,
                                 radius):
    '''
    The purpose of this script is to remove the atoms within a radius using gold atom core as the center
    INPUTS:
        inputGROPath: [str]
            path to gro file
        radius: [float]
            radius you want to remove
    OUTPUTS:
        Updated gro file
        residue_removed_dict: dictionary of removed atoms to update topology. For example: {'BUT': 4} <-- 4 residues were removed!
    '''
    ########################
    ### READING GRO FILE ###
    ########################

    ## READING GRO FILE
    gro_file = extract_gro(inputGROPath)
    ResidueNum, ResidueName, AtomName, AtomNum, xCoord, yCoord, zCoord, Box_Dimensions = gro_file.ResidueNum, \
                                                                                         gro_file.ResidueName, \
                                                                                         gro_file.AtomName, \
                                                                                         gro_file.AtomNum, \
                                                                                         gro_file.xCoord, \
                                                                                         gro_file.yCoord, \
                                                                                         gro_file.zCoord, \
                                                                                         gro_file.Box_Dimensions

    ### CREATING INITIAL GEOMETRY
    initial_geom = np.array([xCoord, yCoord, zCoord]).T # N X 3 Array

    #########################################
    ### FINDING ALL ATOMS WITHIN THE CORE ###
    #########################################
    
    ### FINDING ALL THE GOLD ATOMS
    indices_gold_atoms = [index for index, each_residue in enumerate(ResidueName) if BULKAU_TYPENAME in each_residue]
    
    ### USING GOLD ATOMS, FINDING CENTER OF MASS -- used as a translation vector
    translation = np.mean( initial_geom[indices_gold_atoms], axis=0  )
    
    ### TRANSLATING ALL COORDINATES
    translated_coord = np.array(initial_geom) - translation
    
    ### GETTING ALL NORMS
    translated_coord_norms = np.linalg.norm(translated_coord,axis=1)
    
    ### LOOPING THROUGH EACH NORM TO SEE IF THE ATOM IS WITHIN THE RADIUS (ignoring bulk gold atoms)
    index_within_radius = [index for index, norm_value in enumerate(translated_coord_norms) if norm_value <= radius and BULKAU_TYPENAME not in ResidueName[index] ]
    
    ### GETTING RESIDUE NAMES FOR EACH OF THE INDEXES (UNIQUE)
    residue_numbers_within_radius = list(set([ ResidueNum[index] for index in index_within_radius]))
    residue_names_within_radius = [ [ ResidueName[index] for index, each_res_num in enumerate(ResidueNum) if each_res_num == residue_num_in_radius][0]  for residue_num_in_radius in residue_numbers_within_radius ]
    
    ### COUNTING EACH AND MAKING A DICTIONARY
    residue_removed_dict = dict()
    for i in residue_names_within_radius:
      residue_removed_dict[i] = residue_removed_dict.get(i, 0) + 1
    
    ### REMOVING SPACES FROM DICTIONARY KEYS
    residue_removed_dict = {k.replace(' ', ''): v for k, v in residue_removed_dict.items()}
    
    ### FINDING UNIQUE NAMES
    total_residues_within_radius = len(residue_numbers_within_radius)
    print("FOUND %s NUMBER OF RESIDUES WITHIN RADIUS OF %s nm"%(len(residue_numbers_within_radius),radius )) # PRINTING
    
    ### SEE IF TOTAL RESIDUES ARE GREATER THAN ZERO
    if total_residues_within_radius > 0:
    
        ### FINDING INDEX TO SAVE
        index_saved  = [ index for index, residue_number in enumerate( ResidueNum ) if residue_number not in residue_numbers_within_radius] 
        
        #########################################
        ### PREPARATION TO OUTPUT TO GRO FILE ###
        #########################################
        
        ### GETTING ONLY THE INDEXES TO SAVE
        ResidueNum = [ ResidueNum[index] for index in index_saved ]
        ResidueName = [ ResidueName[index] for index in index_saved ]
        AtomName = [ AtomName[index] for index in index_saved ]
        AtomNum = [ AtomNum[index] for index in index_saved ]
        Geometry = initial_geom[index_saved,:]
        
        ### CORRECTING ATOM NUMBERS
        AtomNum =  [ x for x in range(1, len(AtomNum) + 1 ) ] 
        
        ### CORRECTING ALL RESIDUE NUMBERS
        ResidueNum = correct_residue_numbers(ResidueNum)
        
        ##############################
        ### OUTPUTTING TO GRO FILE ###
        ##############################
        
        ## COMBINING RESIDUE NAMES FOR GRO FILE
        ## GETTING OUTPUT NAMES: combine ligand name with atom name /number to create output name for gro file
        outputnames = []
        for i in range(0, len(AtomNum)):
            # convert resname to 5 characters
            resname = ResidueName[i].rjust(5, ' ')
            # make atom name + id as well
            atomname = AtomName[i] # + str(i+1)
            comboname = resname + atomname.rjust(5, ' ')
            outputnames.append(comboname)
        
        # residue_for_gro = [ x + y for x,y in zip(ResidueName, AtomName) ]
        
        print("Printing GRO file to: %s"%( inputGROPath) )
        
        print_gro_file_combined_res_atom_names(output_gro=inputGROfile,
                                               output_folder=outputDir,
                                               resids = ResidueNum[:],
                                               geom = Geometry, # Geometry numpy N x 3
                                               outputnames = outputnames, # Atom names
                                               box_dimension = Box_Dimensions # Box dimensions in a list
                                               )                 
        print("--- Total time to remove atoms %s seconds ---" % (time.time() - start_time))   
    else:
        print("Since no atoms were found within the radius, nothing was changed in the GRO file")
    
    return residue_removed_dict


### FUNCTION TO CORRECT TOPOLOGY NUMBER
def fix_topology_residue_number(topology_data, residue_name, num_residues_removed):
    '''
    The purpose of this function is to take the residue name and residue number, then fix accordingly if it is different in the topology
    INPUTS:
        topology_data: [LIST] topology data from file.read()
        residue_name: [STRING] name of the residue in topology file (e.g. 'SOL')
        num_residues_removed: [INT] number of residues you have removed
    OUTPUTS:
        corrected_topology_data: Corrected data 
    '''

    # FINDING INDEX OF RESIDUE
    firstIndex = topology_data.index(residue_name)

    # FINDING THE LAST LINE
    lastIndex = firstIndex + topology_data[firstIndex:].index('\n')

    # Cutting out the string
    stringToFix = topology_data[firstIndex:lastIndex]

    # Splitting by white space
    List_Residue = stringToFix.split()
    
    # Correcting value
    List_Residue[-1] = int( List_Residue[-1]) -  num_residues_removed # <-- Correcting number of SOL
        
    # CorrectedString
    correctedString = List_Residue[0] + '    ' + str(List_Residue[1])
     
    # Correcting file data
    corrected_topology_data = topology_data[:firstIndex] + correctedString + topology_data[lastIndex:]
    
    print("Topology has been fixed for residue: %s"%(residue_name))
    
    return corrected_topology_data


###############################
### UPDATING TOPOLOGY  FILE ###
###############################
### FUNCTION TO UPDATE TOPOLOGY FILES BY REMOVING RESIDUES
def check_topology_remove_residues(residue_removed_dict, topologyPath):
    '''
    The purpose of this function is to correct for problems with topology -- checks if there are any residues that need to be removed
    INPUTS:
        residue_removed_dict: [DICT] Dictionary with residues to remove (e.g. {'BUT': 4}) <-- residue name, and number to remove
        topologyPath: path to topology file
    OUTPUTS:
        topologyPath <-- Updated topology within same path
    '''
    
    ## CHECKING IF THERE ARE CHANGES NEEDED TO BE MADE
    if residue_removed_dict:
        ## READING TOPOLOGY FILE
        filedata = None
        with open(topologyPath, 'r') as file:
            filedata = file.read()
        
        ## GETTING THE KEYS
        residue_removed_keys = residue_removed_dict.keys()
    
        ## LOOPING THROUGH EACH KEY
        for each_key in residue_removed_keys:
            filedata = fix_topology_residue_number(filedata, each_key, residue_removed_dict[each_key])
            
        print("Topology has been fixed at", topologyPath)
        ## OUTPUTTING TO FILE
        with open(topologyPath, 'w') as file:
          file.write(filedata)
    else:
        print("Nothing wrong with topology --- no changes have been made")
    return



#%%
if __name__=="__main__":

    ## RECORDING TIME
    start_time = time.time()
    
    # Testing parameters
    Testing = check_testing() # False if you are running this program on command line
    ##  For testing ##
    if Testing == True:
        inputGROfile = 'gold_ligand_solv.gro'
        outputDir = "R:\scratch\LigandBuilder\Prep_System\prep_gold\simulations\hollow_2_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol"
        inputGROPath=outputDir + '/' + inputGROfile
        radius = 1 # nm
        topologyfile = 'gold_ligand.top'
        topologyPath = outputDir + '/' + topologyfile

    else: ## Running on Command Line ##
        use = "Usage: %prog [options]"
        from optparse import OptionParser # Used to allow commands within command line
        parser = OptionParser(usage = use)
        
        # Input File and destination
        parser.add_option("-r", "--rad", dest="sphereRadius", action="store", type="float", help="Radius in nm of your nanoparticle", default="2")
        parser.add_option("-g", "--ogro", dest="output_gro_name", action="store", type="string", help="Output GRO filename", default="double_sam.gro")
        parser.add_option("-t", "--otop", dest="output_top_name", action="store", type="string", help="Output TOP filename", default="double_sam.top")
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
    
        # Converting parser into known variables
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        outputDir = options.output_folder # Output folder name
        inputGROfile = options.output_gro_name # Output gro file name
        inputGROPath= os.path.join(outputDir , inputGROfile)
        radius = options.sphereRadius # Radius of cutoff
        inputTopName = options.output_top_name # Topology folder name
        topologyPath = os.path.join(outputDir, inputTopName)

    #############################
    #### RUNNING MAIN SCRIPT ####
    #############################
    ### FIXING GRO FILE
    residue_removed_dict =  fix_gro_remove_within_sphere(inputGROPath = inputGROPath,
                                                         radius = radius,
                                                         )

    ### UPDATING TOPOLOGY FILE
    check_topology_remove_residues(residue_removed_dict, topologyPath)
    