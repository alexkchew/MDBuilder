# -*- coding: utf-8 -*-
"""
umbrella_sampling_select_configurations.py
The purpose of this script is to select configurations based on an umbrella sampling configurations available.

NOTES:
    - We assume you already ran a pull simulation and just simply need the configurations for umbrella sampling simulations
    - We also assume that you extracted the trajectories such that each trajectory corresponds to a frame with varying center of masses
    - Lastly, we assume that you have some summary.dat file consisting of two columns, the first being the index, and the second being the center of mass between your two pull groups
    - If all the above is satisfied, use this script to quickly generate configurations based on some desired bounds (e.g. 0.1 to 1 nm in increments of 0.1 nm ... etc.)

CREATED ON: 09/12/2018

MAIN FUNCTION EXAMPLE ON TERMINAL:
    python umbrella_sampling_select_configurations.py
    
        --imain main path to input folder
        --isum summary dat file
        --icon input configuration folder
        --ipref input configuration prefix for the gro files
        
        --omain output configuration folder
        --osum output summary file name
        --opref output prefix for gro files
        
        --upper upper bound for umbrella sampling configurations
        --lower lower bound for umbrella sampling configurations
        --inc increment between upper and lower bound. Note that this script is inclusive of both upper and lower bounds!
        
CLASSES:
    read_configuration_summary: reads configuration summary from pulling simulations
    extract_configurations: extract configuration based on the summary file
    
FUNCTIONS:
    find_windows_based_on_bounds: reads windows based on bounds
    
AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
"""

### IMPORTING IMPORTANT FUNCTIONS
import numpy as np
import pandas as pd
import time
import os

## MDBUILDER FUNCTIONS
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

## FUNCTION TO READ TEXT FILE
from MDBuilder.core.import_tools import load_and_clean_text_file

## IMPORTING PARSER
from MDBuilder.core.callback import interpret_options_comma

## FUNCTION TO CHECK IF DIRECTORY EXISTS
from MDBuilder.core.check_tools import check_file_exist, check_dir ## CHECKING PATH FOR TESTING

## COPYING FILE FUNCTION
from shutil import copyfile

## OPTION PARSER
from optparse import OptionParser # Used to allow commands within command line

### FUNCTION TO CONVERT LOWER AND UPPER BOUNDS INTO WINDOWS
def find_windows_based_on_bounds(lower_limit_config, upper_limit_config, increment_config  ):
    '''
    The purpose of this function is to find the array of windows that you are interested in based on lower, upper, and increments
    INPUTS:
        lower_limit_config: [float]
            lower limit of the configuration, e.g. 0.2 nm
        upper_limit_config: [float]
            upper limit of the configuration, e.g. 1 nm
        increment_config: float]
            increment of the configuration, e.g. 0.1 nm
    OUTPUTS:
        configuration_increments: [np.array, shape=(n_increments, 1)]
            array containing values that best fit your desired configurations, e.g. ( 0.2, 0.3, ... )
    '''
    ## FINDING TOTAL REQUIRED TO INCREMENT
    total_windows = int((upper_limit_config-  lower_limit_config)/increment_config) + 1 # Adding one to include end term
    ## DEFINING THE INCREMENTS
    configuration_increments = increment_config * np.array(range(total_windows)) + lower_limit_config  # np.arange( lower_limit_config, upper_limit_config + increment_config, increment_config   )
    return configuration_increments

######################################
### CLASS TO READ PULL SUMMARY.DAT ###
######################################
class read_configuration_summary:
    '''
    The purpose of this object is to read something like "pull_summary.dat". 
    This contains two columns, the first being the configuration number, and 
    the second is the center of mass distances between two groups.
    INPUTS:
        path_to_configuration_summary: [str]
            path to configuration summary file
        
    OUTPUTS:
        ## INITIAL VARIABLES
        self.path: [str]
            path to configuration file
        self.lines: [list]
            list of strings based on extracted configuration files
        self.lines_extract: [list]
                list of lines extracted in the form of the column types, e.g.
                    [[ 1, 0.111],
                     [ 2, 0.333], ...
                     ]
        self.config_index: [np.array, shape=(N,1)]
            configuration index
        self.config_distances: [np.array, shape=(N,1)]
            configuration distances in units of nm (COM-COM distances)
    FUNCTIONS:
        convert_lines_based_on_deliminator: converts the lines from string based to different types (e.g. int, float)
    ALGORITHM:
        - Read text file, split into lines
        - Then, convert those lines into floats and integers, based on configuration summary
        - Afterwards, generate an index and distance array for each of the configurations
    '''
    ### INITIALIZING
    def __init__(self, path_to_configuration_summary):
        ## STORING PATH
        self.path=path_to_configuration_summary
        ## READING THE FILE LINE BY LINE
        self.lines = load_and_clean_text_file(self.path)
        ## CONVERTING LINES BASED ON DELIMINATOR
        self.convert_lines_based_on_deliminator()
        ## DEFINING CONFIGURATIONS
        self.config_index = np.array( [ each_line[0] for each_line in self.lines_extract] )
        self.config_distances =  np.array( [ each_line[1] for each_line in self.lines_extract] )
        return

    ### FUNCTION TO  CONVERT LINES BY SPACE
    def convert_lines_based_on_deliminator( self , deliminator=' ', column_types = [ int, float ]): # 
        '''
        The purpose of this function is to convert the lines of strings (e.g. '1 1.5') into comprehensible strings
        INPUTS:
            self: [object]
                class object
            deliminator: [str]
                deliminator between the columns
            column_types: [list]
                list of types between the columns
        OUTPUTS:
            self.lines_extract : [list]
                list of lines extracted in the form of the column types, e.g.
                    [[ 1, 0.111],
                     [ 2, 0.333], ...
                     ]
        '''
        ## LOOPING THROUGH EACH LINE AND SPLITTING ACCORDING TO A SPACE, THEN CONVERTING INTO STRINGS
        self.lines_extract =[ each_line.split(deliminator) for each_line in self.lines if len(each_line) > 0 ] 
        
        ## LOOPING THROUGH LINES EXTRACTED AND CONVERTING THE COLUMN TYPES
        self.lines_extract = [ [ column_types[col_idx](each_column) for col_idx, each_column in enumerate(each_line) ] for each_line in self.lines_extract
                              ]
        
        return

####################################################
### CLASS FUNCTION TO EXTRACT THE CONFIGURATIONS ###
####################################################
class extract_configurations:
    '''
    The purpose of this function is to extarct the configurations based on your summary file
    INPUTS:
        config_file: [object]
            configuration file based on read_configuration_summary class
        configuration_increments: [np.array]
            configuration increments that you are interested in acquiring
        deviation_error: [float, default=0.1 nm]
            deviation between configuration increments and current available configuration before this function begins to complain
        input_config_folder_path: [str]
            path to your input configuration folder, containing all the gro files
        input_configuration_prefix: [str]
            configuration prefix of your gro files (e.g. 'conf')
        output_config_folder_path: [str]
            path to your output configuration folder
        output_config_prefix: [str]
            configuration prefix of your output gro files
        output_summary_file: [str]
            output summary file
    OUTPUTS:
        ## STORED INPUTS
        self.config_file, self.configuration_increments, self.deviation_error, self.input_config_folder_path, self.input_configuration_prefix,
        self.output_config_folder_path, self.output_config_prefix, self.output_summary_file
        
        ## CLOSEST CONFIGURATION INFORMATION
        self.closest_configurations_pd: [panda dataframe]
            table consisting of all closest values

    FUNCTIONS:
        find_closest_configurations: finds the closest configurations to your index file
        copy_gro_configurations: copies gro configurations from one folder to next
        
    ALGORITHM:
        - find closests configurations
        - output gro files
        - output summary file
    '''
    ### INITIALIZING
    def __init__( self, config_file, configuration_increments,
                 input_config_folder_path, input_configuration_prefix, # INPUT CONFIGURATION AND PREFIX
                 output_config_folder_path, output_config_prefix, output_summary_file, # OUTPUT CONFIGURATION AND PREFIX
                 deviation_error = 0.1
                 ):
        ## STORING INPUTS
        self.config_file = config_file
        self.configuration_increments = configuration_increments
        self.deviation_error = deviation_error
        # CONFIGURATION INFORMATIONS
        self.input_config_folder_path       = input_config_folder_path
        self.input_configuration_prefix     = input_configuration_prefix
        self.output_config_folder_path      = output_config_folder_path
        self.output_config_prefix           = output_config_prefix
        self.output_summary_file            = output_summary_file
        
        ## CHECKING IF DIRECTORY EXISTS
        check_file_exist(self.input_config_folder_path)        
        check_dir(self.output_config_folder_path)
        
        ## FINDING CLOSEST CONFIGURATIONS
        self.find_closest_configurations()
        
        ## DEFINING PATH TO SUMMARY FILE
        self.output_summary_path = os.path.join( self.output_config_folder_path, self.output_summary_file )
        
        ## PRINTING CLEANER FILE
        with open(self.output_summary_path, 'w') as fo:
            fo.write(self.closest_configurations_pd.__repr__())
        
        ## COPYING THE CONFIGURATIONS AND RENAMING THE CONFIGURATIONS
        self.copy_gro_configurations( verbose = True )
        
        return
        
    ### FUNCTION TO COPY AND RENAME THE GRO FILES
    def copy_gro_configurations( self, verbose = False ):
        '''
        The purpose of this function is to look over your closest configuration, then output only the gro files of that configuration
        INPUTS:
            self: [object]
                class object
            verbose: [logical]
                True if you want verbose outputs
        OUTPUTS:
            gro files into your output folder
        '''
        ## PRINTING
        if verbose == True:
            print("*** COPYING GRO CONFIGURATIONS ***")
        ## LOOPING THROUGH THE CLOSESTS CONFIGURATION LIST
        for idx, each_desired_increment in enumerate(self.closest_configurations_pd['Desired_increment'].tolist()):
            ## DEFINING INDEX FOR CONFIGURATION
            conf_index = self.closest_configurations_pd['Conf_index'][idx]
            ## DEFINING ACTUAL INCREMENT
            actual_increment = self.closest_configurations_pd['Actual_increment'][idx]
            
            ## DEFINING OLD GRO FILE NAME
            old_gro_file_name = "%s%d.gro"%( self.input_configuration_prefix , conf_index )
            ## DEFINING OLD GRO PATH
            old_gro_file_path = self.input_config_folder_path + '/' + old_gro_file_name
            ## CHECKING IF FILE EXISTS
            check_file_exist(old_gro_file_path)
            
            ## DEFINING NEW GRO FILE NAME
            new_gro_file_name = "%s_desired_%.3fnm_actual_%.3fnm.gro"%( self.output_config_prefix, each_desired_increment,  actual_increment)
            ## DEFINING NEW GOR FILE PATH
            new_gro_file_path = self.output_config_folder_path + '/' + new_gro_file_name
            
            ## COPYING OVER FILE
            copyfile(old_gro_file_path, new_gro_file_path)
            
            ## PRINTING WHAT WE ARE DOING
            if verbose == True:
                print("Index %d, increment %.3f, closest configuration %d, actual increment %.3f"%( idx, each_desired_increment, conf_index, actual_increment ) )
            
        return
        
    ### FUNCTION TO FIND THE CLOSEST CONFIGURATIONS
    def find_closest_configurations(self):
        '''
        The purpose of this function is to find the closest configuration to the configuration increments
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
            self.closest_configurations_pd: [panda dataframe]
                table consisting of all closest values, e.g.:
                       Desired_increment  Actual_increment  vector_index  Conf_index
                    0                0.2             0.200         208.0       208.0
                    1                0.3             0.300         182.0       182.0
                    2                0.4             0.401         160.0       160.0 ....
                    
                    NOTES:
                        - Desired_increment: indicates your desired increment
                        - Actual_increment: indicates the increment found from the summary file
                        - vector_index: indicates the index based on the python index
                        - Conf_index: configuration index (note, this may not be the same as vector index if it does not start at 0)
        '''
        ## CREATING STORAGE VECTOR
        self.closest_configurations_pd = pd.DataFrame( columns = ['Desired_increment', 'Actual_increment', 'vector_index' , 'Conf_index' ],
                                                       )
        ## LOOPING THROUGH EACH CONFIGURATION REQUIREMENT
        for each_config_value in self.configuration_increments:
            ## MINIMUM INDEX
            idx = np.argmin( np.abs(self.config_file.config_distances - each_config_value) )
            ## FINDING MINIMUM
            config_min = self.config_file.config_distances[idx]
            ## FINDING CONF INDEX
            config_index = self.config_file.config_index[idx]
            ## CHECKING
            if abs(config_min - each_config_value) > self.deviation_error:
                print("Configurations that you selected may not be well-converged! We have found deviations greater than: %.2f"%( self.deviation_error ))
                print("Be careful! A poorly defined reaction coordinate may lead to issues. This increment is having issues: %.3f"%(each_config_value))
                print("Closest point was: %.3f"%(config_min))
                time.sleep(5)
            
            ## APENDING
            self.closest_configurations_pd = self.closest_configurations_pd.append(
                                                {'Desired_increment': each_config_value,  
                                                 'Actual_increment' : config_min,
                                                 'vector_index'     : int(idx),  
                                                 'Conf_index'       : int(config_index),   } , ignore_index = True)
                                                
        ## CONVERTING DATA TYPE
        self.closest_configurations_pd['vector_index'] = self.closest_configurations_pd.vector_index.astype(int)
        self.closest_configurations_pd['Conf_index'] = self.closest_configurations_pd.Conf_index.astype(int)
    
        return
    



#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    ## TESTING IS ON
    if testing is True:
        ### INPUTS
        input_main_path=r"R:/scratch/nanoparticle_project/simulations/20200113-NP_lipid_bilayer_resize/NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
        # r"R:\scratch\SideProjectHuber\prep_mixed_solvent\prep_umbrella_sampling\2_pulling\PULL_xylitol_300.00_6_nm_HYD_10_WtPercWater_spce_dmso" # Folder containing pull information
        input_summary_dat_file="pull_summary.xvg"
        # "pull_summary.dat"   # Summary .dat file
        input_configuration_folder="us_config"     # Configuration folder containing all the gro files
        input_configuration_prefix="conf"           # Prefix of the configurations (e.g. conf*)
        
        ### OUTPUT FILES
        output_config_folder_path=r"R:\scratch\SideProjectHuber\Simulations\180912-Umbrella_sampling_sims\UMB_300.00_6_nm_xylitol_10_WtPercWater_spce_dioxane\2_configurations"
        output_config_prefix="conf"                 # Prefix of the gro files that you want to copy
        output_summary_file="config_summary.csv"    # Summary of the configurations    
        
        ### DEFINING UMBRELLA SAMPLING CONFIGURATIONS
        upper_limit_config  = 1.10   # Upper bound
        increment_config    = 0.10   # Increment configurations
        lower_limit_config  = 0.20   # Lower bound
        
        ### DEFINING DESIRED TIME STEPS
        configuration_increments = [0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.95, 1.00, 
                                    1.05, 1.10, 1.15, 1.20, 1.25, 1.30, 1.35, 1.40, 1.45, 1.50, 1.55, 1.60, 1.65, 1.70, 1.75, 1.80, 1.85, 1.90, 1.95, 2.00]
        
    ## TESTING IS OFF
    else: 
        ### DEFINING PARSER OPTIONS
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## ADDING OPTIONS FOR INPUT
        parser.add_option("--imain", dest="input_main_path", action="store", type="string", help="Input main folder", default=".")
        parser.add_option("--isum", dest="input_summary_dat_file", action="store", type="string", help="Input summary file", default=".")
        parser.add_option("--icon", dest="input_configuration_folder", action="store", type="string", help="Input configuration folder", default=".")
        parser.add_option("--ipref", dest="input_configuration_prefix", action="store", type="string", help="Input prefix for gro files", default=".")
        
        ## ADDING OPTIONS FOR OUTPUT
        parser.add_option("--omain", dest="output_main_path", action="store", type="string", help="Output main folder", default=".")
        parser.add_option("--osum", dest="output_summary_file", action="store", type="string", help="Output summary file", default=".")
        parser.add_option("--opref", dest="output_config_prefix", action="store", type="string", help="Output prefix for gro files", default=".")
        
        ## ADDING UMBRELLA SAMPLING INPUTS
        parser.add_option("--upper", dest="upper_limit_config", action="store", type="string", help="Upper configuration limit", default="0.1")
        parser.add_option("--lower", dest="lower_limit_config", action="store", type="string", help="Lower configuration limit", default="2")
        parser.add_option("--inc", dest="increment_config", action="store", type="string", help="Increment configuration limit", default="0.1")
        parser.add_option("--configs", dest="configuration_increments", action="callback", type="string", callback=interpret_options_comma,
                  help="Configurations, separated by comma separate each ligand name by comma (no white space)", default=None)
        
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## RESTORING VARIABLES
        # INPUT FILES
        input_main_path = options.input_main_path
        input_summary_dat_file = options.input_summary_dat_file
        input_configuration_folder = options.input_configuration_folder
        input_configuration_prefix = options.input_configuration_prefix
        # OUTPUT FILES
        output_config_folder_path = options.output_main_path
        output_summary_file = options.output_summary_file
        output_config_prefix = options.output_config_prefix
        # UMBRELLA SAMPLING PARAMETERS
        upper_limit_config =    float(options.upper_limit_config)
        lower_limit_config =    float(options.lower_limit_config)
        increment_config =      float(options.increment_config)
        configuration_increments = options.configuration_increments
        
    ###################
    ### MAIN SCRIPT ###
    ###################    

    ## DEFINING FULL PATH TO CONFIGURATIONS
    input_summary_dat_path = input_main_path + '/' + input_summary_dat_file
    
    ## DEFINING FULL PATH TO CONFIGURATIONS
    input_config_folder_path= input_main_path + '/' + input_configuration_folder
    
    if configuration_increments == None:
        ## FINDING CONFIGURATION INCREMENTS
        configuration_increments = find_windows_based_on_bounds( 
                                                                lower_limit_config  = lower_limit_config,   # lower limit configuration
                                                                upper_limit_config  = upper_limit_config,   # upper limit configuration
                                                                increment_config    = increment_config,     # increment of the configurations
                                                                    )
    else:
        ## CONVERTING CONFIGURATIONS TO FLOATING POINT NUMBERS
        configuration_increments = np.array(configuration_increments).astype('float')
    
    ## READING CONFIGURATION FILE
    config_file = read_configuration_summary(path_to_configuration_summary = input_summary_dat_path)
    
    ## EXTRACTING CONFIG FILE
    extract_config = extract_configurations(config_file = config_file,
                                            configuration_increments = configuration_increments,
                                            deviation_error = increment_config,
                                            input_config_folder_path = input_config_folder_path,
                                            input_configuration_prefix = input_configuration_prefix,
                                            output_config_folder_path = output_config_folder_path,
                                            output_config_prefix = output_config_prefix,
                                            output_summary_file = output_summary_file,
                                            )
    
    
    