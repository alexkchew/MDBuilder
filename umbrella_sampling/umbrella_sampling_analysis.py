# -*- coding: utf-8 -*-
"""
umbrella_sampling_analysis.py
The purpose of this script is to analyze the outputs of umbrella samplings (e.g. histogram.xvg, etc.)

NOTES:
    - This script assumes you have completed the umbrella sampling simulation and have ran gmx wham

CURRENT COMPATABILITIES:
    - histo.xvg
    - profile.xvg

CREATED ON: 09/14/2018

WRITTEN BY:
    Bradley C. Dallin
    Alex K. Chew (alexkchew@gmail.com)
"""
###############################################################################
### IMPORTS
###############################################################################

from optparse import OptionParser # Used to allow commands within command line
import numpy as np # math functions
import sys
if sys.prefix == '/usr':
    import matplotlib
    matplotlib.use('Agg') # turn off interactive plotting
import matplotlib.pyplot as plt  
import os

## FUNCTION TO CREAT FIGURE
from MDDescriptors.core.plot_tools import create_plot, get_cmap, FIGURE_SIZES_DICT_CM, create_fig_based_on_cm

## FUNCTION TO VIEW FILES
import glob

## MODULE FOR OS FUNCTIONS
import os

## IMPORTING VARIABLES
from MDBuilder.core.global_vars import k_B # Boltzmann constant in kJ/(mol K)

## IMPORTING SIGNALLING TOOLS USED FOR FITTING THE DATA
from scipy import signal

## PLOTTING FUNCTIONS
import MDDescriptors.core.plot_tools as plot_funcs

## SETTING DEFAULTS
plot_funcs.set_mpl_defaults()

## DEFINING FIGURE SIZE
FIGURE_SIZE = FIGURE_SIZES_DICT_CM['1_col_landscape']

###############################################################################
### FUNCTIONS
###############################################################################

### FUNCTION TO READ XVG FILES
def read_xvg( path_xvg ):
    '''
    The purpose of this function is to read a xvg file and extracts the numeric values as floats
    INPUTS: 
        path_xvg: [str]
            full path to xvg file
    OUTPUTS: 
        data: [np.array]
            data in a form of a numpy array
    '''
    ## FINDING FULL HEADER SO WE CAN IGNORE THE INITIAL DATA
    with open( path_xvg, 'r' ) as outputfile: # Opens gro file and make it writable
        fileData = outputfile.readlines()        
    fullHeader = [ line for line in fileData if '#' in line or '@' in line ]
    ## READING DATA USING NUMPY, IGNORING ALL HEADERS
    data = np.genfromtxt( path_xvg, delimiter = '\t', skip_header = len( fullHeader ) )
    return data

### FUNCTION TO PLOT HISTOGRAM
def plot_us_histogram(data, 
                      output_fig_name="us_histo.png", 
                      data_range=None,  # np.arange(0.1,3.0,0.1)
                      save_fig=False,
                      fig_size_cm = FIGURE_SIZE): # 0.05
    '''
    The purpose of this function is to plot the histogram for umbrella sampling simulations (i.e. histo.xvg).
    This function assumes you have extracted the data using read_xvg
    NOTES:
        - In histo.xvg, you have the first column as the x-distances between your two COM
        - Subsequent columns indicate the frequency that the specific center of mass distance was visited within the window
        - If the histograms have poor overlap, then you need a better sample phase space (or a lower spring constant)
    INPUTS:
        data: [np.array]
            data of histo.xvg
        output_fig_name: [str, default="us_histo"]
            output figure name
        save_fig: [logical, default=False]
            True if you want to save the figure
    OUTPUTS:
        figure of number of counts versus distance in nanometers
        
    '''
    ## GENERATING PLOT
    fig, ax = create_fig_based_on_cm( fig_size_cm = fig_size_cm )
    plt.rc( 'font', **{ 'size' : 14 } )
    for idx, col in enumerate(data[ :, 1: ].T):
        ax.plot( data[ :, 0 ], col,  linewidth = 1.5, label=str(idx)  ) # 'k-',
    
    if data_range is not None:
        ax.set_xticks( data_range )
    plt.xticks(rotation=90)
    ax.set_xlim([ np.min( data[ :, 0 ] ) - 0.1, np.max( data[ :, 0 ] ) + 0.1 ])                   
    ax.set_xlabel( 'Distance (nm)' )
    ax.set_ylabel( 'Counts' )
    ## TIGHT LAYOUT
    fig.tight_layout()
    # plt.legend()
    if save_fig == True:
        plt.savefig( output_fig_name, format='png', dpi=1000, bbox_inches='tight' )
    return fig, ax

### FUNCTION TO CORRECT FOR PMF
def pmf_correction_entropic_effects(r, temp=300, n_c=3, k_b = k_B ):
    '''
    The purpose of this function is to calculate the correction terms for the PMF based on entropic contributions
    The equation is as follows:
        V(r) = -(n_c -1) k_b * T * log(r)
    Here, the variables are:
        V(r): Entropic contribution of the PMF
        n_c: number of dimensions for which the constraints work, this is 3 for normal constraint, 1 for when only a single direction (i.e. z) is contrained
        k_b: boltzmann constant
        T: temperature of the system
        r: distance between the two groups
    INPUTS:
        r: [np.array, shape=(n_points,1)]
            radius vector between the two groups
        temp: [int, default=300 K]
            temperature in Kelvins
        n_c: [int, default=3]
            number of dimensions which the constraints work. This should be 1 if you have a single direction (i.e. z pulling only)
        k_b: [float, default: 0.008314462175 kJ/(molK) ]
            boltzmann constant
    OUTPUTS:
        PMF: [np.array, shape=(n_points,1)]
    '''
    ## CALCULATING POTENTIAL
    PMF = - (n_c - 1) * k_b * temp * np.log(r)
    return PMF
    
### FUNCTION TO GET REMOVE DISTANCES AND GET INCREMENTAL
def truncate_by_maximum( array, truncation_value ):
    '''
    The purpose of this function is to get the indices of an array such that the maximum extrema are truncated
        e.g. suppose you have a vector: [0, 1, 2, 3, 4, 5] and I want to truncate 4,5 out. 
        This function is great for that case, you would set truncation value to 1 or 2. Then, use the indices to get back the vector.
    USAGE:
        - get the index from this truncation
        - then, if you want the new array, simply do : array[index]
    INPUTS:
        array: [np.array, shape=(N,1)]
            array you are interested in trunctating. For example, this may be a distance vector.
        truncation_value: [float]
            truncation value that you want to truncate the array based on the maximum.
    OUTPUTS:
        index: [np.array, shape=(M,1)]
            indices you can use with the array to get extract information
    '''
    ## FINDING MAXIMUM
    array_maximum_value = np.max(array)        
    ## FINDING ALL POINTS BELOW DISTNACE AND INCREMENT
    index = np.argwhere( array <= array_maximum_value - truncation_value )
    return index

### FUNCTION TO PLOT THE PMF
def plot_us_pmf(data, 
                fig = None, 
                ax = None, 
                label=None,
                ylim=None, 
                color='k', 
                units = "kCal", 
                output_fig_name="us_pmf.png", 
                want_correction=False, temp=300.00, 
                end_truncate_dist=0.0,
                want_smooth_savgol_filter = False, 
                savgol_filter_params={'window_length':5, 'polyorder': 0, 'mode': 'nearest'}, 
                save_fig=False, 
                want_norm = True,
                fig_size_cm = FIGURE_SIZE,
                want_zero_line = True,
                data_range = None,
                ):
    '''
    The purpose of this function is to plot the pmf profile given profile.xvg
    This function assumes you have extracted the data using read_xvg
    
    INPUTS: (0, 50, 5)
        fig: [matplotlib, default=None]
            figure to add the data to
        ax: [matplotlib, default=None]
            axis to add the figure to
        data: [np.array, shape=(n_points, 2)]
            data such that the first column is the distance and the second column is the pmf
        ylim: [default = None]
            y-axis limits, e.g. (0, 50, 5) <-- indicates minimum is zero, maximum is 50, and increment of 5
        label: [str, default=None]
            label for the plot, not required
        color: [str, default="k"]
            color of the pmf plot
        units: [str, default="kCal"]
            units used for the pmf
        output_fig_name: [str, default="us_pmg.png"]
            output figure name for saving the figure
        temp: [int, default=300 K]
            temperature in Kelvins
        want_correction: [logical, default=False]
            True if you want correction for entropic effects -- highly useful if you have two molecules that are freely unconstrained
        want_smooth_savgol_filter: [logical, default = False]
            True if you want to try to smooth the curve using savgol filter
        savgol_filter_params: [dict, default={'window_length':5, 'mode': 'nearest'}]
            savgol_filter parameters used for the filtering process, please see scipy.signal.savgol_filter details
        end_truncate_dist: [float, default=0.0]
            Amount of distance to truncate from the very end -- useful if you want to avoid spurious errors when you get too far away
        save_fig: [logical, default=False]
            True if you want to save the figure
        want_norm: [logical, default=True]
            True if you want to normalize your PMF with respect to the last 10 points
        want_zero_line: [logical, default=True]
            True if you get a plot in zero line
        data_range: [np.array, default = None]
            default range for the data
    OUTPUTS:
        fig: [matplotlib]
            updated figure object
        ax: [matplotlib]
            updated axis object
    '''
    ## DEFINING DISTANCES
    distance = np.array(data[:,0])
    ## DEFINING PMF DATA
    pmf_data = np.array(data[:,1])
    
    ## SEEING IF YOU WANT TO TRUNCATE INFORMATION
    if end_truncate_dist > 0:
        ## FINDING INDEXES TO TRUNCATE
        truncate_idx = truncate_by_maximum( array = distance, truncation_value = end_truncate_dist )
        ## REDEFINING DISTANCES AND PMF DATA
        distance = distance[truncate_idx]
        pmf_data = pmf_data[truncate_idx]
    
    ## CORRECTING PMF DATA
    if want_correction is True:
        ## PRINTING
        print("\n----------------------------------")
        print("Correction for entropic effects is turned on!")
        print("Using temperature: %d K"%(temp))
        ## COMPUTING THE CORRECTION TERM
        correction_pmf = pmf_correction_entropic_effects( r = distance, temp = temp, )
        ## CORRECTING FOR THE DATA
        pmf_data -= correction_pmf
                
    ## DEFINING PMF DATA NORMALIZED
    if want_norm == True:
        pmf_data = pmf_data - np.mean( pmf_data[-10:] )
        
    ## SMOOTHING THE CURVE OUT IF YOU WANT
    if want_smooth_savgol_filter is True:
        print("\n----------------------------------")
        print("Filtering using savgol_filter on!")
        print("Parameters are shown below: ")
        print(savgol_filter_params)
        ## RUNNING FILTERING METHODS
        pmf_data_filtered = signal.savgol_filter(x=pmf_data, **savgol_filter_params)
        
    ## PLOTTING
    if fig == None or ax == None:
        plt.rc( 'font', **{ 'size' : 14 } )
        fig, ax = create_fig_based_on_cm( fig_size_cm = fig_size_cm )
        ax.set_xlabel( 'Distance (nm)' )
        ax.set_ylabel( r'$\Delta$G (%s)'%(units) )
        
    ## PLOTTING Y = 0 LINE
    if want_zero_line is True:
        ax.axhline(y = 0, color ='k', linestyle = '--', linewidth=1.5)
    ## PLOTTING
    ax.plot( distance, pmf_data, '-', color = color, linewidth = 2.0, label = label )
    
    ## SETTING X LIMITS
    # ax.set_xlim([np.min(distance), np.max( distance ) ])                   

    ## ADDING OTHER PLOTS
    if want_smooth_savgol_filter is True:
        ax.plot( distance, pmf_data_filtered, '-', color = 'r', linewidth = 1.5, label = 'filtered' )
    
    ## SETTING X RANGE
    if data_range is not None:
        ax.set_xticks( data_range )
    
    ## SETTING Y TICKS
    if ylim != None:
        ## DEFINING Y-TICK RANGE
        y_range = np.arange( ylim[0], ylim[1] + ylim[2], ylim[2] )
        ax.set_yticks( y_range ) 
    
    ## SETTING TIGHT LAYOUT
    fig.tight_layout()
    
    if save_fig == True:
        fig.savefig( output_fig_name, format='png', dpi=1000, bbox_inches='tight' )
    return fig, ax, distance, pmf_data

### FUNCTION TO PLOT THE SAMPLING TIME DATA
def plot_sampling_time_pmf( path_to_analysis_directory, 
                           sampling_time_directory,
                           units = 'kJ', 
                           sampling_time_prefix="profile", 
                           output_fig_name ='us_sampling_time.png', 
                           save_fig = False,
                           want_data = False,
                           **args):
    '''
    The purpose of this function is to plot the sampling time of the PMF 
    INPUTS:
        path_to_analysis_directory: [str]
            path to analysis directory
        sampling_time_directory: [str]
            sampling time folder name
        sampling_time_prefix: [str, default="profile"]
            sampling time prefix
        units: [str, default=kJ]
            units of the profile
        save_fig: [logical, default=False]
            True if you want to save the figure
        want_data: [logical]
            True if you want sampling data output
        **args:
            arguments for plot umbrella sampling
    OUTPUTS:
        figure with the sampling time
    '''
    ## FINDING ALL POSSIBLE FILES
    sampling_time_paths=glob.glob( path_to_analysis_directory + '/' + sampling_time_directory + '/' + sampling_time_prefix + "*"   )
    
    ## REMOVING ALL DIRECTORY PATH NAMES
    sampling_time_directory_names = [ os.path.basename(each_path) for each_path in sampling_time_paths]
    
    ## REMOVING ALL EXTENSION
    sampling_time_directory_names_noext = [ os.path.splitext(each_path)[0] for each_path in sampling_time_directory_names]
    
    ## SPLITTING LINES
    sampling_time_directory_names_split = np.array([ each_name.split('_')[2] for each_name in sampling_time_directory_names_noext]).astype('int')
    
    ## SORTING THE TIMES
    sampling_time_sorted=np.argsort(sampling_time_directory_names_split)
    
    ## DEFINING NO FIGURE, AX
    fig, ax = None, None
    
    ## FINDING COLORS
    color_cmap=get_cmap(len(sampling_time_sorted))
    
    ## CREATING EMPTY LIST
    if want_data is True:
        data_storage = {}
    
    ## LOOPING THROUGH EACH
    for each_time in range(len(sampling_time_sorted)):
        ## DEFINING CURRENT TIME
        current_time_index = sampling_time_sorted[each_time]
        current_time_value = sampling_time_directory_names_split[current_time_index]
        ## DEFINING CURRENT PATH
        current_profile_path = sampling_time_paths[current_time_index]
        
        ## READING PROFILE
        profile_xvg_data = read_xvg(current_profile_path)
        
        ## DEFINING CURRENT COLOR (BLACK AS LAST)
        if each_time != len(sampling_time_sorted) - 1:
            current_color=color_cmap(each_time)
        else:
            current_color = 'k'
        
        ## PLOTTING FIGURE
        fig, ax, distance, pmf_data= plot_us_pmf( data = profile_xvg_data, 
                                                  units = units,
                                                  fig = fig, 
                                                  ax = ax, 
                                                  color = current_color, 
                                                  label=str(current_time_value), **args)
        
        ## APPENDING
        data_storage[str(current_time_value)]={
                'x': distance,
                'y': pmf_data,
                }

        
    if save_fig == True:
        print("EXPORTING TO: %s"%(output_fig_name) )
        fig.savefig( output_fig_name, format='png', dpi=1000, bbox_inches='tight' )
    if want_data is True:
        return fig, ax, data_storage
    else:
        return fig, ax 

### FUNCTION TO PLOT MULTIPLE PMFS
def plot_multiple_pmfs(parent_dir,
                       analysis_dir_dict,
                       analysis_dir_within_folder="5_analysis",
                       profile_xvg = "profile.xvg",
                       units = 'kT',
                       temperature = 300.00,
                       end_truncate_dist = 0.05,
                       fig = None,
                       ax = None,
                       **kwargs):
    '''
    The purpose of this function is to plot multiple PMFs
    INPUTS:
        parent_dir: [str]
            parent directory where analysis is stored
        analysis_dir_dict: [dict]
            dictionary of directory
        analysis_dir_within_folder: [str]
            analysis directory within simulation folder
    OUTPUTS:
        fig, ax:
            figure and axis objects
    '''

    ## LOOPING 
    for each_key in analysis_dir_dict:
        
        ## DEFINING ANALYSIS DIR
        analysis_dir = analysis_dir_dict[each_key]['dirname']
        ## DEFINING COLOR
        color = analysis_dir_dict[each_key]['color']
        
        ## DEFINING MAIN ANALYSIS DIR
        main_analysis_dir= analysis_dir_dict[each_key]['main_analysis_dir']
        if 'analysis_dir_within_folder' in analysis_dir_dict[each_key]:
            analysis_dir_within_folder = analysis_dir_dict[each_key]['analysis_dir_within_folder']
        else:
            analysis_dir_within_folder="5_analysis"
        
        ## DEFINING PATH TO ANALYSIS DIRECTORY  # UMB_300.00_6_nm_xylitol_100_WtPercWater_spce_Pure\5_analysis"
        path_to_analysis_directory=os.path.join(parent_dir,
                                                main_analysis_dir,
                                                analysis_dir,
                                                analysis_dir_within_folder)
        
        ## DEFINING FULL PATHS
        path_profile_xvg = os.path.join(path_to_analysis_directory,  profile_xvg)
        
        ## READING XVG FILES
        profile_xvg_data = read_xvg(path_profile_xvg)
        
        ## PLOTTING FIGURE
        fig, ax, distance, pmf_data = plot_us_pmf(profile_xvg_data, 
                                                  units = units, 
                                                  want_correction = False, 
                                                  temp = temperature,
                                                  end_truncate_dist = end_truncate_dist,
                                                  want_smooth_savgol_filter = False, 
                                                  savgol_filter_params={'window_length':101, 'polyorder': 0, 'mode': 'nearest'},
                                                  output_fig_name = None, 
                                                  color = color,
                                                  label = each_key,
                                                  fig = fig,
                                                  ax = ax,
                                                  save_fig = False,
                                                  **kwargs)
        
    
    ## ADDING LEGEND
    ax.legend()
    return fig, ax 

#%%
###############################################################################
### MAIN SCRIPT
###############################################################################
if __name__ == "__main__":
    
    ## SEEING IF TESTING IS TRUE
    testing = True

    ## CREATING VARIABLES IF YOU ARE TESTING THIS SCRIPT
    if testing == True:
        
        ## DEFINING PARENT DIRECTORY
        parent_dir=r"R:\scratch\nanoparticle_project\simulations"
        ## DEFINING MAIN DIRECTORY FOR ANALYSIS
        # main_analysis_dir="180918-Umbrella_sampling_sims_debug_15000"
        main_analysis_dir="20200120-US-sims_NPLM_rerun_stampede"
        analysis_dir=r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
#        analysis_dir=r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
        
        ## REVERSE SIMULATIONS
        main_analysis_dir = r"20200124-US-sims_NPLM_reverse_stampede"
        analysis_dir = r"USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"
        # analysis_dir=r"UMB_433.15_6_nm_12-propanediol_100_WtPercWater_spce_Pure"
        
        # analysis_dir=r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
        
        ## DEFINING ANALYSIS DIR
        analysis_dir_within_folder="5_analysis"
        
        ## DEFINING PATH TO ANALYSIS DIRECTORY  # UMB_300.00_6_nm_xylitol_100_WtPercWater_spce_Pure\5_analysis"
        path_to_analysis_directory=os.path.join(parent_dir,
                                                main_analysis_dir,
                                                analysis_dir,
                                                analysis_dir_within_folder)
        
        ## DEFINING TEMPERATURE
        temperature = 300.00
        
        ## DEFINING DIRECTOYR FOR SAMPLING TIME
        sampling_time_directory="sampling_time"
        sampling_time_prefix="profile"
        ## DEFINING OUTPUT XVG FILE
        histo_xvg       =   "histo.xvg"
        profile_xvg     =   "profile.xvg"
        ## TRUNCATION DISTANCE
        end_truncate_dist = 0.05
        ## DEFINING UNITS
        units           =   "kJ/mol"
        ## SAVING FIGURE
        save_fig        =   False
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        
    ## DEFINING STORE FIG LOCATION
    STORE_FIG_LOC = r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Research_Presentations\20200206-Pedersen_VL_Meeting\images"
    SAVE_FIG = True

    
    # plt.close('all')
    ## DEFINING FULL PATHS
    path_histo_xvg = os.path.join(path_to_analysis_directory, histo_xvg)
    path_profile_xvg = os.path.join(path_to_analysis_directory,  profile_xvg)
    
    ## READING XVG FILES
    profile_xvg_data = read_xvg(path_profile_xvg)
    histo_xvg_data = read_xvg(path_histo_xvg)
    
    ##########################
    ### PLOTTING HISTOGRAM ###
    ##########################
    ## DEFINING FIGURE NAME
    figure_name = analysis_dir + '_histo' 
    
    ## PLOTTING UMBRELLA SAMPLING RANGE
    fig, ax = plot_us_histogram(histo_xvg_data, 
                                  save_fig = False, 
                                  data_range = np.arange(0.0,8.0,0.5),
                                  output_fig_name = figure_name,
                                  fig_size_cm = FIGURE_SIZE)
    ## STORING FIUURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(STORE_FIG_LOC,
                                                 figure_name),
                             save_fig = SAVE_FIG,
                             )
    
    ####################
    ### PLOTTING PMF ###
    ####################
    
    ## PLOTTING PROFILE
    figure_name = analysis_dir + '_pmf' 
    fig, ax, distance, pmf_data = plot_us_pmf(profile_xvg_data, 
                                              units = units, 
                                              ylim= (-100, 900, 200) , # None,    (-1000, 100, 100)
                                              data_range = np.arange(1.0,7.0,1),
                                              want_correction = False, 
                                              temp = temperature,
                                              end_truncate_dist = end_truncate_dist,
                                              want_smooth_savgol_filter = False, 
                                              savgol_filter_params={'window_length':101, 'polyorder': 0, 'mode': 'nearest'},
                                              output_fig_name = figure_name, 
                                              save_fig = False)
    
    ## SAVING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(STORE_FIG_LOC,
                                                 figure_name),
                             save_fig = SAVE_FIG,
                             )
    
    #%%
    
    ## PLOTTING PROFILE
    figure_name = analysis_dir + '_samplingtime' 
    
    ## PLOTTING SAMPLING TIME
    fig, ax = plot_sampling_time_pmf(path_to_analysis_directory, 
                                     sampling_time_directory, 
                                     units = units, 
                                     sampling_time_prefix="profile", 
                                     save_fig = False)
    
    ## CREATING LEGEND
    # ax.legend(loc = 'right')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    
    ## TIGHT LAYOUT
    fig.tight_layout()
    ## ADDING LEGEND
    # ax.legend()
    
    ## SAVING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(STORE_FIG_LOC,
                                                 figure_name),
                             save_fig = SAVE_FIG,
                             )

    #%% COMBINING MULTIPLE PMFS
    
    ## PLOTTING MULTIPLE TOGETHER
    analysis_dir_dict = {
            'C1': 
                {'main_analysis_dir': "20200120-US-sims_NPLM_rerun_stampede",
                 'dirname': r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1",
                 'color' : 'r',
                 },
            'C10': 
                {'main_analysis_dir': "20200120-US-sims_NPLM_rerun_stampede",
                 'dirname': r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1",
                 'color': 'k',
                 },
            }
    
    ## DEFINING COLOR
    fig, ax = plot_multiple_pmfs(parent_dir = parent_dir,
                                   analysis_dir_dict = analysis_dir_dict,
                                   analysis_dir_within_folder="5_analysis",
                                   ylim= (-100, 900, 200), # None,    (-1000, 100, 100)
                                   data_range = np.arange(1.0,7.0,1),)
    
    ## FIGURE NAME
    figure_name = "combined_pmf"
    
    ## SAVING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(STORE_FIG_LOC,
                                                 figure_name),
                             save_fig = SAVE_FIG,
                             )
    
    #%% REVERSE PMF
    
    ## PLOTTING MULTIPLE TOGETHER
    analysis_dir_dict = {
            'C10_forward': 
                {'main_analysis_dir': "20200120-US-sims_NPLM_rerun_stampede",
                 'dirname': r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1",
                 'color': 'k',
                 },
            'C10_reverse': 
                {'main_analysis_dir': "20200124-US-sims_NPLM_reverse_stampede",
                 'dirname': r"USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1",
                 'color': 'b',
                 },
            'C10_combined': 
                {'main_analysis_dir': "20200120-US-sims_NPLM_rerun_stampede",
                 'dirname': r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1",
                 'color': 'r',
                 'analysis_dir_within_folder': '5_analysis_combined'
                 },
            }
    
    ## DEFINING COLOR
    fig, ax = plot_multiple_pmfs(parent_dir = parent_dir,
                                   analysis_dir_dict = analysis_dir_dict,
                                   analysis_dir_within_folder="5_analysis",
                                   ylim= (-100, 900, 200),
                                   data_range = np.arange(1.0,7.0,1),)
    
    ## FIGURE NAME
    figure_name = "combined_pmf_with_reverse"
    
    ## SAVING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(STORE_FIG_LOC,
                                                 figure_name),
                             save_fig = SAVE_FIG,
                             )
    
    
    
    #%%
    
    
    
    # analysis_dir=r"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

    
    
    

    #%%
    
    ''' PLOTTING ENTROPIC CORRECTIONS
    r = np.arange(0.3,3,0.1)
    pmf = pmf_correction_entropic_effects( r )
    
    ## CREATING FIGURE
    fig, ax = create_plot()
    ax.plot( r, pmf, '-', color = 'k', linewidth = 1.5 )
    ax.set_xlabel( 'Distance (nm)' )
    ax.set_ylabel( r'$\Delta$G (%s)'%(units) )
    '''
    #%%


    
    
        
        
        
        
    