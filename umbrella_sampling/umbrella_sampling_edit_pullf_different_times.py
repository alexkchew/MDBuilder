# -*- coding: utf-8 -*-
"""
umbrella_sampling_edit_pullf_different_times.py
The purpose of this function is to edit the pullf file for umbrella sampling 
simulation so you could easily 

CREATED BY: Alex K. Chew (alexkchew@gmail.com)

CREATED ON: 02/03/2020

"""

import numpy as np
import os
import decimal
import glob

## FROM MDBUILDERS
from MDBuilder.core.import_tools import load_and_clean_text_file
from MDBuilder.core.check_tools import check_testing

## FROM MDDESCRIPTORS
from MDDescriptors.core.calc_tools import split_list

## COPYING FILE FUNCTION
from shutil import copyfile

##########################################
### CLASS FUNCTION TO LOAD PULLF FILES ###
##########################################
class load_gmx_pullf_file:
    '''
    The purpose of this function is to load a pullf file. 
    INPUTS:
        data_path: [str]
            path to the data
            
    OUTPUTS:
        self.data_path: [str]
            location of the data file
        self.lines: [list]
            list of the lines
        self.header_lines: [list]
            list of the header lines
        self.header_last_index: [int]
            last index for the header group
        self.data: [np.array]
            data array
    '''
    ## INITIALIZING
    def __init__(self,
                 data_path,):
        ## STORING DATA PATH
        self.data_path = data_path
        
        ## LOADING DATA FILE
        self.lines, self.header_lines, self.header_last_index = self.load_data_file(data_path = self.data_path)
        
        ## LOADING ALL DATA
        self.data, self.data_lines = self.extract_data(lines = self.lines,
                                                       header_last_index = self.header_last_index)
        
        ## GETTING NUMBER OF DECIMAL PLACES
        self.time_decimal = abs(decimal.Decimal(self.data_lines[0].split()[0]).as_tuple().exponent) # Returns 4 for 4 decimal places
        self.force_decimal = abs(decimal.Decimal(self.data_lines[0].split()[1]).as_tuple().exponent) # Returns 4 for 4 decimal places
        
        return
    
    ### FUNCTION TO LOAD A SINGLE DATA FILE
    @staticmethod
    def load_data_file(data_path):
        ''' This function loads a single data file '''
        ## LOADING XVG FILE
        lines = load_and_clean_text_file(text_file_path = data_path)

        ## DEFINING HEADER LINES
        header_lines = [ each_line for each_line in lines if each_line[0] == '#' or each_line[0] == '@' ]
        header_last_index =[i for i, j in enumerate(header_lines) if '@' in j][-1] 
        
        return lines, header_lines, header_last_index
    
    ### FUNCTION TO LOAD ALL THE DATA
    @staticmethod
    def extract_data(lines, header_last_index):
        '''
        The purpose of this function is to extract the data from the pullf file
        INPUTS:
            lines: [list]
                list of lines for your data
            header_last_index: [int]
                header last index to start from.
        OUTPUTS:
            data: [np.array]
                data information
            data_lines: [lines]
                data lines
        '''
        ## DEFINING DATA LINES
        data_lines = lines[header_last_index+1:] # Plus one to go after heading ended
        data = np.array([ x.split() for x in data_lines ]).astype('float')
        
        return data, data_lines

##################################
### CLASS METHOD TO EDIT PULLF ###
##################################
class edit_pullf_file:
    '''
    The purpose of this function is to edit the pullf file so that you could 
    get the same sampling time for an umbrella sampling script. The whole goal
    is that some windows may be longer in terms of simulation time. Therefore, 
    we need a script that could get the last N frames from each window, then 
    perform GMX wham on the last N frames. 
    INPUTS:
        parent_path: [str]
            parent path where you have your data files
        data_file: [str]
            file that contains the list of pullf files that you want to fix
        total_time: [int, default = -1]
            total time desired. If total_time is -1, then it will take all possible 
            time from the pullf file. 
        skip: [int, default = 1]
            total frames to skip. This flag is useful when you are trying to 
            figure out if you are sampling too frequently. If so, use the skip 
            to downsample your umbrella sampling windows and see if it effects 
            your convergence. 
    OUTPUTS:
        self.parent_path: [str]
            parent path string
        self.data_file: [str]
            data file string
        self.total_time: [float]
            total time that is desired in ps. This is computed based on the 
            last frame. Therefore, if this is 50000.000, then it will take the 
            last 50 ns of the pullf file.
    FUNCTIONS:
        find_all_data_files: 
            locates all data files
        get_indices_to_correct:
            function to load the data file and see if corrections are necessary
        update_pullf_file:
            updates the pullf file
        update_all_pullf_files:
            loops through all pullf files and corrects them
    '''
    def __init__(self,
                 parent_path,
                 data_file,
                 total_time = -1,
                 skip = 1):
        ## STORING INPUTS
        self.parent_path = parent_path
        self.data_file = data_file
        self.total_time = total_time
        self.skip = skip
        
        ## FINDING ALL DATA FILES
        self.find_all_data_files()
        
        return
    
    ### GETTING TOTAL LIST
    def find_all_data_files(self):
        '''
        This function gets all the data files. 
        INPUTS:
            self: [obj]
                class object
        OUTPUTS:
            self.path_data_files: [list]
                list of paths to the data files
        '''
        ## LOADING ALL THE LINES
        lines = load_and_clean_text_file(text_file_path = os.path.join(self.parent_path,
                                                                       self.data_file))
        
        ## GETTING ALL THE DATA FILES
        self.path_data_files = [ os.path.join(self.parent_path,
                                              each_line) for each_line in lines]
        
        return
    
    ### FUNCTION TO GET INDICES TO CORRECT FOR DATA FILE
    def get_indices_to_correct(self, 
                               path_data_file, 
                               verbose = True):
        '''
        The purpose of this function is to correct a single data file. 
        INPUTS:
            self: [obj]
                class object
            path_data_file: [str]
                path to the data file
            verbose: [logical]
                True if you want to print information
        OUTPUTS:
            indexes: [list, shape = 2]
                min and max indices
            pullf_file: [obj]
                pullf object
            need_corrections: [logical]
                True or False if you need a correction
        '''
        ## LOADING THE DATA FILE
        pullf_file = load_gmx_pullf_file(data_path = path_data_file)
        
        ## GETTING BASENAME
        pullf_file_name = os.path.basename(pullf_file.data_path)
        
        ## FINDING INITIAL AND FINAL TIME
        initial_time = pullf_file.data[0][0]
        final_time = pullf_file.data[-1][0]
        
        ## GETTING TOTAL TIME
        pullf_total_time = final_time - initial_time
        
        ## DEFINING DESIRED TIME
        if self.total_time < 0:
            desired_total_time = pullf_total_time
        else:
            desired_total_time = self.total_time
        
        ## DEFINING TOTAL FRMES
        pullf_file_data_total_frames = len(pullf_file.data)
        
        ## CHECKING TO SEE IF THE PULLF TIME IS EQUAL
        if desired_total_time != pullf_total_time:
            ## GETTING LINE INDEXES
            locate_time = pullf_total_time - desired_total_time
            ## FINDING INDEX OF CLOSEST TIME
            idx_time = np.argmin( np.sqrt( (pullf_file.data[:,0] - locate_time)**2 ) )
            
            ## DEFINING NEED CORRECTION LOGICAL
            need_corrections = True
            
            ## PRINTING
            if verbose is True:
                print("%s: Desired time is: %.3f ps --> CORRECTING with index %d to %d"%(pullf_file_name, desired_total_time, idx_time, pullf_file_data_total_frames)  )
        else:
            idx_time = 0
            need_corrections = False
            if verbose is True:
                print("%s: Desired time is: %.3f ps --> NO CHANGES NECESSARY "%(pullf_file_name, desired_total_time)  )
        ## GETTING INDEXES
        indexes = [idx_time, pullf_file_data_total_frames]
        
        return indexes, pullf_file, need_corrections
    
    ### FUNCTION TO UPDATE ALL FILES
    def update_all_pullf_files(self, **kwargs):
        '''
        The purpose of this function is to update all pullf files using the 
        update_pullf_file function
        INPUTS:
            **kwargs: 
                inputs for the pullf file script
        OUTPUTS:
            Each script will be updated based on the total time. The idea 
            is to output pullf files with the last N frames. 
        '''
        ## LOOPING
        for each_path in self.path_data_files:
            ## UPDATNG EACH PATH
            self.update_pullf_file(path_data_file = each_path,
                                   **kwargs)
        return
    
    
    ### FUNCTION TO EDIT SINGLE FILE
    def update_pullf_file(self, 
                          path_data_file,
                          verbose = True,
                          backup_file = True,
                          ):
        '''
        The purpose of this function is to update a pullf file.
        INPUTS:
            path_data_file: [str]
                path to data file
            verbose: [logical]
                True if you want to print out details
            backup_file: [logical]
                True if you want to make a copy of the pullf file
        '''
        ## DEFINING BACKUP FILE
        path_backup = os.path.splitext(path_data_file)[0] + "_backup.xvg"
        
        ## DEFINING EDITING FILE
        editing_file = path_data_file
        
        ## CHECKING IF BACK UP IS AVAILABLE
        if backup_file is True and os.path.isfile(path_backup) is True:
            ## CHANGING EDITING FILE TO BACKUP FILE
            editing_file = path_backup
        
        ## GETTING CORRECTION INDICES
        indexes, pullf_file, need_corrections = self.get_indices_to_correct(path_data_file = editing_file, 
                                                                            verbose = verbose)
        ## DEFINING CURRENT DATA
        data_list = pullf_file.data[indexes[0]:indexes[1]:self.skip]
        
        ## RENUMBERING BY THE FIRST TIME
        data_list[:,0] -= data_list[0,0]
        
        ## CORRECTING IF NECESSARY
        if need_corrections is True:    
        
            ## DEFINING BACKUP FILE
            if backup_file is True:
                if verbose is True:
                    print("Backing up the file!")
                    print("Back up file: %s"%(path_backup))
                if os.path.isfile(path_backup) is False:
                    ## COPYING FILE
                    copyfile(path_data_file, path_backup)
            ## PRINTING
            if verbose is True:
                print("Correcting file: %s"%( os.path.basename(path_data_file)) )
            ## CREATING A FILE
            with open(path_data_file, 'w' ) as f: # For reading and writing
                ## WRITING HEADER LINES
                for each_header_line in pullf_file.header_lines:
                    f.write("%s\n"%(each_header_line))
                ## WRITING ALL THE DATA
                for idx, current_data in enumerate(data_list):
                    ## PRINTING EACH LINE)
                    current_time = format(current_data[0],
                                          '.' + str(pullf_file.time_decimal) + 'f'  ) 
                    current_force = format(current_data[1],
                                          '.' + str(pullf_file.force_decimal) + 'f'  ) 
                    
                    ## JOINING LINES
                    joined_lines = ' '.join([current_time, current_force])
                    ## PRINTING
                    f.write("%s\n"%(joined_lines))

        return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
        ## DEFINING INPUTS
        total_time = 50000.000 # total time in ps from the last frame
        
        ## DEFINING PARENT PATH
        parent_path = "/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200120-US-sims_NPLM_rerun_stampede/US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/5_analysis"
        # r"R:\scratch\nanoparticle_project\simulations\20200120-US-sims_NPLM_rerun_stampede\US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1\5_analysis"
        
        ## DEFINING DATA FILE
        data_file = r"pullf_files.dat"
        
        ## DEFINING SKIP
        skip = 1
        
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## PATH INFORMATION
        parser.add_option('--parentpath', dest = 'parent_path', help = 'Full parent path to xvg files', default = '.')
        ## DEFINING DATA FILE
        parser.add_option('--data_file', dest = 'data_file', help = 'Data file containing list of xvgs', default = '.')
        ## DEFINING TOTAL TIME
        parser.add_option('--total_time', dest = 'total_time', type=float, help = 'Total time from the last frame in ps', default = 50000.000)
        ## DEFINING DATA FILE
        parser.add_option('--skip', dest = 'skip', type=int, help = 'Frames to skip', default = 1)
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ### DEFINING ARGUMENTS
        parent_path = options.parent_path
        data_file = options.data_file
        total_time = options.total_time
        skip = options.skip
        
    ## DEFINING INPUTS
    input_vars = {
            'parent_path' : parent_path,
            'data_file' : data_file,
            'total_time': total_time,
            'skip': skip,
            }
    
    ## RUNNING CLASS FUNCTION
    edited_pullf = edit_pullf_file(**input_vars)
    ## UPDATING ALL PULLF FILES
    edited_pullf.update_all_pullf_files(verbose = True,
                                        backup_file = True) # Turn backup on when debugging
    # False
    
    
        