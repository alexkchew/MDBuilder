# -*- coding: utf-8 -*-
"""
umbrella_sampling_get_desired_configs.py
The purpose of this function is to get desired configurations for umbrella 
sampling simulations. The inputs are the center-of-mass positions versus time, 
and the desired cnonfigurations

Written by: Alex K. Chew (01/15/2020)
"""
import numpy as np
import sys
import pandas as pd
import time
import os

## OPTION PARSER
from optparse import OptionParser # Used to allow commands within command line

## CHECKING PATH FOR TESTING
from MDBuilder.core.check_tools import check_testing 

## READING XVG TOOLS
from MDDescriptors.core.import_tools import read_xvg

## GETTING FUNCTION TO EXTRACT BOUNDS
from MDBuilder.umbrella_sampling.umbrella_sampling_select_configurations import find_windows_based_on_bounds

## IMPORTING PARSER
from MDBuilder.core.callback import interpret_options_comma

## IMPORTING DESCDRIPTORS READ COVAR
from MDDescriptors.core.import_tools import read_plumed_covar

#################################################
### CLASS FUNCTION TO GENERATE CONFIGURATIONS ###
#################################################
class extract_configs_from_summary:
    '''
    The purpose of this function is to extract configurations from summary file.
    INPUTS:
        path_summary: [str]
            path to summary file
        desired_increments: [np.array]
            increments for the configurations
        dist_dimension: [str]
            dimension to use from the summary file. It could be:
                'x' : x-dimension
                'y' : y-dimension
                'z' : z-dimension
                'first': same as x dimension, but for the first axis
        deviation_error: [float, default=0.1 nm]
            deviation between configuration increments and current available configuration before this function begins to complain
        verbose: [logical]
            True if you want to print verbosely
        want_reverse: [logical]
            True if you want to reverse the desired increments
        is_plumed: [logical]
            True if your summary file is a plumed COVAR file
        plumed_column: [str]
            column to use. If None, it will use the first column that is not the time.
        output_format: [str]
            output format for configs
    OUTPUTS:
        self.closest_configurations_pd: [pandas]
            closest configurations as a pandas list
    '''
    ## INITIALIZING
    def __init__(self,
                 path_summary,
                 desired_increments,
                 dist_dimension,
                 deviation_error=0.1,
                 verbose = True,
                 want_reverse = False,
                 is_plumed = False,
                 plumed_column = None,
                 output_format = ".3f",
                 ):
        ## STORING INPUTS
        self.path_summary = path_summary
        self.desired_increments = desired_increments
        self.dist_dimension = dist_dimension
        self.deviation_error = deviation_error
        self.verbose = verbose
        self.want_reverse = want_reverse
        self.output_format = output_format
        
        if self.want_reverse is True:
            if self.verbose is True:
                print("Since reverse is set, we are reversing the desired increment array")
            self.desired_increments = self.desired_increments[::-1]
        
        ## LOADING THE SUMMARY
        if is_plumed is False:
            self.summary_full, self.summary_extract = read_xvg(self.path_summary)
            ## CONVERTING THE SUMMARY EXTRACT TO ARRAY
            self.summary_extract_array = np.array(self.summary_extract).astype('float')
            ## GETTING DIMENSION INDEX
            self.dim_index = self.get_dim_index()
            ## DEFINING TIME ARRAY
            self.time_array = self.summary_extract_array[:,0]
            ## DEFINING DISTANCE ARRAY
            self.distance_array = self.summary_extract_array[:,self.dim_index]
        else:
            ## EXTRACTING THE FILE
            self.summary_extract = read_plumed_covar(self.path_summary)
            ## GETTING THE COORDINATE
            if plumed_column is None:
                self.dim_index = self.summary_extract.columns[1]
            else:
                self.dim_index = plumed_column
            ## DEFINING TIME ARRAY
            self.time_array = np.array(self.summary_extract['time'])
            ## DEFINING DISTANCE ARRAY
            self.distance_array = np.array(self.summary_extract[self.dim_index])

        ## GETTING CLOSEST CONFIGURATION
        self.find_closest_configurations(time_array = self.time_array,
                                         distance_array = self.distance_array)
        if self.verbose is True:
            print("Configurations ranging from %.2f to %.2f"%(np.min(self.desired_increments), np.max(self.desired_increments) ))
            print("Total configurations: %d"%(len(self.desired_increments) ) )
            
        return
    ### FUNCTION TO GET INDEX
    def get_dim_index(self):
        ''' Function that gets the dimension index that you want '''
        ## GETTING THE INDEX
        if self.dist_dimension == "x" or self.dist_dimension == "first":
            dim_index = 1
        elif self.dist_dimension == "y":
            dim_index = 2
        elif self.dist_dimension == "z":
            dim_index = 3
        else:
            print("Error! Dimension not found for: %s"%(dist_dimension) )
            print("Check the dimension index function: get_dim_index")
            print("This function looks for which index your distances are in from the summary file")
            sys.exit()
            
        ## PRINTING
        print("Getting dimensions for: %s"%(self.dist_dimension ))
        return dim_index 

    ### FUNCTION TO FIND THE CLOSEST CONFIGURATIONS
    def find_closest_configurations(self, time_array, distance_array):
        '''
        The purpose of this function is to find the closest configuration to the configuration increments
        INPUTS:
            self: [object]
                class object
            time_array: [np.array]
                time array information
            distance_array: [np.array]
                distance array information
                
        OUTPUTS:
            self.closest_configurations_pd: [panda dataframe]
                table consisting of all closest values, e.g.:
                       Desired_increment  Actual_increment  vector_index  Conf_index
                    0                0.2             0.200         208.0       208.0
                    1                0.3             0.300         182.0       182.0
                    2                0.4             0.401         160.0       160.0 ....
                    
                    NOTES:
                        - Desired_increment: indicates your desired increment
                        - Actual_increment: indicates the increment found from the summary file
                        - vector_index: indicates the index based on the python index
                        - Conf_index: configuration index (note, this may not be the same as vector index if it does not start at 0)
        '''
        ## CREATING STORAGE VECTOR
        self.closest_configurations_pd = pd.DataFrame( columns = ['Desired_increment', 
                                                                  'Actual_increment', 
                                                                  'Time_ps' ],
                                                       )
        ## LOOPING THROUGH EACH CONFIGURATION REQUIREMENT
        for each_config_value in self.desired_increments:
            ## MINIMUM INDEX
            idx = np.argmin( np.abs(distance_array - each_config_value) )
            ## GETTING TIME AND DISTANCE
            config_time = time_array[idx]
            config_distance = distance_array[idx]
            
            ## CHECKING
            if abs(config_distance - each_config_value) > self.deviation_error:
                print("Configurations that you selected may not be well-converged! We have found deviations greater than: %.2f"%( self.deviation_error ))
                print("Be careful! A poorly defined reaction coordinate may lead to issues. This increment is having issues: %.3f"%(each_config_value))
                print("Closest point was: %.3f"%(config_distance))
                time.sleep(5)
            
            ## APPENDING
            self.closest_configurations_pd = self.closest_configurations_pd.append(
                                                {'Desired_increment': self.output_format%(each_config_value),  
                                                 'Actual_increment' : self.output_format%(config_distance),
                                                 'Time_ps'     : config_time,  
                                                 } ,ignore_index = True)
    
        return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    ## TESTING IS ON
    if testing is True:
        ### INPUTS
        path_summary=r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200608-Pulling_with_hydrophobic_contacts/NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/" + \
                        "COVAR.dat"
        # r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200517-plumed_test_pulling_new_params_debugging_stride1000_6ns/NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/COVAR.dat"
        # r"R:/scratch/nanoparticle_project/simulations/20200113-NP_lipid_bilayer_resize/NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/pull_summary.xvg"

        ### OUTPUTS
        path_output_configs="/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/20200615-US_PLUMED_rerun_with_10_spring/UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1/2_configurations/config_summary.csv"
        
        ## DEFINING DESIRED DIMENSION
        dist_dimension="z"
        
        ## DEFINING BOUNDS
        upper_bound=5
        lower_bound=1.3
        increments=0.2
        
        ## CONFING CONFIGS
        configuration_increments=[10]
        
        ## DEFINING REVERSE
        want_reverse = True
        
        ## DEFINING COLUMN
        plumed_column = "coord"
        
        ## DEFINING IF PLUMED
        is_plumed = True
        
        ## DEFINING OUTPUT FORMAT
        output_format="%.1f"
        
    else:
        ### DEFINING PARSER OPTIONS
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## GETTING SUMMARY
        parser.add_option("--sum", dest="path_summary", action="store", type="string", help="Path to summary file", default=".")
        
        ## DEFINING DIMENSION
        parser.add_option("--dim", dest="dist_dimension", action="store", type="string", help="Dimension that you care about, could be x, y, z, or first", default="z")
        
        ## GETTING OUTPUT
        parser.add_option("--out", dest="path_output_configs", action="store", type="string", help="Path to output file", default=".")
        
        ## ADDING UMBRELLA SAMPLING INPUTS
        parser.add_option("--upper", dest="upper_bound", action="store", type=float, 
                          help="Upper configuration limit", default=5)
        parser.add_option("--lower", dest="lower_bound", action="store", type=float, 
                          help="Lower configuration limit", default=1)
        parser.add_option("--inc", dest="increments", action="store", type=float,
                          help="Increment configuration limit", default=0.1)
        
        ## GETTING SPECIFIC CONFIGURATIONS
        parser.add_option("--configs", dest="configuration_increments", action="callback", type="string", callback=interpret_options_comma,
                  help="Configurations, separated by comma separate each config by comma (no white space)", default=None)
        
        ## ADDING OPTION TO REVERSE THE TREND
        parser.add_option("--reverse", 
                          dest="want_reverse", 
                          action="store_true", 
                          help="If --reverse is set, then we reverse the increment configuration", 
                          default=False)
        
        ## GETTING OUTPUT
        parser.add_option("--is_plumed", 
                          dest="is_plumed", 
                          action="store_true",
                          help="True if your summary xvg is a plumed file", 
                          default=False)
        parser.add_option("--plumed_column", dest="plumed_column", action="store", type="string", help="Plumed column to look into", default=None)
        ## DEFINING DECIMAL FORMATS        
        parser.add_option("--oformat", dest="output_format", action="store", type="string", help="Format output for configs, e.g. %.2f means 2 decimal places", default="%.3f")
        
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## OUTPUTTING VARIABLES
        dist_dimension = options.dist_dimension
        path_summary = options.path_summary
        path_output_configs = options.path_output_configs
        ## DEFINING BOUNDS
        upper_bound  = options.upper_bound
        lower_bound = options.lower_bound
        increments = options.increments
        ## INCREMENTS
        configuration_increments = options.configuration_increments
        print(configuration_increments)
        ## DEFINING REVERSE
        want_reverse = options.want_reverse
        
        ## STORING FORMAT
        output_format = options.output_format
        
        ## PLUMED INPUTS
        is_plumed = options.is_plumed
        plumed_column = options.plumed_column
        
    ## GETTING CONFIGURATION INCREMENTS
    if configuration_increments == None:
        configuration_increments = find_windows_based_on_bounds(lower_limit_config = lower_bound, 
                                                                upper_limit_config = upper_bound, 
                                                                increment_config = increments  )
        
    else:
        ## CONVERTING CONFIGURATIONS TO FLOATING POINT NUMBERS
        configuration_increments = np.array(configuration_increments).astype('float')
        
    ## PRINTING
    print("Output format: %s"%(output_format))
    ## INPUTS
    summary_inputs={
            'path_summary': path_summary,
            'dist_dimension': dist_dimension,
            'desired_increments': configuration_increments,
            'want_reverse' : want_reverse,
            'is_plumed': is_plumed,
            'plumed_column': plumed_column,
            'output_format': output_format,
            }
    
    ## GETTING THE SUMMARY
    summary = extract_configs_from_summary(**summary_inputs)
    
    ## OUTPUTTING TO SUMMARY
    if os.path.isfile(path_output_configs) is True:
        print("Since output configurations exists, we are reading the file")
        print("Path: %s"%(path_output_configs) )
        ## READING FILE
        current_pd = pd.read_csv(path_output_configs)
        
        ## DEFINING CURRENT INCRMEENTS
        current_increments = np.array(current_pd['Desired_increment'])
        
        ## COPYING DATAFRAME
        new_pd = current_pd.copy()
        
        ## LOOPING THROUGH ROWS AND CHECKING IF CONFIGURATION IS WITHIN
        for idx, each_row in summary.closest_configurations_pd.iterrows():
            ## CHECKING FOR THE VALUE
            value = each_row['Desired_increment']
            ## ADDING IF NOT THERE
            logical = np.isin(value, current_increments)
            
            ## IF WITHIN, SKIPPING
            if logical:
                print("Skipping since already in dataframe: %s"%(value))
            else:
                print("Adding %s"%(value))
                new_pd = new_pd.append(each_row, 
                                       ignore_index = True,
                                       sort=False)
                
                ## ADDING TO INCREMENTS
                current_increments = np.append(current_increments, value)
        
        
#        new_pd = current_pd.append(summary.closest_configurations_pd, 
#                                   ignore_index = True,
#                                   sort=False)
        new_pd = new_pd.drop(columns = ["index"])
        ## DROPING ANY DUPLICATES (DEPRECIATED)
        #        new_pd = new_pd.drop_duplicates(subset = ['Desired_increment'])
        #        new_pd = new_pd.drop_duplicates(subset = ['Time_ps'])
        print("New panda dataframes")
        print(new_pd)
    else:
        print("Since the output configs do not exist, we are making new file!")
        print("Path: %s"%(path_output_configs) )
        print("Outputting summary file to: %s"%( path_output_configs ) )
        new_pd = summary.closest_configurations_pd
    
    ## ADDING TO CSV
    new_pd.to_csv(path_output_configs,
                  index_label = "index")

    