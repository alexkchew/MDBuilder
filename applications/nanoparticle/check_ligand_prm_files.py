#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
check_ligand_prm_files.py

This function checks ligand parameter files to ensure that the dihedral types 
are not duplicated. If they are, then we will comment out the dihedral types 
that have multiple consecutive blocks, e.g. 

[ dihedraltypes ]
;      i        j        k        l  func         phi0         kphi  mult
  CG3C51    SG301    SG301   CG3C52     9     0.000000     4.184000     1
  CG3C51    SG301    SG301   CG3C52     9     0.000000    17.154400     2
  CG3C51    SG301    SG301   CG3C52     9     0.000000     3.765600     3

If this type of dihedral is replicated elsewhere, you might have an issue with 
duplicated parameters. This will cause an error in Gromacs, so we will 
update any itp files that might have this issue. 

INPUTS:
    - directory location
    - prm files
OUTPUTS:
    - updated prm files
    - summary of changes
    - copy of original prm / itp file
ALGORITHM:
    - load each parameter file
    - 

Written by: Alex K. Chew (12/21/2020)


FUNCTIONS:
    find_prm_types:
        finds parameter information within parameter types

"""
import os
import numpy as np
from optparse import OptionParser # Used to allow commands within command line

## CHECKING TOOLS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
from MDBuilder.core.import_tools import load_and_clean_text_file
from MDBuilder.builder.ligand_builder import get_ligand_args

## FUNCTION TO LOAD DIHEDRAL TYPES
def find_prm_types(prm_data,
                   prm_type = '[ dihedraltypes ]'):
    '''
    This function loads all data according to a specific type given the 
    parameter data.
    INPUTS:
        prm_data: [list]
            list of data from parameter file
        prm_type: [str]
            parameter type to locate. Examples are:
                [ bondtypes ]
                [ angletypes ]
                [ dihedraltypes ]    
    OUTPUTS:
        storage_lines: [list]
            list of lines that match your parameters. Note that comments 
            will be removed as well as empty spaces
    '''
    ## STORING LINES
    storage_lines = []
    
    ## DEFINING STORAGE
    begin_storing = False
    
    ## LOOPING THROUGH THE DATA
    for idx, each_line in enumerate(prm_data):
        ## SEEING IF DIHEDRAL TYPES IS NOT AVAILABLE
        if each_line.startswith('[ ') and prm_type not in each_line:
            begin_storing = False
        
        ## SEEING IF DIHEDRAL TYPES IS AVAILABLE
        if prm_type in each_line:
            begin_storing = True
        
        ## SEEING IF STORAGE IS TRUE
        if begin_storing is True:
            ## AVOIDING ALL COMMENTS
            if each_line.startswith(';') is False and each_line != '' and prm_type not in each_line:
                storage_lines.append([idx, each_line.split()])

    return storage_lines

### FUNCTION TO LOCATE ALL ATOMNAMES
def identify_replicate_dihedral_types(storage_lines):
    '''
    This function identifies all identical dihedral types
    INPUTS:
        storage_lines: [list]
            list of storage lines as index, lines (splitted)
    OUTPUTS:
        lines_matched: [list]
            list of lines that matched in terms of atom names
    '''
    ## GETTING ALL LINES
    data_lines = [each_line[1] for each_line in storage_lines]
    
    ## GETTING NUMPY ARRAY
    data_array = np.array(data_lines)
    
    ## GETTING ALL ATOM NAMES
    atom_names = data_array[:, 0:4]
    
    ## GETTING ALL UNIQUE NAMES
    unique_atomnames, indices, counts = np.unique(atom_names, 
                                                  axis = 0, 
                                                  return_index = True,
                                                  return_counts = True)
    
    ## GETTING INDEX WHERE IT'S GREATER THAN 1
    counts_greater_than_one = np.where(counts>1)[0]
    
    ## GETTING ATOM LIST
    atoms_duplicated = unique_atomnames[counts_greater_than_one]
    
    ## GETTING INDEX LINES THAT MATCHES
    idx_duplicated = [ [idx for idx, each_array in enumerate(atom_names) if np.array_equal(each_duplicate, each_array)] 
                        for each_duplicate in atoms_duplicated 
                           ]
    
    ## GETTING LINES
    lines_matched = [ [storage_lines[each_idx] for each_idx in dups] for dups in idx_duplicated]

    return lines_matched

### FUNCTION TO GET INDICES REQUIRED TO COMMENT
def find_indices_replicated_dihedrals(dihedrals_replicated_atomnames):
    '''
    This function finds all the indices that need to be commented out 
    due to replicates of the dihedral angles
    INPUTS:
        dihedrals_replicated_atomnames: [list]
            list of replicated dihedrals
    OUTPUTS:
        indices_required_to_comment_out: [list]
            list of indices to comment out from each prm file
    '''
    ## LOOPING THROUGH EACH LIST AND STORING AS NECESSARY
    stored_duplicates = []
    indices_required_to_comment_out = []
    
    ## LOOPING THROUGH EACH LIST
    for each_list in dihedrals_replicated_atomnames:
        
        ## DEFINING CURENT INDEXES
        current_indices_to_comment = []
        
        ## LOOPING THROUGH EACH DUPLICATE
        for each_duplicate in each_list:
            ## GETTING THE DATA
            index_list = [line[0] for line in each_duplicate]
            data_list = [line[1] for line in each_duplicate]
            
            ## CHECKING IF IS IN STORAGE
            if data_list not in stored_duplicates:
                stored_duplicates.append(data_list)
            else:
                ## IF IT IS IN STORAGE, THEN GET THE INDICES TO COMMENT OUT
                current_indices_to_comment.extend(index_list)
            
        ## AT THE END, STORE INDICES
        indices_required_to_comment_out.append(current_indices_to_comment)
    ## RETURNING
    return indices_required_to_comment_out

### FUNCTION TO RE-SAVE PRM FILES
def resave_prm_with_indices_commented_out(path_to_prm,
                                          prm_loaded_files,
                                          indices_required_to_comment_out,
                                          verbose = True):
    '''
    This function resaves prm files with specific indices commented out. 
    The commenting out is designed to ensure topology is not duplicated 
    in terms of dihedral angles. If so, then we will get errors in Gromacs 
    where all functionals are the same. Therefore, to remove duplicated parameters, 
    we will comment out the parameter file. 
    
    INPUTS:
        path_to_prm: [list]
            list of paths to parameter files
        prm_loaded_files: [list]
            list of data for each parameter file
        indices_required_to_comment_out: [list]
            list of indices required to be commented out
        verbose: [logical]
            True if you want to print out details
    OUTPUTS:
        
    '''
    ## LOOPING THROUGH EACH FILE
    for prm_idx, each_prm_file in enumerate(path_to_prm):
        ## SEEING IF INDICES IS GREATER THAN 0
        comment_out_indices = indices_required_to_comment_out[prm_idx]
        data = prm_loaded_files[prm_idx]
        ## WILL NEED TO UPDATE
        if len(comment_out_indices) > 0:
            print("*** Duplicate parameters found for: %s"%(each_prm_file))
            print("Commenting out the indices: %s"%(', '.join([str(each_idx) for each_idx in comment_out_indices])))
            
            ## COMMENTING OUT DATA
            for each_idx in comment_out_indices:
                data[each_idx] = '; ' + data[each_idx] + '; Commented out due to duplicated parameters'
            
            ## RESAVING TO PARAMETER FILES
            with open(each_prm_file, 'w') as f:
                for each_line in data:
                    f.write("%s\n"%(each_line))
        else:
            print("--> No duplicate parameters found for: %s"%(each_prm_file))
        
        ## PRINTING
        print("")
        
    
    return

### MAIN FUNCTION TO RESAVE PARAMETERS
def main_resave_prms_without_duplicated_dihedraltypes(path_to_sim,
                                                      prm_files,
                                                      ):
    '''
    Main function that resave parameter files without duplicated dihedral types. 
    INPUTS:
        path_to_sim: [str]
            path to simulation file
        prm_files: [list]
            list of parameter files
    OUTPUTS:
        indices_required_to_comment_out: [list]
            list of indices to comment out for each parameter file
    '''

    ## RUNNING MAIN FUNCTION
    path_to_prm = [ os.path.join(path_to_sim, each_file) for each_file in prm_files]
    
    ## LOADING EACH FILE
    prm_loaded_files = [load_and_clean_text_file(each_path,
                                                 strip_lines = False,
                                                 strip_backlash_n = True) for each_path in path_to_prm ]
    
    ## GETTING PARAMETER DIHEDRALS
    dihedral_type_lines = [ find_prm_types(prm_data = each_file,
                                           prm_type = '[ dihedraltypes ]') for each_file in prm_loaded_files]
        
    ## GETTING DIHEDRALS THAT ARE MATCHING
    dihedrals_replicated_atomnames = [ identify_replicate_dihedral_types(each_dihedral_lines) 
                                            for each_dihedral_lines in dihedral_type_lines]
    ''' 
    # For debugging purposes, you could load a single parameter file
    # and analyze the results: 
    
    ## GETTING LINES
    storage_lines = find_prm_types(prm_data = prm_loaded_files[0],
                       prm_type = '[ dihedraltypes ]')
    
    ## GETTING LINES THAT MATCHED
    lines_matched = identify_replicate_dihedral_types(storage_lines)
    '''
    
    ## GETTING INDICES TO COMMENT OUT
    indices_required_to_comment_out = find_indices_replicated_dihedrals(dihedrals_replicated_atomnames)
    
    ## RESAVING PARAMETER FILE
    resave_prm_with_indices_commented_out(path_to_prm = path_to_prm,
                                          prm_loaded_files = prm_loaded_files,
                                          indices_required_to_comment_out = indices_required_to_comment_out,
                                          verbose = True)
    
    return indices_required_to_comment_out

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    
    ## TESTING IS ON
    if testing is True:
        
        ## DEFINING PATH TO DIRECTORY
        path_to_sim = "/Volumes/akchew/scratch/nanoparticle_project/simulations/20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed/spherical_300.00_K_GNP141_CHARMM36jul2020_Trial_1"
        
        ## DEFINING PARM FILES
        prm_files = [ 'LIG79.prm', 'LIG89.prm'] # , 
    
    else:
        ### DEFINING PARSER OPTIONS
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## SHAPE
        parser.add_option("--path", dest="path_to_sim", type="string",help="Path to simulation", default=".")
        ## LIGAND FRACTIONS
        parser.add_option("--prm_files", dest="prm_files", action="callback", type="string", callback=get_ligand_args,
                          help="Parameter files. For multiple parameter files, separate each ligand name by comma (no whitespace)")
    
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## LOADING PARAMETERS
        prm_files = options.prm_files
        path_to_sim = options.path_to_sim

    
    ## RUNNING MAIN FUNCTION
    indices_required_to_comment_out = main_resave_prms_without_duplicated_dihedraltypes(path_to_sim = path_to_sim,
                                                                                        prm_files = prm_files,
                                                                                        )