#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
get_ligand_indices.py
The purpose of this script is to locate all ligand indices relating to 
sulfur atoms and the end group atoms. 


Written by: Alex K. Chew (11/09/2020)

FUNCTIONS:
    identify_sulfur_head_groups: 
        function that identifies sulfur head groups
    get_furthest_atom_dict:
        function that finds the atom dictionary that is furthest from the sulfur
    main_generate_single_ligand_index:
        function that generates ligand index for pulling simulations

"""
import os
import numpy as np
from optparse import OptionParser # Used to allow commands within command line

## CHECKING TOOLS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
## IMPORTING CALC TOOLS
import MDDescriptors.core.import_tools as import_tools # Loading trajectory details
import MDDescriptors.core.read_write_tools as read_write_tools # Reading itp file tool

## IMPORTING FUNCTIONS
from MDBuilder.applications.nanoparticle.get_nanoparticles_indices import find_bonding_from_initial

### IDENTIFYING SULFUR HEAD GROUPS
def identify_sulfur_head_groups(traj_data,
                                itp_info):
    '''
    This function identifies sulfur head groups. 
    INPUTS:
        traj_data: [obj]
            trajectory object
        itp_info: [obj]
            itp information
    OUTPUTS:
        output_sulfur_indices: [list]
            list of output sulfur indices
    '''
    ## GETTING ALL SULFUR INDICES
    all_sulfur_indices = [atom.index for atom in traj_data.topology.atoms if atom.element.symbol == 'S']
    
    ## IF INDICES IS 1, THEN THAT'S YOUR INDEX. OTHERWISE, WE WILL USE BONDING INFO. 
    if len(all_sulfur_indices) == 1:
        output_sulfur_indices = all_sulfur_indices
    elif len(all_sulfur_indices) == 0:
        print("Error! No sulfurs found. Check your ligand structure!")
    else:
        ## IDENTIFYING SULFUR HEAD GROUPS THAT ARE BOUND TOGETHER
        # DEFINING BONDING INFORMATION
        bonds = itp_info.bonds - 1 # Subtracting 1 to match python
        
        ## GETTING SULFUR INDEX
        output_sulfur_indices = []
        
        ## FINDING ALL BONDS
        for each_index in all_sulfur_indices:
            ## FINDING ALL CURRENT BONDS
            current_bonds = bonds[np.where(bonds == each_index)[0]]
            ## FLATTENING AND REMOVING CURRENT ATOM INDEX
            current_bonds = current_bonds.flatten()
            current_atoms_bonded = current_bonds[current_bonds!=each_index]
            ## FINDING ALL SULFURS
            current_atoms_bonded_sulfur = [each_atom for each_atom in current_atoms_bonded if each_atom in all_sulfur_indices]

            ## SEEING IF CURRENT ATOMS ARE WITHIN SULFUR INDEX
            is_sulfur_bonded = np.any(np.isin(current_atoms_bonded_sulfur, all_sulfur_indices))
            if is_sulfur_bonded == True:
                ## DEFINING ATOM INDICES
                include_atom_indices = [ each_index ] + list(current_atoms_bonded_sulfur)
                ## STORING
                for included_atom in include_atom_indices:
                    if included_atom not in output_sulfur_indices:
                        output_sulfur_indices.append(included_atom)
        ## IF NO SULFUR INDICES, THEN SET TO INITIAL SULFUR INDICES
        if len(output_sulfur_indices) == 0:
            ## SEEINING IF THERE IS AN S1
            output_sulfur_indices = [atom.index for atom in traj_data.topology.atoms if atom.name == 'S1']
            ## IF LENGTH IS STILL 0, JUST USE FIRST INDEX
            if len(output_sulfur_indices) == 0:
                output_sulfur_indices = all_sulfur_indices[0]
    return output_sulfur_indices

### FUNCTION TO GET FURTHEST ATOM DICTIONARY
def get_furthest_atom_dict(traj_data,
                           itp_info,
                           output_sulfur_indices):
    '''
    This function finds the furthest heavy atom from the sulfur positions. 
    INPUTS:
        traj_data: [obj]
            trajectory data object
        itp_info: [obj]
            itp information of ligands
        output_sulfur_indices: [list]
            list of sulfur indices
    OUTPUTS:
        furthest_atom_dict: [dict]
            dictionary containing atomname and bond_length of the atom 
            furthestly bond to sulfur
    '''

    ## GETTING ATOM NAME OF FIRST
    sulfur_name_first_atom = traj_data.topology.atom(output_sulfur_indices[0]).name
    
    ## GETTING BOND ORDER LIST
    bond_order_list = find_bonding_from_initial( atom_names = itp_info.atom_atomname, 
                                   bonds = itp_info.bonds,
                                   initial_atom_name = sulfur_name_first_atom)
    
    ## GETTING ONLY HEAVY ATOM
    bond_order_list_heavy_atoms = [ bond_list for bond_list in bond_order_list if 'H' not in bond_list[0]]
    
    ## LOCATING MAXIMA
    maxima_details = bond_order_list_heavy_atoms[np.argmax(np.array(bond_order_list_heavy_atoms)[:,1].astype('int'))]
    
    ## GETTING ATOM INDICES
    atomname = maxima_details[0]
    indices_furthest = [each_atom.index for each_atom in traj_data.topology.atoms if each_atom.name == atomname]
    
    ## APPENDING
    furthest_atom_dict = {
            'atomname': atomname,
            'bond_length': maxima_details[1],
            'indices': indices_furthest,
            }
    
    return furthest_atom_dict

### MAIN FUNCTION TO GETNERATE SINGLE LIGAND INDEX
def main_generate_single_ligand_index(path_gro,
                                      path_itp,
                                      path_ndx,):
    '''
    This function generates a single index for ligand, such as the sulfur 
    indices that are bound and the atom that is furthest away from 
    the sulfur. This function is designed to get indices such that we 
    could the end-end ligand indices, which is useful for pulling ligands 
    apart in an all-trans configuration.
    INPUTS:
        path_gro: [str]
            gro file with a single ligand
        path_itp: [str]
            path to itp file for the ligand containing bonding information
        path_ndx: [str]
            path to index file
        
    OUTPUTS:
        output_sulfur_indices: [list]
            list of sulfur indices to output
        furthest_atom_dict: [dict]
            dictionary containing atom indices of furthest atom
    '''
    ## DEFINING INPUT PATH
    input_path = os.path.dirname(path_gro)
    input_gro = os.path.basename(path_gro)
    

    ### LOADING TRAJECTORY
    traj_data = import_tools.import_traj( directory = input_path, # Directory to analysis
                 structure_file = input_gro, # structure file
                 xtc_file = input_gro, # trajectories
                 )
    
    ## LOADING ITP OF LIGAND
    itp_info = read_write_tools.extract_itp(path_itp)
    
    ## GETTING SULFUR INDICES
    output_sulfur_indices = identify_sulfur_head_groups(traj_data = traj_data,
                                                        itp_info = itp_info)
            
    ## GETTING FURTHEST ATOM INFORMATION
    furthest_atom_dict = get_furthest_atom_dict(traj_data = traj_data,
                                                itp_info = itp_info,
                                                output_sulfur_indices = output_sulfur_indices)
    
    ## OUTPUTTING TO INDEX FILE
    output_ndx_dict = {
            'BOUND_SULFURS': output_sulfur_indices,
            'END_GROUP_ATOM': furthest_atom_dict['indices']
            }
    
    ## LOOPING AND OUTPUTTING
    ## DEFINING INDEX OBJECT
    idx_obj = read_write_tools.import_index_file(path_to_index = path_ndx)
    
    ## READING GROUPS
    idx_obj.split_to_groups()
    
    ## LOOPING
    for idx_key in output_ndx_dict:
        
        ## WRITING INDEX OBJECT
        idx_obj.write( 
                      index_key = idx_key, 
                      index_list = output_ndx_dict[idx_key],
                      backup_file = False,
                      verbose = True,
                      n_chunks=15)
    
    return output_sulfur_indices, furthest_atom_dict

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    
    ## TESTING IS ON
    if testing is True:
        
        ## DEFINING PATH
        input_path=r"/Volumes/akchew/scratch/MDLigands/database_ligands/final_structures/charmm36-jul2020.ff/LIG7"
        
        ## INPUT GRO
        input_gro="LIG7.gro"
        
        ## INPUT ITP FILE
        input_itp = "LIG7.itp"
        
        ## OUTPUTTING INDEX FILE
        output_ndx = "ligand_indices.ndx"
            
        ## DEFINING PATH TO ITP
        path_gro = os.path.join(input_path, input_gro)
        path_itp = os.path.join(input_path, input_itp)
        ## DEFINING PATH TO INDEX
        path_ndx = os.path.join(input_path, output_ndx)
    
    else:
        ### DEFINING PARSER OPTIONS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ## ADDING PATH INFORMATION
        parser.add_option("--path_gro", 
                          dest="path_gro", 
                          action="store", 
                          type="string", 
                          help="Path to gro file", 
                          default=".")    
        
        parser.add_option("--path_itp", 
                          dest="path_itp", 
                          action="store", 
                          type="string", 
                          help="Path to itp file", 
                          default=".")
        
        parser.add_option("--path_ndx", 
                          dest="path_ndx", 
                          action="store", 
                          type="string", 
                          help="path to index file", 
                          default=".")
        
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## OUTPUTTING VARIABLES
        path_gro = options.path_gro
        path_itp = options.path_itp
        path_ndx = options.path_ndx

    
    ## MAIN FUNCTION TO GENERATE SINGLE LIGAND INDEX
    output_sulfur_indices, furthest_atom_dict = main_generate_single_ligand_index(path_gro = path_gro,
                                                                                  path_itp = path_itp,
                                                                                  path_ndx = path_ndx,)
    
    