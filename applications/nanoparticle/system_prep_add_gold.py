#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
system_prep_add_gold.py

This function adds gold to a gro file and top file. The idea is that sometimes 
we want to initialize the system with ligands only so we could perform 
energy minimization, etc. Then, we add the gold after ligands have been 
minimized. This two-step procedure is done to ensure that you could efficiently 
sample ligand space by avoiding issues with the gold core.

Written by: Alex K. Chew (11/04/2020)

"""

## IMPORTING MODULES
import os
import numpy as np
from optparse import OptionParser # Used to allow commands within command line

## IMPORTING GRO FILE TOOLS
from MDBuilder.core.import_tools import extract_gro ## READING GRO FILE

## CHECKING TOOLS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

## IMPORTING PICKLE TOOLS
import MDDescriptors.core.pickle_tools as pickle_funcs

## LOADING FROM TRANSFER LIGAND
from MDBuilder.builder.transfer_ligand_builder import \
    DEFAULT_GOLD_PICKLE, add_gold_details, print_gro_file_combined_res_atom_names, \
    check_if_gold_in_topology

### FUNCTION TO ADD GOLD
class add_gold_to_topology:
    '''
    This function adds gold atoms to a gro file. 
    INPUTS:
        void
        
    FUNCTIONS:
        load_gold_pickle:
            loads gold pickle file
        load_gro_file:
            loads gro file
    '''
    
    ## INITIALIZING
    def __init__(self
                 ):
        return
    
    ## LOADING PICKLE
    def load_gold_pickle(self,
                         path_pickle
                         ):
        '''
        This function loads the gold pickle
        INPUTS:
            path_pickle: [str]
                path to pickle file to load
        OUTPUTS:
            gold_dict: [dict]
                dictionary containing all information required for gold 
                addition to a gro file.
        '''
        ## LOADING DICTIONARY
        gold_dict = pickle_funcs.load_pickle(path_pickle)[0]
        
        return gold_dict
    
    ## FUNCTION TO LOAD GRO
    def load_gro_file(self,
                      path_gro,
                      verbose = False):
        '''
        This function loads the path to gro file.
        INPUTS:
            path_gro: [str]
                path to gro file.
        OUTPUTS:
            gro_file: [obj]
                gro file objecdt
            input_dict: [dict]
                dictionary containin information from gro file
        '''
        ## LOADNG GRO
        gro_file = extract_gro(path_gro,
                               verbose = verbose)
        
        ## DEFINING INPUT DICT
        input_dict = {
                'total_atoms': gro_file.total_atoms,
                'total_resids': gro_file.ResidueNum,
                'total_geom': np.array([gro_file.xCoord,gro_file.yCoord,gro_file.zCoord]).T,
                'total_outputnames': gro_file.ResidueName
                }
        return gro_file, input_dict

### MAIN FUNCTION TO ADD GOLD ATOMS TO GRO FILE
def main_add_gold_to_gro(path_pickle,
                         path_input_gro,
                         path_output_gro,
                         verbose = False):
    '''
    This is the main function that adds gold atom to the end of a gro file. 
    INPUTS:
        path_pickle: [str]
            path to pickle file that has gold dictionary information
        path_input_gro: [str]
            path to gro input gro file
        path_output_gro: [str]
            path to output gro file
    OUTPUTS:
        gro_file: [obj]
            gro file object
        input_dict: [dict]
            input dictionary for gro file
        gold_dict: [dict]
            gold dictionary
    '''
    ## DEFINING OBJECT
    gold_to_top = add_gold_to_topology()
    
    ## LOADING PICKLE FILE
    gold_dict = gold_to_top.load_gold_pickle(path_pickle = path_pickle)
    ## GETTING INPUT GRO FILE 
    gro_file, input_dict = gold_to_top.load_gro_file(path_gro = path_input_gro,
                                                     verbose = verbose)
    
    ## LOADNG GRO
    gro_file = extract_gro(path_input_gro)
    
    ## GETING OUTPUT NAME
    output_names = []
    
    ## GETTING OUTPUT NAMES
    for idx in range(gro_file.total_atoms):
        ## GETTING RESNAME
        resname = gro_file.ResidueName[idx]
        atom_name = gro_file.AtomName[idx]
        
        ## ADJUSTING
        current_output_name = resname.rjust(5, ' ') + atom_name.rjust(5, ' ')
        
        ## APPENDING
        output_names.append(current_output_name)
    
    
    ## DEFINING INPUT DICT
    input_dict = {
            'total_atoms': gro_file.total_atoms,
            'total_resids': gro_file.ResidueNum,
            'total_geom': np.array([gro_file.xCoord,gro_file.yCoord,gro_file.zCoord]).T,
            'total_outputnames': output_names,
            }

    ## GETTING UPDATED OUTPUT
    total_atoms, total_resids, total_geom, total_outputnames = add_gold_details(input_dict = input_dict,
                                                                                gold_dict = gold_dict,                         
                                                                                )
    
    ## GETTING OUTPUT INFORMATION
    output_gro = os.path.basename(path_output_gro)
    output_folder = os.path.dirname(path_output_gro)
    
    # OUTPUTTING GRO FILE
    print_gro_file_combined_res_atom_names(output_gro = output_gro, 
                                           output_folder = output_folder, 
                                           resids = total_resids, 
                                           geom = total_geom, 
                                           outputnames = total_outputnames, 
                                           box_dimension = gro_file.Box_Dimensions
                                           )
    
    return gro_file, input_dict, gold_dict



#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    
    ## TESTING IS ON
    if testing is True:
        ## DEFINING PATH TO SIM
        path_to_sim = "/Volumes/akchew/scratch/nanoparticle_project/simulations/20201104-GNPs_database_water_sims-try4/spherical_300.00_K_GNP148_CHARMM36mar2019mod_Trial_1"
        
        ## DEFINING FILE TO LOAD
        pickle_file = DEFAULT_GOLD_PICKLE
        
        ## DEFINING GRO FILE
        input_gro_file = "sam.gro"
        
        ## DEFINING OUTPUT GRO FILE
        output_gro_file = "sam_with_gold.gro"
        
        ## DEFINING TOP FILE
        top_file = "sam.top"
        
        ## DEFINING VERBOSE
        verbose = False
        
        ## PATH TO PICKLE FILE
        path_pickle = os.path.join(path_to_sim,
                                   pickle_file,
                                   )
        ## PATH TO INPUT GRO FILE
        path_input_gro = os.path.join(path_to_sim,
                                      input_gro_file)
        
        ## PATH TO OUTPUT GRO
        path_output_gro = os.path.join(path_to_sim,
                                       output_gro_file)
        
        ## PATH TO TOPOLOGY
        path_topology = os.path.join(path_to_sim,top_file)

    else:
        ### DEFINING PARSER OPTIONS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## ADDING PATH INFORMATION
        parser.add_option("--path_pickle", 
                          dest="path_pickle", 
                          action="store", 
                          type="string", 
                          help="Path to pickle file", 
                          default=".")    
        
        parser.add_option("--path_input_gro", 
                          dest="path_input_gro", 
                          action="store", 
                          type="string", 
                          help="Path to input gro file", 
                          default=".")
        
        parser.add_option("--path_output_gro", 
                          dest="path_output_gro", 
                          action="store", 
                          type="string", 
                          help="Path to output gro file", 
                          default=".")
        
        parser.add_option("--path_topology", 
                          dest="path_topology", 
                          action="store", 
                          type="string", 
                          help="Path to topology file", 
                          default=".")
        
        ## VERBOSE OPTION
        parser.add_option("--verbose", dest="verbose", action="store_true", default=False, 
                          help="True if you want verbose output")   
    
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## GETTING ARGUMENTS
        path_pickle = options.path_pickle
        path_input_gro = options.path_input_gro
        path_output_gro = options.path_output_gro
        path_topology = options.path_topology
        verbose = options.verbose
    
    #%% MAIN CODE TO EDIT THE GRO FILE
    
    ## RUNNING MAIN FUNCTION TO ADD GOLD ATOMS
    gro_file, input_dict, gold_dict = main_add_gold_to_gro(path_pickle = path_pickle,
                                                           path_input_gro = path_input_gro,
                                                           path_output_gro = path_output_gro,
                                                           verbose = verbose)
    ## DEFINING PATH TO TOPOLOGY
    check_if_gold_in_topology(path_topology = path_topology,
                              verbose = verbose)