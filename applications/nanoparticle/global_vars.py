# -*- coding: utf-8 -*-
"""
global_vars.py
This script contains all global variables used for the nanoparticle project

CREATED ON: 05/02/2018

AUTHOR(S): 
    Alex K. Chew (alexkchew@gmail.com)
"""

## GLOBAL VARIABLES FOR GOLD
GOLD_DIAMETER_NM = 0.2884 # in nm; covalent bond diameter ## RADIUS OF 144.2 picometers
LIGANDPERNM2 = 4.62 # Number of ligands per nm^2 -- for a planar 111 surface, equivalent to ~21.6 A^2/ligand
# obtain number of ligands from 1/3 number of gold atoms; assuming
# root 3 root R30 hexagonal overlayer (simplification, but fine)
## DEFINING CUTOFF FOR GOLD-SULFUR --- based on:
#   Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).
GOLD_SULFUR_CUTOFF = 0.327 # nms (3.27 angstroms)
GOLD_GOLD_CUTOFF = 0.2900 # nms (overestimated for diameter)
GOLD_MOLECULE_NAME='AUNP' # Name of residue
SULFUR_ATOM_NAME='S1' # Atom name of the sulfur atom that is chemi-adsorbed onto the surface
GOLD_ATOM_NAME="Au" # Atom type name
BULKAU_TYPENAME = "AU" # Bulk Au TYP NAME
GOLD_MASS = 196.9965 # MASS
AU_CHARGE = 0.000 # Default
AU_TYPENAME = 'BAu' # Type name for gold

## VARIABLES FOR POSITION RESTRAINT
# DEFAULT_POSITION_RESTRAINT ="100000.0"  # "10000.0" # very high to keep sulfur in place
DEFAULT_POSITION_RESTRAINT ="500000.0"  # "10000.0" # very high to keep sulfur in place <-- temporary testing
GOLD_PLANAR_POSITION_RESTRAINT = "500000.0" #  "100000.0"
GOLD_FCC_LATTICE_CONSTANT_ANGS=4.0782 # Angstroms Same as the lattice constant for the runs in LAMMPS
GOLD_FCC_LATTICE_CONSTANT_NM=0.40782 # Angstroms Same as the lattice constant for the runs in LAMMPS
# REFERENCE: A. Maeland and T. B. Flanagan, Can. J. Phys., 1964, 42, 2364.
# https://www.webelements.com/gold/crystal_structure.html

### DEFINING RESTRAINTS
### PROPERTIES FOR SPECIFIC SHAPES
HOLLOW_SHELL_MINIMUM_GOLD_GOLD_BONDS=6 # Minimum number of bonds for a hollow shell

## TYPENAMES FOR PLANAR SAMs
PLANAR_SURFACEAU_TYPENAME   = "SAu"     # Surface gold atom type name
PLANAR_SURFACEAU_ATOMNAME   = "Au"      # Surface gold atom name
PLANAR_BULK_AU_TYPENAME     = "BAu"     # Bulk gold atomtype name
PLANAR_BULK_AU_ATOMNAME     = "BAu"     # Bulk gold atom name
## VIRTUAL SITES
VIRTUAL_TYPENAME = "MAU"    # Typename for virtual sites (GOLP)
VIRTUAL_ATOMNAME = "M"      # Atomnames for virtual sites (GOLP)
## DEFINING PLANAR GOLD ITP NAME
PLANARGOLDITPNAME = "Au_Planar"
PLANARGOLDRESNAME = "AUI"
