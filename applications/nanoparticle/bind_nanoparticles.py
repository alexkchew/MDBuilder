# -*- coding: utf-8 -*-
"""
bind_gold_nanoparticles.py
The purpose of this script is to take itp, top, and gro files to bind the sulfur to the gold, and likewise gold-gold

Author: Alex K. Chew (alexkchew@gmail.com, created at 02/21/2018)

INPUTS:
	- GRO file after replacing the ligands
	- ITP file for the ligands
	- TOP file -- needs to be updated
	- ITP file for the gold

OUTPUTS:
	- ITP file with Au-S bonded and Au-Au bonded
	- TOP file with updated information

ALGORITHM:
	- Load the gro file
	- Load the ITP details
	- Load the topology details
	- Identify all sulfur atoms
	- For each sulfur atom, find all gold atoms that are within 3.27 Angstroms
		- Generate atom pairs between each sulfur atom and nearby gold atoms
	- For each gold atom, find all nearby gold atoms
		- Generate atom pairs that are non-repeating to bind the gold together
	- Create a new itp file with the suggested changes
	- Fix topology file with suggested changes

** UPDATES **
180223 - Added function to edit the topology file
180501 - Fixed bug with missing [ pairs ] section!
180503 - Editing binding ligand builder to MDBuilders

"""

#%% 

### IMPORTING MODULES
# import numpy as np # Math functions
# import numpy.matlib as npm # For matrix algebra
import os
from optparse import OptionParser # Used to allow commands within command line
## CHECK PATH FUNCTION
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
### IMPORTING FUNCTIONS
from MDBuilder.core.import_tools import extract_itp, extract_gro, extract_top
### EXPORTING FUNCTIONS
from MDBuilder.core.export_tools import write_itp_type_comment, add_before_and_after_2_string
## LIGAND BUILDER
from MDBuilder.builder.ligand_builder import get_ligand_args
## IMPORTING SELF-ASSEMBLY LIGANDS
from MDBuilder.builder.transfer_ligand_builder import self_assembled_gold, print_itp_file_gold_with_shape # self_assembled_gold: Class for self-assembled gold

### DEFINING GLOBAL VARIABLES
## IMPORTING DETAILS ABOUT THE GOLD ITP FILE NAME & SULFUR ATOM NAME FOR LIGANDS ATTACHED TO GOLD
from MDBuilder.applications.nanoparticle.global_vars import GOLD_MOLECULE_NAME # GOLD_MOLECULE_NAME: Is the molecule name of gold -- used for itp file name

## GETTING READ WRITE TOOLS
import MDDescriptors.core.read_write_tools as read_write_tools


gold_gold_bonding_function = 6 # 6  Bonding function for itp file
gold_gold_bonding_kb = 50000 # Bonding strength between gold-gold
gold_sulfur_bonding_function = 6 # 6 Bonding function for itp file
gold_sulfur_bonding_kb = 50000 # Bonding strnegth between gold-sulfur
forcefield_string = 'forcefield.itp' # string that all forcefields require

input_gold_itp=GOLD_MOLECULE_NAME + '.itp' # ITP file for the gold

### DEFINING LOGICALS
want_sulfur_gold_bonding = True # Sulfur-Gold bonds
want_sulfur_gold_angle = False # Sulfur-gold angle constraints
want_gold_gold_bonding = True # Gold-gold bonds
want_gold_gold_angle = False # Gold-gold angle constraints

## DEFINING PARAMETERS FOR SULFUR_GOLD_ANGLES
ANGLE_PARAMS={
        # CHARMM FORCE FIELD
        'urey-bradley': [
                         5, # Functional form in GROMACS
                         '500.0000', #k_theta
                         '0.0000', # ub0
                         '0.0000', #kub
                         ]
        }
## SELECTING THE FORCE FIELD PARAMETERS
SULFUR_GOLD_ANGLE_PARAMS = ANGLE_PARAMS['urey-bradley']

##########################
### DEFINING FUNCTIONS ###
##########################
### FUNCTION TO OUTPUT COMBINED ITP FILE
def combine_itp_file(full_path_output_itp, 
                     itp_info, 
                     gro_info, 
                     output_res_name, 
                     self_assembly_info = None, 
                     want_sulfur_gold_bonding = False, 
                     want_gold_gold_bonding = False ):
        '''
        The purpose of this function is to combine multiple itp files. This can also bind gold to sulfurs!
        INPUTS:
            full_path_output_itp: full path to itp file
            itp_info: List of itp files from the extract_itp class
            gro_info: gro file that uses all the itp details
            output_res_name: output residue name
            self_assembly_info: self assembly information by loading self_assembled_gold class
            want_sulfur_gold_bonding: True if you want sulfur-gold bonding
            want_gold_gold_bonding: True if you want gold-gold bonding
        OUTPUTS:
            itp file with the combined itp based on the gro file
        '''
        ### CREATING FILE
        outputfile = open(full_path_output_itp, "w") # Now, writing in output file
        
        ## DEFINING MANDATORY ITP DETAILS
        mandatory_itp_list = [ 
                                '[ atomtypes ]', # Atom types 
                                '[ bondtypes ]', # Bond types
                                '[ pairtypes ]', # Pair types
                                '[ dihedraltypes ]', # Dihedral types
                                '[ constrainttypes ]', # Constraint types
                                '[ nonbond_params ]', # LJ parameters
                              ]
        
        ### START BY GETTING ALL MANDATORY ITEMS ALL NONBONDED PARMS, ETC.
        mandatory_items = list( set([ current_info[0] for input_itp_info in itp_info 
                                     if len(input_itp_info.mandatory_items)>0 
                                     for current_info in input_itp_info.mandatory_items  ] ) )

        ## REORDERING MANDATORY ITEMS
        ## ADDING SORTING ALGORITHM FOR MANDATORY
        matching_reordered = []
        for each_item in mandatory_itp_list:
            if each_item in mandatory_items:
                matching_reordered.append(each_item)
            
        ## COPYING REORDERED ITEMS
        mandatory_items = matching_reordered[:]
            
        ## LOOPING THROUGH MANDATORY ITEMS
        for each_mandatory_item in mandatory_items:
            ## WRITING HEADER
            outputfile.write( "%s\n"%( each_mandatory_item ) )
            ## WRITING COMMENTS
            write_itp_type_comment(outputfile,each_mandatory_item)
            ## COLLECTING INFORMATION AND OUTPUTTING FOR EACH MOLECULE TYPE
            for each_itp in itp_info:
                mandatory_info = each_itp.mandatory_items
                if len(mandatory_info) > 0: # Means that some mandatory information is there
                    ## FINDING INDEX OF THE INFORMATION THAT YOU CARE ABOUT
                    indices = [index for index, info in enumerate(mandatory_info) if info[0] == each_mandatory_item][0] # Assuming first index
                    ## FINDING THE INFORMATION
                    within_mandatory_info = mandatory_info[indices][1]
                    ## LOOPING THROUGH MANDATORY INFO AND PRINTING
                    for current_info in within_mandatory_info:
                        ## FINDING IF YOU HAVE A COMMENT (denoted by ;)
                        if ';' in current_info:
                            comment_index = current_info.index(';')
                            mandatory_params = current_info[0:comment_index] # All details up to commenting
                            comment_info = current_info[comment_index:-1]
                        else:
                            mandatory_params = current_info; comment_info = '[]'
                        ## JOINING STRING AND OUTPUTTING
                        full_mandatory_string = '  '.join(mandatory_params) + '   ' + ' '.join(comment_info)
                        outputfile.write( "%s\n"%(full_mandatory_string))
                    # ADDING SPACE BETWEEN EACH MANDATORY INFO
                    outputfile.write("\n")
        
        ### MOLECULE TYPE
        outputfile.write("[ moleculetype ]\n")
        write_itp_type_comment(outputfile,'[ moleculetype ]')
        outputfile.write("%s    3\n\n"%(output_res_name))
        
        ## ATOMS
        outputfile.write("[ atoms ]\n")
        write_itp_type_comment(outputfile,'[ atoms ]')
        
        ### LOOPING THROUGH GRO FILE AND ADDING TO ITP TO EACH ATOMIC INFORMATION
        for current_gro_line in range(gro_info.total_atoms):
            ## READING RESIDUE NAME
            current_residue_name = gro_info.ResidueName[current_gro_line]
            ## READING ATOM NAME
            current_atom_name = gro_info.AtomName[current_gro_line]
            ## MATCHING RESIDUE NAME WITH ITP FILE
            for each_itp in itp_info:
                ## MATCHING RESIDUE NAME
                if each_itp.residue_name == current_residue_name:
                    ## LOCATING THE ATOM TYPE
                    atom_type_index = each_itp.atom_atomname.index(current_atom_name)
                    ## PRINTING INFORMATION TO ITP FILE GIVEN THE INDEX
                    outputfile.write("%4d %10s  %d %s    %s %4d  %.3f   %.4f\n"%(current_gro_line+1, each_itp.atom_type[atom_type_index],
                                                                                  1, current_residue_name.rjust(10, ' '), 
                                                                                  each_itp.atom_atomname[atom_type_index].rjust(5, ' '), 1, 
                                                                                  float(each_itp.atom_charge[atom_type_index]), 
                                                                                  float(each_itp.atom_mass[atom_type_index])) )
        ## BONDS / ANGLES / DIHEDRALS
        ## TYPES TO INCLUDE
        Additional_types = ['[ bonds ]', '[ angles ]', '[ dihedrals ]', '[ pairs ]']
        
        for current_type in Additional_types:
            ## STRING TO OUTPUT
            type_string = add_before_and_after_2_string(current_type, '\n')
            outputfile.write(type_string)
            
            ## ADDING COMMENT
            write_itp_type_comment(outputfile,current_type)
        
            ### LOOPING THROUGH EACH RESIDUE AND ADDING BONDING/ANGLE/DIHEDRAL, etc. INFORMATION
            for each_res in range(gro_info.total_residues):
                ## CURRENT RESIDUE NUMBER
                current_res_num = gro_info.unique_resid[each_res]
                ## FINDING CURRENT INDEX FOR RESIDUE -- USED TO CORRECT BONDING INFORMATION
                current_gro_res_index = gro_info.ResidueNum.index(current_res_num)
                ## READING RESIDUE NAME FROM GRO FILE
                current_residue_name = gro_info.ResidueName[gro_info.ResidueNum.index(current_res_num)]
                ## MATCHING RESIDUE NAME WITH ITP FILE
                for each_itp in itp_info:
                    ## MATCHING RESIDUE NAME
                    if each_itp.residue_name == current_residue_name:
                        ### BONDING INFORMATION
                        if current_type == '[ bonds ]':
                            ## DEFINING BONDING INFORMATION
                            bonding_info = each_itp.bonds
                            ## SEEING IF THERE ARE BONDS
                            if len(bonding_info) > 0:
                                ## LOOPING EACH BOND
                                for each_bond in range(len(bonding_info)):
                                    current_bond = bonding_info[each_bond] + current_gro_res_index # Correcting for residue index
                                    current_bond_func = each_itp.bonds_func[each_bond]                        
                                    outputfile.write("%d   %d   %d\n"%(current_bond[0], current_bond[1], current_bond_func))
                            
                        elif current_type == '[ angles ]':
                            ## DEFINING ANGLE INFORMATION
                            angle_info = each_itp.angles
                            ## SETTING IF THERE ARE ANGLES
                            if len(angle_info) > 0:
                                for each_angle in range(len(angle_info)):
                                    current_angle = angle_info[each_angle] + current_gro_res_index # Correcting for residue index
                                    current_angle_func =  each_itp.angles_func[each_angle]    
                                    outputfile.write("%d   %d   %d   %d\n"%(current_angle[0],
                                                                            current_angle[1],
                                                                            current_angle[2],
                                                                            current_angle_func))
                        elif current_type == '[ dihedrals ]':
                            ## DEFINING DIHEDRAL INFORMATION
                            dihedral_info = each_itp.dihedrals
                            ## SEEING IF THERE IS DIHEDRALS
                            if len(dihedral_info) > 0:
                                for each_dihedral in range(len(dihedral_info)):
                                    current_dihedral = dihedral_info[each_dihedral] + current_gro_res_index # Correcting for residue index
                                    current_dihedral_func = each_itp.dihedrals_func[each_dihedral]
                                    outputfile.write("%d   %d   %d   %d    %d\n"%(current_dihedral[0],
                                                                                  current_dihedral[1],
                                                                                  current_dihedral[2],
                                                                                  current_dihedral[3],
                                                                                  current_dihedral_func))
                                    
                        elif current_type == '[ pairs ]':
                            ## DEFINING DIHEDRAL INFORMATION
                            pair_info = each_itp.pairs
                            ## SEEING IF THERE IS DIHEDRALS
                            if len(pair_info) > 0:
                                for each_pair in range(len(pair_info)):
                                    current_pair = pair_info[each_pair] + current_gro_res_index # Correcting for residue index
                                    current_pair_func = each_itp.pairs_func[each_pair]
                                    outputfile.write("%d   %d   %d\n"%(current_pair[0],
                                                                      current_pair[1],
                                                                      current_pair_func,
                                                                      ))
                
    
            ### LOOPING THROUGH SPECIFIC ADDITIONAL DETAILS
            #### BONDS
            if want_sulfur_gold_bonding is True and current_type == '[ bonds ]':
                print("***ADDING SULFUR-GOLD BONDING INFORMATION***")
                outputfile.write("%s\n"%("; SULFUR-GOLD BONDING INFORMATION"))
                for index, each_bond in enumerate(self_assembly_info.gold_sulfur_bonding_gro_index):
                    outputfile.write("%d   %d   %d   %.4f   %.4f\n"%(each_bond[0],each_bond[1], gold_sulfur_bonding_function,
                                                                     self_assembly_info.gold_sulfur_bonding_values[index],
                                                                     gold_sulfur_bonding_kb ))
            if want_gold_gold_bonding is True and current_type == '[ bonds ]':
                outputfile.write("%s\n"%("; GOLD-GOLD BONDING INFORMATION"))
                for index,each_bond in enumerate(self_assembly_info.gold_bonding_gro_index):
                    outputfile.write("%d   %d   %d   %.4f   %.4f\n"%(each_bond[0],each_bond[1], gold_gold_bonding_function, 
                                                                     self_assembly_info.gold_bonding_values[index],
                                                                     gold_gold_bonding_kb))
                    
            #### ANGLES <-- NEED TO FIX!
            if want_sulfur_gold_angle is True and current_type == '[ angles ]':
                outputfile.write("%s\n"%("; SULFUR-GOLD ANGLE INFORMATION"))
                for index,each_angle_index in enumerate(self_assembly_info.sulfur_gold_angles_gro_index):
                    outputfile.write("%d   %d   %d   %d   %.4f   %s\n"%(each_angle_index[0],each_angle_index[1], each_angle_index[2],
                                                                          SULFUR_GOLD_ANGLE_PARAMS[0], self_assembly_info.sulfur_gold_angles_degrees[index], 
                                                                          '   '.join(SULFUR_GOLD_ANGLE_PARAMS[1:])
                                                                          ))
    
        ## SUMMARY
        print("MANDATORY CATEGORIES ADDED: %s"%(' '.join(mandatory_items)))
        print("TOTAL ATOMS ADDED: %s"%(gro_info.total_atoms))
        return

### FUNCTION TO LOOK INTO TOPOLOGY FILE AND CORRECT DUE TO BINDING OF ITPS
def correct_top_file(full_path_input_top, output_res_name, itp_list, itp_info):
    '''
    The purpose of this function is to extract the topology, find all itp files that have been combined and remove them. Then, this will look for molecules that are no longer defined and remove those.
    INPUTS:
        full_path_input_top: full path to input topology -- note that this will be re-written
        output_res_name: residue name of that will be outputted
        itp_list: list of itp files that you want to remove
        itp_info: extract_itp class as a list -- used to find residue names (can be changed later)            
    OUTPUTS:
        Updated topology file
    '''
    ### EXTRACTING TOPOLOGY INFORMATION
    top_info = extract_top(full_path_input_top)
    
    ### CREATING INDEX OF VALUES TO REMOVE
    removing_index = []    

    ### FINDING INDEX WHERE THE LIGAND ITPS ARE -- REMOVING PREVIOUS ITP FILES
    indexes = [ index for input_itp_file in itp_list for index,itp_files in enumerate(top_info.itp_list) if input_itp_file in itp_files ]
    removing_index.extend([ top_info.itp_list_index[current_index] for current_index in indexes])
    
    ### FINDING ALL MOLECULE NAMES
    # GETTING KEYS FOR TOPOLOGY
    top_keys = top_info.bracket_extract.keys()
    # NOW, FINDING MOLECULES AS A KEY
    mol_key =[ keys for keys in top_keys if 'molecules' in keys][0]
    # FINDING MOLECULE AND NUMBER THAT WE CARE ABOUT (E.G. ['BUT', '693'])
    mol_of_interest = [ each_residue for each_residue in top_info.bracket_extract[mol_key] for each_itp in itp_info if each_residue[0] == each_itp.residue_name ]
    # GOING THROUGH THE FILE AND FINDING THE INDEX TO REMOVE
    mol_index = [index for index, line in enumerate(top_info.topology_lines) for molecules in mol_of_interest if molecules[0] in line and molecules[1] in line  ]
    # STORING 
    removing_index.extend(mol_index)
    
    ### USING REMOVAL INDEX TO TAKE OUT GARBAGE FROM TOPOLOGY
    keep_index = [index for index in range(len(top_info.topology_lines)) if index not in removing_index ]
    new_top_lines = [top_info.topology_lines[index] for index in keep_index]
    
    #### INSERTION OF ITP FILES
    ### FINDING FORCE FIELD INDEX
    force_field_line_index = [ index for index, line in enumerate(new_top_lines) if forcefield_string in line or ".prm" in line][-1] + 1 # +1 for Indexing after the force field
    
    ### ADDING TO TOPOLOGY: #include this_molecule.itp
    ## SEE IF THE ITP FILE IS THERE
    index_itp = [ index for index, current_line in enumerate(new_top_lines) if output_itp_file in current_line]
    ## ADD ITP IF NOT AVAILABLE
    if len(index_itp) == 0:
        new_top_lines = new_top_lines[:force_field_line_index] + ['#include "'+ output_itp_file + '"\n'] + new_top_lines[force_field_line_index:]
    
    #### INSERTION OF MOLECULE TYPES
    ## SEE IF RESIDUE IS ALREADY THERE
    top_residue = [ index for index, current_line in enumerate(new_top_lines) if output_res_name in current_line and '1' in current_line ] # Residue and 1
    
    ## ADD RESIDUE / MOLECULE IF NOT AVAILABLE
    if len(top_residue) == 0:
        ### FINDING [ molecules ] LINE
        molecule_line_index = [ index for index, line in enumerate(new_top_lines) if '[ molecules ]' in line ][0] # Assuming this is defined only once
        ### FINDING LAST LINE WITHIN MOLECULE
        current_molecule_index = molecule_line_index; current_line = new_top_lines[current_molecule_index]
        while len(current_line) != 0: # Finding blank spot
            current_molecule_index += 1
            try: 
                current_line = new_top_lines[current_molecule_index]
            except:
                new_top_lines = new_top_lines + [''] # Out of range issue! -- adding blank line at the end
                current_line = new_top_lines[current_molecule_index] # Writing current line now that it is fixed
                pass
        ### INSERTING WITHIN THE LINE OF INTEREST
        new_top_lines = new_top_lines[:current_molecule_index] + ['  ' + output_res_name + '  1\n'] + new_top_lines[current_molecule_index:]
    
    ### OUTPUTTING TOPOLOGY USING THE NEW LINES
    ## CREATING FILE
    if len(index_itp) == 0 or len(top_residue) == 0:
        print('*** WRITING NEW TOPOLOGY FILE ***')
        with open(full_path_input_top, 'w+') as output_top_file:
            for each_line in new_top_lines:
                output_top_file.write(each_line)
    else:
        print("ITP files and number of residues satisfied. No changes have been made to topology.")
    
    return

### MAIN SCRIPT
def bind_nanoparticle_main(full_path_input_gro, 
                           full_path_input_top, 
                           full_path_output_itp, 
                           itp_list, 
                           shape = 'spherical',
                           want_sulfur_gold_bonding=True, 
                           want_gold_gold_bonding=True, 
                           wantPlot = False,
                           assembly_lig_type = "SCCCC",
                           full_path_sulfur_ndx = None,
                           want_updated_aunp_itp = False,
                           forcefield_folder = "charmm36-nov2016.ff"):
    '''
    This main script collects all the itp files into one single itp file. 
    Then, binds the sulfur and gold. 
    Afterwards, it corrects the topology file with new itp information
    INPUTS:
        full_path_input_gro: [str]
            full path to the GRO file of interest
        full_path_input_top: [str]
            full path to the TOP file that will be changed
        full_path_output_itp: [str]
            full path to the output itp file
        itp_list: [list]
            list of itp files
        full_path_sulfur_ndx: [str]
            full path to 
        shape: shape of the object, e.g.
            'spherical': assuming spherical core
            'hollow': assuming hollow core
        want_sulfur_gold_bonding: True if you want sulfur-gold bonding
        want_gold_gold_bonding: True if you want gold-gold bonding
        wantPlot: True if you want to see plots for self_assembly_ligands
        want_updated_aunp_itp: [logical]
            True if you want updated gold nanoparticle itp file. This will 
            reuse the self-assembly info to re-create a new itp file. 
        forcefield_folder: [str]
            force field file -- which is just useful when implementing updated gold
    OUTPUTS:
        new combined itp file based on your output itp
        new topology file that has been updated with output itp
    '''
    ### LOADING GRO FILE
    gro_info = extract_gro(full_path_input_gro)
    
    ## DEFINING PATH
    output_folder = os.path.dirname(full_path_input_gro)
    
    ## CHECKING SULFUR INDICES
    if full_path_sulfur_ndx is not None:
        print("Loading bound sulfur indexes from path: %s"%(full_path_sulfur_ndx))
        ## DEFINING INDEX OBJECT
        idx_obj = read_write_tools.import_index_file(path_to_index = full_path_sulfur_ndx)
        ## READING GROUPS
        idx_obj.split_to_groups()
        
        ## CHECKING IF SULFUR INDICES ARE INSIDE
        if 'NEARBY_SULFURS' in  idx_obj.data_groups.keys():
            sulfur_index = idx_obj.data_groups['NEARBY_SULFURS'] - 1 # Subtract 1 for Python nomenclature
            print("Nearby sulfurs found, total: %d"%(len(sulfur_index) ))
        else:
            print("Since no NEARBY_SULFURS is within the index, we are not loading any sulfur indices")
            print("Please check to makre sure the index file has 'NEARBY_SULFURS' key")
            sulfur_index = None
        
    print("\n--- FINDING SULFUR-GOLD / GOLD-GOLD BONDS  ---")
    self_assembly_info = self_assembled_gold(full_path_input_gro, 
                                             shape=shape, 
                                             wantPlot = wantPlot,
                                             assembly_lig_type = assembly_lig_type,
                                             sulfur_index = sulfur_index)
    
    ## CHECKING IF YOU WANT TO RE-CREATE AN ITP FILE
    if want_updated_aunp_itp is True:
        print("Creating new itp file for AUNP - using gro file information")
        print_itp_file_gold_with_shape(total_gold_atoms = self_assembly_info.gold_num_atoms,
                                       self_assembled_gold_info = self_assembly_info,
                                       output_folder = output_folder,
                                       forcefield_folder = forcefield_folder,
                                       shape = shape)
        

    ### LOADING ITP INFORMATION
    itp_info = []
    for input_itp_file in itp_list:
        ## DEFINING FULL PATH TO ITP
        full_path_itp_file = check_server_path( os.path.join( input_folder, input_itp_file)  )
        itp_info.append( extract_itp(full_path_itp_file) )
    

    
    ### CREATING ITP FILE
    print("\n--- CREATING ITP FILE (%s) WITH RESIDUE NAME (%s) ---"%(output_itp_file, output_res_name))
    combine_itp_file(full_path_output_itp = full_path_output_itp, 
                     itp_info = itp_info, 
                     gro_info = gro_info,  
                     output_res_name = output_res_name, 
                     self_assembly_info = self_assembly_info, 
                     want_sulfur_gold_bonding = want_sulfur_gold_bonding, 
                     want_gold_gold_bonding = want_gold_gold_bonding )
    
    ### UPDATING TOPOLOGY FILE
    print("\n--- CORRECTING TOPOLOGY FILE (%s)---"%(input_top_file))
    correct_top_file(full_path_input_top = full_path_input_top, 
                     output_res_name = output_res_name, 
                     itp_list = itp_list, 
                     itp_info = itp_info)
    
    return self_assembly_info, itp_info


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### *** USER-DEFINED DETAILS *** ###
    ### TURNING TEST ON / OFF
    testing = check_testing() # False if you're running this script on command prompt!!!
    
    ### TESTING IS ON
    if testing is True:
        ### DEFINING INPUT GRO FILE
        input_folder = check_server_path(r'/Users/alex/Downloads/spherical_300.00_K_GNP100_CHARMM36jul2020_Trial_1')
        input_gro_file = "sam_FEP_em_4_with_gold_resized.gro"
        # "sam_em.gro" # GRO File
        input_top_file = "sam.top" # TOP File
        
        ### DEFINING LIGANDS -- will look for itp files of this type
        ligand_names=['LIG70'] # Name of the ligand 'ROT_NS',
        
        ### DEFINING LOGICALS
        wantPlot = True # Plots for self-assembly gold
        
        ### DEFINING OUTPUT
        output_res_name = "sam"
        output_itp_file = "sam.itp"
        
        ## DEFINING SHAPE
        # shape='hollow'
        shape='spherical'
        
        ## DEFINING INDEX FILE
        sulfur_ndx = "nearby_sulfurs.ndx"
        
        ## DEFINING LIGAND ASSEMBLY TYPE
        assembly_lig_type = "C1CCSS1"
        
        ## DEFINING IF YOU WANT UPDATED ITP
        want_updated_aunp_itp = True
        
    ### TESTING IS OFF
    else:
        ### DEFINING PARSER OPTIONSn
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ### INPUT
        ## INPUT FOLDER
        parser.add_option("--ifold", dest="input_folder", action="store", type="string", help="Input gro file", default=".")
        ## INPUT GRO FILE
        parser.add_option("--igro", dest="input_gro_file", action="store", type="string", help="Input gro folder", default=".")        
        ## INPUT TOP FILE
        parser.add_option("--top", dest="input_top_file", action="store", type="string", help="Input top file", default=".")        
        ## LIGAND NAMES
        parser.add_option("-n", "--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        
        ## SHAPE
        parser.add_option("--geom", dest="shape", type="string",help="Geometric shape of the core (e.g. spherical, hollow, etc.)", default="spherical")
        
        ## ASSEMBLY TYPE
        parser.add_option("--assembly", dest="assembly_lig_type", type="string",help="assembly type desired", default="SCCCC")
        
        ### OUTPUT
        ## RESIDUE NAME
        parser.add_option("--ores", dest="output_res_name", action="store", type="string", help="Output residue name for new itp file", default="SAM")
        ## ITP FILE NAME
        parser.add_option("--oitp", dest="output_itp_file", action="store", type="string", help="Output itp file", default="sam")
        
        ## ADDING OPTION TO RECHECK ITP FILE FOR GOLD
        parser.add_option("--want_updated_aunp_itp", dest="want_updated_aunp_itp", action="store_true", default=False, 
                          help="This option will recheck AuNP itp parameters and output a new file")      
        
        ## option to include sulfur index
        parser.add_option("--sulfur_ndx", dest="sulfur_ndx", action="store", type="string", 
                          help="Sulfur indices of closest bound sulfur atoms", default=None)
        
        ### PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## INPUT FILES
        input_folder = options.input_folder
        input_gro_file = options.input_gro_file
        input_top_file = options.input_top_file
        
        ## TURNING PLOTS OFF
        wantPlot = False
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
            
        ## OUTPUT FILES
        output_res_name = options.output_res_name
        output_itp_file = options.output_itp_file
        
        ## SHAPE
        shape = options.shape
        
        ## DEFINING ASSEMBLY TYPE
        assembly_lig_type = options.assembly_lig_type
        
        ## WANT UPDATED AUNP ITP
        want_updated_aunp_itp = options.want_updated_aunp_itp
        
        ## ADDING SULFUR INDEX
        sulfur_ndx = options.sulfur_ndx
        if sulfur_ndx == "None":
            sulfur_ndx = None

    ### *** USER-DEFINED DETAILS (END) *** ###
    
    ### DEFINING FULL ITP LIST
    name_list = ligand_names + [ GOLD_MOLECULE_NAME ] # NAMES OF EACH
    itp_list = [ names + '.itp' for names in name_list] # ITP LIST
    
    ### DEFINING FULL PATHS FROM INPUT
    full_path_input_gro = check_server_path( os.path.join(input_folder , input_gro_file) )
    full_path_input_top = check_server_path( os.path.join(input_folder ,  input_top_file) )
    full_path_output_itp = check_server_path( os.path.join(input_folder, output_itp_file) )
    if sulfur_ndx is not None:
        full_path_sulfur_ndx = check_server_path( os.path.join(input_folder, sulfur_ndx) )
    else:
        full_path_sulfur_ndx = None
    
    ### RUNNING MAIN SCRIPT
    self_assembly_info, itp_info = bind_nanoparticle_main(full_path_input_gro = full_path_input_gro, 
                                                          full_path_input_top = full_path_input_top, 
                                                          full_path_output_itp = full_path_output_itp, 
                                                          itp_list = itp_list,
                                                          full_path_sulfur_ndx = full_path_sulfur_ndx,
                                                          shape = shape, 
                                                          want_sulfur_gold_bonding = want_sulfur_gold_bonding, 
                                                          want_gold_gold_bonding=want_gold_gold_bonding, 
                                                          wantPlot = wantPlot,
                                                          assembly_lig_type = assembly_lig_type,
                                                          want_updated_aunp_itp = want_updated_aunp_itp)