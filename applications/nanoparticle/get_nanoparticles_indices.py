#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
get_nanoparticles_indices.py

The purpose of this function is to load a gro file, look for all the ligands, 
then select all indices that are considered the last heavy atom. We will only 
focus on heavy atoms for the sake of this code. 

Written by: Alex K. Chew (11/06/2020)

FUNCTIONS:
    find_bonding_from_initial:
        finds bonding information from an initial starting point. Useful for 
        finding the furthest bond from a ligand head group atom
    get_furthest_atomname_per_ligand:
        gets furthest atomname per ligand basis.
    get_sulfur_indices_bound:
        finds sulfur indices that are bound to gold
    get_atom_indices_furthest:
        gets atom indices using furthest atomname in ligand
    get_sulfur_indices_bound_from_index_list:
        function that gets sulfurs that are bound
"""
## IMPORTING MODULES
import os
import numpy as np
from optparse import OptionParser # Used to allow commands within command line

## CHECKING TOOLS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING

## IMPORTING CALC TOOLS
import MDDescriptors.core.import_tools as import_tools # Loading trajectory details
import MDDescriptors.core.calc_tools as calc_tools
import MDDescriptors.core.read_write_tools as read_write_tools

## NANOPARTICLE CODE
from MDDescriptors.application.nanoparticle.nanoparticle_structure import nanoparticle_structure

## GLOBAL VARS
from MDBuilder.applications.nanoparticle.global_vars import GOLD_SULFUR_CUTOFF

### FUNCTION TO FIND THE BONDING FROM A STARTING NAME
def find_bonding_from_initial( atom_names, 
                               bonds,
                               initial_atom_name = 'S1'):
    '''
    This function locates the number of bonds relative from a reference 
    atom name. The idea is to find the longest bond.
    For example:
        S - C1 - C2 - C3
    The output should be:
        [['S', 0],
         ['C1, 1]', etc.
        ]
    INPUTS:
        atom_names: [list]
            list of atom names
        bonds: [list]
            list of each bond. The first index starts with 1
        initial_atom_name: [str]
            initial atom name to base order on
    OUTPUTS:
        bond_order_list: [list]
            list of bonds away from an atom
    '''
    ## DEFINING INITIAL BOND
    num_bonds = 0
    ## STARTING INITIAL LIST
    bond_order_list = [[initial_atom_name, num_bonds]]
    
    ## TRACKING ATOM NAMES REVIEWD
    track_atomnames_reviewed = [initial_atom_name]
    
    ## GETTING BONDS BY SERIAL
    bonds_by_serial = bonds - 1
    
    ## LOCATING ALL ATOMS BONDED TO INITIAL ATOM
    list_of_atoms_to_check = [atom_names.index(initial_atom_name)]
    
    ## LOOPING THROUGH EACH ATOM TO CHECK
    while len(list_of_atoms_to_check) > 0:
        ## ADDING TO NUMBER OF BONDS
        num_bonds += 1
        
        ## STORING ATOMS ATTACHED
        storage_atom_attached = []
        
        ## IDENTIFYING WHICH BONDS ARE ATTACHED
        for each_atom in list_of_atoms_to_check:
            ## GETTING BOND ATTACHED
            bonds_matched = np.where(bonds_by_serial==each_atom)[0]
            ## CHECKING IF ANY BOND MATCHED
            if len(bonds_matched) > 0:
                ## GETTING ALL BONDS THAT ARE MATCHED
                bonds_attached =  bonds_by_serial[bonds_matched,:]
                ## LOCATING ALL ATOMS THAT ARE BOUND
                atom_attached = [atom_names[each_bond[each_bond!= each_atom][0]] 
                                 for each_bond in bonds_attached ]
                ## CHECKING TO MAKE SURE THE ATOM ATTACHED IS NOT THE SAME
                atom_attached = [ each_atom for each_atom in atom_attached 
                                    if each_atom not in track_atomnames_reviewed]
            else:
                atom_attached = []    
            
            ## STORING IN FILE
            for current_attached_atom in atom_attached:
                bond_order_list.append([current_attached_atom, num_bonds])
                track_atomnames_reviewed.append(current_attached_atom)
                
            ## RE-DEFINING LIST OF ATOMS TO CHECK
            storage_atom_attached.extend(atom_attached)

        ## AFTER FOR LOOP, RE-STORE
        list_of_atoms_to_check =[atom_names.index(each_atom) for each_atom in storage_atom_attached]   
        
    return bond_order_list

### FUNCTION TO GET SULFUR INDICES BOUND
def get_sulfur_indices_bound_from_index_list(structure,
                                             sulfur_indices,):
    '''
    This function gets the sulfur indices per ligand basis
    INPUTS:
        structure: [obj]
            structure object 
        sulfur_indices: [list]
            list of sulfur indices
    OUTPUTS:
        sulfur_indices_within_bound: [list]
            list of list for sulfur
    '''
    ## GETTING SULFUR WITHIN BOUND
    sulfur_indices_within_bound = []
    
    ## LOOPING THROUGH EACH LIGAND
    for each_lig in structure.ligand_atom_index_list:
        
        ## FINDING INDICES
        indices_that_are_sulfur = np.where(np.isin(each_lig, sulfur_indices ))[0]
        
        ## GETTING INDICES
        indices_within_lig = [ each_lig[each_idx] for each_idx in indices_that_are_sulfur]
        
        ## GETTING INDICES
        sulfur_indices_within_bound.append(indices_within_lig)
        
    return sulfur_indices_within_bound


### FUNCTION TO GET STORAGE OF MAXIMA ATOM NAME
def get_furthest_atomname_per_ligand(traj_data,
                                     sulfur_indices_within_bound,
                                     structure):
    '''
    This function gets the atomnames that are furtherst from the ligand 
    sulfur atoms. The idea would be to use these maximum distance atoms to 
    pull the ligand away from the gold core.
    INPUTS:
        traj_data: [obj]
            trajectory object
        sulfur_indices_within_bound: [list]
            list of sulfur indices that are within bunds
        structure: [obj]
            nanoparticle structure object
    OUTPUTS:
        storage_maxima_atom_name: [dict]
            dictionary storing the atomname that is furthest from the 
            sulfur positions by number of bonds and the number of bonds
    '''

    ## GETTING MAXIMA ATOM NAME
    storage_maxima_atom_name = {}
    
    ## LOOPING THROUGH EACH RESIDUE
    for each_ligand in structure.ligand_names:
        ## LOCATING ATOM NAME THAT IS FURTHEST FROM SULFUR
        # LOADING ITP FILE
        itp_file = structure.itp_dict[each_ligand]

        ## GETTING RESIDUE AND ATOM INDEX
        res_index, atom_index = calc_tools.find_residue_atom_index( traj = traj_data.traj,
                                                                   residue_name = each_ligand)
        
        ## FINDING ATOM NAMES
        single_res_index = res_index[0]
        
        ## GETTING SULFUR INDICES (first atom)
        sulfur_index_first_atom = sulfur_indices_within_bound[single_res_index][0]
        
        ## GETTING ATOM INDICES
        atom_indices_first_res = np.array(atom_index[0]) # single_res_index
        
        ## GETTING ATOM NAMES
        atom_names_first_res = [ traj_data.traj.topology.atom(each_atom).name 
                                for each_atom in atom_indices_first_res]
        ## GETTING IDX
        idx_of_sulfur = np.where(atom_indices_first_res == sulfur_index_first_atom)[0][0]
        ## GETTING SULFUR NAME
        sulfur_name_first_atom = atom_names_first_res[idx_of_sulfur] # sulfur_index_first_atom
        
        ## GETTING BOND ORDER LIST
        bond_order_list = find_bonding_from_initial( atom_names = itp_file.atom_atomname, 
                                       bonds = itp_file.bonds,
                                       initial_atom_name = sulfur_name_first_atom)
        
        ## GETTING ONLY HEAVY ATOM
        bond_order_list_heavy_atoms = [ bond_list for bond_list in bond_order_list if 'H' not in bond_list[0]]
        
        ## LOCATING MAXIMA
        maxima_details = bond_order_list_heavy_atoms[np.argmax(np.array(bond_order_list_heavy_atoms)[:,1].astype('int'))]
        
        ## APPENDING
        storage_maxima_atom_name[each_ligand] = {
                'atomname': maxima_details[0],
                'bond_length': maxima_details[1],
                }
        
    return storage_maxima_atom_name
    
### FUNCTION TO GET SULFUR INDICES BOUND
def get_sulfur_indices_bound(traj_data,
                             structure):
    '''
    This function gets the sulfur indices that are bound. 
    INPUTS:
        traj_data: [obj]
            trajectory object
        structure: [obj]
            structure object that contains nanoparticle information
    OUTPUTS:
        sulfur_indices_within_bound: [list]
            list with sulfur bound for each ligand
    '''
    ## GETTING ALL SULFUR INDICES
    sulfur_indices = [ [ traj_data.topology.atom(each_atom).index 
                            for each_atom in each_lig if traj_data.topology.atom(each_atom).element.symbol == 'S' ]
                            for each_lig in structure.ligand_atom_index_list 
                            ]
    
    ## GETTING ALL DISTANCES 
    sulfur_indices_as_flat = np.concatenate(sulfur_indices).ravel()
    
    ## GETTING ALL DISTANCES
    distances = calc_tools.calc_pair_distances_between_two_atom_index_list(traj = traj_data.traj,
                                                                           atom_1_index = sulfur_indices_as_flat,
                                                                           atom_2_index = structure.gold_atom_index,
                                                                           periodic = False)[0] # Turn off pbc, first frame
    ## DISTANCES SHAPE IS 292 / 1766 OR #SULFURS TO #GOLD
    sulfurs_within_bound = np.any(distances < GOLD_SULFUR_CUTOFF, axis = 1)
    ## RETURNS: SHAPE OF SULFURS
    
    ## GETTING SULFURS WITHIN BOUND
    sulfur_indices_within_bound = [ [each_atom for atom_idx, each_atom in enumerate(each_lig) 
                                            if sulfurs_within_bound[atom_idx + lig_idx] == True ]
                                        for lig_idx, each_lig in enumerate(sulfur_indices) 
                                       ]
    return sulfur_indices_within_bound

### FUNCTION TO GET ALL ATOM INDICES
def get_atom_indices_furthest(storage_maxima_atom_name,
                              traj_data,
                              structure):
    '''
    This function gets the indices of atom names furhest per ligand. Then, 
    we will sort so atom indices start in correct order. 
    INPUTS:
        storage_maxima_atom_name: [dict]
            dictionary storing all maxima atom anmes
        traj_data: [obj]
            trajetory object
        structure: [obj]
            struture object
    OUTPUTS:
        atom_indices_furthest_from_sulfur: [np.array]
            list of atom indices furthest from the sulfur
        num_bonds_from_sulfur_sorted: [np.array]
            number of bonds from sulfur
    '''
    ## STORING ATOM INDICES
    atom_indices_furthest_from_sulfur = []
    num_bonds_from_sulfur = []

    ## LOOPING THROUGH EACH STRUCTURE
    for each_ligand in structure.ligand_names:
        ## GETTING RESIDUE AND ATOM INDEX
        res_index, atom_index = calc_tools.find_residue_atom_index(traj = traj_data.traj,
                                                                   residue_name = each_ligand)
        
        ## GETTING THE FURTHEST ATOM ANME
        furthest_atom_name = storage_maxima_atom_name[each_ligand]['atomname']
        furthest_bond_length = storage_maxima_atom_name[each_ligand]['bond_length']
        
        ## LOOPING THROUGH RES INDEXES AND STORING
        current_atom_indices_furthest = [ [ each_atom.index for each_atom in traj_data.topology.residue(each_res_index).atoms 
                                           if each_atom.name ==  furthest_atom_name][0]
                                          for each_res_index in res_index ]
        
        ## GETTING BONDS
        current_bond_maxima = [furthest_bond_length for each_index in current_atom_indices_furthest]
        
        ## EXTENDING
        atom_indices_furthest_from_sulfur.extend(current_atom_indices_furthest)
        num_bonds_from_sulfur.extend(current_bond_maxima)

    ## GETTING NUMPY
    atom_indices_array = np.array(atom_indices_furthest_from_sulfur)
    
    ## SORTING
    idx_sort = np.argsort(atom_indices_array)
    
    ## GETTING ATOM INDICES FURTHEST FROM SUFUR
    atom_indices_furthest_from_sulfur = atom_indices_array[idx_sort]
    num_bonds_from_sulfur_sorted = np.array(num_bonds_from_sulfur)[idx_sort]
    
    return atom_indices_furthest_from_sulfur, num_bonds_from_sulfur_sorted

### DEFINING MAIN FUNCTION
def main_get_nanoparticle_indices(input_path,
                                  input_gro,
                                  output_index,
                                  separate_furthest_indices = None,
                                  num_bonds_indices = None,):
    '''
    Main function to get nanoparticle indices.
    INPUTS:
        input_path: [str]
            input simulation path containing itp files, gro files, etc.
        input_gro: [str]
            input gro file
        output_index: [str]
            output index file
        separate_furthest_indices: [str]
            separate file to store furthest indices. If None, no file will be made.
        num_bonds_indices: [str]
            number of bonds indices
    OUTPUTS:
        storage_maxima_atom_name: [dict]
            dictionary of maxima atom 
        atom_indices_furthest_from_sulfur: [list]
            list of atom indices furthest from the sulfur
        sulfur_indices_bound_flattened: [list]
            list of sulfur indices bound toe the surface            
    '''
    ## DEFINING PATH TO OUTPUT INDEX
    path_output_index = os.path.join(input_path, output_index)
    
    ## DEFINING INDEX OBJECT
    idx_obj = read_write_tools.import_index_file(path_to_index = path_output_index)
    
    ## READING GROUPS
    idx_obj.split_to_groups()
    
    ### LOADING TRAJECTORY
    traj_data = import_tools.import_traj( directory = input_path, # Directory to analysis
                 structure_file = input_gro, # structure file
                 xtc_file = input_gro, # trajectories
                 )
    
    ### GETTING UNIQUE RESIDUE NAMES
    unique_residue_names=list(traj_data.residues.keys())
    
    ## REMOVING GOLD NANOPARTICLE FROM LIST
    if 'AUNP' in unique_residue_names:
        unique_residue_names.remove('AUNP')
    
    ## DEFINING INPUT DETAILS
    input_details = {
                        'ligand_names': unique_residue_names,      # Name of the ligands of interest
                        'itp_file': 'match', # '', # ,                      # ITP FILE
                        'structure_types': None,         # Types of structurables that you want , 'distance_end_groups'
                        'save_disk_space': False,                    # Saving space
                        'separated_ligands': True, # Separated ligands
                        }
    
    ## RUNNING NANOPARTICLE STRUCTURE
    structure = nanoparticle_structure(traj_data, **input_details )
    
    ## GETTING GOLD INFORMATION
    structure.find_gold_info(traj = traj_data.traj)
    
    ###########################################
    ### GETTING SULFUR ATOMS THAT ARE BOUND ###
    ###########################################
    ## GETTING INDICES
    if 'NEARBY_SULFURS' in idx_obj.data_groups:
        print("Nearby sulfurs found, loading them!")
        sulfur_indices = idx_obj.data_groups['NEARBY_SULFURS'] - 1 # Subtract 1 to get back to python index
        ## GETTING SULFUR INDICES WITHIN BOUND
        sulfur_indices_within_bound = get_sulfur_indices_bound_from_index_list(structure = structure,
                                                                               sulfur_indices = sulfur_indices,)

    else:
        print("No sulfurs found nearby, so not loading them!")
        ## GETTING SULFUR INDICES THAT ARE BOUND    
        sulfur_indices_within_bound = get_sulfur_indices_bound(traj_data = traj_data,
                                                               structure = structure)
    # return structure, sulfur_indices_within_bound, sulfur_indices

    ## FLATTENING SULFUR INDICES
    sulfur_indices_bound_flattened = np.concatenate(sulfur_indices_within_bound).ravel()
    
    #################################################
    ### GETTING LIGAND ATOM INDICES FURTHEST AWAY ###
    #################################################
    
    ## GETTING ATOM NAMES OF MAXIMA
    storage_maxima_atom_name = get_furthest_atomname_per_ligand(traj_data = traj_data,
                                                                sulfur_indices_within_bound = sulfur_indices_within_bound,
                                                                structure = structure)
    
    
    
    ## GETTING ATOM INDICES FURTHEST        
    atom_indices_furthest_from_sulfur, num_bonds_from_sulfur_sorted = get_atom_indices_furthest(storage_maxima_atom_name = storage_maxima_atom_name,
                                                                                                traj_data = traj_data,
                                                                                                structure = structure) 
    ##################################
    ### OUTPUTTING DATA INTO INDEX ###
    ##################################
    
    ## DEFINING DICT
    output_ndx_dict = {
            'FURTHEST_ATOM': atom_indices_furthest_from_sulfur,
#            'NEARBY_SULFURS': sulfur_indices_bound_flattened,
            }
    
    ## LOOPING
    for idx_key in output_ndx_dict:
        
        ## WRITING INDEX OBJECT
        idx_obj.write( 
                      index_key = idx_key, 
                      index_list = output_ndx_dict[idx_key],
                      backup_file = False,
                      verbose = True,
                      n_chunks=15)
    ## CHECKING IF YOU WANT SEPARATE FURTHEST INDICES
    if separate_furthest_indices is not None:
        path_separate_indices = os.path.join(input_path,
                                             separate_furthest_indices)
        
        ## OPENING FILE AND WRITING
        with open(path_separate_indices, 'w') as f:
            indices_to_output = output_ndx_dict['FURTHEST_ATOM'] + 1 # Adding 1 for gro
            for each_indices in indices_to_output:
                f.write("%d\n"%(each_indices))
    
    ## CHECKING BOND INDICES
    if num_bonds_indices is not None:
        path_bond_indices = os.path.join(input_path,
                                         num_bonds_indices)
        
        ## OPENING FILE AND WRITING
        with open(path_bond_indices, 'w') as f:
            ## LOOPING
            for each_bonds in num_bonds_from_sulfur_sorted:
                f.write("%d\n"%(each_bonds))

    return storage_maxima_atom_name, atom_indices_furthest_from_sulfur, sulfur_indices_bound_flattened

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### TURNING TEST ON / OFF
    testing = check_testing()  # False if you're running this script on command prompt!!!
    
    ## TESTING IS ON
    if testing is True:
        
        ## DEFINING INPUTS
        input_path="/Volumes/akchew/scratch/nanoparticle_project/simulations/20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed/spherical_300.00_K_GNP124_CHARMM36jul2020_Trial_1"
        # "/Volumes/akchew/scratch/nanoparticle_project/simulations/20201106-GNPs_database_water_sims-try14_gold_addition/spherical_300.00_K_GNP15_CHARMM36mar2019mod_Trial_1"
        input_gro="sam.gro"
        
        ## DEFINING OUTPUT INDICES
        output_index = "nearby_sulfurs.ndx" 
        # "np_indices.ndx"
        
        ## GETTING FURTHEST IDICES
        separate_furthest_indices = 'furthest_indices.txt'
        
        ## GETTING ALL BOND LENGTHS
        num_bonds_indices = "bond_lengths.txt"
        
    else:
        ### DEFINING PARSER OPTIONS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## ADDING PATH INFORMATION
        parser.add_option("--input_path", 
                          dest="input_path", 
                          action="store", 
                          type="string", 
                          help="Input simulation folder", 
                          default=".")    
        
        parser.add_option("--input_gro", 
                          dest="input_gro", 
                          action="store", 
                          type="string", 
                          help="input gro file", 
                          default=".")
        
        parser.add_option("--output_index", 
                          dest="output_index", 
                          action="store", 
                          type="string", 
                          help="output index file", 
                          default=".")
        
        parser.add_option("--separate_furthest_indices", 
                          dest="separate_furthest_indices", 
                          action="store", 
                          type="string", 
                          help="file for furthest indices", 
                          default="None")
        
        parser.add_option("--num_bonds_indices", 
                          dest="num_bonds_indices", 
                          action="store", 
                          type="string", 
                          help="file for furthest indices", 
                          default="None")
        
        ## PARSING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## RESTORING ARGUMENTS
        input_path = options.input_path
        input_gro = options.input_gro
        output_index = options.output_index
        separate_furthest_indices = options.separate_furthest_indices
        num_bonds_indices = options.num_bonds_indices
        
        ## CHECKING
        if separate_furthest_indices == "None":
            separate_furthest_indices = None
            
        if num_bonds_indices == "None":
            num_bonds_indices = None
        
    ## RUNNING MAIN FUNCTION
    storage_maxima_atom_name, atom_indices_furthest_from_sulfur, sulfur_indices_bound_flattened = \
        main_get_nanoparticle_indices(input_path = input_path,
                                      input_gro = input_gro,
                                      output_index = output_index,
                                      separate_furthest_indices = separate_furthest_indices,
                                      num_bonds_indices = num_bonds_indices) #     structure, sulfur_indices_within_bound, sulfur_indices =\