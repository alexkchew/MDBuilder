# -*- coding: utf-8 -*-
"""
most_likely_configuration.py
The purpose of this code is to find the most likely configuration. We will do this based on dihedral angle analysis. 
This is especially important for the cases where you need a stable conformer, perhaps frozen across simulation time.

CLASSES:
    
FUNCTIONS:
    load_itp_file: loads itp file by checking the current available itp files
    convert_dihedrals_to_atom_index: converts dihedral itp file into atom index
    plot_3d_molecule: plots 3D molecule using matplotlib

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
CREATED ON: 12/13/2018

UPDATES:
    20190219    - Edited code to look for maxima of all dihedrals, then find the closest configurations based on that.
                - Also included a way to look for dihedral angles of hydrogen as well.
"""
## IMPORTANT FUNCTIONS
import sys
import pandas as pd
import glob
import numpy as np
import mdtraj as md
import os

## MD BUILDERS FUNCTIONS
from MDBuilder.core.pickle_funcs import store_class_pickle, load_class_pickle
from MDBuilder.core.check_tools import check_testing

## MD DESCRIPTOR FUNCTIONS
import MDDescriptors.core.import_tools as import_tools # Loading trajectory details
import MDDescriptors.core.calc_tools as calc_tools # Loading calculation tools
import MDDescriptors.core.read_write_tools as read_write_tools # Reading itp file tool
import MDDescriptors.core.plot_tools as plot_tools
### IMPORTING FUNCTIONS
from MDDescriptors.application.solvent_effects.check_functions import check_traj_names, check_solute_solvent_names

## LIGAND BUILDER
from MDBuilder.builder.ligand_builder import get_ligand_args

## IMPORTING PATHS
from MDDescriptors.core.check_tools import check_path

### CLASS FUNCTION TO ANALYZE DIHEDRAL ANGLES
class calc_dihedrals:
    '''
    The purpose of this function is to compute the dihedral angles of a single solute to find the most likely configuration
    Assumptions:
        - We have one residue that we are interested in optimizing -- will expand to multiple residues in the future!
    NOTES:
        - to reload pickle: dihedral_results = load_class_pickle( path2AnalysisDir + '/most_likely_config.pickle'  )
    INPUTS:
        traj_data: [class]
            Data taken from import_traj class
        solute_name: [list] 
            list of the solutes you are interested in (Note that this script will look for all possible solutes)
        degree_increment: [int, default=5]
            degree increment
        output_folder_path: [str, default = None]
            path to output folder. If None, no pickle / output file will be generated
        output_file_name: [str, default="most_likely_config"]
            name of output file
        dihedral_range: [tuple, size=2]
            dihedral range of interest. If 'None', then simply get the dihedral maximum across -180 to 180 degrees. 
        want_H: [logical, default=False]
            True if you want to account for the dihedral angles of hydrogens
        want_pickle: [logical, default=True]
            True if you want to save a pickle
    OUTPUTS:
        
        
        
    FUNCTIONS:
        find_closest_structure_to_maxima: functions that finds the closest structure to the maxima and outputs the frame index sorted
        find_histo_maxima: finds the maximum within a histogram
        prep_solute_position_plots: function that stores all solute positions for pltos
        find_all_solute_positions: function that finds all solute positions across the trajectories
        calc_2d_histogram: calculates 2d histogram with numpy -- then computes maxima with the histogram
        calc_maxima_2d_histogram: computes maxima for a 2d histogram
        
        ACTIVE FUNCTIONS:
            plot_structure_for_frame: plots the structure per frame
            plot_all_dihedral_angle_dist: plots between all possible dihedral angle and shows a histogram
            plot_dihedral_angle_dist: plots single dihedral angle given dihedral index
            plot_2d_dihedral_angle_per_frame: plots 2d histogram based on dihedral indexes
            plot_2d_dihedral_angle_dist: plots 2D dihedral angle distribution
    '''
    ### INITIALIZATION
    def __init__(self, 
                 traj_data, 
                 solute_name, 
                 degree_increment = 5, 
                 output_folder_path = None, 
                 output_file_name="most_likely_config",
                 dihedral_range= None,
                 want_pickle = True, 
                 want_H=False):
        ### PRINTING
        print("**** CLASS: %s ****"%(self.__class__.__name__))
        
        ### DEFINING VARIABLES
        self.solute_name = solute_name
        self.degree_increment = degree_increment
        self.dihedral_range = dihedral_range
        
        ### TRAJECTORY
        traj = traj_data.traj
        
        ### STORING TIME
        self.traj_time = traj.time
        
        ### CHECK SOLUTE
        self.solute_name = check_traj_names( traj_data, residue_names = self.solute_name)
        
        ## SELECTING ONLY ONE SOLUTE
        self.solute_name = self.solute_name[0]
        
        ### LOADING ITP FILE
        self.itp_files =  read_write_tools.load_itp_file( itp_file_path = traj_data.directory,
                                                          residue_names = self.solute_name)
        
        ## FINDING ATOM INDEX OF SOLUTE
        self.solute_residue_index, self.solute_atom_index = calc_tools.find_residue_atom_index( traj = traj, residue_name = self.solute_name)
        
        ## ASSUMING ONE SOLUTE
        self.solute_residue_index = self.solute_residue_index[0]
        self.solute_atom_index =  np.array(self.solute_atom_index[0])
        
        ## FINDING DIHEDRAL ANGLES
        self.solute_dihedrals_atom_index = read_write_tools.convert_dihedrals_to_atom_index( atom_index = self.solute_atom_index, 
                                                                                            itp_file = self.itp_files)
        
        ## CONVERTING TO SYMBOLS
        self.solute_dihedrals_symbols = read_write_tools.convert_atom_index_to_elements( traj = traj, 
                                                                                         atom_index = self.solute_dihedrals_atom_index,
                                                                                         element_type = "symbol"
                                                                                        )
        ## FINDING INDEX THAT ARE HEAVY ATOM DIHEDRALS
        if want_H is False:
            self.solute_dihedrals_heavy_atoms_index = self.solute_dihedrals_atom_index[np.where(np.all(self.solute_dihedrals_symbols != 'H', axis =1))]
        else: # INDEXES INCLUDING HYDROGEN ATOMS
            self.solute_dihedrals_heavy_atoms_index = self.solute_dihedrals_atom_index[:]
        
        ## FINDING NUMBER OF DIHEDRALS
        self.num_solute_dihedrals_heavy_atoms = len( self.solute_dihedrals_heavy_atoms_index )
        
        ## CONVERTING TO ATOM NAMES
        self.solute_dihedrals_heavy_atoms_index_names = read_write_tools.convert_atom_index_to_elements( traj = traj, 
                                                                                        atom_index = self.solute_dihedrals_heavy_atoms_index,
                                                                                        element_type = "name"
                                                                                        )
        
        ## COMPUTING DIHEDRALS (IN RADIANS)
        print("*** COMPUTING DIHEDRALS ***")
        self.dihedrals_radians = md.compute_dihedrals( traj = traj,
                                               indices  = self.solute_dihedrals_heavy_atoms_index,
                                               periodic = True,
                                              ) ## RETURNS FRAME X NUM_DIHEDRALS, e.g. 1001, 2 for 1001 frames and 2 dihedral angles
        
        ## COMPUTING IN DEGREES
        self.dihedrals_degrees = np.rad2deg(self.dihedrals_radians)
        
        ## FINDING HISTOGRAM RANGES
        self.bin_range = np.arange(-180, 180 + self.degree_increment, self.degree_increment)        
        
        ## FINDING HISTOGRAM CENTERS
        self.bin_range_centers = (self.bin_range[:-1] + self.bin_range[1:]) / 2
        
        ## COMPUTING HISTOGRAMS
        self.dihedrals_histogram = [ np.histogram(self.dihedrals_degrees[:,each_dihedral], bins = self.bin_range)[0] for each_dihedral in range(self.num_solute_dihedrals_heavy_atoms)]
        
        ## COMPUTING MAXIMA OF THE HISTOGRAM
        self.find_histo_maxima()
        
        ## PREPARING SOLUTE PLOTS
        self.prep_solute_position_plots(traj = traj)
    
        ## COMPUTING THE CLOSEST STRUCTURES TO MAXIMA
        self.closest_structure_time_index, self.closest_structure_time_index_norms = self.find_closest_structure_to_maxima()
    
        ## CREATING DATA BASE FOR CLOSEST STRUCTURES
        self.create_database_closest_structures()
    
        ## WRITING FILE IF OUTPUT PATH DOES EXIST
        if output_folder_path is not None:
            ## COMBINING PATHS
            self.output_full_path=os.path.join(output_folder_path, output_file_name + ".summary")
            ## PRINTING
            print("Creating summary file in %s:"%(self.output_full_path) )
            ## PRINTING CLOSEST STRUCTURE
            self.closest_structure_database.to_csv(self.output_full_path, sep='\t', index=None, mode='w')
            ## OUTPUTING TO PICKLE
            if want_pickle is True:
                store_class_pickle(self, os.path.join(output_folder_path, output_file_name + ".pickle"))
        
        return
    
    
    ### FUNCTION TO CREATE TABLE OF CLOSEST STRUCTURES
    def create_database_closest_structures(self):
        '''
        The purpose of this function is to create the closest structures in a form of a pandas table. Then, 
        we can output the table as a form of a text file. The idea here is that we would like to use the database 
        in the bash shell by exporting a specific configuration.
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
             self.closest_structure_database: [pandas.DataFrame]
                 database containing frames and norm from maxima
                      Frame_index  Norm_from_maxima
                0             474          0.220377
                1             250          0.389766
                2             870          0.531745
        '''
        ## SETTING PANDAS OPTIONS TO SPECIFIED DECIMAL PLACES
        # pd.options.display.float_format = '{:.3f}'.format # 3 decimal places
        
        ## DEFINING STORAGE VECTOR
        self.closest_structure_database = None
        self.closest_structure_database = pd.DataFrame( columns = [ 'Index',
                                                                    'Python_index',
                                                                    'Frame_index',
                                                                    'Frame_time_ps',
                                                                    'Norm_from_maxima',
                                                                    ]
                                                        )
        ## LOOPING THROUGH EACH TIME FRAME TO GET THE CONFIGURATION
        for idx, each_frame in enumerate(self.closest_structure_time_index):
            ## ADDING TO THE PANDAS
            self.closest_structure_database = self.closest_structure_database.append(
                                                {
                                                 'Index': idx,   
                                                 'Python_index': each_frame,
                                                 'Frame_index': each_frame + 1,  
                                                 'Frame_time_ps': self.traj_time[each_frame],
                                                 'Norm_from_maxima' : self.closest_structure_time_index_norms[idx],
                                                 } , ignore_index = True)
        
        ## CONVERTING DATA TYPE
        self.closest_structure_database['Index'] = self.closest_structure_database.Index.astype(int)
        self.closest_structure_database['Python_index'] = self.closest_structure_database.Python_index.astype(int)
        self.closest_structure_database['Frame_index'] = self.closest_structure_database.Frame_index.astype(int)
        self.closest_structure_database['Frame_time_ps'] = self.closest_structure_database['Frame_time_ps'].map(lambda x: '%.3f' % x) # 3 decimal places
        self.closest_structure_database['Norm_from_maxima'] = self.closest_structure_database['Norm_from_maxima'].map(lambda x: '%.3f' % x) # 3 decimal places
        # self.closest_structure_database['Norm_from_maxima'] = self.closest_structure_database.Norm_from_maxima.astype(float)
        
        return    
    
    ### FUNCTION TO PLOT STRUCTURE CLOSESTS TO MAXIMA
    def plot_closest_structure_to_maxima(self ):
        ''' Function that plots the closest structure to maxima '''
        self.plot_structure_for_frame( self.closest_structure_time_index[0] )
        return
    
    ### FUNCTION TO FIND CLOSEST STRUCTURE TO MAXIMA
    def find_closest_structure_to_maxima(self):
        '''
        The purpose of this function is to find the closest structure to the maxima and output the frame index
        INPUTS:
            self: [object]
                class object
            dihedral_index_comparison: [list, length = 2]
                first and second index of dihedral angles you want to compare with
        OUTPUTS:
            sorted_index: [np.array, shape=(num_frames, 1)]
                sorted index in terms of frames
            sorted_norms: [np.array, shape=(num_frames, 1)]
                sorted norms based on the dihedral angles
        '''
        ## SEEING IF A DIHEDRAL RANGE IS SPECIFIED
        if self.dihedral_range is None:
            ## FINDING DIHEDRAL MAXIMA
            dihedrals_maxima =  self.dihedrals_maxima # Shape: num_dihedrals
            dihedrals_degree = self.dihedrals_degrees # Shape: num_frames, num_dihedrals    
        else:
            dihedrals_maxima =  self.dihedrals_maxima['max_degrees'] # Shape: num_dihedrals
            dihedrals_degree = self.dihedrals_degrees[:, self.dihedrals_maxima['idx']][:,np.newaxis] # Shape: num_frames, num_dihedrals    
        ## COMPUTING DIFFERENCES
        dihedral_differences = dihedrals_degree -  dihedrals_maxima ## RETURNS NUM_FRAMES X 2
        ## FINDING NORM
        dihedral_norms = np.linalg.norm( dihedral_differences, axis = 1 )
        ## SORTING SMALLEST TO LARGEST
        sorted_index = np.argsort( dihedral_norms )
        ## SORTED NORMS
        sorted_norms = dihedral_norms[sorted_index]
        return sorted_index, sorted_norms
    
    ### FUNCTION TO FIND THE MAXIMUM OF THE HISTOGRAM
    def find_histo_maxima(self):
        '''
        The purpose of this function is to find the maximum of the histogram of the dihedrals.
        INPUTS:
            self: [object]
                class object
        OUTPUTS:
            self.dihedrals_maxima: [np.array, shape=(num_dihedrals)]
                Maxima of the dihedral angle histograms
        '''
        ## SEEING IF A DIHEDRAL RANGE IS SPECIFIED
        if self.dihedral_range is None:
            ## FINDING MAXIMA
            max_index = [ np.argwhere( histo == histo.max() ) for histo in self.dihedrals_histogram ]
            ## FINDING DIHEDRAL CORRESPONDENCE
            self.dihedrals_maxima = np.array([ self.bin_range_centers[idx][0][0] for idx in max_index ]) # Returns maxima dihedral angles (e.g. [-75, 170] )
        else:            
            ## PRINTING
            print("Since dihedral_range is not None, we are enforcing a constraint in our system.")
            print("Selected dihedral: %s" %( '-'.join( list(self.dihedral_range[0]) ) ) )
            print("Dihedral range: %d to %d"%( self.dihedral_range[1][0], self.dihedral_range[1][1] ) )
            
            ## LOCATING THE DIHEDRAL INDEX
            dihedral_index = [ idx for idx,each_dihedral in enumerate(self.solute_dihedrals_heavy_atoms_index_names) if set(self.dihedral_range[0]) == set(each_dihedral) ][0]
            
            ## LOCATING BIN RANGES
            bin_ranges_of_interest_index = np.where( (self.bin_range >= self.dihedral_range[1][0]) & (self.bin_range <= self.dihedral_range[1][1]))
            
            ## FINDING MAXIMAS
            self.dihedrals_maxima = {'idx' : dihedral_index,
                                     'max_degrees': self.bin_range_centers[bin_ranges_of_interest_index][np.argmax(self.dihedrals_histogram[dihedral_index][bin_ranges_of_interest_index])],
                                     'bin_of_interest': bin_ranges_of_interest_index,
                                    }
            
            ## PRINTING
            print("Maxima found at: %.1f degrees"%( self.dihedrals_maxima['max_degrees'] ) )
            
        return
    
    ### FUNCTION TO PREPARE FOR PLOTTING SOLUTE POSITIONS
    def prep_solute_position_plots(self, traj):
        '''
        The purpose of this function is to prepare solute position plots by finding positions, atom elements, etc.
        INPUTS:
            self: [object]
                class object
            traj: [md.traj]
                trajectory from md.traj 
        '''
        ### COMPUTING SOLUTE POSITIONS
        self.find_all_solute_positions(traj = traj)
        
        ## FINDING SYMBOLS
        self.solute_atom_symbols =  read_write_tools.convert_atom_index_to_elements( traj = traj, 
                                                                   atom_index = self.solute_atom_index,
                                                                   element_type = "symbol"
                                                                   )
        ## COMPUTING BONDS
        self.solute_bonds = self.itp_files.bonds - 1
        ## FINDING ATOM NAMES
        self.solute_atom_names = read_write_tools.convert_atom_index_to_elements( traj = traj, 
                                                                   atom_index = self.solute_atom_index,
                                                                   element_type = "name"
                                                                   )
        
        return
    
    ### FUNCTION TO FIND ALL SOLUTE POSITIONS
    def find_all_solute_positions(self, traj):
        '''
        The purpose of this function is to find all solute positions across the trajectories. It is important to store these trajectories to find how the solute looks like across the trajectory.
        INPUTS:
            self: [object]
                class object
            traj: [md.traj]
                trajectory from md.traj            
        OUTPUTS:
            self.solute_positions: [np.array, shape=(num_frames, num_solute_atoms, 3)]
                solute positions per frame basis (expensive to store if trajectory is extremely large)
        '''
        ## FINDING ALL POSITIONS
        self.solute_positions = traj.xyz[:, self.solute_atom_index, : ]
        return 
        
    ### FUNCTION TO PLOT ATOMIC STRUCTURE FOR A SPECIFIC FRAME
    def plot_structure_for_frame(self, frame = 0):
        '''
        The purpose of this function is to plot a structure with atom names listed so you can see how the structure looks like in three-dimensions.
        We are assuming you ran the "find_all_solute_positions" function, enabling us to use solute positions
        INPUTS:
            self: [object]
                class object
        '''
        ## PRINTING
        print("PLOTTING STRUCTURE FOR FRAME: %d"%(frame)  )
        ## DEFINING INPUTS
        atom_positions = self.solute_positions[frame]
        
        ## PLOTTING
        fig, ax = plot_tools.plot_3d_molecule( atom_positions = atom_positions, 
                                            atom_symbols = self.solute_atom_symbols, 
                                            bonds = self.solute_bonds, 
                                            atom_names = self.solute_atom_names
                                            )
        
        ## SETTING TITLE
        ax.set_title("Frame: %d"%(frame) )
        
        return

    ### FUNCTION TO PLOT ALL DIHEDRAL ANGLE DISTRIBUTIONS
    def plot_all_dihedral_angle_dist(self, n_bins = None, save_plot = False):
        '''
        The purpose of this function is to loop through all possible dihedral angles, then plot the dihedral angles
        INPUTS:
            self: [object]
                class object
            n_bins: [int, default=72]
                number of bins
        OUTPUTS:
            
        '''
        ## LOOPING THROUGH EACH DIHEDRAL
        for dihedral_idx in range(len(self.solute_dihedrals_heavy_atoms_index_names)):
            fig, ax = self.plot_dihedral_angle_dist( bins = self.bin_range, dihedral_index = dihedral_idx, save_plot = save_plot )
            
        return
    
    ### FUNCTION TO PLOT THE DIHEDRAL ANGLE DISTRIBUTION
    def plot_dihedral_angle_dist(self, bins = None, dihedral_index = 0, want_maxima = True, save_plot=False):
        '''
        The purpose of this script is to plot the dihedral angle distribution for each of the dihedral angles
        INPUTS:
            self: [object]
                class object
            bins: [int, default=None]
                array with the bins
            dihedral_index: [int, default = 0]
                dihedral index of interest
            want_maxima: [logical, default = False]
                True if you want maximum line drawn
        OUTPUTS:
            fig, ax: figure and axis for the plot
        '''
        ## IMPORTING MODULES
        import matplotlib.pyplot as plt
        from MDDescriptors.global_vars.plotting_global_vars import FONT_SIZE, FONT_NAME, COLOR_LIST, LINE_STYLE, DPI_LEVEL
        import MDDescriptors.core.plot_tools as plot_tools # Loading calculation tools
        
        ## SEEING IF  BINS EXISTS
        if bins is None:
            bins = self.bin_range
        
        ## CREATING PLOT
        fig, ax = plot_tools.create_plot() 
        
        ## DEFINING NAME OF THE DIHEDRAL ANGLE
        dihedral_name=self.solute_dihedrals_heavy_atoms_index_names[dihedral_index]
        
        ## RENAMING
        renamed_dihedral_name = '-'.join( list(dihedral_name) )
        
        ## DEFINING TITLE
        ax.set_title("Dihedral angle histogram for: %s"%( renamed_dihedral_name ) )
        
        ## DEFINING X AND Y LABELS
        ax.set_xlabel('Angle (degrees)',fontname=FONT_NAME,fontsize = FONT_SIZE)
        ax.set_ylabel('Number of occurances',fontname=FONT_NAME,fontsize = FONT_SIZE)
        
        ## SETTING X AXIS LABELS
        increment = 20
        ax.set_xlim(-180, 180)
        ax.set_xticks(np.arange(-180, 180 + increment, increment))
        
        ## SETTING AXIS 
        ax.hist(self.dihedrals_degrees[:,dihedral_index], bins = bins, color='k',density = False)

        ## FINDING MAXIMA
        if want_maxima == True:
            if self.dihedral_range is None:
                dihedral_maxima = self.dihedrals_maxima[dihedral_index]
            else:
                ## SEEING IF MATCHING INDEX
                if dihedral_index == self.dihedrals_maxima['idx']:
                    dihedral_maxima = self.dihedrals_maxima['max_degrees']
                    ## DRAWING RANGE
                    ax.axvspan( self.dihedral_range[1][0], self.dihedral_range[1][1], color='red', alpha=0.5, label='Search region')
                else:
                    dihedral_maxima = self.dihedrals_degrees[self.closest_structure_time_index[0], dihedral_index]
            ## DRAWING MAXIMA
            ax.axvline(dihedral_maxima, linestyle='--', color='blue', label="Maxima: %.1f degrees"%(dihedral_maxima) )
            ## SHOWING LEGEND
            ax.legend()
            
        ## SAVING PLOT
        if save_plot is True:
            plot_tools.save_fig_png( fig = fig, label = "dihedral_angle_%s"%(renamed_dihedral_name) )
        
        return fig, ax
        
    ### FUNCTION TO PLOT 2D DIHEDRAL ANGLE PER FRAME
    def plot_2d_dihedral_angle_per_frame(self, dihedral_index_comparison=[0, 1], want_maxima=False, save_plot=False):
        '''
        The purpose of this function is to plot the dihedrals of two indices and cluster to see the most likely clusters. Note that we are assuming that you have multiple dihedral angles.
        This is known as the "Ramachandran map" in the MDTraj
        http://mdtraj.org/1.6.1/examples/ramachandran-plot.html
        INPUTS:
            self: [object]
                class object
            dihedral_index_comparison: [list, length = 2]
                first and second index of dihedral angles you want to compare with
            want_maxima: [logical, Default= False]
                True if you want the maxima computed via np.histogram2d using the bin ranges from the object
        OUTPUTS:
            plot of 2D Ramachandran plot
        '''
        ## IMPORTING MODULES
        import matplotlib.pyplot as plt
        from MDDescriptors.global_vars.plotting_global_vars import FONT_SIZE, FONT_NAME, COLOR_LIST, LINE_STYLE, DPI_LEVEL
        import MDDescriptors.core.plot_tools as plot_tools # Loading calculation tools
        # from pylab import *
        ## CREATING PLOT
        fig, ax = plot_tools.create_plot() 
        
        ## DEFINING NAMES OF EACH DIHEDRAL ANGLE
        dihedral_names = [ self.solute_dihedrals_heavy_atoms_index_names[dihedral_index] for dihedral_index in dihedral_index_comparison ]
        
        ## DEFINING DATA
        dihedral_angles = [ self.dihedrals_degrees[:,dihedral_index] for dihedral_index in dihedral_index_comparison ]
        
        ## ADDING TITLE
        ax.set_title("Ramachandran map")
        
        ## DEFINING X AND Y LABELS
        ax.set_xlabel('%s Dihedral angle (degrees)'%( '-'.join( list(dihedral_names[0]) )))
        ax.set_ylabel('%s Dihedral angle (degrees)'%( '-'.join( list(dihedral_names[1]) )))
        
        ## DEFINING INCREMENTS FOR TICKS
        tick_increment = 30
        ax.set_xlim(-180, 180)
        ax.set_ylim(-180, 180)
        ax.set_xticks(np.arange(-180, 180 + tick_increment, tick_increment))
        ax.set_yticks(np.arange(-180, 180 + tick_increment, tick_increment))
        
        ## PLOTTING THE DATA
        scatter = ax.scatter( dihedral_angles[0], dihedral_angles[1], marker = 'x', c = self.traj_time/1000.0 )
        ## ADDING COLOR BAR
        colorbar = fig.colorbar(scatter, ax=ax)
        colorbar.set_label('Time [ns]')
        
        ## COMPUTING MAXIMA
        if want_maxima is True:
            ## COMPUTING MAXIMA
            H, x_center, y_center = self.calc_2d_histogram( dihedral_index_comparison = dihedral_index_comparison, want_maxima = want_maxima  )
            ## PLOTTING THE CENTERS ON THE GRAPH
            ax.plot( x_center, y_center, 'o', color='blue', mfc='none', markersize = 12, markeredgewidth = 3, label="Maximum")

        ## SAVING PLOT
        if save_plot is True:
            plot_tools.save_fig_png( fig = fig, label = "Ramachandran_%s_%s"%('-'.join( list(dihedral_names[0])), '-'.join( list(dihedral_names[1])) ) )

        return
        
    ### FUNCTION TO COMPUTE HISTOGRAMS WITH RESPECT TO TWO DIHEDRALS
    def calc_2d_histogram( self, dihedral_index_comparison=[0, 1], want_maxima=False ):
        '''
        The purpose of this function is to compute a 2D histogram using NumPy. The idea here is that we want the histogram details and compute maxima / minima
        INPUTS:
            self: [object]
                class object
            dihedral_index_comparison: [list, length = 2]
                first and second index of dihedral angles you want to compare with
            want_maxima: [logical, default = False]
                True if you want the maxima of the histogrma
        OUTPUTS:
            H: [np.array, shape = (num_bins - 1, num_bins - 1) ]
                Output from np.histogram2d showing a 2D matrix of the number of occurances
        '''
        ## DEFINING DATA
        dihedral_angles = [ self.dihedrals_degrees[:,dihedral_index] for dihedral_index in dihedral_index_comparison ]
        ## USING 2D NUMPY HISTOGRAM
        H, xedges, yedges = np.histogram2d(dihedral_angles[0], dihedral_angles[1], bins=(self.bin_range, self.bin_range))
        ## TRANSPOSING
        H = H.T  # Let each row list bins with common y range.
        ## SEEING IF YOU WANT MAXIMA
        if want_maxima:
            x_center, y_center = self.calc_maxima_2d_histogram( H = H )
            return H, x_center, y_center
        else:
            return H
    
    ### FUNCTION TO COMPUTE MAXIMA OF HISTOGRAM
    def calc_maxima_2d_histogram( self, H ):
        '''
        The purpose of this function is to compute the maxima of a 2D histogram
        INPUTS:
            self.bin_range_centers: [np.array, shape=(num_bins, 1)]
                range of bins with it centered at the center of the bin
            H: [np.array, shape = (num_bins - 1, num_bins - 1) ]
                Output from np.histogram2d showing a 2D matrix of the number of occurances
        OUTPUTS:
            x_center: [float]
                float for the x-value maxima
            y_center: [float]
                float for the y-value maxima
        '''
        ## COMPUTING INDICES
        indices = np.argwhere(H == H.max())
        ## FINDING X AND Y CENTERS
        x_center, y_center = self.bin_range_centers[indices[0][1]], self.bin_range_centers[indices[0][0]]
        return x_center, y_center 
    
    ### FUNCTION TO PLOT 2D DIHEDRAL PLOTS
    def plot_2d_dihedral_angle_dist(self, dihedral_index_comparison=[0, 1], n_bins = 50, want_maximum = True, save_plot=False):
        '''
        The purpose of this function is to plot a 2D dihedral angle distribution, assuming you have more than one dihedral angle
        INPUTS:
            dihedral_index_comparison: [list, length = 2]
                first and second index of dihedral angles you want to compare with
            want_maximum: [logical, default=True]
                True if you want maximum of the 2D dihedral angle distribution
        OUTPUTS:
            fig, ax: figure and axis for the plot
        '''
        ## IMPORTING MODULES
        import matplotlib as mpl
        import matplotlib.pyplot as plt
        from MDDescriptors.global_vars.plotting_global_vars import FONT_SIZE, FONT_NAME, COLOR_LIST, LINE_STYLE, DPI_LEVEL
        import MDDescriptors.core.plot_tools as plot_tools # Loading calculation tools
        from matplotlib import colors
        
        ## DEFINING NAMES OF EACH DIHEDRAL ANGLE
        dihedral_names = [ self.solute_dihedrals_heavy_atoms_index_names[dihedral_index] for dihedral_index in dihedral_index_comparison ]
        
        ## DEFINING FONTS
        font = {'family' : 'normal',
                'weight' : 'normal',
                'size'   : 12}
        
        ## CHANGING FONT
        mpl.rc('font', **font)
        
        ## CREATING PLOT
        fig = plt.figure()
        
        ## SETTING FIGURE SIZE
        fig.set_size_inches(8, 8)
        
        ## CREATING AXIS
        ax = fig.add_subplot(111, aspect='equal', xlim=self.bin_range[[0, -1]], ylim=self.bin_range[[0, -1]])
        
        ## COMPUTING 2D HISTOGRAM
        H = self.calc_2d_histogram( dihedral_index_comparison= dihedral_index_comparison )
        
        ##  FINDING IMAGES
        im = mpl.image.NonUniformImage(ax, interpolation='bilinear', cmap='Reds') # cmap='hot'
        im.set_data(self.bin_range_centers, self.bin_range_centers, H)
        ax.images.append(im)
        
        ## ENSURING THAT COLOR BAR FITS PLOT
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        
        ## DRAWING COLOR BAR
        fig.colorbar(im, ax=ax, cax = cax)
        
        ## DEFINING X AND Y LABELS
        ax.set_xlabel('%s Dihedral angle (degrees)'%( '-'.join( list(dihedral_names[0]) )))
        ax.set_ylabel('%s Dihedral angle (degrees)'%( '-'.join( list(dihedral_names[1]) )))
        
        ## DEFINING INCREMENTS FOR TICKS
        tick_increment = 30
        ax.set_xlim(-180, 180)
        ax.set_ylim(-180, 180)
        ax.set_xticks(np.arange(-180, 180 + tick_increment, tick_increment))
        ax.set_yticks(np.arange(-180, 180 + tick_increment, tick_increment))
        
        ## FINDING MAXIMA
        # idx = list(H.flatten()).index(H.max())
        # x, y = idx / H.shape[1], idx % H.shape[1]
        ## FINDING MAXIMA (Note that this is transposed, so data is also transposed)
        if want_maximum is True:
            ## COMPUTING THE CENTERS
            x_center, y_center = self.calc_maxima_2d_histogram(H = H)
            ## PLOTTING
            ax.plot( x_center, y_center, 'o', color='blue', mfc='none', markersize = 12, markeredgewidth = 3, label="Maximum")
            ## DRAWING LEGEND
            ax.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", ncol=3, mode="expand", frameon=False)
        
        ## TIGHT LAYOUT
        plt.tight_layout()
        
        ## SAVING PLOT
        if save_plot is True:
            plot_tools.save_fig_png( fig = fig, label = "Ramachandran_histo_%s_%s"%('-'.join( list(dihedral_names[0])), '-'.join( list(dihedral_names[1])) ) )

        return fig, ax
        
    
    

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
        ## DEFINING OTHER INPUTS
        solute_name ="PDO"  # "PDO"
        
        ### DIRECTORY TO WORK ON
        analysis_dir=r"181107-PDO_DEHYDRATION_FULL_DATA_300NS" # Analysis directory
        specific_dir=solute_name+"/mdRun_433.15_6_nm_"+ solute_name + "_100_WtPercWater_spce_Pure" # Directory within analysis_dir r"mdRun_363.15_6_nm_tBuOH_100_WtPercWater_spce_Pure"
        # specific_dir="ACE/mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dioxane" # Directory within analysis_dir r"mdRun_363.15_6_nm_tBuOH_100_WtPercWater_spce_Pure"
        # specific_dir=r"Planar_310.15_ROT_TMMA_10x10_CHARMM36_withGOLP" # Directory within analysis_dir
        path2AnalysisDir=r"R:\scratch\SideProjectHuber\Analysis\\" + analysis_dir + '\\' + specific_dir # PC Side
        
        ### DEFINING FILE NAMES
        gro_file=r"mixed_solv_prod.gro" # Structural file
        xtc_file=r"mixed_solv_prod_10_ns_whole_290000.xtc"
        
        ## DEFINING DEGREE INCREMENT
        degree_increment = 5
        
        ## OUTPUT INFORMATION
        output_folder_path = path2AnalysisDir # Using current path 2 analysis as a way of getting the specific directory
        output_file_name = "most_likely_config"
        
        ## LOGICALS
        want_pickle = False
        want_H = False
        
        ## DIHEDRAL ANGLES
        dihedral_range = [ [ 'O2', 'C2', 'C3', 'O1'], (40, 100) ]
        
    else:
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        parser = OptionParser()
        
        ## PATH INFORMATION
        parser.add_option('-i', '--inputpath', dest = 'input_path', help = 'Full input path to files', default = '.')
        
        ## OUTPUT FILES
        parser.add_option('-o', '--outputpath', dest = 'output_folder_path', help = 'Full output path to file', default = '.')
        parser.add_option('-s', '--summary', dest = 'output_file_name', help = 'Summary file name', default = '.')
        
        ## INPUT FILE INFORMATION
        parser.add_option('-g', '--gro', dest = 'gro_file', help = 'input gro file', default = '.')
        parser.add_option('-x', '--xtc', dest = 'xtc_file', help = 'input xtc file', default = '.')
        
        ## OTHER VARIABLES
        parser.add_option('-n', '--solutename', dest = 'solute_name', help = 'Solute residue name', default = '.')
        parser.add_option('-d', '--deginc', dest = 'degree_increment', help = 'Degree increment to study the dihedral angles', default = 5, type = int)
        
        ## LOGICALS
        parser.add_option('-p', '--pickle', dest = 'want_pickle', help = 'Stores pickle if flag is turned on. Default: False.',
                          default = False, action = 'store_true')
        parser.add_option('-H', '--wantH', dest = 'want_H', help = 'Looks for hydrogens and heavy atoms. Default: False.',
                          default = False, action = 'store_true')

        ## ADDING SPECIFIC DEGREES
        parser.add_option("--dihedral_range_region", dest="dihedral_range_region", action="callback", type="string", callback=get_ligand_args,
                  help="Range of dihedral angles, should be a list of 2 values from minima to maxima, separate each by comma (no whitespace)", default = None)
        
        ## ADDING SPECIFIC DEGREES
        parser.add_option("--dihedral_range_names", dest="dihedral_range_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of dihedral range names, should be a list of 4 atomnames, separate each name by comma (no whitespace)", default = None)
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## REDEFINING VARIABLES
        input_path = options.input_path
        output_folder_path = options.output_folder_path
        output_file_name = options.output_file_name
        gro_file = options.gro_file
        xtc_file = options.xtc_file
        solute_name = options.solute_name
        degree_increment = options.degree_increment
        want_pickle = options.want_pickle
        want_H = options.want_H
        
        ## SPECIFIC DIHEDRAL ANGLES
        dihedral_range_names = options.dihedral_range_names
        dihedral_range_region = options.dihedral_range_region
        
        ## SEEING IF DIHEDRALS ARE SPECIFIED
        if dihedral_range_names is None and dihedral_range_region is None or \
          dihedral_range_names == ["None"] and dihedral_range_region == ["None"]:
            dihedral_range = None
        else:
            dihedral_range = [ dihedral_range_names, [float(i) for i in dihedral_range_region]]
        
    ####################################################################
    ### MAIN SCRIPTS 
    ####################################################################
    
    ### LOADING TRAJECTORY
    traj_data = import_tools.import_traj( directory = output_folder_path, # Directory to analysis
                 structure_file = gro_file, # structure file
                  xtc_file = xtc_file, # trajectories
                  )
    
    
    #%%
    
    input_details={
                    'traj_data'                     : traj_data,                        # traj data
                    'solute_name'                   : [solute_name],                          # Solute of interest as a form of a list ['XYL', 'HYD']
                    'degree_increment'              : degree_increment,                                # Degree increment for the histograms
                    'output_folder_path'            : check_path(output_folder_path),                 # output folder path
                    'output_file_name'              : output_file_name,                 # file name for summary files and pickles
                    'want_pickle'                   : want_pickle,                      # logical for pickles
                    'want_H'                        : want_H,
                    'dihedral_range'                : dihedral_range,
                    }
    
    ### RUNNING DIHEDRALS
    dihedrals = calc_dihedrals(**input_details)
    
    
    #%%
    # dihedrals.plot_all_dihedral_angle_dist()
    
    
    #%%
    
    # dihedrals.plot_closest_structure_to_maxima()
    
    #%%
    
    
    '''
    ## FINDING ALL CLOSEST POINTS
    dihedral_angles = np.array([ self.dihedrals_degrees[:,dihedral_index] for dihedral_index in dihedral_index_comparison ])
    ## FINDING MAXIMA
    H, x_center, y_center = self.calc_2d_histogram( dihedral_index_comparison = dihedral_index_comparison, want_maxima = True  )

    ## FINDING THE DIFFERENCES
    dihedral_differences = dihedral_angles.T -  np.array([x_center, y_center]) ## RETURNS NUM_FRAMES X 2
    ## FINDING NORM
    dihedral_norms = np.linalg.norm( dihedral_differences, axis = 1 )
    ## SORTING SMALLEST TO LARGEST
    sorted_index = np.argsort( dihedral_norms )
    ## SORTED NORMS
    sorted_norms = dihedral_norms[sorted_index]
    return sorted_index, sorted_norms
    '''
    #%%
    '''
    import matplotlib.pyplot as plt
    plt.close('all')
    
    
    #%%
    
    dihedrals.plot_closest_structure_to_maxima()
    
    #%% LOADING PICKLE PATH
    path2AnalysisDir = r'R:\scratch\SideProjectHuber\Simulations\181107-PDO_DEHYDRATION_FULL_DATA_300NS\mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane'
    dihedral_results = load_class_pickle( path2AnalysisDir + '/most_likely_config.pickle'  )
    
    #%%
    dihedral_results.plot_closest_structure_to_maxima()
    
    #%%
    dihedral_results.plot_all_dihedral_angle_dist()
    # dihedral_results.plot_2d_dihedral_angle_per_frame()
    dihedral_results.plot_2d_dihedral_angle_dist()
    
    #%%
    
    import pandas as pd
    results = pd.read_pickle(  path2AnalysisDir + '/' + 'most_likely_config.pickle' )[0]
    
    
    #%%
    ###
    save_plot = False
    
    dihedrals.plot_all_dihedral_angle_dist( save_plot = save_plot)
    
    
    dihedrals.plot_2d_dihedral_angle_per_frame(want_maxima = True, save_plot = save_plot)
    
    #%%
    
    
    
    # dihedrals.plot_2d_dihedral_angle_dist(save_plot = save_plot)
    
    ## FINDING STRUCTURE
    sorted_index, sorted_norms = dihedrals.find_closest_structure_to_maxima()
    
    
    #%%
    dihedrals.plot_structure_for_frame( sorted_index[0] )
    
    '''
    