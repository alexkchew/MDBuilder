# -*- coding: utf-8 -*-
"""
extract_most_likely_configuration.py
The purpose of this script is to extract most likely configuration information. 
We are assuming you already ran most likely configuration and you would like 
to extract information from it.

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
CREATED ON: 05/07/2019

"""



## IMPORTANT FUNCTIONS
import sys
import pandas as pd
import glob
import numpy as np
import mdtraj as md
import os

## MD BUILDERS FUNCTIONS
from MDBuilder.core.pickle_funcs import store_class_pickle, load_class_pickle
from MDBuilder.core.check_tools import check_testing
## IMPORTING MOST LIKELY CONFIG
from MDBuilder.applications.solvent_effects.most_likely_configuration import calc_dihedrals

#%% MAIN SCRIPT
if __name__ == "__main__":

    
    ## TESTING
    testing = check_testing() #  check_testing()
    
    ## TESTING VARIABLES
    if testing is True:
        ## DEFINING SIMULATION DIR
        simulation_dir = r"190621-Run_equil_THD_CHD"
        # simulation_dir = r"181107-PDO_DEHYDRATION_FULL_DATA_300NS"
        ## DEFINING INPUT PATH
        input_basename = r"R:\scratch\SideProjectHuber\Simulations\FINAL_PDO_DEHYDRATION\Normal_MD_300_ns_sims"
        # input_basename = os.path.join(r"R:\scratch\SideProjectHuber\Simulations", simulation_dir)
        # input_dir = r"mdRun_433.15_6_nm_PDO_100_WtPercWater_spce_Pure" 
        # input_dir = r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dmso" 
        input_dir = r"mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane"
        # input_dir = r"mdRun_433.15_6_nm_CHD_100_WtPercWater_spce_Pure"
        # "mdRun_433.15_6_nm_PDO_100_WtPercWater_spce_Pure"
        # input_dir = "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane"
        # input_dir = "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dmso"
        ## DEFINING PATH
        input_path = os.path.join( input_basename,  input_dir )
        
        ## DEFINING PICKLE
        pickle_name="most_likely_config_final.pickle" # FOR TRANS
        # pickle_name="most_likely_config_final_gauche.pickle" # FOR TRANS
        # "most_likely_config_final_gauche.pickle" # FOR GAUCHE
        # "most_likely_config.pickle"
        # "most_likely_config.summary.pickle" # FOR GAUCHE
        # 
        
        ## DEFINING PICKLE PATH
        pickle_path = os.path.join( input_path, pickle_name )
    
    
    ## LOADING CLASS
    most_likely = pd.read_pickle(pickle_path)[0]
    # load_class_pickle(str(pickle_path))
    
    #%%
    ## PLOTTING ALL DIHEDRAL ANGLE DISTRIBUTIONS
    most_likely.plot_all_dihedral_angle_dist()
    
    ## PLOTTING CLOSEST STRUCTURE TO MAXIMA
    most_likely.plot_closest_structure_to_maxima()

    #%%
    ## EXTRACTING DETAILS FOR DIHEDRAL INFO

    
    ### FUNCTION TO GET THE HISTOGRAM
    def get_histogram_with_maxima( most_likely, dihedral_index, verbose = True):
        '''
        The purpose of this function is to generate a histogram with the maxima. 
        INPUTS:
            most_likely: [obj]
                most likely object
            dihedral_index: [int]
                dihedral angle index
        OUTPUTS:
            hist: [np.array]
                histogram number of occurances
            edges: [np.array]
                edges for the histogram (x values)
            dihedral_maxima: [float]
                dihedral maxima
        '''
        ## DEFINING BINS
        bins = most_likely.bin_range
        
        ## DIHEDRAL NAME
        dihedral_name=most_likely.solute_dihedrals_heavy_atoms_index_names[dihedral_index]
        ## RENAMING
        renamed_dihedral_name = '-'.join( list(dihedral_name) )
        
        ## MAXIMA
        if most_likely.dihedral_range is None:
            dihedral_maxima = most_likely.dihedrals_maxima[dihedral_index]
        else:
            if dihedral_index == most_likely.dihedrals_maxima['idx']:
                dihedral_maxima = most_likely.dihedrals_maxima['max_degrees']
            else:
                dihedral_maxima = most_likely.dihedrals_degrees[most_likely.closest_structure_time_index[0], dihedral_index]
                
        ## GETTING HISTOGRAM
        hist, edges = np.histogram( most_likely.dihedrals_degrees[:,dihedral_index], bins = bins )
    
        ## PRINTING
        if verbose is True:
            print("Dihedral angle: %s (max: %.2f)" %(dihedral_name, dihedral_maxima) )
            
        return hist, edges, dihedral_maxima
    
    ## RUNNING FUNCTION -- ['C1' 'C2' 'C3' 'O1']
    hist_0, edges_0, dihedral_maxima_0 = get_histogram_with_maxima( most_likely = most_likely,
                                                                   dihedral_index = 0 )
    
    
    ## RUNNING FUNCTION -- ['O2' 'C2' 'C3' 'O1']
    hist_1, edges_1, dihedral_maxima_1 = get_histogram_with_maxima( most_likely = most_likely,
                                                                   dihedral_index = 1 )
    
    ## COPY DIHEDRAL MAX, HIST, EDGE, THEN USE ORIGINLAB
